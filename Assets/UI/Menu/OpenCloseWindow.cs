﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;


public class OpenCloseWindow : MonoBehaviour {

	// The current menu.
	private Animator menu;

	// The open/closed parameter hash.
	private int openParameterID;

	// The names of the open and closed states.
	private const string openParameterName = "Open";


	public void OnEnable()
	{
		// Get the parameter to tell the menu to open.
		openParameterID = Animator.StringToHash (openParameterName);
	}

	// Open the new panel.
	public void OpenPanel (Animator anim)
	{		
		//Activate the new Screen hierarchy so we can animate it.
		anim.gameObject.SetActive(true);

		// Move the opened Menu to the front.
		anim.transform.SetAsLastSibling();
		
		//Set the new Screen as then open one.
		menu = anim;
		//Start the open animation
		menu.SetBool(openParameterID, true);
	}
	
	// Closes the menu
	public void CloseCurrent()
	{
		if (menu == null)
			return;
		
		// Mark state change to closed menu, animation will begin.
		menu.SetBool(openParameterID, false);

		// Deactivate animation after the menu has closed.
		StartCoroutine(DisablePanelDelayed(menu));

		// Menu no longer needed in manager.
		menu = null;
	}
	
	// End transition loop after finishing the animation.
	IEnumerator DisablePanelDelayed(Animator anim)
	{
		bool wantToClose = true;
		while (wantToClose)
		{
			wantToClose = !anim.GetBool(openParameterID);

			// Wait until the animation has progressed.
			yield return new WaitForEndOfFrame();
		}
		
		if (wantToClose)
			anim.gameObject.SetActive(false);
	}

}
