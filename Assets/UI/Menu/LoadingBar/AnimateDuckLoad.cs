﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class AnimateDuckLoad : MonoBehaviour {
	
	// The duck animator.
	private Animator duckAnimator;
	
	// The loading parameter hash.
	private int loadingParameterID;
	
	// The names of the loading and not loading states.
	private const string loadingStateName = "loading";
	private const string notLoadingStateName = "not Loading";
	
	
	public void OnEnable()
	{
		// Get the parameter to tell the animation to play.
		 loadingParameterID = Animator.StringToHash (loadingStateName);
	}
	
	// Start loading animation.
	public void PlayLoadingAnimation (Animator anim)
	{
		// Set the new Animator as the one being operated on.
		duckAnimator = anim;

		// Start the loading animation.
		duckAnimator.SetBool(loadingParameterID, true);
	}
	
	// Stops running the loading animation.
	public void StopLoading()
	{
		if (duckAnimator == null)
			return;
		
		// Mark state change to finished loading, animation will finish.
		duckAnimator.SetBool(loadingParameterID, false);
		
		// Deactivate animation after the loading has stopped.
		StartCoroutine(StopLoadingDelayed(duckAnimator));
		
		// Duck animator no longer needed in manager.
		duckAnimator = null;
	}
	
	// End transition loop after finishing the animation.
	IEnumerator StopLoadingDelayed(Animator anim)
	{
		bool stoppedStateReached = false;
		bool wantToStop = true;
		while (!stoppedStateReached && wantToStop)
		{
			if (!anim.IsInTransition(0))
				// The animation has finished and the menu is now in the "Closed" state
				stoppedStateReached = anim.GetCurrentAnimatorStateInfo(0).IsName(notLoadingStateName);
			
			wantToStop = !anim.GetBool(loadingParameterID);
			
			// Wait until the animation has progressed.
			yield return new WaitForEndOfFrame();
		}
	}
	

}
