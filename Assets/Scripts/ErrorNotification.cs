﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SocketIO;

/// <summary>
/// The <c>Error notification</c> class is used for
/// individual game objects, which are expected to 
/// register with a notification manager when they
/// are created and deregister when they are destroyed.
/// </summary>
public class ErrorNotification : MonoBehaviour {
	
	// The error notification object's animator.
	/// <summary>
	/// The menu is the animator used to animate
	/// the dialogue box being shown and hidden 
	/// from the scene.
	/// </summary>
	public Animator menu;
	
	// The notification currently waiting on a response.
	/// <summary>
	/// The notification that is currently being 
	/// shown on the error notification object.
	/// </summary>
	private SocketIOEvent notification = null;

	/// <summary>
	/// The notification manager that is notified when the
	/// error notification either finishes displaying a 
	/// message or when it is being registered/deregistered.
	/// </summary>
	private NotificationManager notificationManager = null;
	
	// The Text object which will contain messages for the user.
	/// <summary>
	/// The notification text object, which is where the 
	/// messages given as errors will be displayed.
	/// </summary>
	public Text notificationText;
	
	// The names of the ready and busy states.
	// Ready is when there is no event for the error menu to display
	// Busy is when there is an event currently displayed.
	/// <summary>
	/// The name of the ready state.
	/// </summary>
	private const string readyStateName = "Ready";
	/// <summary>
	/// The name of the busy state.
	/// </summary>
	private const string busyStateName = "Busy";

	// The waiting parameter hash.
	// True = Busy
	// False = Ready
	/// <summary>
	/// The waiting parameter ID, used to access 
	/// the animator's parameter.
	/// </summary>
	private int waitingParameterID;

	/// <summary>
	/// The name of the waiting parameter.
	/// </summary>
	private const string waitingParameterName = "Waiting";

	/// <summary>
	/// Whether the window is currently closing or not.
	/// </summary>
	private bool closing = false;
	
	// Use this for initialization
	void Awake () {
		// Get the parameter to tell the menu to open.
		waitingParameterID = Animator.StringToHash (waitingParameterName);	
		notificationManager = NotificationManager.GetInstance ();
		// Get the notification manager and mark as new delivery point for
		// Error notifications.
		notificationManager.RegisterErrorNotification (this);
	}

	// Used for when the object is destroyed.
	void OnDestroy () {
		notificationManager.DeregisterErrorNotification (this);
		// If an event was waiting, pass it back to the Notification Manager.
		// NOTE: this will insert it at the end of the queue, but this probably does
		// not matter, errors are errors and should all be processed.
		if (notification != null) {
			notificationManager.ReceiveNotification (notification);
			notification = null;
		}
	}
	
	// Open the new panel.
	/// <summary>
	/// Opens the error notification panel.
	/// </summary>
	public void OpenPanel () {
		//Activate the new Screen hierarchy so we can animate it.
		menu.gameObject.SetActive (true);
		
		// Move the opened Menu to the front.
		menu.transform.SetAsLastSibling ();
		
		//Start the open animation
		menu.SetBool (waitingParameterID, true);
	}
	
	// Closes the menu
	/// <summary>
	/// Closes the error notification panel.
	/// </summary>
	public void ClosePanel () {
		if (menu == null)
			return;
		
		// Mark state change to closed menu, animation will begin.
		menu.SetBool (waitingParameterID, false);
		
		// Deactivate animation after the menu has closed.
		StartCoroutine (DisablePanelDelayed ());
	}
	
	// End transition loop after finishing the animation.
	/// <summary>
	/// Disables the error notification panel after the animation has finished.
	/// </summary>
	/// <returns>A coroutine enumerator for the continuing operation.</returns>
	IEnumerator DisablePanelDelayed () {
		bool closedStateReached = false;
		bool wantToClose = true;
		while (!closedStateReached && wantToClose) {
			if (!menu.IsInTransition (0))
				// The animation has finished and the menu is now in the "Closed" state
				closedStateReached = menu.GetCurrentAnimatorStateInfo (0).IsName (busyStateName);
			
			wantToClose = !menu.GetBool (waitingParameterID);
			
			// Wait until the animation has progressed.
			yield return new WaitForEndOfFrame ();
		}
		
		if (wantToClose)
			menu.gameObject.SetActive (false);
		closing = false;
	}

	// Handle the notification event.
	/// <summary>
	/// Handles the notification event.
	/// </summary>
	/// <param name="evt">Evt is the socket.IO event with the error message in it.</param>
	public void DoNotification (SocketIOEvent evt) {
		notification = evt;
		
		if (notificationText == null) {
			Debug.Log ("Failed to get menu notification text object");
			return;
		}

		// If some additional handling of error messages wants to be done
		// it can be handled here.
		if (evt.data.HasField ("message")) {
			notificationText.text = evt.data.GetField ("message").ToString ();
			OpenPanel ();
		} else {
			Dismiss ();
		}
	}
	
	// Lets the notification manager know the last request delivered has been dismissed.
	/// <summary>
	/// Dismiss the current notification and expect the next one, closing if all have
	/// been responded to.
	/// </summary>
	public void Dismiss () {
		// Clear the event that has just been handled and send the response.
		SocketIOEvent notificationEvent = notification;
		notification = null;
		
		notificationManager.Dismiss (notificationEvent);
		
		// Close after all have been responded to.
		if (notification == null && ! closing) {
			closing = true;
			ClosePanel ();
		}
	}
}
