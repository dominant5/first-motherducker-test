﻿using UnityEngine;
using System.Collections;

namespace MotherDucker {
	/// <summary>
	/// Game status codes. Used to keep track of the game status.
	/// </summary>
	public enum GameStatus {
		WAITING_TO_START,
		STARTED,
		ENDED
	}
}
