﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SocketIO;

/// <summary>
/// Menu notification, registers with a notification
/// manager and receives all notifications requiring
/// an accept/reject response, displaying them to the
/// user.
/// </summary>
public class MenuNotification : MonoBehaviour {
	// The menu notification object's animator.
	/// <summary>
	/// The menu notification object's animator.
	/// </summary>
	public Animator menu;

	// The notification currently waiting on a response.
	/// <summary>
	/// The notification item currently waiting on a response.
	/// Contains the Socket.IO event as well as a callback.
	/// </summary>
	private NotificationManager.MenuNotificationItem notification = null;

	/// <summary>
	/// The notification manager that is notified with
	/// the response to the given notification as well as
	/// is used to register/deregister with.
	/// </summary>
	private NotificationManager notificationManager = null;

	// The Text object which will contain messages for the user.
	/// <summary>
	/// The notification text.
	/// </summary>
	public Text notificationText;

	// The names of the ready and busy states.
	// Ready is when there is no event for the notification menu to display
	// Busy is when there is an event currently displayed.
	/// <summary>
	/// The name of the ready state.
	/// </summary>
	private const string readyStateName = "Ready";

	/// <summary>
	/// The name of the busy state.
	/// </summary>
	private const string busyStateName = "Busy";

	// The waiting parameter hash.
	// True = Busy
	// False = Ready
	/// <summary>
	/// The waiting parameter ID.
	/// </summary>
	private int waitingParameterID;

	/// <summary>
	/// The name of the waiting parameter.
	/// </summary>
	private const string waitingParameterName = "Waiting";

	/// <summary>
	/// Whether the menu notification panel is 
	/// currently closing or not.
	/// </summary>
	private bool closing = false;

	// Use this for initialization
	void Awake () {
		// Get the parameter to tell the menu to open.
		waitingParameterID = Animator.StringToHash (waitingParameterName);

		// Get the notification manager and mark as new delivery point for
		// Menu notifications.
		notificationManager = NotificationManager.GetInstance ();
		notificationManager.RegisterMenuNotification (this);
	}

	// Used for when the object is destroyed.
	void OnDestroy () {
		notificationManager.DeregisterMenuNotification (this);
		// If an event was waiting, pass it back to the Notification Manager.
		// NOTE: this will insert it at the end of the queue, but this is probably desirable behaviour.
		if (notification != null) {
			notificationManager.ReceiveNotification (notification.evt, notification.callback);
			notification = null;
		}
	}

	// Open the new panel.
	/// <summary>
	/// Opens the menu.
	/// </summary>
	public void OpenPanel () {		
		//Activate the new Screen hierarchy so we can animate it.
		menu.gameObject.SetActive (true);
		
		// Move the opened Menu to the front.
		menu.transform.SetAsLastSibling ();

		//Start the open animation
		menu.SetBool (waitingParameterID, true);
	}

	// Closes the menu
	/// <summary>
	/// Closes the menu.
	/// </summary>
	public void ClosePanel () {
		if (menu == null)
			return;
		
		// Mark state change to closed menu, animation will begin.
		menu.SetBool (waitingParameterID, false);
		
		// Deactivate animation after the menu has closed.
		StartCoroutine (DisablePanelDelayed ());
	}

	// End transition loop after finishing the animation.
	/// <summary>
	/// Disables the error notification panel after the animation has finished.
	/// </summary>
	/// <returns>A coroutine enumerator for the continuing operation.</returns>
	IEnumerator DisablePanelDelayed () {
		bool closedStateReached = false;
		bool wantToClose = true;
		while (!closedStateReached && wantToClose) {
			if (!menu.IsInTransition (0))
				// The animation has finished and the menu is now in the "Closed" state
				closedStateReached = menu.GetCurrentAnimatorStateInfo (0).IsName (busyStateName);
			
			wantToClose = !menu.GetBool (waitingParameterID);
			
			// Wait until the animation has progressed.
			yield return new WaitForEndOfFrame ();
		}
		
		if (wantToClose)
			menu.gameObject.SetActive (false);
		closing = false;
	}

	// Handle the notification event.
	/// <summary>
	/// Displays the notification to the user and waits for their 
	/// accept or reject input.
	/// </summary>
	/// <param name="evt">
	/// The pair of socketIO event and the callback for when that event is
	/// processed.
	/// </param>
	public void DoNotification (NotificationManager.MenuNotificationItem evt) {
		notification = evt;

		if (notificationText == null) {
			Debug.Log ("Failed to get menu notification text object");
			return;
		}

		if (notification.evt.name.Equals ("acc:friend-request")) {
			if (notification.evt.data.HasField ("from")) {
				notificationText.text = "Friend invite from " + notification.evt.data.GetField ("from");
			} else {
				Debug.Log ("Friend invite was missing from field.");
				notificationText.text = "Friend invite from " + "unknown individual.";
			}
		} else if (notification.evt.name.Equals ("party:invite")) {
			if (notification.evt.data.HasField ("leader")) {
				notificationText.text = "Party invite from " + notification.evt.data.GetField ("leader");
			} else {
				Debug.Log ("Party invite was missing leader field.");
				notificationText.text = "Party invite from " + "unknown leader.";
			}
		} else {
			// Event unhandled, get next event. All events sent to menu notification
			// should be handled.
			notificationManager.Respond (evt, false);
			Debug.Log ("Unhandled event.");
		}
		OpenPanel ();
	}

	// Lets the notification manager know the response to the last request delivered.
	/// <summary>
	/// Respond to the currently stored event and then get the next event until
	/// all events have been responded to.
	/// </summary>
	/// <param name="accept">If set to <c>true</c> accept.</param>
	public void Respond (bool accept) {
		notificationText.text = "No Notifications.";

		// Clear the event that has just been handled and send the response.
		NotificationManager.MenuNotificationItem notificationEvent = notification;
		notification = null;

		notificationManager.Respond (notificationEvent, accept);

		// Close after all have been responded to.
		if (notification == null && ! closing) {
			closing = true;
			ClosePanel ();
		}
	}
}
