﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// The audio manager script to manage the audio playing for background music and sound effects.
/// </summary>
public class AudioManager : MonoBehaviour {

	/// <summary>
	/// The master volume for background music.
	/// </summary>
	public static float masterVolumeBGM;

	/// <summary>
	/// The master volume for sound effects.
	/// </summary>
	public static float masterVolumeSFX;

	/// <summary>
	/// The current background music that is playing.
	/// </summary>
	public static AudioSource currentBGM;

	/// <summary>
	/// The background music clip samples to use.
	/// </summary>
	public List<AudioClip> bgmClips;

	/// <summary>
	/// The sound effects clip samples to use.
	/// </summary>
	public List<AudioClip> sfxClips;

	/// <summary>
	/// The audio source list containing background music clips.
	/// </summary>
	private static List<AudioSource> bgmMusic;

	/// <summary>
	/// The audio source list containing sound effect clips.
	/// </summary>
	private static List<AudioSource> sfxMusic;

	/// <summary>
	/// The audio manager singleton instance.
	/// </summary>
	private static AudioManager instance;

	/// <summary>
	/// The current scene this instance is in.
	/// </summary>
	private int currentScene;

	void Awake () {
		// Check if this instance is a singleton
		if (instance != null && instance != this) {
			Destroy (this.gameObject);
			return;
		} else {
			instance = this;
		}
		
		DontDestroyOnLoad (this.gameObject);

		// Create music lists
		bgmMusic = new List<AudioSource> ();
		sfxMusic = new List<AudioSource> ();

		// Set initial master volumes
		if (PlayerPrefs.HasKey ("musicVolume")) {
			masterVolumeBGM = PlayerPrefs.GetFloat ("musicVolume");
		} else {
			PlayerPrefs.SetFloat ("musicVolume", 1f);
			masterVolumeBGM = 1f;
		}
		if (PlayerPrefs.HasKey ("sfxVolume")) {
			masterVolumeSFX = PlayerPrefs.GetFloat ("sfxVolume");
		} else {
			PlayerPrefs.SetFloat ("sfxVolume", 1f);
			masterVolumeSFX = 1f;
		}

		// Create new audio sources for each background audio clip
		foreach (AudioClip clip in bgmClips) {
			AudioSource audioSource = gameObject.AddComponent<AudioSource> ();
			audioSource.clip = clip;
			audioSource.loop = true;
			audioSource.volume = masterVolumeBGM;
			if (audioSource.clip.name == "MainMenuMusic") {
				audioSource.Play ();
				currentBGM = audioSource;
			}
			bgmMusic.Add (audioSource);
		}
	}

	void OnLevelWasLoaded (int scene) {
		// Update scene, pause current music, update music volumes
		currentScene = scene;
		currentBGM.Pause ();
		UpdateMusicVolume (masterVolumeBGM);
		UpdateSFXVolume (masterVolumeSFX);
	}

	/// <summary>
	/// Gets the instance.
	/// </summary>
	/// <returns>The audio manager instance.</returns>
	public static AudioManager GetInstance () {
		return instance;
	}

	/// <summary>
	/// Plays the specified background music.
	/// </summary>
	/// <param name="bgm">The name of the background music clip.</param>
	public static void PlayBGM (string bgm) {
		// Find the music and play it
		AudioSource audio = bgmMusic.Find (name => name.clip.name == bgm);

		if (!audio)
			return;

		if (audio.isPlaying)
			return;

		currentBGM.Pause ();
		audio.Play ();
		currentBGM = audio;
	}

	/// <summary>
	/// Stops the background music from playing.
	/// </summary>
	/// <param name="bgm">Bgm.</param>
	public static void StopBGM (string bgm) {
		// Find the music and stop it
		AudioSource audio = bgmMusic.Find (name => name.clip.name == bgm);

		if (!audio)
			return;

		audio.Stop ();
	}

	/// <summary>
	/// Updates all background music volumes.
	/// </summary>
	/// <param name="volume">Volume level.</param>
	public static void UpdateMusicVolume (float volume) {
		masterVolumeBGM = volume;
		foreach (AudioSource bgm in bgmMusic) {
			bgm.volume = masterVolumeBGM;
		}
	}

	/// <summary>
	/// Updates all sound effect volumes.
	/// </summary>
	/// <param name="volume">Volume level.</param>
	public static void UpdateSFXVolume (float volume) {
		masterVolumeSFX = volume;
		foreach (AudioSource sfx in sfxMusic) {
			sfx.volume = masterVolumeSFX;
		}
	}

	public void Update () {
		// Don't do anything if music is playing
		if (currentBGM.isPlaying)
			return;

		AudioSource bgm;

		// Set the background music depending on the scene the instance is in
		if (currentScene == LoadGame.MENUSCENE) {
			bgm = bgmMusic.Find (name => name.clip.name == "MainMenuMusic");
			bgm.Play ();
			currentBGM = bgm;
		} else if (currentScene == LoadGame.GAMESCENE) {
			bgm = bgmMusic.Find (name => name.clip.name == "bossa");
			bgm.Play ();
			currentBGM = bgm;
		}

		// Pause any background music playing that is not the current
		foreach (AudioSource music in bgmMusic) {
			if (music != currentBGM) {
				music.Pause ();
			}
		}
	}
}
