﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SocketIO;


public class AccountManager : MonoBehaviour {
	protected SocketIOComponent socket;

	// List of party members
	private ArrayList partyMembers = null;

	// The name of the leader of the party
	private string partyLeader = null;

	// The username and password entered in the relevant fields currently.
	private string username = null;
	private string password = null;

	// The username of the person whom a friend request will be delivered to.
	private string friendRequestName = null;

	// The name of the game object.
	private const string singletonName = "accountManagerSingleton";

	// The singleton instance of the AccountManager.
	private static AccountManager instance = null;

 	// The MenuManager in the scene.
	protected MenuManager menuManager = null;

	// Used to acquire a lock.
	private static object _lock = new object ();
	protected SessionManager sessionManager = null;
	protected NotificationManager notificationManager = null;

	//The party list contents from UI
	private GameObject partyListContent = null;

	//The party leader text
	private GameObject partyLeaderText = null;

	//The leave party button
	private GameObject partyLeaveButton = null;

	//The party member prefab
	public GameObject partyMemberPrefab;
	
	void Awake () {
		GetInstance ();
		instance.menuManager = GameObject.Find ("MenuManager").GetComponent<MenuManager>();
		instance.menuManager.SetValidModes(instance.partyMembers.Count + 1);

		Transform[] transforms = GameObject.Find ("Canvas").transform.GetComponentsInChildren<Transform> (true);
		foreach (Transform child in transforms) {
			if (child.name == "PartyListContent") {
				instance.partyListContent = child.gameObject;
			}
			if (child.name == "LeavePartyButton") {
				instance.partyLeaveButton = child.gameObject;
			}
			if (child.name == "PartyLeaderText") {
				instance.partyLeaderText = child.gameObject;
			}
			if (PlayerPrefs.HasKey ("username")) {
				if (child.name == "UsernameField") {
					string name = PlayerPrefs.GetString ("username");
					child.GetComponent<InputField>().text = name;
					instance.username = name;
				}
			}
		}
	}

	void OnLevelWasLoaded (int scene) {
		instance.menuManager = GameObject.Find ("MenuManager").GetComponent<MenuManager>();
		instance.menuManager.SetValidModes(instance.partyMembers.Count + 1);
		if (scene == LoadGame.MENUSCENE) {
			Transform[] transforms = GameObject.Find ("Canvas").transform.GetComponentsInChildren<Transform> (true);
			foreach (Transform child in transforms) {
				if (child.name == "PartyListContent") {
					instance.partyListContent = child.gameObject;
				}
				if (child.name == "LeavePartyButton") {
					instance.partyLeaveButton = child.gameObject;
				}
				if (child.name == "PartyLeaderText") {
					instance.partyLeaderText = child.gameObject;
				}
				if (PlayerPrefs.HasKey ("username")) {
					if (child.name == "UsernameField") {
						string name = PlayerPrefs.GetString ("username");
						child.GetComponent<InputField>().text = name;
						instance.username = name;
					}
				}
			}
			UpdatePartyUI ();
		}
	}

	void OnDestroy () {
		if (this == instance) {
			if (! SocketIOManager.instanceDestroyed) {
				instance.socket.Off ("acc:signup-success", instance.SignUpSuccess);
				instance.socket.Off ("acc:friend-request", instance.ReceiveFriendRequest);
				instance.socket.Off ("acc:friend-request-sent", instance.FriendRequestSent);
				instance.socket.Off ("acc:friend-request-accepted", instance.FriendRequestAccepted);
				instance.socket.Off ("party:invite", instance.PartyInvite);
				instance.socket.Off ("party:invite-sent", instance.PartyInviteSent);
				instance.socket.Off ("party:joined", instance.JoinedParty);
				instance.socket.Off ("party:left", instance.LeftParty);
				instance.socket.Off ("party:closed", instance.PartyClosed);
				SocketIOManager.GetInstance ().DeregisterCloseCallback (instance.GetNewSocket);
			}
			Debug.Log ("Destroyed Singleton AccountManager");
		}
	}

	// Get the singleton instance of the AccountManager.
	public static AccountManager GetInstance () {
		// Checks instance before locking to decrease system calls/improve parallel performance where possible.
		if (instance == null) {
			// If account manager object not already present, begin mutual exclusion.
			lock (_lock) {
				// Check instance was not created between the existence check and the start of the mutual exclusion.
				if (instance == null) {
					// Check whether an Account Manager has been added to the scene 
					// but not registered in the AccountManager class.
					instance = Object.FindObjectOfType (typeof(AccountManager)) as AccountManager;
					if (instance == null) {
						Debug.Log ("no account manager found");
						// No existence of accountManager object, create object and register all callbacks.
						GameObject accountManagerSingleton = new GameObject (singletonName);
						DontDestroyOnLoad (accountManagerSingleton);
						instance = accountManagerSingleton.AddComponent<AccountManager> ();
					} else {
						DontDestroyOnLoad (instance.gameObject);
					}
					SocketIOManager.GetInstance ().RegisterCloseCallback (instance.GetNewSocket);
					instance.GetNewSocket ();
					instance.sessionManager = SessionManager.GetInstance ();
					instance.notificationManager = NotificationManager.GetInstance ();
					instance.partyMembers = new ArrayList ();
				}
			}
		}
		return instance;
	}

	// Get the newest socket and register callbacks.
	public void GetNewSocket () {
		instance.socket = SocketIOManager.GetInstance ().GetSocket ();
		instance.socket.On ("acc:signup-success", instance.SignUpSuccess);
		instance.socket.On ("acc:friend-request", instance.ReceiveFriendRequest);
		instance.socket.On ("acc:friend-request-sent", instance.FriendRequestSent);
		instance.socket.On ("acc:friend-request-accepted", instance.FriendRequestAccepted);
		instance.socket.On ("party:invite", instance.PartyInvite);
		instance.socket.On ("party:invite-sent", instance.PartyInviteSent);
		instance.socket.On ("party:joined", instance.JoinedParty);
		instance.socket.On ("party:left", instance.LeftParty);
		instance.socket.On ("party:closed", instance.PartyClosed);

	}

	// Sign up with the values currently stored in the appropriate fields.
	public void SignUpNew () {
		if (this != instance) {
			// Not sure whether this is the nicer way to do this or not.
			instance.SignUpNew ();
			return;
		}
		if (username == null) {
			notificationManager.ReceiveNotification ("Please enter a username");
			Debug.Log ("Tried to sign up without username.");
			return;
		}
		if (password == null) {
			notificationManager.ReceiveNotification ("Please enter a password");
			Debug.Log ("Tried to sign up without password.");
			return;
		}

		/* Sign up. */
		JSONObject data = new JSONObject (JSONObject.Type.OBJECT);
		
		data.AddField ("username", username);
		data.AddField ("password", password);
		
		socket.Emit ("acc:signup", data);
		Debug.Log ("Sent signup request.");
	}

	// Send a friend request to the person with the username specified in the
	// relevant field.
	public void SendFriendRequest () {
		if (this != instance) {
			instance.SendFriendRequest ();
			return;
		}
		/* TODO: ADD CHECK HERE IF IN FRIENDS ALREADY */
		JSONObject data = new JSONObject (JSONObject.Type.OBJECT);

		data.AddField ("name", instance.friendRequestName);
		socket.Emit ("acc:friend-request", data);
		Debug.Log ("Requested friend " + friendRequestName);
	}

	// Send the party request using the friend request name (This should 
	// eventually probably be changed to something nicer, I was planning on doing this
	// but the functionality needed on the server side to do it wasn't there
	// at the time, so I held off).
	public void SendPartyRequestWithFriendRequestName () {
		Debug.Log ("Sent Party Request with Friend Request Name");
		SendPartyRequest (friendRequestName);
	}

	// Send a party request to a friend with the given username.
	public void SendPartyRequest (string friendName) {
		if (friendName == null)
			return;
		if (this != instance) {
			instance.SendPartyRequest (friendName);
		}
		JSONObject data = new JSONObject (JSONObject.Type.OBJECT);
		data.AddField ("name", friendName);
		socket.Emit ("party:invite", data);
		Debug.Log ("Sent Party request " + data.ToString ());
	}

	// Get the username currently stored (the data from the field).
	public string GetUsername () {
		if (this != instance) {
			return instance.GetUsername ();
		}
		if (username != null) {
			return username;
		} else {
			return "No username";
		}
	}

	// Get the password currently stored (the data from the field).
	public string GetPassword () {
		if (this != instance) {
			return instance.GetPassword ();
		}
		if (password != null) {
			return password;
		} else {
			return "No password";
		}
	}

	// Update the username with a new value.
	public void UpdateUsername (string username) {
		if (this != instance) {
			instance.UpdateUsername (username);
			return;
		}
		this.username = username;
	}

	// Update the password with a new value.
	public void UpdatePassword (string password) {
		if (this != instance) {
			instance.UpdatePassword (password);
		}
		this.password = password;
	}

	// Update the friend request's name with a new value.
	public void UpdateFriendRequestName (string newName) {
		if (this != instance) {
			instance.UpdateFriendRequestName (newName);
			return;
		}
		this.friendRequestName = newName;
	}

	// Handles the success of a signup.
	// Signup logs you out, so need to make a more friendly
	// way to handle that.
	void SignUpSuccess (SocketIOEvent evt) {
		instance.UpdateUsername (evt.data.GetField ("username").str);
		Debug.Log ("Signed up successfully.");
		notificationManager.ReceiveNotification ("Signed up successfully, Hello " + instance.GetUsername () + "!");
	}

	// Receive a request for a friend.
	protected void ReceiveFriendRequest (SocketIOEvent evt) {
		notificationManager.ReceiveNotification (evt, RespondFriendRequest);
		Debug.Log ("Received Friend Request " + evt.data);
	}

	// Notify the user that the friend request has successfully been
	// sent.
	protected void FriendRequestSent (SocketIOEvent evt) {
		string to;
		if (evt.data.HasField ("to")) {
			to = evt.data.GetField ("to").ToString ();
		} else {
			to = "unknown user";
		}
		notificationManager.ReceiveNotification ("Friend request sent to " + to);
		Debug.Log ("Sent Friend Request " + evt.data);
	}

	// Notify the user that the friend request has been accepted.
	protected void FriendRequestAccepted (SocketIOEvent evt) {
		string name;
		if (evt.data.HasField ("to")) {
			name = evt.data.GetField ("to").ToString ();
		} else {
			name = "Unknown user"; 
		}
		notificationManager.ReceiveNotification (name + " accepted your friend request!");
		Debug.Log ("Friend Request Accepted " + evt.data);
	}

	// Receive a request to join a party.
	protected void PartyInvite (SocketIOEvent evt) {
		notificationManager.ReceiveNotification (evt, RespondPartyRequest);
		Debug.Log ("Invited to Party " + evt.data);
	}

	// Notify the user that the party invite has been successfully sent.
	protected void PartyInviteSent (SocketIOEvent evt) {
		string to;
		if (evt.data.HasField ("to")) {
			to = evt.data.GetField ("to").ToString ();
		} else {
			to = "unknown user";
		}
		notificationManager.ReceiveNotification ("Party request sent to " + to);
		Debug.Log ("Sent party invite " + evt.data);
	}

	// Notify the user that they have joined a party and update the party UI.
	protected void JoinedParty (SocketIOEvent evt) {
		string leader;
		if (evt.data.HasField ("leader")) {
			leader = evt.data.GetField ("leader").str;
		} else {
			leader = "unknown user";
		}

		string memberName = evt.data.GetField ("name").str;

		notificationManager.ReceiveNotification (memberName + " has joined " + leader + "'s party.");
		partyLeader = leader;


		partyMembers.Clear ();

		if (evt.data.HasField ("members")){
			foreach(JSONObject name in evt.data.GetField ("members").list.ToArray ()){
				// Exclude leader from list.
				if (! name.str.Equals (leader))
					partyMembers.Add (name.str);
			}
		}

		UpdatePartyUI ();

	}

	// Notify the user that they have successfully left their party.
	protected void LeftParty (SocketIOEvent evt) {
		string leader;
		string leaver;
		if (evt.data.HasField ("leader")) {
			leader = evt.data.GetField ("leader").str;
		} else {
			leader = "unknown user";
		}
		if (evt.data.HasField ("name")) {
			leaver = evt.data.GetField ("name").str;
		} else {
			leaver = "unknown user";
		}

		notificationManager.ReceiveNotification (leaver + " left " + leader + "'s party.");

		string partyMember = null;

		foreach (string member in partyMembers) {
			if (member.Equals (leaver)) {
				partyMember = member;
				break;
			}
		}

		partyMembers.Remove (partyMember);

		UpdatePartyUI ();

		Debug.Log ("Left party " + evt.data);
	}

	// Notify the user that their party has been closed successfully.
	protected void PartyClosed (SocketIOEvent evt) {
		notificationManager.ReceiveNotification ("Closed party.");
		Debug.Log ("Closed party " + evt.data);
		if (evt.data.HasField ("leader")) {
			EmptyParty (evt.data.GetField ("leader").str);
		} else {
			Debug.Log ("Failed to get leader from party:closed event");
		}
	}

	// Remove all members in the party.
	protected void EmptyParty (string leader) {
		if (partyLeader.Equals(leader)) {
			partyMembers.Clear ();
			partyLeader = null;
		}
		UpdatePartyUI ();
	}

	// Update the UI with new party.
	protected void UpdatePartyUI (){
		if (this != instance) {
			instance.UpdatePartyUI ();
			return;
		}
		Debug.Log ("Leader: " + partyLeader);
		ArrayList children = new ArrayList ();
		foreach (Transform child in partyListContent.transform) children.Add(child.gameObject);
		foreach (GameObject child in children) {
			Destroy (child);
		}
		foreach(string member in partyMembers){
			Debug.Log ("Member: " + member);
			GameObject partyMember = (GameObject)Instantiate (partyMemberPrefab);
			partyMember.GetComponent<Text>().text = member;
			partyMember.transform.SetParent (partyListContent.transform, false);
		}
		if (partyMembers.Count == 0) {
			GameObject partyMember = (GameObject)Instantiate (partyMemberPrefab);
			partyMember.GetComponent<Text> ().text = "Not in a party";
			partyMember.transform.SetParent (partyListContent.transform, false);
		}
		Debug.Log ("partyLeader = " + instance.partyLeader);

		if (partyLeader != null) {
			partyLeaveButton.SetActive (true);
			partyLeaderText.GetComponent<Text>().text = "Party Leader: " + partyLeader;
		} else {
			partyLeaveButton.SetActive (false);
			partyLeaderText.GetComponent<Text>().text = "Not in a party";
		}

		menuManager.SetValidModes(partyMembers.Count + 1);
	}

	// Leave the current party
	public void LeaveParty () {
		instance.socket.Emit ("party:leave");
	}

	// Respond back to an earlier friend request
	public void RespondFriendRequest (SocketIOEvent evt, bool response) {
		if (response) {
			JSONObject data = new JSONObject (JSONObject.Type.OBJECT);
			data.AddField ("from", evt.data.GetField ("from"));
			instance.socket.Emit ("acc:friend-request-accepted", data);
		}
	}

	// Respond back to an earlier party join request
	public void RespondPartyRequest (SocketIOEvent evt, bool response) {
		if (response) {
			JSONObject data = new JSONObject (JSONObject.Type.OBJECT);
			data.AddField ("leader", evt.data.GetField ("leader"));
			instance.socket.Emit ("party:join", data);
		}
	}
}
