﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SocketIO;

/// <summary>
/// The manager script for menu behaviour.
/// </summary>
public class MenuManager : MonoBehaviour {
	/// <summary>
	/// The socket instance.
	/// </summary>
	SocketIOComponent socket;

	/// <summary>
	/// The desired game mode.
	/// </summary>
	string gameMode;

	/// <summary>
	/// The desired number of clients in the game.
	/// </summary>
	int numClients;

	/// <summary>
	/// The desired team size.
	/// </summary>
	int teamSize;

	/// <summary>
	/// The desired game duration in seconds.
	/// </summary>
	int gameTimer;

	/// <summary>
	/// True if currently searching for a game.
	/// </summary>
	bool finding;

	/// <summary>
	/// The game mode toggle buttons.
	/// </summary>
	Toggle[] gameModeToggles;

	/// <summary>
	/// The notification manager.
	/// </summary>
	protected NotificationManager notificationManager;

	void Awake () {
		SocketIOManager.GetInstance ().RegisterCloseCallback (GetNewSocket);
		GetNewSocket ();
		gameMode = "ffa";
		numClients = 2;
		teamSize = 1;
		gameTimer = 120;
		finding = false;
		Transform[] transforms = GameObject.Find ("Canvas").transform.GetComponentsInChildren<Transform> (true);
		Transform gameModePanel = null;
		foreach (Transform child in transforms) {
			if (child.name == "GameModePanel") {
				gameModePanel = child;
			}
		}
		gameModeToggles = gameModePanel.GetComponentsInChildren<Toggle>(true);
		notificationManager = NotificationManager.GetInstance();
	}

	/// <summary>
	/// Gets the newest socket and registers callbacks.
	/// </summary>
	void GetNewSocket () {
		socket = SocketIOManager.GetInstance ().GetSocket ();
		socket.On ("game:ready", OnGameReady);
	}

	void OnDestroy () {
		if (! SocketIOManager.instanceDestroyed) {
			socket.Off ("game:ready", OnGameReady);
			SocketIOManager.GetInstance ().DeregisterCloseCallback (GetNewSocket);
		}
	}

	/// <summary>
	/// Notifies the server to search for a game.
	/// </summary>
	/// <param name="inGame">Whether the client is currently in a game.</param>
	public void FindGame (bool inGame) {
		JSONObject data = new JSONObject (JSONObject.Type.OBJECT);
		data.AddField ("gameMode", gameMode);
		data.AddField ("numClients", numClients);
		data.AddField ("teamSize", teamSize);
		data.AddField ("gameTimer", gameTimer);
		if (inGame)
			socket.Emit ("game:leave");
		socket.Emit ("game:find", data);
		Debug.Log ("finding game with: {NumClients: " + numClients + ", TeamSize: " + teamSize + ", GameDuration: " + gameTimer + ", GameMode: " + gameMode);
		GameObject.Find ("Canvas/GameModePanel/Menu/FindGameButton").GetComponent<Button> ().interactable = false;
		finding = true;
	}

	/// <summary>
	/// Cancels the game search.
	/// </summary>
	public void CancelGameSearch () {
		socket.Emit ("game:leave");
		GameObject.Find ("Canvas/GameModePanel/Menu/FindGameButton").GetComponent<Button> ().interactable = true;
		finding = false;
	}

	/// <summary>
	/// Sets the game mode.
	/// </summary>
	/// <param name="gameMode">Game mode.</param>
	public void SetGameMode (string gameMode) {
		this.gameMode = gameMode;
		GameObject teamModeGroup = GameObject.Find ("Canvas/GameModePanel/Menu/PlayerToggleGroup/TeamModeToggleGroup");
		GameObject playerModeGroup = GameObject.Find ("Canvas/GameModePanel/Menu/PlayerToggleGroup/PlayerModeToggleGroup");
		foreach (Toggle toggle in gameModeToggles) {
			if (toggle.name == "2PlayerToggle") {
				toggle.isOn = false;
			}
			if (toggle.name == "3PlayerToggle") {
				toggle.isOn = false;
			}
			if (toggle.name == "4PlayerToggle") {
				toggle.isOn = false;
			}
			if (toggle.name == "2v2Toggle") {
				toggle.isOn = false;
			}
			if (toggle.name == "3v3Toggle") {
				toggle.isOn = false;
			}
			if (toggle.name == "4v4BotsToggle") {
				toggle.isOn = false;
			}
		}
		if (gameMode == "ffa") {
			teamModeGroup.SetActive (false);
			playerModeGroup.SetActive (true);
			numClients = 0;
			teamSize = 1;
		} else if (gameMode == "team") {
			playerModeGroup.SetActive (false);
			teamModeGroup.SetActive (true);
			numClients = 0;
			teamSize = 0;
		}
	}

	/// <summary>
	/// Sets the desired number of clients.
	/// </summary>
	/// <param name="numClients">Number clients.</param>
	public void SetNumClients (int numClients) {
		this.numClients = numClients;
	}

	/// <summary>
	/// Sets the desired team size.
	/// </summary>
	/// <param name="teamSize">Team size.</param>
	public void SetTeamSize (int teamSize) {
		this.teamSize = teamSize;
	}

	/// <summary>
	/// Sets the desired game duration in seconds.
	/// </summary>
	/// <param name="gameTimer">Game timer.</param>
	public void SetGameTime (int gameTimer) {
		this.gameTimer = gameTimer;
	}

	/// <summary>
	/// Transitions to the game scene.
	/// </summary>
	/// <param name="evt">Evt.</param>
	void OnGameReady (SocketIOEvent evt) {
		LoadGame.OpenScene (LoadGame.GAMESCENE);
	}

	/// <summary>
	/// Sets the valid modes for the game mode panel depending on party size.
	/// </summary>
	/// <param name="partySize">Party size.</param>
	public void SetValidModes(int partySize) {
		switch (partySize) {
		case 1:
			foreach (Toggle toggle in gameModeToggles) {
				if (toggle.name == "2PlayerToggle") {
					toggle.interactable = true;
					toggle.isOn = true;
				}
				if (toggle.name == "3PlayerToggle") {
					toggle.interactable = true;
					toggle.isOn = false;
				}
				if (toggle.name == "4PlayerToggle") {
					toggle.interactable = true;
					toggle.isOn = false;
				}
				if (toggle.name == "2v2Toggle") {
					toggle.interactable = true;
					toggle.isOn = false;
				}
				if (toggle.name == "3v3Toggle") {
					toggle.interactable = true;
					toggle.isOn = false;
				}
				if (toggle.name == "4v4BotsToggle") {
					toggle.interactable = true;
					toggle.isOn = false;
					toggle.onValueChanged.AddListener((value) => {
						SetNumClients(partySize);
					});
				}
			}
			break;
		case 2:
			foreach (Toggle toggle in gameModeToggles) {
				if (toggle.name == "2PlayerToggle") {
					toggle.interactable = true;
					toggle.isOn = true;
				}
				if (toggle.name == "3PlayerToggle") {
					toggle.interactable = true;
					toggle.isOn = false;
				}
				if (toggle.name == "4PlayerToggle") {
					toggle.interactable = true;
					toggle.isOn = false;
				}
				if (toggle.name == "2v2Toggle") {
					toggle.interactable = true;
					toggle.isOn = false;
				}
				if (toggle.name == "3v3Toggle") {
					toggle.interactable = true;
					toggle.isOn = false;
				}
				if (toggle.name == "4v4BotsToggle") {
					toggle.interactable = true;
					toggle.isOn = false;
					toggle.onValueChanged.AddListener((value) => {
						SetNumClients(partySize);
					});
				}
			}
			break;
		case 3:
			foreach (Toggle toggle in gameModeToggles) {
				if (toggle.name == "2PlayerToggle") {
					toggle.interactable = false;
					toggle.isOn = false;
				}
				if (toggle.name == "3PlayerToggle") {
					toggle.interactable = true;
					toggle.isOn = true;
				}
				if (toggle.name == "4PlayerToggle") {
					toggle.interactable = true;
					toggle.isOn = false;
				}
				if (toggle.name == "2v2Toggle") {
					toggle.interactable = false;
					toggle.isOn = false;
				}
				if (toggle.name == "3v3Toggle") {
					toggle.interactable = true;
					toggle.isOn = false;
				}
				if (toggle.name == "4v4BotsToggle") {
					toggle.interactable = true;
					toggle.isOn = false;
					toggle.onValueChanged.AddListener((value) => {
						SetNumClients(partySize);
					});
				}
			}
			break;
		case 4:
			foreach (Toggle toggle in gameModeToggles) {
				if (toggle.name == "2PlayerToggle") {
					toggle.interactable = false;
					toggle.isOn = false;
				}
				if (toggle.name == "3PlayerToggle") {
					toggle.interactable = false;
					toggle.isOn = false;
				}
				if (toggle.name == "4PlayerToggle") {
					toggle.interactable = true;
					toggle.isOn = true;
				}
				if (toggle.name == "2v2Toggle") {
					toggle.interactable = false;
					toggle.isOn = false;
				}
				if (toggle.name == "3v3Toggle") {
					toggle.interactable = false;
					toggle.isOn = false;
				}
				if (toggle.name == "4v4BotsToggle") {
					toggle.interactable = true;
					toggle.isOn = false;
					toggle.onValueChanged.AddListener((value) => {
						SetNumClients(partySize);
					});
				}
			}
			break;
		}
	}

	void Update() {
		if (finding) return;
		GameObject submitButton = GameObject.Find ("FindGameButton");
		if (submitButton == null) return;
		if (numClients == 0 || teamSize == 0) {
			if (submitButton.GetComponent<Button>().IsInteractable()) {
				submitButton.GetComponent<Button>().interactable = false;
			}
		} else {
			if (!submitButton.GetComponent<Button>().IsInteractable()) {
				submitButton.GetComponent<Button>().interactable = true;
			}
		}
	}
}
