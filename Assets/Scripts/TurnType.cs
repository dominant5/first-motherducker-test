﻿using UnityEngine;
using System.Collections;

namespace MotherDucker {
	/// <summary>
	/// Motherduck turn type.
	/// </summary>
	public enum TurnType {
		/// <summary>
		/// The motherduck is not turning.
		/// </summary>
		STOP,
		/// <summary>
		/// The motherduck is turning left.
		/// </summary>
		LEFT,
		/// <summary>
		/// The motherduck is turning right.
		/// </summary>
		RIGHT
	}
}