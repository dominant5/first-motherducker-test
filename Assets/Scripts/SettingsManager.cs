﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text.RegularExpressions;

/// <summary>
/// The settings manager singleton handles user settings for the application
/// such as volume, notification settings and changing connections.
/// </summary>
public class SettingsManager : MonoBehaviour {
	/// <summary>
	/// The music slider tag.
	/// </summary>
	private const string musicSliderTag = "MusicVolumeSlider";

	/// <summary>
	/// The sfx slider tag.
	/// </summary>
	private const string sfxSliderTag = "SFXVolumeSlider";

	/// <summary>
	/// The address text tag.
	/// </summary>
	private const string addressTextTag = "ServerAddressText";

	/// <summary>
	/// The streaming notifications in game toggle tag.
	/// </summary>
	private const string streamGameTag = "StreamingNotificationsInGameToggle";

	/// <summary>
	/// The error notifications as streaming notifications tag.
	/// </summary>
	private const string errorStreamTag = "ErrorsAsStreamingNotifications";

	/// <summary>
	/// The connect button tag.
	/// </summary>
	private const string connectButtonTag = "ConnectNewServerButton";

	/// <summary>
	/// The music volume.
	/// </summary>
	public static float musicVolume = 1;

	/// <summary>
	/// The sfx volume.
	/// </summary>
	public static float sfxVolume = 1;

	/// <summary>
	/// The server address temporarily stored that can later be connected to.
	/// </summary>
	private static string serverAddress;

	/// <summary>
	/// The streaming notifications in game setting.
	/// </summary>
	private static bool streamingNotificationsInGame = true;

	/// <summary>
	/// The errors as streaming notifications setting.
	/// </summary>
	private static bool errorsAsStreamingNotifications = false;

	// Accept either IPv4 Address or hostname (RFC 1123).
	// Note: I'm not sure how complex running for IPv6 would be (hardware wise).
	/// <summary>
	/// The server regex.
	/// </summary>
	/// <remarks>
	/// Conforms to RFC 1123, does not cover IPv6 due to shaky 
	/// web socket support in some cases for some Socket.IO connections.
	/// </remarks>
	private static Regex serverRegex = new Regex ("(^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$)" +
		"|(^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$)");

	// Use this for initialization
	void Start () {
		//Grab player preferences for options
		if (!PlayerPrefs.HasKey ("musicVolume")) {
			PlayerPrefs.SetFloat ("musicVolume", 1f);
		} else {
			AudioManager.UpdateMusicVolume(PlayerPrefs.GetFloat("musicVolume"));
		}
		if (!PlayerPrefs.HasKey ("sfxVolume")) {
			PlayerPrefs.SetFloat ("sfxVolume", 1f);
		} else {
			AudioManager.UpdateSFXVolume(PlayerPrefs.GetFloat("sfxVolume"));
		}
		if (!PlayerPrefs.HasKey ("serverAddress")) {
			PlayerPrefs.SetString ("serverAddress", "127.0.0.1");
		}
		if (!PlayerPrefs.HasKey ("streamingNotificationsInGame")) {
			PlayerPrefs.SetInt ("streamingNotificationsInGame", 1);
		} else {
			NotificationManager.UpdateGameStreamingNotifications (PlayerPrefs.GetInt("streamingNotificationsInGame") == 1);
		}
		if (!PlayerPrefs.HasKey ("errorsAsStreamingNotifications")) {
			PlayerPrefs.SetInt ("errorsAsStreamingNotifications", 0);
		} else {
			NotificationManager.UpdateErrorsStreamingNotifications (PlayerPrefs.GetInt("errorsAsStreamingNotifications") == 1);
		}
	}

	// Finds the Music Volume slider and then updates the music volume to that value.
	/// <summary>
	/// Updates the music volume.
	/// </summary>
	/// <param name="volume">New volume.</param>
	public void UpdateMusicVolume (float volume) {
		PlayerPrefs.SetFloat ("musicVolume", volume);
		GameObject musicSliderTagged = GameObject.FindGameObjectWithTag (musicSliderTag);
		if (musicSliderTagged == null)
			return;
		if (musicSliderTagged.GetComponent<Slider> () == null)
			return;
		musicVolume = volume;

		AudioManager.UpdateMusicVolume (volume);
	}

	// Finds the SFX Volume slider and then updates the SFX volume to that value.
	/// <summary>
	/// Updates the SFX volume.
	/// </summary>
	/// <param name="volume">New volume.</param>
	public void UpdateSFXVolume (float volume) {
		PlayerPrefs.SetFloat ("sfxVolume", volume);
		GameObject sfxSliderTagged = GameObject.FindGameObjectWithTag (sfxSliderTag);
		if (sfxSliderTagged == null)
			return;
		if (sfxSliderTagged.GetComponent<Slider> () == null)
			return;
		sfxVolume = volume;

		AudioManager.UpdateSFXVolume (volume);
	}

	/// <summary>
	/// Updates the server address value stored in the settings manager.
	/// Only stores if the value is a valid address.
	/// </summary>
	/// <param name="newServerAddress">New server address to try and store.</param>
	public void UpdateServerAddress (string newServerAddress) {
		if (newServerAddress != null && serverRegex.IsMatch (newServerAddress)) {
			PlayerPrefs.SetString ("serverAddress", newServerAddress);
			serverAddress = newServerAddress;
		}
	}

	/// <summary>
	/// Updates the connect button.
	/// </summary>
	/// <param name="serverAddress">Server address.</param>
	public void UpdateConnectButton (string serverAddress) {
		if (serverAddress == "") {
			FindComponent<Button> (connectButtonTag).interactable = false;
		} else {
			FindComponent<Button> (connectButtonTag).interactable = true;
		}
	}

	/// <summary>
	/// Updates the game streaming notifications option.
	/// </summary>
	public void UpdateGameStreamingNotifications () {
		PlayerPrefs.SetInt ("streamingNotificationsInGame", ! streamingNotificationsInGame ? 1 : 0);
		streamingNotificationsInGame = ! streamingNotificationsInGame;
		Debug.Log ("streamingNotificationsInGame = " + streamingNotificationsInGame.ToString ());

		NotificationManager.UpdateGameStreamingNotifications (streamingNotificationsInGame);
	}

	/// <summary>
	/// Updates the errors as streaming notifications option.
	/// </summary>
	public void UpdateErrorsStreamingNotifications () {
		PlayerPrefs.SetInt ("errorsAsStreamingNotifications", ! errorsAsStreamingNotifications ? 1 : 0);
		errorsAsStreamingNotifications = ! errorsAsStreamingNotifications;
		Debug.Log ("errorsAsStreamingnotifications = " + errorsAsStreamingNotifications.ToString ());

		NotificationManager.UpdateErrorsStreamingNotifications (errorsAsStreamingNotifications);
	}

	// Runs updateOptionsMenu with all settings monitored by settings manager.
	/// <summary>
	/// Runs updateOptionsMenu with all settings monitored by the settings manager.
	/// </summary>
	public void UpdateOptionsMenuAuto () {
		UpdateOptionsMenu (FindComponent<Slider> (musicSliderTag), FindComponent<Slider> (sfxSliderTag), 
		                   FindComponent<Text> (addressTextTag), FindComponent<Toggle> (streamGameTag),
		                   FindComponent<Toggle> (errorStreamTag), FindComponent<Button> (connectButtonTag));
	}

	/// <summary>
	/// Finds the component of the given type, with the given tag, searching through hidden/disabled items as well.
	/// </summary>
	/// <returns>The component.</returns>
	/// <param name="tag">Tag.</param>
	/// <typeparam name="T">The type of the component to find.</typeparam>
	public T FindComponent<T> (string tag) where T : UnityEngine.Component {
		// Set null or 0 depending on type.
		T temp = default(T);

		T[] x = Resources.FindObjectsOfTypeAll<T> ();
		foreach (T y in x) {
			if (y.CompareTag (tag)) {
				Debug.Log (y.ToString ());
				return y.GetComponent<T> ();
			}
		}
		return temp;
	}

	/// <summary>
	/// Updates the options menu sliders, boxes and buttons to
	/// match the settings stored.
	/// </summary>
	/// <param name="musicSlider">Music slider.</param>
	/// <param name="sfxSlider">Sfx slider.</param>
	/// <param name="addressText">Address text.</param>
	/// <param name="streamGame">Streaming Notifications in game Toggle.</param>
	/// <param name="errorStream">Error notifications as streaming notifications toggle.</param>
	/// <param name="connectButton">Connect button.</param>
	public void UpdateOptionsMenu (Slider musicSlider, Slider sfxSlider, Text addressText, Toggle streamGame, 
	                              Toggle errorStream, Button connectButton) {
		if (musicSlider != null)
			musicSlider.value = PlayerPrefs.GetFloat ("musicVolume");
		if (sfxSlider != null)
			sfxSlider.value = PlayerPrefs.GetFloat ("sfxVolume");
		if (addressText != null)
			addressText.text = PlayerPrefs.GetString ("serverAddress");
		if (streamGame != null)
			streamGame.isOn = PlayerPrefs.GetInt ("streamingNotificationsInGame") == 1 ? true : false;
		if (errorStream != null)
			errorStream.isOn = PlayerPrefs.GetInt ("errorsAsStreamingNotifications") == 1 ? true : false;
		if (connectButton != null)
			connectButton.interactable = ! SocketIOManager.GetInstance ().SameServer (PlayerPrefs.GetString ("serverAddress"));
	}

	/// <summary>
	/// Connects to the stored server.
	/// </summary>
	public void ConnectServer () {
		if (! serverRegex.IsMatch (PlayerPrefs.GetString ("serverAddress"))) {
			NotificationManager.GetInstance ().ReceiveNotification ("Invalid server address " + serverAddress);
		} else {
			SocketIOManager.GetInstance ().ConnectToNewServer (PlayerPrefs.GetString ("serverAddress"));
		}
	}
}
