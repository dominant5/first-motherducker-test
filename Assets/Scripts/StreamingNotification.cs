﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SocketIO;

/// <summary>
/// Streaming notification object.
/// </summary>
public class StreamingNotification : MonoBehaviour {
	// The streaming notification object's animator.
	/// <summary>
	/// The streaming notification object's animator.
	/// </summary>
	public Animator menu;
	
	// The notification currently to be displayed.
	/// <summary>
	/// The notification currently to be displayed.
	/// </summary>
	private string notification = null;

	/// <summary>
	/// The notification manager.
	/// </summary>
	private NotificationManager notificationManager = null;
	
	// The Text object which will contain messages for the user.
	/// <summary>
	/// The notification text.
	/// </summary>
	public Text notificationText;

	// The open/closed parameter hash.
	/// <summary>
	/// The panel open parameter ID.
	/// </summary>
	private int openParameterID;
	
	// The names of the open and closed states.
	/// <summary>
	/// The name of the open state.
	/// </summary>
	private const string openStateName = "Open";

	/// <summary>
	/// The name of the open parameter.
	/// </summary>
	private const string openParameterName = "Open";

	/// <summary>
	/// Whether the panel is currently closing or not.
	/// </summary>
	private bool closing = false;
	
	// The number of seconds a notification should display for.
	/// <summary>
	/// The number of seconds a notification should display for.
	/// </summary>
	private const float notificationDisplayTime = 3;

	// Use this for initialization
	void Awake () {
		// Get the parameter to tell the menu to open.
		openParameterID = Animator.StringToHash (openParameterName);	
		notificationManager = NotificationManager.GetInstance ();
		
		// Get the notification manager and mark as new delivery point for
		// Error notifications.
		notificationManager.RegisterStreamingNotification (this);
	}

	// Used when the object is destroyed.
	void OnDestroy () {
		notificationManager.DeregisterStreamingNotification (this);
		// If an event was showing, pass it back to the Notification Manager.
		// Note: this is much less important pass back than in either of the other
		// kinds of notification managers.
		if (notification != null) {
			notificationManager.ReceiveNotification (notification);
			notification = null;
		}
	}

	// Open the new panel.
	/// <summary>
	/// Opens the streamingNotifications object's panel.
	/// </summary>
	public void OpenPanel () {		
		// Activate the new Screen hierarchy so we can animate it.
		menu.gameObject.SetActive (true);
		
		// Move the opened Menu to the front.
		menu.transform.SetAsLastSibling ();
		
		// Start the open animation
		menu.SetBool (openParameterID, true);

		// Set timer for end of notification display period.
		StartCoroutine (GetNextWhenDone ());
	}

	// Closes the menu
	/// <summary>
	/// Closes the panel.
	/// </summary>
	public void ClosePanel () {
		if (menu == null)
			return;
		
		// Mark state change to closed menu, animation will begin.
		menu.SetBool (openParameterID, false);
		
		// Deactivate animation after the menu has closed.
		StartCoroutine (DisablePanelDelayed ());
	}

	// End transition loop after finishing the animation.
	/// <summary>
	/// Disables the streaming notification panel after the animation has finished.
	/// </summary>
	/// <returns>A coroutine enumerator for the continuing operation.</returns>
	IEnumerator DisablePanelDelayed () {
		bool wantToClose = true;
		while (wantToClose) {			
			wantToClose = !menu.GetBool (openParameterID);
			
			// Wait until the animation has progressed.
			yield return new WaitForEndOfFrame ();
		}
		
		if (wantToClose)
			menu.gameObject.SetActive (false);
		closing = false;
	}

	// Handle the notification event.
	/// <summary>
	/// Shows the notification.
	/// </summary>
	/// <param name="evt">The notification message's text.</param>
	public void DoNotification (string evt) {
		notification = evt;
		
		if (notificationText == null) {
			Debug.Log ("Failed to get menu notification text object");
			return;
		}
		
		// If some additional handling of error messages wants to be done
		// it can be handled here.
		if (notification != null) {
			notificationText.text = notification;
			OpenPanel ();
		} else {
			FinishedNotification ();
		}
	}
	
	// Lets the notification manager know there's room for the next notification.
	/// <summary>
	/// Lets the notification manager know there's room for the next notification.
	/// </summary>
	public void FinishedNotification () {
		// Clear the event that has just been handled and send the response.
		string notificationEvent = notification;
		notification = null;
		
		notificationManager.FinishedNotification (notificationEvent);
		
		// Close after all have been sent.
		if (notification == null && ! closing) {
			closing = true;
			ClosePanel ();
		}
	}

	// Gets the next notification after the length of time a notification displays 
	// for has passed.
	/// <summary>
	/// Waits for the length of time specified and then notifies the notificationManager 
	/// that it is ready for the next streaming notification.
	/// </summary>
	/// <returns>The next when done.</returns>
	IEnumerator GetNextWhenDone () {
		yield return new WaitForSeconds (notificationDisplayTime);
		FinishedNotification ();
	}
}
