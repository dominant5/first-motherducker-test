﻿using UnityEngine;
using System.Collections;

/// <summary>
/// The in-game follow cam script. Keeps the GameObject (usually the camera) at a given offset from the target.
/// </summary>
public class FollowCam : MonoBehaviour {
	/// <summary>
	/// The target.
	/// </summary>
	public Transform target;

	/// <summary>
	/// How far back the GameObject should be relative to the target.
	/// </summary>
	public float distance = 10.0f;

	/// <summary>
	/// How high up the GameObject should be relative to the target.
	/// </summary>
	public float height = 3.0f;

	/// <summary>
	/// The height damping.
	/// </summary>
	public float heightDamping = 2.0f;

	/// <summary>
	/// The rotation damping.
	/// </summary>
	public float rotationDamping = 3.0f;

	/// <summary>
	/// An arbitrary factor used to scale damping effects.
	/// </summary>
	public float factor = 1.0f;

	/// <summary>
	/// Updates the position based on the target's position.
	/// </summary>
	void LateUpdate () {
		if (!target)
			return;

		float wantedRotationAngle = target.eulerAngles.y;
		float wantedHeight = target.position.y + height;
		
		float currentRotationAngle = transform.eulerAngles.y;
		float currentHeight = transform.position.y;
		
		currentRotationAngle = Mathf.LerpAngle (currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime / factor);
		
		currentHeight = Mathf.Lerp (currentHeight, wantedHeight, heightDamping * Time.deltaTime / factor);
		
		Quaternion currentRotation = Quaternion.Euler (0, currentRotationAngle, 0);
		
		transform.position = target.position;
		transform.position -= currentRotation * Vector3.forward * distance;
		Vector3 temp = transform.position;
		temp.y = currentHeight; 
		transform.position = temp;
		
		transform.LookAt (target);
	}
}