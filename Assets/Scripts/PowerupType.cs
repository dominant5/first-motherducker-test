﻿using UnityEngine;
using System.Collections;

namespace MotherDucker {
	/// <summary>
	/// Powerup type.
	/// </summary>
	public enum PowerupType {
		/// <summary>
		/// The speed powerup.
		/// </summary>
		SPEED,
		/// <summary>
		/// The bullettime powerup.
		/// </summary>
		BULLETTIME,
		/// <summary>
		/// The magnet powerup.
		/// </summary>
		MAGNET,
		/// <summary>
		/// The invert others control powerup.
		/// </summary>
		INVERTOTHERS,
		/// <summary>
		/// The steal powerup.
		/// </summary>
		STEAL,
		/// <summary>
		/// The lobbed bomb powerup.
		/// </summary>
		LOBBEDBOMB,
		/// <summary>
		/// No powerup.
		/// </summary>
		NONE
	}
}