﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SocketIO;

/// <summary>
/// The singleton managing the Socket.IO socket used to connect to the server.
/// </summary>
public class SocketIOManager : MonoBehaviour {
	/// <summary>
	/// The socket.
	/// </summary>
	private SocketIOComponent socket = null;

	/// <summary>
	/// The name of the client.
	/// </summary>
	private string clientName = null;

	/// <summary>
	/// The singleton instance.
	/// </summary>
	private static SocketIOManager instance = null;

	/// <summary>
	/// The start of time.
	/// </summary>
	private System.DateTime epochStart = new System.DateTime (1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);

	/// <summary>
	/// The time the last ping was sent.
	/// </summary>
	private double pingSentTime;

	/// <summary>
	/// The name of the singleton.
	/// </summary>
	private const string singletonName = "socketIOManagerSingleton";

	// Sets to false when the instance has been destroyed, this is to avoid recreating things that should
	// not be recreated, this is only used for onDestroy calls.
	/// <summary>
	/// Whether the instance has been destroyed or not.
	/// </summary>
	public static bool instanceDestroyed = false;

	// The address the user last tried to connect to.
	/// <summary>
	/// The address the user last tried to connect to.
	/// </summary>
	public string connectedAddress = "127.0.0.1";

	// The close callback type
	public delegate void CloseCallback ();

	/// <summary>
	/// The socket close callbacks to run whenever a new socket is opened.
	/// </summary>
	private List <CloseCallback> socketCloseCallbacks = new List<CloseCallback> ();

	// Used to acquire a lock.
	/// <summary>
	/// Used to acquire a lock for getting the socket.IO manager singleton.
	/// </summary>
	private static object _lock = new object ();

	// Used for initialisation
	void Awake () {
		GetInstance ();
	}

	// Used when the objects are destroyed
	void OnDestroy () {
		// These should only be called when the unity instance is stopped, so should not cause issue.
		if (this == instance) {
			instance.socket.Off ("connect", instance.OnConnect);
			instance.socket.Off ("client:status-change", instance.OnStatusChange);
			instance.socket.Off ("ping-update-request", instance.SendPing);
			instance.socket.Off ("pong", instance.SendPingUpdate);
			SocketIOManager.instanceDestroyed = true;
			Debug.Log ("Destroyed SocketIO Singleton");
		}
	}

	/// <summary>
	/// Gets the instance if it has been created, otherwise creates it and gets it.
	/// </summary>
	/// <returns>The instance.</returns>
	public static SocketIOManager GetInstance () {
		// Checks instance before locking to decrease system calls where possible.
		if (instance == null) {
			// If socketIO object not already present, begin mutual exclusion.
			lock (_lock) {
				// Check instance was not created between the existence check and the start of the mutual exclusion.
				if (instance == null) {
					// Check whether a Socket.IO Manager has been added to the scene 
					// but not registered in the socketIOManager class.
					instance = Object.FindObjectOfType (typeof(SocketIOManager)) as SocketIOManager;
					if (instance == null) {
						// No existence of socketIO object, create object and register all callbacks.
						GameObject socketIOSingleton = new GameObject (singletonName);
						DontDestroyOnLoad (socketIOSingleton);
						instance = socketIOSingleton.AddComponent<SocketIOManager> ();
						SocketIOManager.instanceDestroyed = false;
					} else {
						DontDestroyOnLoad (instance.gameObject);
					}
					// Create a new socket even if one already exists, I'm not sure why this is necessary,
					// if anyone knows, please let me know. I suspect it's to do with Unity's instantiation
					// ordering, but it doesn't matter too much (I think we'll just delete the existing component
					// from the scene).
					if (instance.socket == null) {
						instance.BuildNewSocket ("ws://127.0.0.1:9000/socket.io/?EIO=4&transport=websocket");
						instance.connectedAddress = "127.0.0.1";
					}
				}
			}
		}
		return instance;
	}

	// Attach a new socket to the instance (destroy the old one if it exists).
	// Connect to the specified address on that socket.
	/// <summary>
	/// Destroys the old socket if it exists and builds the new socket and notifies all listening
	/// objects that the socket has changed.
	/// </summary>
	/// <param name="socketAddress">New socket host name.</param>
	private void BuildNewSocket (string socketAddress) {
		if (instance.socket != null) {
			instance.socket.Close ();
			Destroy (instance.socket);
		}

		// Create new socket.
		instance.socket = instance.gameObject.AddComponent<SocketIOComponent> ();
		instance.socket.url = socketAddress;
		instance.socket.Awake ();
		instance.socket.Start ();
		instance.socket.On ("connect", instance.OnConnect);
		instance.socket.On ("client:status-change", instance.OnStatusChange);
		instance.socket.On ("ping-update-request", instance.SendPing);
		instance.socket.On ("pong", instance.SendPingUpdate);

		// Notify all subscribers that the socket has been destroyed and a new
		// socket is ready.
		instance.ClosedConnection ();
	}

	// Get the socket component of the instance.
	/// <summary>
	/// Gets the socket from the instance.
	/// </summary>
	/// <returns>The socket.</returns>
	public SocketIOComponent GetSocket () {
		// Ensure socket is present.
		if (! socket) {
			GetInstance ();
		}
		if (! instance) {
			Debug.Log ("Failed to get SocketIOManager instance.");
		}
		if (! instance.socket) {
			Debug.Log ("Failed to get socket instance.");
		}
		return instance.socket;
	}

	/// <summary>
	/// Callback for connecting.
	/// </summary>
	/// <remarks>
	/// This will likely be refactored in a later branch as this callback does not
	/// actually particularly relate to the Socket.IO Manager.
	/// </remarks>
	/// <param name="evt">Evt.</param>
	public void OnConnect (SocketIOEvent evt) {
		NotificationManager.GetInstance ().ReceiveNotification ("Connected to server");
		SessionManager.GetInstance ().LoginAsGuest ();
	}
	
	// Perform some kind of action on status change.
	/// <summary>
	/// Updates the status of the client to reflect the given new status.
	/// </summary>
	/// <param name="evt">Socket.IO event for the callback.</param>
	public void OnStatusChange (SocketIOEvent evt) {
		Debug.Log ("client:status-change");
		ClientStatusCode status = (ClientStatusCode)evt.data.GetField ("status").n;
		// if (status != ClientStatusCode.GAME_STARTED) return;
		clientName = evt.data.GetField ("name").str;
		Debug.Log ("ClientName = " + clientName);
	}

	// Return the client name.
	/// <summary>
	/// Gets the name of the client.
	/// </summary>
	/// <returns>The client name.</returns>
	public string GetClientName () {
		return clientName;
	}
	

	// Send a ping to the server.
	/// <summary>
	/// Sends a ping to the server.
	/// </summary>
	/// <param name="evt">Details of the event that lead to the callback.</param>
	public void SendPing (SocketIOEvent evt) {
		pingSentTime = (System.DateTime.UtcNow - epochStart).TotalMilliseconds;
		socket.Emit ("ping");
	}

	// Send a ping update to the server.
	/// <summary>
	/// Sends the ping update back to the server.
	/// </summary>
	/// <param name="evt">Details of the event that lead to the callback.</param>
	public void SendPingUpdate (SocketIOEvent evt) {
		double ping = (System.DateTime.UtcNow - epochStart).TotalMilliseconds - pingSentTime;
		Debug.Log ("ping: " + ping);
		JSONObject data = new JSONObject (JSONObject.Type.OBJECT);
		data.AddField ("ping", (float)ping);
		socket.Emit ("ping-update", data);
	}

	// Notify all listeners that the connection been closed.
	/// <summary>
	/// Notify all listeners that the connection has been closed.
	/// </summary>
	private void ClosedConnection () {
		foreach (CloseCallback callback in socketCloseCallbacks) {
			callback ();
		}
	}

	// Register a new callback to be called when the socket is closed to
	// start a new connection.
	/// <summary>
	/// Registers a new close callback if the given callback does not exist
	/// in the list of callbacks yet.
	/// </summary>
	/// <param name="callback">The callback.</param>
	public void RegisterCloseCallback (CloseCallback callback) {

		if (!socketCloseCallbacks.Contains (callback)) {
			Debug.Log ("Already have callback " + callback.ToString ());
			socketCloseCallbacks.Add (callback);
		}
	}

	// Deregister an existing callback from the list of callbacks for when 
	// a socket is closed to start a new connection.
	/// <summary>
	/// Deregisters the given close callback.
	/// </summary>
	/// <param name="callback">Callback to deregister.</param>
	public void DeregisterCloseCallback (CloseCallback callback) {
		if (!socketCloseCallbacks.Contains (callback)) {
			socketCloseCallbacks.Remove (callback);
		}
	}

	// Connect to a new server on the standard port.
	/// <summary>
	/// Connects to a given server based on the given host name.
	/// </summary>
	/// <param name="newServerName">New server hostname.</param>
	public void ConnectToNewServer (string newServerName) {
		if (this != instance) {
			instance.ConnectToNewServer (newServerName);
			return;
		}

		if (! newServerName.Equals (instance.connectedAddress)) {
			NotificationManager.GetInstance ().ReceiveNotification ("Attempting to connect to " + newServerName);

			instance.connectedAddress = newServerName;
			instance.BuildNewSocket ("ws://" + newServerName + ":9000/socket.io/?EIO=4&transport=websocket");
		} else {
			NotificationManager.GetInstance ().ReceiveNotification ("Already connecting to " + newServerName);
		}
	}

	/// <summary>
	/// Checks whether the server is the same server as the hostname given.
	/// </summary>
	/// <remarks>
	/// This is a purely string-wise operation, no name resolution occurs.
	/// </remarks>
	/// <returns><c>true</c>, if hostname and newServerName were the same, <c>false</c> otherwise.</returns>
	/// <param name="newServerName">New server name.</param>
	public bool SameServer (string newServerName) {
		if (this != instance) {
			instance.SameServer (newServerName);
		}
		return newServerName.Equals (instance.connectedAddress);
	}
}
