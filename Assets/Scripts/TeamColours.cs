﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// The set of team, text and duckling colours.
/// </summary>
public class TeamColours : MonoBehaviour {
	/// <summary>
	/// The list of colours for the game.
	/// </summary>
	private List<ColourList> colours = new List<ColourList> ();

	/// <summary>
	/// The duckling colour.
	/// </summary>
	public Color32 ducklingColour;

	/// <summary>
	/// The set of team and text colours for the game.
	/// </summary>
	public class ColourList {
		/// <summary>
		/// The team colour.
		/// </summary>
		public Color32 teamColour;

		/// <summary>
		/// The text colour.
		/// </summary>
		public Color32 textColour;

		/// <summary>
		/// The name of the colour.
		/// </summary>
		public string colourName;

		/// <summary>
		/// Initializes a new instance of the <see cref="TeamColours+ColourList"/> class.
		/// </summary>
		/// <param name="teamColour">The set of team and text colours.</param>
		public ColourList (JSONObject teamColour) {
			JSONObject teamC = teamColour.GetField ("teamColour");
			this.teamColour = new Color32 ((byte)teamC.GetField ("x").n, (byte)teamC.GetField ("y").n, (byte)teamC.GetField ("z").n, 255);
			JSONObject textC = teamColour.GetField ("textColour");
			this.textColour = new Color32 ((byte)textC.GetField ("x").n, (byte)textC.GetField ("y").n, (byte)textC.GetField ("z").n, 255);
			this.colourName = teamColour.GetField ("colourName").str;
		}
	}

	/// <summary>
	/// Gets the team colours.
	/// </summary>
	/// <returns>The team colours for the game.</returns>
	/// <param name="teamColours">The set of team and text colours.</param>
	public ColourList[] GetTeamColours (JSONObject teamColours) {
		for (int i = 0; i < teamColours.Count; i++) {
			colours.Add (new ColourList (teamColours [i]));
		}
		return colours.ToArray ();
	}
}