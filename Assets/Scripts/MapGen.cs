﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This script generates a textured mesh given a spec containing information about the verts, faces, and UV coords.
/// Also attaches a mesh collider to the GameObject.
/// </summary>
public class MapGen : MonoBehaviour {
	/// <summary>
	/// Generates the mesh.
	/// </summary>
	/// <param name="mapName">The name of the map. Used to load map-specific texture resources.</param>
	/// <param name="spec">The map spec. Should contain info about verts, faces, UV coords, and the map object's
	/// types.</param>
	public void Generate (string mapName, JSONObject spec) {
		string matName = null;
		JSONObject types = spec.GetField ("types");
		bool noShadows = false;
		bool twoSided = false;
		bool isWater = false;
		bool isHazard = false;
		bool isHexagons = false;
		for (int i = 0; i < types.Count; i++) {
			if (types [i].str == "water") {
				isWater = true;
				break;
			} else if (types [i].str == "hazard") {
				isHazard = true;
				break;
			} else if (types [i].str == "hexagons") {
				twoSided = true;
				isHexagons = true;
			}
		}
		if (isWater) {
			matName = "water";
			noShadows = true;
		} else if (isHazard) {
			matName = "hazard";
		} else if (isHexagons) {
			matName = "hexagons";
			noShadows = true;
		} else {
			JSONObject matSpec = spec.GetField ("material");
			if (matSpec != null)
				matName = "Maps/" + mapName + "/" + matSpec.str;
		}
		
		if (matName != null) {
			Material mat = (Material)Resources.Load ("Materials/" + matName, typeof(Material));
			Renderer r = GetComponent<Renderer> ();
			if (noShadows)
				r.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
			r.sharedMaterial = mat;
		}

		List<Vector3> verts = new List<Vector3> ();
		List<int> tris = new List<int> ();
		List<Vector2> uv = new List<Vector2> ();

		JSONObject srcVerts = spec.GetField ("v");
		List<Vector3> tempVerts = new List<Vector3> ();
		for (int i = 0; i < srcVerts.Count; i++) {
			JSONObject vert = srcVerts [i];
			tempVerts.Add (new Vector3 (vert.GetField ("x").n, vert.GetField ("y").n, vert.GetField ("z").n));
		}
		JSONObject quads = spec.GetField ("f");
		JSONObject srcUV = spec.GetField ("uv");
		for (int i = 0; i < quads.Count; i++) {
			JSONObject quad = quads [i];
			addQuad (tempVerts, srcUV, quad, i, verts, tris, uv, false);
			if (twoSided)
				addQuad (tempVerts, srcUV, quad, i, verts, tris, uv, true);
		}

		Mesh mesh = GetComponent<MeshFilter> ().mesh;
		mesh.Clear ();
		mesh.vertices = verts.ToArray ();
		mesh.triangles = tris.ToArray ();
		mesh.uv = uv.ToArray ();
		mesh.Optimize ();
		mesh.RecalculateNormals ();

		MeshCollider collider = gameObject.AddComponent (typeof(MeshCollider)) as MeshCollider;
		collider.sharedMesh = mesh;
	}

	/// <summary>
	/// Adds a quad face to the mesh.
	/// </summary>
	/// <param name="tempVerts">Array of verts in the mesh.</param>
	/// <param name="srcUV">UV info.</param>
	/// <param name="quad">The indices in the tempVerts array of the verts that make up the quad.</param>
	/// <param name="quadIndex">Quad index. Used for getting the right UV coords out of the srvUV array.</param>
	/// <param name="verts">The destination verts array.</param>
	/// <param name="tris">The destination tris array.</param>
	/// <param name="uv">The destination UV array.</param>
	/// <param name="flipNormal">If set to <c>true</c>, flips the normal.</param>
	private void addQuad (List<Vector3> tempVerts, JSONObject srcUV, JSONObject quad, int quadIndex, List<Vector3> verts, List<int> tris, List<Vector2> uv, bool flipNormal) {
		int vertCount = verts.Count;
		verts.Add (tempVerts [(int)quad [0].n]);
		verts.Add (tempVerts [(int)quad [1].n]);
		verts.Add (tempVerts [(int)quad [2].n]);
		verts.Add (tempVerts [(int)quad [3].n]);
		
		tris.Add (vertCount + (flipNormal ? 0 : 3));
		tris.Add (vertCount + 1);
		tris.Add (vertCount + (flipNormal ? 3 : 0));
		
		tris.Add (vertCount + (flipNormal ? 1 : 3));
		tris.Add (vertCount + 2);
		tris.Add (vertCount + (flipNormal ? 3 : 1));
		
		if (srcUV != null) {
			JSONObject u0 = srcUV [quadIndex * 4];
			JSONObject u1 = srcUV [quadIndex * 4 + 1];
			JSONObject u2 = srcUV [quadIndex * 4 + 2];
			JSONObject u3 = srcUV [quadIndex * 4 + 3];
			uv.Add (new Vector2 ((float)u0 [0].n, (float)u0 [1].n));
			uv.Add (new Vector2 ((float)u1 [0].n, (float)u1 [1].n));
			uv.Add (new Vector2 ((float)u2 [0].n, (float)u2 [1].n));
			uv.Add (new Vector2 ((float)u3 [0].n, (float)u3 [1].n));
		}
	}
}
