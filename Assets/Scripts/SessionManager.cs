﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SocketIO;

/// <summary>
/// Session manager is a singleton that handles sessions.
/// </summary>
public class SessionManager : MonoBehaviour {
	/// <summary>
	/// The current socket.
	/// </summary>
	protected SocketIOComponent socket;

	/// <summary>
	/// The name of the singleton.
	/// </summary>
	private const string singletonName = "sessionManagerSingleton";

	/// <summary>
	/// The singleton instance.
	/// </summary>
	private static SessionManager instance = null;

	// Used to acquire a lock.
	/// <summary>
	/// Used to acquire a lock.
	/// </summary>
	private static object _lock = new object ();

	// The account manager.
	/// <summary>
	/// The account manager.
	/// </summary>
	protected AccountManager accountManager = null;

	// The notification manager.
	/// <summary>
	/// The notification manager.
	/// </summary>
	protected NotificationManager notificationManager = null;

	// The username of the currently logged in user.
	/// <summary>
	/// The username of the currently logged in user.
	/// </summary>
	private static string username = null;

	// Whether the user is currently logged in to an account.
	/// <summary>
	/// Whether the user is currently logged in to an account.
	/// </summary>
	private bool loggedInFull = false;

	// Whether the user is currently logged in (guest or an account).
	/// <summary>
	/// Whether the user is currently logged in to either a guest account
	/// or a proper account or not.
	/// </summary>
	private bool loggedIn = false;

	// Used to make SessionManager singleton if first run, otherwise does nothing
	// but allow object to serve as an interface to singleton for Unity objects.
	void Awake () {
		GetInstance ();
	}

	// When the object is destroyed.
	void OnDestroy () {
		if (this == instance) {
			if (! SocketIOManager.instanceDestroyed) {
				instance.socket.Off ("session:login-success", instance.LoginSuccess);
				instance.socket.Off ("session:logout-success", instance.LogoutSuccess);
				instance.socket.Off ("session:play-as-guest-success", instance.LoginAsGuestSuccess);
				SocketIOManager.GetInstance ().DeregisterCloseCallback (instance.GetNewSocket);
			}
			Debug.Log ("Destroyed Session Manager Singleton");
		}
	}

	/// <summary>
	/// Gets the singleton instance if it is stored, otherwise creates it and gets it.
	/// </summary>
	/// <returns>The singleton instance.</returns>
	public static SessionManager GetInstance () {
		// Checks instance before locking to decrease system calls where possible.
		if (instance == null) {
			// If session manager object not already present, begin mutual exclusion.
			lock (_lock) {
				// Check instance was not created between the existence check and the start of the mutual exclusion.
				if (instance == null) {
					// Check whether a Session Manager has been added to the scene 
					// but not registered in the SessionManager class.
					instance = Object.FindObjectOfType (typeof(SessionManager)) as SessionManager;
					if (instance == null) {
						// No existence of Session Manager object, create object and register all callbacks.
						GameObject sessionManagerSingleton = new GameObject (singletonName);
						DontDestroyOnLoad (sessionManagerSingleton);
						instance = sessionManagerSingleton.AddComponent<SessionManager> ();
					} else {
						DontDestroyOnLoad (instance.gameObject);
					}
					SocketIOManager.GetInstance ().RegisterCloseCallback (instance.GetNewSocket);
					instance.GetNewSocket ();
					instance.accountManager = AccountManager.GetInstance ();
					instance.notificationManager = NotificationManager.GetInstance ();
				}
			}
		}
		return instance;
	}

	/// <summary>
	/// Raises the level was loaded event, updating any elements of the
	/// scene that rely on the session manager.
	/// </summary>
	/// <param name="scene">Scene that was loaded.</param>
	void OnLevelWasLoaded (int scene) {
		if (scene == LoadGame.MENUSCENE) {
			Text usernameUI = GameObject.Find ("Canvas/Username").GetComponent<Text> ();
			if (usernameUI) {
				usernameUI.text = "Logged in as: " + username;
			}
			if (loggedIn) {
				GameObject.Find ("Canvas/LogOutButton").GetComponent<Button> ().interactable = true;
				Button[] buttons = GameObject.Find ("Canvas").transform.GetComponentsInChildren<Button> (true);
				foreach (Button button in buttons) {
					if (button.name == "LogInButton") {
						button.interactable = (loggedInFull) ? false : true;
					}
					if (button.name == "LogOutButton") {
						button.interactable = (loggedInFull) ? true : false;
					}
					if (button.name == "MenuSignUpButton") {
						button.interactable = (loggedInFull) ? false : true;
					}
					if (button.name == "FriendButton") {
						button.interactable = (loggedInFull) ? true : false;
					}
				}
			}
		}
	}

	// Get the newest socket and register callbacks.
	/// <summary>
	/// Gets the new socket and registers manager's callbacks on it.
	/// </summary>
	public void GetNewSocket () {
		
		Debug.Log ("Got new socket");

		instance.loggedInFull = false;
		instance.loggedIn = false;
		instance.socket = SocketIOManager.GetInstance ().GetSocket ();

		instance.socket.On ("session:login-success", instance.LoginSuccess);
		instance.socket.On ("session:logout-success", instance.LogoutSuccess);
		instance.socket.On ("session:play-as-guest-success", instance.LoginAsGuestSuccess);
	}

	/// <summary>
	/// Login with currently stored details for username and password from AccountManager.
	/// </summary>
	public void Login () {
		if (this != instance) {
			instance.Login ();
			return;
		}
		JSONObject data = new JSONObject (JSONObject.Type.OBJECT);
		if (loggedIn || loggedInFull) {
			Logout ();
		}
		if (accountManager.GetUsername () == "No username" || accountManager.GetPassword () == "No password") {
			notificationManager.ReceiveNotification ("Please fill in all fields");
		}
		data.AddField ("username", accountManager.GetUsername ());
		data.AddField ("password", accountManager.GetPassword ());

		Debug.Log ("Tried to log in using " + data.ToString ());
		socket.Emit ("session:login", data);
	}

	/// <summary>
	/// Runs when successfully logging in, updates all items to reflect
	/// the event of logging in.
	/// </summary>
	/// <param name="evt">The SocketIO event that caused the callback.</param>
	public void LoginSuccess (SocketIOEvent evt) {
		if (evt.data.HasField ("name")) {
			loggedInFull = true;
			loggedIn = true;
			
			username = evt.data.GetField ("name").str;
			notificationManager.ReceiveNotification ("Logged in as: " + username + ". Welcome!");
			PlayerPrefs.SetString ("username", username);
			Text usernameUI = GameObject.Find ("Canvas/Username").GetComponent<Text> ();
			if (usernameUI) {
				usernameUI.text = "Logged in as: " + username;
			}
			GameObject.Find ("Canvas/LogInPanel/LogInMenuPanelManager").GetComponent<OpenCloseWindow> ().CloseCurrent ();
			Button[] buttons = GameObject.Find ("Canvas").transform.GetComponentsInChildren<Button> (true);
			foreach (Button button in buttons) {
				if (button.name == "LogInButton") {
					button.interactable = false;
				}
				if (button.name == "LogOutButton") {
					button.interactable = true;
				}
				if (button.name == "MenuSignUpButton") {
					button.interactable = false;
				}
				if (button.name == "FriendButton") {
					button.interactable = true;
				}
			}
			Debug.Log ("Logged in.");
		} else {
			notificationManager.ReceiveNotification ("Logged in!");
			Debug.Log ("Failed to retrieve name from log in event. " + evt.ToString ());
		}
	}

	/// <summary>
	/// Logout from current account.
	/// </summary>
	public void Logout () {
		if (this != instance) {
			instance.Logout ();
			return;
		}
		if (loggedIn || loggedInFull) {
			instance.socket.Emit ("session:logout");
		}

		Debug.Log ("Tried to log out");
	}

	/// <summary>
	/// Callback for when the user was successfully logged out.
	/// </summary>
	/// <param name="evt">The Socket.IO event for the callback.</param>
	public void LogoutSuccess (SocketIOEvent evt) {
		Debug.Log ("Logged out!");

		//If logged out as user, re-login as guest.
		if (loggedInFull) {
			LoginAsGuest ();
			return;
		}

		loggedInFull = false;
		loggedIn = false;
	}

	/// <summary>
	/// Determines whether this instance is logged in at all, even as a guest.
	/// </summary>
	/// <returns><c>true</c> if this instance is logged in; otherwise, <c>false</c>.</returns>
	public bool IsLoggedIn () {
		return loggedIn;
	}

	/// <summary>
	/// Determines whether this instance is logged in properly in a full account.
	/// </summary>
	/// <returns><c>true</c> if this instance is logged in fully; otherwise, <c>false</c>.</returns>
	public bool IsLoggedInFull () {
		return loggedInFull;
	}

	/// <summary>
	/// Gets the username the user is currently logged in with.
	/// </summary>
	/// <returns>The username.</returns>
	public string GetUsername () {
		return username;
	}

	/// <summary>
	/// Logs in as a guest.
	/// </summary>
	public void LoginAsGuest () {
		if (this != instance) {
			instance.LoginAsGuest ();
			return;
		}
		if (loggedIn || loggedInFull) {
			Logout ();
		}
		socket.Emit ("session:play-as-guest");
		Debug.Log ("Logging in as guest");
	}

	/// <summary>
	/// Callback for when the logins as guest request was a success.
	/// </summary>
	/// <param name="evt">The Socket.IO event for the callback.</param>
	public void LoginAsGuestSuccess (SocketIOEvent evt) {
		if (this != instance) {
			instance.LoginAsGuestSuccess (evt);
			return;
		}
		notificationManager.ReceiveNotification ("Logged in as guest!");
		if (evt.data.HasField ("name")) {
			loggedInFull = false;
			loggedIn = true;
			
			username = evt.data.GetField ("name").str;
			Text usernameUI = GameObject.Find ("Canvas/Username").GetComponent<Text> ();
			if (usernameUI) {
				usernameUI.text = "Logged in as: " + username;
			}
			Debug.Log ("Logged in as guest.");
			Button[] buttons = GameObject.Find ("Canvas").transform.GetComponentsInChildren<Button> (true);
			foreach (Button button in buttons) {
				if (button.name == "LogInButton") {
					button.interactable = true;
				}
				if (button.name == "LogOutButton") {
					button.interactable = false;
				}
				if (button.name == "MenuSignUpButton") {
					button.interactable = true;
				}
				if (button.name == "FriendButton") {
					button.interactable = false;
				}
			}
		} else {
			Debug.Log ("Failed to retrieve name from log in event. " + evt.ToString ());
		}
	}
}
