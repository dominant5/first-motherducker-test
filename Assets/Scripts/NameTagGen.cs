﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This script generates an in-game name-tag mesh that serves as the coloured background to their name-tag text.
/// </summary>
public class NameTagGen : MonoBehaviour {
	/// <summary>
	/// The border offset.
	/// </summary>
	private float borderOffset = 0.05f;

	/// <summary>
	/// Generates the mesh.
	/// </summary>
	/// <param name="playerNameBounds">Player name bounds.</param>
	public void Generate (Bounds playerNameBounds) {
		List<Vector3> verts = new List<Vector3> ();
		List<int> tris = new List<int> ();
		List<Vector2> uv = new List<Vector2> ();
		Mesh mesh = GetComponent<MeshFilter> ().mesh;

		float xSize = playerNameBounds.extents.x;
		float ySize = playerNameBounds.extents.y;

		//Top Left
		verts.Add (new Vector3 (-xSize - borderOffset, ySize + borderOffset, 0));
		//Top Right
		verts.Add (new Vector3 (xSize + borderOffset, ySize + borderOffset, 0));
		//Bottom Left
		verts.Add (new Vector3 (-xSize - borderOffset, -ySize - borderOffset, 0));
		//Bottom Right
		verts.Add (new Vector3 (xSize + borderOffset, -ySize - borderOffset, 0));

		//Top Left
		tris.Add (0);
		//Bottom Left
		tris.Add (2);
		//Top Right
		tris.Add (1);

		//Top Right
		tris.Add (1);
		//Bottom Left
		tris.Add (2);
		//Bottom Right
		tris.Add (3);

		uv.Add (new Vector2 (0, 0));
		uv.Add (new Vector2 (1, 0));
		uv.Add (new Vector2 (1, 1));
		uv.Add (new Vector2 (0, 1));

		mesh.Clear ();
		mesh.vertices = verts.ToArray ();
		mesh.triangles = tris.ToArray ();
		mesh.uv = uv.ToArray ();
		mesh.Optimize ();
		mesh.RecalculateNormals ();
	}
}