﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class housing methods for loading the game scene and other
/// scenes.
/// </summary>
public class LoadGame : MonoBehaviour {

	/* The index for the game scene.						 */
	/// <summary>
	/// The index for the game scene.
	/// </summary>
	public const int GAMESCENE = 1;

	/// <summary>
	/// The index for the menu scene.
	/// </summary>
	public const int MENUSCENE = 0;

	/* Load the level with the game scene in the background. */
	/// <summary>
	/// Loads the game scene asynchronously.
	/// </summary>
	/// <remarks> 
	/// Only works in Unity Pro so actually requires a bit of
	/// work to make work again. A good time to call this function
	/// now would be when the finding game bar appears.
	/// </remarks>
	public static void loadGame(){
		/* These two lines can be added in Unity Pro, but    */
		/* won't work otherwise, the corresponding code to   */
		/* use this code needs to be added to the 			 */
		/* openGameScene function in such a case.			 */
		/*loadReturn = Application.LoadLevelAsync (GAMESCENE);
		Debug.Log ("Began loading GAMESCENE");*/
	}

	/* Finish loading and change to scene.					 */
	/// <summary>
	/// Opens the game scene.
	/// </summary>
	/// <param name="scene">Scene index to load.</param>
	public static void OpenScene(int scene){
		Application.LoadLevel (scene);
	}
}
