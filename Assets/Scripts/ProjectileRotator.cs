﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A script to rotate all projectiles.
/// </summary>
public class ProjectileRotator : MonoBehaviour {

	void Update () {
		// Rotate in time
		transform.Rotate (new Vector3 (0, 0, 180) * Time.deltaTime);
	}
}
