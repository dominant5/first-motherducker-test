using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using SocketIO;

/// <summary>
/// The game manager which manages the rendering and positioning of the game objects.
/// </summary>
public class GameManager : MonoBehaviour {
	/// <summary>
	/// The socketIO component
	/// </summary>
	private SocketIOComponent socket;

	/// <summary>
	/// The name of the client.
	/// </summary>
	private string clientName;

	/// <summary>
	/// The intial touch position.
	/// </summary>
	private Vector2 baseTouch;

	/// <summary>
	/// The start position of the swipe.
	/// </summary>
	private Vector2 swipeStart;

	/// <summary>
	/// The width of the screen third.
	/// </summary>
	private float screenThirdWidth;

	/// <summary>
	/// A dictionary of entities of each game object in the game.
	/// </summary>
	private Dictionary<string, GameObject> entities;

	/// <summary>
	/// The status of the game.
	/// </summary>
	private MotherDucker.GameStatus status;

	/// <summary>
	/// The current time of the game.
	/// </summary>
	private float gameTimer;

	/// <summary>
	/// The type of the turn input.
	/// </summary>
	private MotherDucker.TurnType turnType;

	/// <summary>
	/// The slow motion factor.
	/// </summary>
	private float slowMotionFactor;

	/// <summary>
	/// The last server message.
	/// </summary>
	private JSONObject lastServerMessage;

	/// <summary>
	/// Boolean check to see if client has fireable equipped.
	/// </summary>
	private bool hasFireable;

	/// <summary>
	/// The fireable that the client has equipped.
	/// </summary>
	private GameObject fireable;

	/// <summary>
	/// The type of the fireable.
	/// </summary>
	private MotherDucker.PowerupType fireableType;

	/// <summary>
	/// The ammunition of the fireable.
	/// </summary>
	private int ammunition;
	
	/// <summary>
	/// The team colours for this game.
	/// </summary>
	private TeamColours.ColourList[] teamColours;

	/// <summary>
	/// The set of colours.
	/// </summary>
	private TeamColours colours;

	/// <summary>
	/// The duckling colour.
	/// </summary>
	private Color32 ducklingColour;

	/// <summary>
	/// The properties block for the material.
	/// </summary>
	private MaterialPropertyBlock props;

	/// <summary>
	/// The timer text display.
	/// </summary>
	public Text timerText;

	/// <summary>
	/// The animation.
	/// </summary>
	private Animator anim;

	/// <summary>
	/// The menu button toggle in game HUD.
	/// </summary>
	public GameObject menuButtonToggle;

	/// <summary>
	/// The escape menu in game HUD.
	/// </summary>
	public GameObject escapeMenu;

	/// <summary>
	/// The powerup timer displays.
	/// </summary>
	private Dictionary<string, Dictionary<MotherDucker.PowerupType, GameObject>> powerups;

	/// <summary>
	/// The winner text on end of game.
	/// </summary>
	public Text winnerText;

	/// <summary>
	/// The end game menu.
	/// </summary>
	public GameObject endGameMenu;

	/// <summary>
	/// The statistics text that holds the end of game stats.
	/// </summary>
	public GameObject statisticsText;

	/// <summary>
	/// The column header prefab.
	/// </summary>
	public Text columnHeaderPrefab;

	/// <summary>
	/// The stat prefab.
	/// </summary>
	public Text statPrefab;

	/// <summary>
	/// The row header prefab.
	/// </summary>
	public Text rowHeaderPrefab;

	/// <summary>
	/// The column stats prefab.
	/// </summary>
	public GameObject columnStatsPrefab;

	/// <summary>
	/// The mother duck prefab.
	/// </summary>
	public GameObject motherDuckPrefab;

	/// <summary>
	/// The duckling prefab.
	/// </summary>
	public GameObject ducklingPrefab;

	/// <summary>
	/// The map mesh prefab.
	/// </summary>
	public GameObject mapMeshPrefab;

	/// <summary>
	/// The player name prefab.
	/// </summary>
	public GameObject playerNamePrefab;

	/// <summary>
	/// The palm tree prefab.
	/// </summary>
	public GameObject palmTreePrefab;

	/// <summary>
	/// The small rock prefab.
	/// </summary>
	public GameObject smallRockPrefab;

	/// <summary>
	/// The speed powerup prefab.
	/// </summary>
	public GameObject speedPowerupPrefab;

	/// <summary>
	/// The lobbed bomb powerup prefab.
	/// </summary>
	public GameObject lobbedBombPowerupPrefab;

	/// <summary>
	/// The bullet time powerup prefab.
	/// </summary>
	public GameObject bulletTimePowerupPrefab;

	/// <summary>
	/// The steal powerup prefab.
	/// </summary>
	public GameObject stealPowerupPrefab;

	/// <summary>
	/// The magnet powerup prefab.
	/// </summary>
	public GameObject magnetPowerupPrefab;

	/// <summary>
	/// The invert others powerup prefab.
	/// </summary>
	public GameObject invertOthersPowerupPrefab;

	/// <summary>
	/// The lobbed bomb projectile prefab.
	/// </summary>
	public GameObject lobbedBombProjectilePrefab;

	/// <summary>
	/// The steal projectile prefab.
	/// </summary>
	public GameObject stealProjectilePrefab;

	/// <summary>
	/// The lobbed bomb ammo prefab.
	/// </summary>
	public GameObject lobbedBombAmmoPrefab;

	/// <summary>
	/// The steal ammo prefab.
	/// </summary>
	public GameObject stealAmmoPrefab;

	/// <summary>
	/// The speed timer prefab.
	/// </summary>
	public GameObject speedTimerPrefab;

	/// <summary>
	/// The bullet time timer prefab.
	/// </summary>
	public GameObject bulletTimeTimerPrefab;

	/// <summary>
	/// The magnet timer prefab.
	/// </summary>
	public GameObject magnetTimerPrefab;

	/// <summary>
	/// The invert others timer prefab.
	/// </summary>
	public GameObject invertOthersTimerPrefab;

	/// <summary>
	/// The explosion prefab.
	/// </summary>
	public GameObject explosionPrefab;

	void Awake () {
		// Set target fps
		Application.targetFrameRate = 60;

		// Get socketIO instance
		SocketIOManager.GetInstance ().RegisterCloseCallback (GetNewSocket);
		GetNewSocket ();

		clientName = SocketIOManager.GetInstance ().GetClientName ();

		// Set defaults for touch input
		baseTouch = new Vector2 (-1, -1);
		swipeStart = baseTouch;
		screenThirdWidth = Screen.width / 3;

		// Set game settings
		entities = new Dictionary<string, GameObject> ();
		status = MotherDucker.GameStatus.WAITING_TO_START;
		turnType = MotherDucker.TurnType.STOP;
		slowMotionFactor = 1.0f;

		// No fireable equipped at start
		hasFireable = false;

		// Get the set of colours ready for use
		colours = GetComponent<TeamColours> ();
		ducklingColour = colours.ducklingColour;
		props = new MaterialPropertyBlock ();

		// Grab the animator for the UI
		anim = GameObject.Find ("Canvas").GetComponent<Animator> ();

		// Initialize the powerup timer dictionary
		powerups = new Dictionary<string, Dictionary<MotherDucker.PowerupType, GameObject>> ();
	}

	/// <summary>
	/// Gets the new socket and registers callbacks.
	/// </summary>
	void GetNewSocket () {
		socket = SocketIOManager.GetInstance ().GetSocket ();

		// Register callbacks on socket.
		socket.On ("game:started", OnGameStarted);
		socket.On ("game:spawn", OnSpawn);
		socket.On ("game:update", OnGameUpdate);
		socket.On ("game:ended", OnGameEnded);
		socket.On ("game:get-powerup", OnGetPowerup);
		socket.On ("game:get-fireable", OnGetFireable);
		socket.On ("game:update-ammo", OnUpdateAmmo);
		socket.On ("game:update-ptimer", OnUpdatePTimer);
		socket.On ("game:explosion", OnExplosion);
		socket.On ("game:remove-entity", OnRemoveEntity);
		socket.On ("game:assign-team", OnAssignTeam);
	}

	void Start () {
		// Let server know that game is ready
		socket.Emit ("game:ready");
	}

	void OnDestroy () {
		// Deregisters callbacks.
		if (! SocketIOManager.instanceDestroyed) {
			socket.Off ("game:started", OnGameStarted);
			socket.Off ("game:spawn", OnSpawn);
			socket.Off ("game:update", OnGameUpdate);
			socket.Off ("game:ended", OnGameEnded);
			socket.Off ("game:get-powerup", OnGetPowerup);
			socket.Off ("game:get-fireable", OnGetFireable);
			socket.Off ("game:update-ammo", OnUpdateAmmo);
			socket.Off ("game:update-ptimer", OnUpdatePTimer);
			socket.Off ("game:explosion", OnExplosion);
			socket.Off ("game:remove-entity", OnRemoveEntity);
			socket.Off ("game:assign-team", OnAssignTeam);
			SocketIOManager.GetInstance ().DeregisterCloseCallback (GetNewSocket);
		}
	}

	/// <summary>
	/// Raises the game started event callback.
	/// </summary>
	/// <param name="evt">The event emit data.</param>
	public void OnGameStarted (SocketIOEvent evt) {
		status = MotherDucker.GameStatus.STARTED;

		// Update name in case it changed since the game started.
		clientName = SocketIOManager.GetInstance ().GetClientName ();

		Debug.Log ("game:started");
		JSONObject world = evt.data;

		// Render the map objects
		string mapName = world.GetField ("mapName").str;
		JSONObject mapObjects = world.GetField ("mapObjects");
		foreach (string key in mapObjects.keys) {
			JSONObject spec = mapObjects.GetField (key);

			GameObject mapObj = (GameObject)Instantiate (mapMeshPrefab);
			MapGen mapGen = mapObj.GetComponent<MapGen> ();
			mapGen.Generate (mapName, spec);

			JSONObject p = spec.GetField ("p");
			mapObj.transform.position = new Vector3 (p.GetField ("x").n, p.GetField ("y").n, p.GetField ("z").n);

			JSONObject q = spec.GetField ("q");
			mapObj.transform.rotation = new Quaternion (q.GetField ("x").n, q.GetField ("y").n, q.GetField ("z").n, q.GetField ("w").n);
		}

		// Render the map proxy objects
		JSONObject mapProxies = world.GetField ("mapProxies");
		foreach (string type in mapProxies.keys) {
			JSONObject proxies = mapProxies.GetField (type);
			for (int i = 0; i < proxies.Count; i++) {
				JSONObject proxy = proxies [i];

				GameObject obj = null;
				if (type == "tree")
					obj = (GameObject)Instantiate (palmTreePrefab);
				else if (type == "smallRock")
					obj = (GameObject)Instantiate (smallRockPrefab);
				else
					continue;

				JSONObject p = proxy.GetField ("p");
				obj.transform.position = new Vector3 (p.GetField ("x").n, p.GetField ("y").n, p.GetField ("z").n);
				
				JSONObject q = proxy.GetField ("q");
				obj.transform.rotation = new Quaternion (q.GetField ("x").n, q.GetField ("y").n, q.GetField ("z").n, q.GetField ("w").n);
			}
		}

		// Get the team colours for this game.
		teamColours = colours.GetTeamColours (world.GetField ("teamColours"));

		// Create the motherducks
		JSONObject mdList = world.GetField ("motherducks");
		foreach (string key in mdList.keys) {
			JSONObject e = mdList.GetField (key);
			GameObject eObj = (GameObject)Instantiate (motherDuckPrefab);
			int team = (int)e.GetField ("team").n;
			Color32 teamColour = teamColours [team].teamColour;
			SetColourProp (teamColour, eObj);
			JSONObject p = e.GetField ("p");
			eObj.transform.position = new Vector3 (p.GetField ("x").n, p.GetField ("y").n, p.GetField ("z").n);
			Quaternion oldRotation = eObj.transform.rotation;
			JSONObject q = e.GetField ("q");
			eObj.transform.rotation = new Quaternion (q.GetField ("x").n, q.GetField ("y").n, q.GetField ("z").n, q.GetField ("w").n);
			entities.Add (key, eObj);

			// Add each motherduck to the powerup dictionary
			string name = e.GetField ("name").str;
			powerups.Add (name, new Dictionary<MotherDucker.PowerupType, GameObject> ());

			// Set followcam to the player, and set player name tags for others
			if (name == clientName) {
				FollowCam fc = Camera.main.GetComponent<FollowCam> ();
				fc.target = eObj.transform;
			} else {
				// Set the player names above head
				TextMesh playerNameText = Instantiate (playerNamePrefab).GetComponent<TextMesh> ();
				playerNameText.text = name;
				playerNameText.transform.SetParent (eObj.transform, false);
				playerNameText.transform.rotation = oldRotation;
				Bounds playerNameBounds = playerNameText.GetComponent<Renderer> ().bounds;
				// Generate name tag background
				NameTagGen nameTagGen = playerNameText.transform.Find ("NameTagMesh").GetComponent<NameTagGen> ();
				nameTagGen.Generate (playerNameBounds);
				Color32 textColour = teamColours [team].textColour;
				SetColourProp (teamColour, textColour, playerNameText.gameObject);
			}
		}

		// Set game duration
		gameTimer = world.GetField ("gt").n;
	}

	/// <summary>
	/// Raises the spawn event callback for spawned entities.
	/// </summary>
	/// <param name="evt">The event emit data.</param>
	public void OnSpawn (SocketIOEvent evt) {
		JSONObject spawnInfo = evt.data;
		string type = spawnInfo.GetField ("type").str;

		// Instantiate ducklings
		if (type == "duckling") {
			GameObject obj = (GameObject)Instantiate (ducklingPrefab);
			JSONObject p = spawnInfo.GetField ("p");
			obj.transform.position = new Vector3 (p.GetField ("x").n, p.GetField ("y").n, p.GetField ("z").n);
			JSONObject q = spawnInfo.GetField ("q");
			obj.transform.rotation = new Quaternion (q.GetField ("x").n, q.GetField ("y").n, q.GetField ("z").n, q.GetField ("w").n);
			string id = spawnInfo.GetField ("id").ToString ();
			entities.Add (id, obj);
		}

		// Instantiate powerups
		if (type == "powerup") {
			string powerupType = spawnInfo.GetField ("powerupType").str;
			GameObject obj = null;
			switch (powerupType) {
			case "speed":
				obj = (GameObject)Instantiate (speedPowerupPrefab);
				break;
			case "lobbedBomb":
				obj = (GameObject)Instantiate (lobbedBombPowerupPrefab);
				break;
			case "bulletTime":
				obj = (GameObject)Instantiate (bulletTimePowerupPrefab);
				break;
			case "steal":
				obj = (GameObject)Instantiate (stealPowerupPrefab);
				break;
			case "magnet":
				obj = (GameObject)Instantiate (magnetPowerupPrefab);
				break;
			case "invertOthers":
				obj = (GameObject)Instantiate (invertOthersPowerupPrefab);
				break;
			}

			if (obj) {
				JSONObject p = spawnInfo.GetField ("p");
				obj.transform.position = new Vector3 (p.GetField ("x").n, p.GetField ("y").n, p.GetField ("z").n);
				string id = spawnInfo.GetField ("id").ToString ();
				entities.Add (id, obj);
			}
		}

		// Instantiate projectiles
		if (type == "projectile") {
			string projectileType = spawnInfo.GetField ("projectileType").str;
			GameObject obj = null;
			switch (projectileType) {
			case "lobbedBomb":
				obj = (GameObject)Instantiate (lobbedBombProjectilePrefab);
				break;
			case "steal":
				obj = (GameObject)Instantiate (stealProjectilePrefab);
				break;
			}

			if (obj) {
				JSONObject p = spawnInfo.GetField ("p");
				obj.transform.position = new Vector3 (p.GetField ("x").n, p.GetField ("y").n, p.GetField ("z").n);
				JSONObject q = spawnInfo.GetField ("q");
				obj.transform.rotation = new Quaternion (q.GetField ("x").n, q.GetField ("y").n, q.GetField ("z").n, q.GetField ("w").n);
				string id = spawnInfo.GetField ("id").ToString ();
				entities.Add (id, obj);
			}
		}
	}

	/// <summary>
	/// Raises the game update event callback.
	/// </summary>
	/// <param name="evt">The event emit data.</param>
	public void OnGameUpdate (SocketIOEvent evt) {
		lastServerMessage = evt.data;
	}

	/// <summary>
	/// Raises the game ended event callback for when the game ends.
	/// </summary>
	/// <param name="evt">The event emit data.</param>
	public void OnGameEnded (SocketIOEvent evt) {
		status = MotherDucker.GameStatus.ENDED;

		// Destroy any fireables remaining from the display
		if (hasFireable) {
			Destroy (fireable);
		}
		// Destroy all powerups remaining from the display
		foreach (KeyValuePair<string, Dictionary<MotherDucker.PowerupType, GameObject>> client in powerups) {
			foreach (KeyValuePair<MotherDucker.PowerupType, GameObject> pair in client.Value) {
				Destroy (pair.Value);
			}
		}
		// Show game ended stats and text
		GameEndedText (evt.data);
	}

	/// <summary>
	/// Raises the get powerup event callback when a player gets a powerup.
	/// </summary>
	/// <param name="evt">The event emit data.</param>
	public void OnGetPowerup (SocketIOEvent evt) {
		JSONObject powerupInfo = evt.data;
		
		if (powerupInfo.GetField ("motherduckName").str != clientName)
			return;

		// Check which powerup it is
		string powerupName = powerupInfo.GetField ("powerupType").str;
		MotherDucker.PowerupType powerupType;
		switch (powerupName) {
		case "speed":
			powerupType = MotherDucker.PowerupType.SPEED;
			break;
		case "bulletTime":
			powerupType = MotherDucker.PowerupType.BULLETTIME;
			slowMotionFactor *= powerupInfo.GetField ("factor").n;
			Camera.main.GetComponent<FollowCam> ().factor = slowMotionFactor;
			break;
		case "magnet":
			powerupType = MotherDucker.PowerupType.MAGNET;
			break;
		case "invert":
			powerupType = MotherDucker.PowerupType.INVERTOTHERS;
			break;
		default:
			powerupType = MotherDucker.PowerupType.NONE;
			break;
		}

		// Check if we can get the motherduck's powerup inventory
		Dictionary<MotherDucker.PowerupType, GameObject> clientPowerups;
		if (!powerups.TryGetValue (clientName, out clientPowerups))
			return;

		// Checks if powerup is already present
		GameObject powerup;
		if (clientPowerups.TryGetValue (powerupType, out powerup)) {
			clientPowerups.Remove (powerupType);
			Destroy (powerup);
		}
		
		float lifetime = powerupInfo.GetField ("lifetime").n;
		
		if (lifetime == 0)
			return;

		// Instantiate the powerup timer
		switch (powerupType) {
		case MotherDucker.PowerupType.SPEED:
			powerup = (GameObject)Instantiate (speedTimerPrefab);
			break;
		case MotherDucker.PowerupType.BULLETTIME:
			powerup = (GameObject)Instantiate (bulletTimeTimerPrefab);
			break;
		case MotherDucker.PowerupType.MAGNET:
			powerup = (GameObject)Instantiate (magnetTimerPrefab);
			break;
		case MotherDucker.PowerupType.INVERTOTHERS:
			powerup = (GameObject)Instantiate (invertOthersTimerPrefab);
			break;
		}

		// Set the object's parent and numerical settings
		if (powerup) {
			powerup.transform.SetParent (GameObject.Find ("Canvas").transform, false);
			powerup.transform.SetAsFirstSibling ();
			powerup.transform.GetComponent<Image> ().fillAmount = lifetime / 3f;
			clientPowerups.Add (powerupType, powerup);
		}
	}

	/// <summary>
	/// Raises the update powerup timer event callback.
	/// </summary>
	/// <param name="evt">The event emit data.</param>
	public void OnUpdatePTimer (SocketIOEvent evt) {
		JSONObject updateTimerInfo = evt.data;
		// Only show for the client
		if (updateTimerInfo.GetField ("motherduckName").str != clientName)
			return;
		
		// Get the powerup type
		string powerupName = updateTimerInfo.GetField ("powerupType").str;
		MotherDucker.PowerupType powerupType;
		float lifetime = updateTimerInfo.GetField ("lifetime").n;
		switch (powerupName) {
		case "speed":
			powerupType = MotherDucker.PowerupType.SPEED;
			break;
		case "bulletTime":
			powerupType = MotherDucker.PowerupType.BULLETTIME;
			if (lifetime <= 0f) {
				slowMotionFactor /= updateTimerInfo.GetField ("factor").n;
				Camera.main.GetComponent<FollowCam> ().factor = slowMotionFactor;
			}
			break;
		case "magnet":
			powerupType = MotherDucker.PowerupType.MAGNET;
			break;
		case "invert":
			powerupType = MotherDucker.PowerupType.INVERTOTHERS;
			break;
		default:
			powerupType = MotherDucker.PowerupType.NONE;
			break;
		}

		// Checks if we can get the motherduck's powerup inventory
		Dictionary<MotherDucker.PowerupType, GameObject> clientPowerups;
		if (!powerups.TryGetValue (clientName, out clientPowerups))
			return;

		// Checks if powerup is present
		GameObject powerup;
		if (!clientPowerups.TryGetValue (powerupType, out powerup))
			return;

		// Remove if powerup has expired
		if (lifetime <= 0) {
			clientPowerups.Remove (powerupType);
			Destroy (powerup);
		// Update powerup timer
		} else {
			powerup.transform.GetComponent<Image> ().fillAmount = lifetime / 3f;
		}
	}

	/// <summary>
	/// Raises the get fireable event callback for when the client picks up a fireable.
	/// </summary>
	/// <param name="evt">The event emit data.</param>
	public void OnGetFireable (SocketIOEvent evt) {
		JSONObject fireableInfo = evt.data;
		if (fireableInfo.GetField ("motherduckName").str != clientName)
			return;

		// Get the fireable type
		string fireableName = fireableInfo.GetField ("fireableType").str;
		switch (fireableName) {
		case "steal":
			fireableType = MotherDucker.PowerupType.STEAL;
			break;
		case "lobbedBomb":
			fireableType = MotherDucker.PowerupType.LOBBEDBOMB;
			break;
		default:
			fireableType = MotherDucker.PowerupType.NONE;
			break;
		}

		// Set the amount of ammunition
		ammunition = (int)fireableInfo.GetField ("ammunition").n;

		// Destroy current fireable
		if (hasFireable) {
			Destroy (fireable);
		}

		// Instantiate fireable display
		switch (fireableType) {
		case MotherDucker.PowerupType.LOBBEDBOMB:
			fireable = (GameObject)Instantiate (lobbedBombAmmoPrefab);
			break;
		case MotherDucker.PowerupType.STEAL:
			fireable = (GameObject)Instantiate (stealAmmoPrefab);
			break;
		}

		// Set object's parent and numerical settings
		if (fireable) {
			fireable.GetComponent<Button> ().onClick.AddListener (() => {
				SendFire ();
			});
			fireable.transform.SetParent (GameObject.Find ("Canvas").transform, false);		
			fireable.transform.GetComponent<Image> ().fillAmount = ammunition / 10f;

			hasFireable = true;
		}
	}

	/// <summary>
	/// Raises the update ammo event callback for when ammo is used.
	/// </summary>
	/// <param name="evt">The event emit data.</param>
	public void OnUpdateAmmo (SocketIOEvent evt) {
		if (evt.data.GetField ("motherduckName").str != clientName)
			return;

		// Update the ammo count
		ammunition = (int)evt.data.GetField ("ammunition").n;
		if (ammunition == 0) {
			fireableType = MotherDucker.PowerupType.NONE;
			Destroy (fireable);
			hasFireable = false;
		} else {
			fireable.transform.GetComponent<Image> ().fillAmount = ammunition / 10f;
		}
	}

	/// <summary>
	/// Raises the explosion event callback.
	/// </summary>
	/// <param name="evt">The event emit data.</param>
	public void OnExplosion (SocketIOEvent evt) {
		JSONObject p = evt.data.GetField ("p");
		JSONObject q = evt.data.GetField ("q");
		Vector3 explosionPosition = new Vector3 (p.GetField ("x").n, p.GetField ("y").n, p.GetField ("z").n);
		Quaternion explosionRotation = new Quaternion (q.GetField ("x").n, q.GetField ("y").n, q.GetField ("z").n, q.GetField ("w").n);
		Instantiate (explosionPrefab, explosionPosition, explosionRotation);
	}

	/// <summary>
	/// Raises the remove entity event callback.
	/// </summary>
	/// <param name="evt">Evt.</param>
	public void OnRemoveEntity (SocketIOEvent evt) {
		JSONObject eList = evt.data;
		GameObject e = null;
		foreach (string key in eList.keys) {
			e = null;
			string id = eList.GetField (key).ToString ();
			if (!entities.TryGetValue (id, out e))
				return;
			entities.Remove (id);
			Destroy (e);
		}
	}

	/// <summary>
	/// Raises the assign team event callback.
	/// </summary>
	/// <param name="evt">Evt.</param>
	public void OnAssignTeam (SocketIOEvent evt) {
		// Assigns a duckling's colour to the team or default duckling colour
		JSONObject duckling = evt.data;
		GameObject e = null;
		string id = duckling.GetField ("id").ToString ();
		if (!entities.TryGetValue (id, out e))
			return;
		int team = (int)duckling.GetField ("team").n;
		Color32 colour = (team == -1) ? ducklingColour : teamColours [team].teamColour;
		SetColourProp (colour, e);
	}

	void Update () {
		// Game has ended, do not update further
		if (status == MotherDucker.GameStatus.ENDED) {
			slowMotionFactor = 1.0f;
			Time.timeScale = slowMotionFactor;
			AudioManager.PlayBGM ("bossa");
			escapeMenu.SetActive (false);
			menuButtonToggle.SetActive (false);
			endGameMenu.SetActive (true);
		} else {
			if (lastServerMessage != null) {
				int i = 0;
				JSONObject eList = lastServerMessage.GetField ("entities");
				int len = eList.Count;
				string eId = null;
				GameObject e = null;
				// Update all entity position and rotations
				while (i < len) {
					int token = (int)eList [i].n;
					if (token >= 0) { // entity id
						eId = token.ToString ();
						entities.TryGetValue (eId, out e);
						i++;
					} else if (token == -1) { // position
						if (e != null) {
							e.transform.position = new Vector3 (eList [i + 1].n, eList [i + 2].n, eList [i + 3].n);
							Transform playerNameText = e.transform.Find ("PlayerNameText(Clone)");
							if (playerNameText) {
								Vector3 cameraPosition = new Vector3 (Camera.main.transform.position.x, playerNameText.position.y, Camera.main.transform.position.z);
								playerNameText.LookAt (cameraPosition);
							}
						}
						i += 4;
					} else if (token == -2) { // identity quaternion
						if (e != null)
							e.transform.rotation = new Quaternion (0, 0, 0, 1);
						i++;
					} else if (token == -3) { // quaternion
						if (e != null)
							e.transform.rotation = new Quaternion (eList [i + 1].n, eList [i + 2].n, eList [i + 3].n, eList [i + 4].n);
						i += 5;
					} else { // shouldn't happen, but a catch-all just in case
						i = len;
					}
				}

				// Update timer
				gameTimer = lastServerMessage.GetField ("gt").n;
				DisplayTimer ();
				lastServerMessage = null;
			}

			// Update slow motion effects
			Time.timeScale = slowMotionFactor;

			// Play music if slow motion is in effect
			if (slowMotionFactor < 1.0f) {
				AudioManager.PlayBGM ("Epig");
			} else {
				AudioManager.StopBGM ("Epig");
				AudioManager.PlayBGM ("bossa");
			}

			// Handle inputs for turning
			MotherDucker.TurnType turnInput = GetTurnInput ();
			if (turnInput != this.turnType) {
				this.turnType = turnInput;
				SendTurn (turnInput);
			}

			// Handle inputs for HUD interaction
			#if UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_EDITOR
			if (Input.GetKeyDown(KeyCode.Space)) SendFire();
			if (Input.GetKeyDown(KeyCode.Escape)) escapeMenu.SetActive(!escapeMenu.activeInHierarchy);
			#endif

			//Hand inputs for diving
			if (GetDiveInput ())
				SendDive ();
		}
	}

	/// <summary>
	/// Gets the turn input.
	/// </summary>
	/// <returns>The turn input.</returns>
	MotherDucker.TurnType GetTurnInput () {
		#if UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_EDITOR
		int tmp = 0x00;
		if (Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow)) return MotherDucker.TurnType.STOP;
		if (Input.GetKey(KeyCode.LeftArrow)) tmp = tmp | 0x10;
		if (Input.GetKey(KeyCode.RightArrow)) tmp = tmp | 0x01;
		if (tmp == 0x10) return MotherDucker.TurnType.LEFT;
		if (tmp == 0x01) return MotherDucker.TurnType.RIGHT;
		return MotherDucker.TurnType.STOP;
		#else
		int nTouches = Input.touchCount;
		if (nTouches == 0)
			return MotherDucker.TurnType.STOP;
		int tmp = 0x00;
		int mask;

		// Check where tap was made
		for (int i = 0; i < nTouches; i++) {
			Touch touch = Input.GetTouch (i);
			if (touch.phase == TouchPhase.Canceled || touch.phase == TouchPhase.Ended)
				return MotherDucker.TurnType.STOP;
			if (touch.position.x < screenThirdWidth)
				mask = 0x10;
			else if (touch.position.x > screenThirdWidth * 2)
				mask = 0x01;
			else
				continue;
			tmp = tmp | mask;
		}

		if (tmp == 0x01)
			return MotherDucker.TurnType.RIGHT;
		if (tmp == 0x10)
			return MotherDucker.TurnType.LEFT;
		return MotherDucker.TurnType.STOP;
		#endif
	}

	/// <summary>
	/// Gets the dive input.
	/// </summary>
	/// <returns><c>true</c>, if dive input was gotten, <c>false</c> otherwise.</returns>
	bool GetDiveInput () {
		#if UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_EDITOR
		return Input.GetKeyDown(KeyCode.DownArrow);
		#else
		int nTouches = Input.touchCount;
		if (nTouches == 0) {
			swipeStart = baseTouch;
			return false;
		}

		// Check if touch input was a swipe down
		for (int i = 0; i < nTouches; i++) {
			Touch touch = Input.GetTouch (i);
			if (touch.phase == TouchPhase.Canceled || touch.phase == TouchPhase.Ended)
				continue;
			
			if (touch.position.x >= screenThirdWidth && touch.position.x <= screenThirdWidth * 2) {
				if (touch.phase == TouchPhase.Began && touch.position.y > Screen.height * 0.2) {
					if (swipeStart == baseTouch)
						swipeStart = touch.position;
					return false;
				}
				if (swipeStart == baseTouch)
					continue;
				
				float swipeDist = touch.position.y - swipeStart.y;
				float swipeValue = Mathf.Sign (swipeDist);
				swipeDist = Mathf.Abs (swipeDist);
				if (swipeDist >= 20 && swipeValue < 0) {
					swipeStart = baseTouch;
					return true;
				}
				return false;
			}
		}
		swipeStart = baseTouch;
		return false;
		#endif
	}

	/// <summary>
	/// Sends the turn input to the server.
	/// </summary>
	/// <param name="type">Type of turn.</param>
	void SendTurn (MotherDucker.TurnType type) {
		if (type == MotherDucker.TurnType.STOP) {
			socket.Emit ("game:turn-stop");
		} else if (type == MotherDucker.TurnType.LEFT) {
			socket.Emit ("game:turn-left");
		} else {
			socket.Emit ("game:turn-right");
		}
	}

	/// <summary>
	/// Sends the fire message to the server.
	/// </summary>
	void SendFire () {
		socket.Emit ("game:fire");
	}

	/// <summary>
	/// Sends the dive message to the server.
	/// </summary>
	void SendDive () {
		socket.Emit ("game:dive");
	}

	/// <summary>
	/// Displays the timer on the UI.
	/// </summary>
	void DisplayTimer () {
		int minutes = Mathf.FloorToInt (gameTimer / 60f);
		int seconds = Mathf.FloorToInt (gameTimer - minutes * 60);
		
		if (seconds == 11 && minutes == 0) {
			anim.SetTrigger ("TenSeconds");
		}
		
		// Format the time
		timerText.text = string.Format ("{0:0}:{1:00}", minutes, seconds);
	}

	/// <summary>
	/// Shows the game ended text
	/// </summary>
	/// <param name="stats">The stats information.</param>
	void GameEndedText (JSONObject stats) {
		timerText.text = "Time's up!";

		JSONObject winner = stats.GetField ("winner");
		winnerText.text = "";

		// Show who is the winner
		if (winner.Count == 1) {
			winnerText.text += teamColours [(int)winner [0].n].colourName + " Team Wins!";
		} else {
			for (int i = 0; i < winner.Count-1; i++) {
				winnerText.text += teamColours [(int)winner [i].n].colourName + " and ";
			}
			winnerText.text += teamColours [(int)winner [winner.Count - 1].n].colourName + " Team Win!";
		}
		winnerText.gameObject.SetActive (true);

		// Get the statistics
		JSONObject statistics = stats.GetField ("statistics");

		Transform rowHeaders = statisticsText.transform.Find ("RowHeaders");
		Transform rowStats = statisticsText.transform.Find ("RowStats");
		Transform columnHeaders = rowStats.Find ("ColumnHeaders");
		JSONObject mvp = stats.GetField ("mvp");

		// Create the statistics table
		for (int team = 0; team < statistics.Count; team++) {
			// Iterate through each row
			foreach (string name in statistics[team].keys) {
				JSONObject statistic = statistics [team].GetField (name);
				// Create a row of column statistics
				GameObject columnStats = Instantiate (columnStatsPrefab);
				columnStats.transform.SetParent (rowStats, false);

				// Iterate through each statistic
				foreach (string key in statistic.keys) {
					// Create a statistic
					string stat = statistic.GetField (key).ToString ();
					Text statText = Instantiate (statPrefab);
					if (name == team.ToString ()) {
						statText.fontStyle = FontStyle.Bold;
						statText.fontSize = 65;
					}
					statText.text = stat;
					statText.color = teamColours [team].teamColour;
					statText.transform.SetParent (columnStats.transform, false);

					// Make sure rowHeader is unique, if not, don't instantiate again
					if (columnHeaders.Find (key) == null) {
						Text statHeaderText = Instantiate (columnHeaderPrefab);
						statHeaderText.name = key;
						if (key == "finalScore") {
							statHeaderText.text = "Final Score";
						} else if (key == "ducklingCount") {
							statHeaderText.text = "Ducklings Collected";
						} else if (key == "highestCount") {
							statHeaderText.text = "Highest Score";
						} else if (key == "stolenCount") {
							statHeaderText.text = "Stolen Ducklings";
						} else if (key == "detachedCount") {
							statHeaderText.text = "Detached Ducklings";
						}
						statHeaderText.transform.SetParent (columnHeaders, false);
					}
				}

				// Create a row header
				Text clientText = Instantiate (rowHeaderPrefab);
				if (name == team.ToString ()) {
					clientText.text = teamColours [team].colourName + " Team";
					clientText.fontStyle = FontStyle.BoldAndItalic;
					clientText.fontSize = 65;
				} else {
					clientText.text = name;
					if (mvp.HasField (name)) {
						clientText.text += "*";
					}

				}
				clientText.color = teamColours [team].teamColour;
				clientText.transform.SetParent (rowHeaders, false);

				// Apply scrolling to the stats
				ScrollRect sr = rowStats.gameObject.AddComponent<ScrollRect> ();
				sr.content = columnStats.GetComponent<RectTransform> ();
				sr.horizontal = true;
				sr.vertical = false;
				sr.movementType = ScrollRect.MovementType.Clamped;
				sr.inertia = false;
				sr.scrollSensitivity = 1;
				sr.horizontalScrollbar = statisticsText.transform.Find ("Scrollbar Horizontal").GetComponent<Scrollbar> ();
			}
		}

		statisticsText.SetActive (true);
	}

	/// <summary>
	/// Quits the game.
	/// </summary>
	public void QuitGame () {
		slowMotionFactor = 1.0f;
		socket.Emit ("game:leave");
		LoadGame.OpenScene (LoadGame.MENUSCENE);
	}

	/// <summary>
	/// Sets the colour property.
	/// </summary>
	/// <param name="colour">Colour to set.</param>
	/// <param name="e">The game object to colour.</param>
	void SetColourProp (Color32 colour, GameObject e) {
		GameObject dBody = e.transform.Find ("Body").gameObject;
		Renderer renderer = dBody.GetComponent<Renderer> ();
		props.Clear ();
		props.AddColor ("_Color", colour);
		renderer.SetPropertyBlock (props);
	}

	/// <summary>
	/// Sets the colour property.
	/// </summary>
	/// <param name="teamColour">The team colour to set.</param>
	/// <param name="textColour">The text colour to set.</param>
	/// <param name="e">The game object to colour.</para>.</param>
	void SetColourProp (Color32 teamColour, Color32 textColour, GameObject e) {
		Renderer textRenderer = e.GetComponent<Renderer> ();
		props.Clear ();
		props.AddColor ("_Color", textColour);
		textRenderer.SetPropertyBlock (props);
		GameObject nameTagMesh = e.transform.Find ("NameTagMesh").gameObject;
		Renderer meshRenderer = nameTagMesh.GetComponent<Renderer> ();
		props.Clear ();
		props.AddColor ("_EmissionColor", teamColour);
		meshRenderer.SetPropertyBlock (props);
	}
}
