using System;
using UnityEngine;

namespace UnityStandardAssets.Effects {
	/// <summary>
	/// A simple script to scale the size, speed and lifetime of a particle system
	/// </summary>
	public class ParticleSystemMultiplier : MonoBehaviour {
		/// <summary>
		/// The particle systems.
		/// </summary>
		private ParticleSystem[] systems;

		/// <summary>
		/// Boolean check to see if game object is alive.
		/// </summary>
		private bool alive;

		/// <summary>
		/// The multiplier.
		/// </summary>
		public float multiplier = 1;

		private void Start () {
			// Set initial particle system variables
			alive = true;
			systems = GetComponentsInChildren<ParticleSystem> ();
			foreach (ParticleSystem system in systems) {
				system.startSize *= multiplier;
				system.startSpeed *= multiplier;
				system.startLifetime *= Mathf.Lerp (multiplier, 1, 0.5f);
				system.Clear ();
				system.Play ();
			}
		}

		private void Update () {
			foreach (ParticleSystem system in systems) {
				if (system.IsAlive (true)) {
					alive = true;
				} else {
					alive = false;
				}
			}
			if (!alive) {
				Destroy (gameObject);
			}
		}
	}
}
