﻿using UnityEngine;
using System.Collections;
using SocketIO;

/// <summary>
/// Notification manager, stores queues of notifications and delivers them
/// to the appropriate notification objects when they become available.
/// </summary>
public class NotificationManager : MonoBehaviour {

	// Callback format for menu notifications.
	public delegate void ResponseCallback (SocketIOEvent evt, bool response);

	// The pair of an event and a response callback, used in menu notifications.
	/// <summary>
	/// Used in place of a tuple, defines a structure with a socket.IO event
	/// and a callback to a function to call with the response to the event.
	/// </summary>
	public class MenuNotificationItem {
		public SocketIOEvent evt;
		public ResponseCallback callback;
	};

	// The index for the game scene.
	/// <summary>
	/// The GAMESCENE index.
	/// </summary>
	public const int GAMESCENE = 1;

	// Whether to display streaming notifications in the game scene (true) or not.
	/// <summary>
	/// Whether to display streaming notifications in the game scene (<c>true</c>)
	/// or not.
	/// </summary>
	private static bool gameStreamingNotifications = true;

	// Whether to display errors as streaming notifications (true) or not.
	/// <summary>
	/// Whether to display errors as streaming notifications (<c>true</c>) or not.
	/// </summary>
	private static bool errorsStreamingNotifications = false;

	/// <summary>
	/// The name of the singleton if a new object needs to be created.
	/// </summary>
	private const string singletonName = "notificationManagerSingleton";

	/// <summary>
	/// The instance of the singleton.
	/// </summary>
	private static NotificationManager instance = null;

	// Used to acquire a lock for getting the instance.
	/// <summary>
	/// Lock used for acquiring singleton.
	/// </summary>
	private static object _lock = new object ();

	// Used to acquire a lock for updating the menu notification ready state.
	/// <summary>
	/// Lock used for acquiring menu notification ready state.
	/// </summary>
	private static object _readyLock = new object ();
	
	// Used to acquire a lock for updating the error notification ready state.
	/// <summary>
	/// Lock used for acquiring error notification ready state.
	/// </summary>
	private static object _errorReadyLock = new object ();

	// Used to acquire a lock for updating the streaming notification ready state.
	/// <summary>
	/// Lock used for acquiring streaming notification ready state.
	/// </summary>
	private static object _streamingReadyLock = new object ();

	/// <summary>
	/// The account manager.
	/// </summary>
	protected AccountManager accountManager = null;

	// Used only to receive error notifications
	/// <summary>
	/// The Socket.IO socket currently connected to.
	/// </summary>
	protected SocketIOComponent socket = null;

	// The notification menu to pass notifications to, notifications are
	// stored while the menuNotification is not set.
	/// <summary>
	/// The menu notification object currently registered for the 
	/// notification manager, if there is no menu notification object registered,
	/// notifications for that destination will be stored until one is registered.
	/// </summary>
	protected MenuNotification menuNotification = null;

	// Whether notifications can be pushed to the menu or whether it is busy
	// with an older notification.
	/// <summary>
	/// Whether the menu notification object is ready to receive a notification.
	/// </summary>
	private bool menuNotificationReady = false;

	// The error notification menu to pass notifications to, though it is expected
	// that this will be present in every scene, will hold onto error notifications
	// until a new error notification menu registers itself.
	/// <summary>
	/// The error notification object currently registered for the 
	/// notification manager, if there is no error notification object registered,
	/// notifications for that destination will be stored until one is registered.
	/// </summary>
	protected ErrorNotification errorNotification = null;

	// Whether notifications can be pushed to the error menu or whether it is
	// busy with an older notification.
	/// <summary>
	/// Whether the error notification object is ready to receive a notification.
	/// </summary>
	private bool errorNotificationReady = false;

	// The streaming notifications menu to pass notifications to, though it is expected
	// that this will be present in every scene, will hold onto notifications until a
	// new streaming notification menu registers itself.
	/// <summary>
	/// The streaming notification object currently registered to the 
	/// notification manager, if there is no streaming notification object registered,
	/// notifications for that destination will be stored until one is registered.
	/// </summary>
	protected StreamingNotification streamingNotification = null;

	// Whether notifications can be pushed to the streaming notification menu or whether
	// it is busy with an older notification.
	/// <summary>
	/// Whether the streaming notification object is ready to receive a notification.
	/// </summary>
	private bool streamingNotificationReady = false;

	// Streaming notifications waiting to be delivered.
	/// <summary>
	/// The streaming notifications queue.
	/// </summary>
	protected Queue streamingQueue;

	// Error notifications waiting to be delivered.
	/// <summary>
	/// The error notifications queue.
	/// </summary>
	protected Queue errorQueue;

	// Request notifications to deliver
	/// <summary>
	/// The menu notifications queue.
	/// </summary>
	protected Queue waitingNotifications;

	// Use this for initialization
	void Awake () {
		GetInstance ();
	}

	// Used for when the object is destroyed.
	void OnDestroy () {
		Debug.Log ("Notification Manager Singleton Destroyed.");
		if (this == instance) {
			if (! SocketIOManager.instanceDestroyed) {
				socket.Off ("err", instance.ReceiveNotification);
				SocketIOManager.GetInstance ().DeregisterCloseCallback (instance.GetNewSocket);
			}
		}
	}

	/// <summary>
	/// Gets the instance of the singleton if one exists, if one does not exist, create one
	/// and then get it.
	/// </summary>
	/// <returns>The instance.</returns>
	public static NotificationManager GetInstance () {
		// Checks instance before locking to decrease system calls where possible.
		if (instance == null) {
			// If notification manager object not already present, begin mutual exclusion.
			lock (_lock) {
				// Check instance was not created between the existence check and the start of the mutual exclusion.
				if (instance == null) {
					// Check whether a Notification Manager has been added to the scene 
					// but not registered in the NotificationManager class.
					instance = Object.FindObjectOfType (typeof(NotificationManager)) as NotificationManager;
					if (instance == null) {
						// No existence of Notification Manager object, create object and register all callbacks.
						GameObject notificationManagerSingleton = new GameObject (singletonName);
						DontDestroyOnLoad (notificationManagerSingleton);
						instance = notificationManagerSingleton.AddComponent<NotificationManager> ();
					} else {
						DontDestroyOnLoad (instance.gameObject);
					}
					instance.streamingQueue = new Queue ();
					instance.errorQueue = new Queue ();
					instance.waitingNotifications = new Queue ();
					instance.accountManager = AccountManager.GetInstance ();
					SocketIOManager.GetInstance ().RegisterCloseCallback (instance.GetNewSocket);
					instance.GetNewSocket ();
				}
			}
		}
		return instance;
	}

	// Get the newest socket and register callbacks.
	/// <summary>
	/// Gets the new socket from the Socket.IO Manager and registers
	/// the callbacks for it.
	/// </summary>
	public void GetNewSocket () {
		instance.socket = SocketIOManager.GetInstance ().GetSocket ();
		instance.socket.On ("err", instance.ReceiveNotification);
	}

	// Receive a notification to pass onto a relevant outlet.
	/// <summary>
	/// Receives an informational notification.
	/// </summary>
	/// <param name="evt">informational message.</param>
	public void ReceiveNotification (string evt) {
		streamingQueue.Enqueue (evt);
		DeliverNotification ();
	}

	// Receive a notification to pass onto a relevant outlet.
	/// <summary>
	/// Receives an error notification.
	/// </summary>
	/// <param name="evt">Error Event.</param>
	public void ReceiveNotification (SocketIOEvent evt) {
		if (! errorsStreamingNotifications) {
			errorQueue.Enqueue (evt);
		} else {
			if (evt.data.HasField ("message")) {
				streamingQueue.Enqueue(evt.data.GetField ("message").ToString ());
			} else {
				Debug.Log ("Error message missing message (tried to send as streaming notification).");
			}
		}
		DeliverNotification ();
		
	}

	// Receive a notification to pass onto a relevant outlet.
	/// <summary>
	/// Receives a response notification.
	/// </summary>
	/// <param name="evt">The event that triggered the request for the response.</param>
	/// <param name="callback">The callback that will be called with the response.</param>
	public void ReceiveNotification (SocketIOEvent evt, ResponseCallback callback){
		MenuNotificationItem notificationPair = new MenuNotificationItem();
		notificationPair.evt = evt;
		notificationPair.callback = callback;
		waitingNotifications.Enqueue (notificationPair);
		DeliverNotification ();
	}

	// Deliver a notification to the user.
	/// <summary>
	/// Delivers the next notifications that are waiting in the queue.
	/// </summary>
	private void DeliverNotification () {
		lock (_streamingReadyLock) {
			// Check ready
			// Assume streaming notification object is busy until it finishes its notification.
			if (streamingQueue.Count != 0) {
				if (streamingNotification != null && streamingNotificationReady && 
				    	(gameStreamingNotifications || Application.loadedLevel != GAMESCENE)) {
					streamingNotification.DoNotification (streamingQueue.Dequeue () as string);
					streamingNotificationReady = false;
				}
			}
		}

		lock (_errorReadyLock) {
			// Check ready
			// Assume an error notification object is busy until it dismisses its item.
			if (errorQueue.Count != 0) {
				if (errorNotification != null && errorNotificationReady) {
					errorNotification.DoNotification (errorQueue.Dequeue () as SocketIOEvent);
					errorNotificationReady = false;
				}
			}
		}

		lock (_readyLock) {
			// Check ready
			// Assume a menu notification object is busy until it responds.
			if (waitingNotifications.Count != 0) {
				if (menuNotification != null && menuNotificationReady) {
					menuNotification.DoNotification (waitingNotifications.Dequeue () as MenuNotificationItem);
					menuNotificationReady = false;
				}
			}
		}
	}

	// Current message from streaming notification finished displaying for alloted time.
	/// <summary>
	/// Notifies the manager that the informational notification has finished being displayed.
	/// </summary>
	/// <param name="notification">The notification that has finished being displayed.</param>
	public void FinishedNotification (string notification) {
		lock (_streamingReadyLock) {
			streamingNotificationReady = true;
		}
		DeliverNotification ();
	}

	// Current message from error stream dismissed, send next one if it exists.
	// Returns event in case in future we want to do any handling of specific errors.
	/// <summary>
	/// Notify the NotificationManager that the specified error notification has been dismissed,
	/// therefore the next error notification can be sent if there is one.
	/// </summary>
	/// <param name="evt">The event containing the error message.</param>
	public void Dismiss (SocketIOEvent evt) {
		lock (_errorReadyLock) {
			errorNotificationReady = true;
		}
		DeliverNotification ();
	}

	// Pass a response to the callback given for the processed event.
	/// <summary>
	/// Respond to the specified notification event with the given response.
	/// Calls the callback with the response.
	/// </summary>
	/// <param name="notification">The menu notification pair.</param>
	/// <param name="response">If set to <c>true</c> accept.</param>
	public void Respond (MenuNotificationItem notification, bool response) {
		lock (_readyLock) {
			menuNotificationReady = true;
		}
		notification.callback (notification.evt, response);
		DeliverNotification ();
	}

	// Set the new menu notification outlet.
	/// <summary>
	/// Registers the menu notification.
	/// </summary>
	/// <param name="notification">New menu notification object.</param>
	public void RegisterMenuNotification (MenuNotification notification) {
		if (menuNotification == null) {
			menuNotification = notification;
			// Assume a menu notification object is not busy when it begins.
			menuNotificationReady = true;
			// Deliver notifications in case notifications were waiting.
			DeliverNotification ();
		} else {
			// NOTE: If this happens, the new menu notification object may lose its ability to receive notifications.
			Debug.Log ("Tried to add new menu notification where one was already registered to the manager.");
		}
	}

	// Pause delivery of menu notifications, wait for a new menu notification object to register.
	/// <summary>
	/// Deregisters the menu notification.
	/// </summary>
	/// <param name="notification">Menu notification object to deregister.</param>
	public void DeregisterMenuNotification (MenuNotification notification) {
		if (notification == menuNotification) {
			menuNotification = null;
		} else {
			Debug.Log ("Tried to deregister menu notification that was not the current menu notification.");
		}
	}

	// Set the new error notification outlet.
	/// <summary>
	/// Registers the error notification.
	/// </summary>
	/// <param name="notification">New error notification object.</param>
	public void RegisterErrorNotification (ErrorNotification notification) {
		if (errorNotification == null) {
			errorNotification = notification;
			// Assume an error notification object is not busy when it begins.
			errorNotificationReady = true;
			// Deliver notifications in case notifications were waiting.
			DeliverNotification ();
		} else {
			// NOTE: If this happens, the new menu notification object may lose its ability to receive notifications.
			Debug.Log ("Tried to add new error notification object where one was already registered to the manager.");
		}
	}
	
	// Pause delivery of error notifications, wait for a new error notification object to register.
	/// <summary>
	/// Deregisters the error notification.
	/// </summary>
	/// <param name="notification">Error notification object to deregister.</param>
	public void DeregisterErrorNotification (ErrorNotification notification) {
		if (notification == errorNotification) {
			errorNotification = null;
		} else {
			Debug.Log ("Tried to deregister menu notification that was not the current error notification.");
		}
	}

	// Set the new streaming notification outlet.
	/// <summary>
	/// Registers the streaming notification object.
	/// </summary>
	/// <param name="notification">New treaming notification object.</param>
	public void RegisterStreamingNotification (StreamingNotification notification) {
		if (streamingNotification == null) {
			streamingNotification = notification;
			// Assume a streaming notification object is not busy when it begins.
			streamingNotificationReady = true;
			// Deliver notifications in case notifications were waiting.
			DeliverNotification ();
		} else {
			// NOTE: If this happens, the new menu notification object may lose its ability to receive notifications.
			Debug.Log ("Tried to add new streaming notification object where one was already registered to the manager.");
		}
	}
	
	// Pause delivery of streaming notifications, wait for a new streaming notification object to register.
	/// <summary>
	/// Deregisters the streaming notification object.
	/// </summary>
	/// <param name="notification">Streaming notification object to deregister.</param>
	public void DeregisterStreamingNotification (StreamingNotification notification) {
		if (notification == streamingNotification) {
			streamingNotification = null;
		} else {
			Debug.Log ("Tried to deregister menu notification that was not the current error notification.");
		}
	}

	// Set whether to deliver streaming notifications during the game scene or not.
	/// <summary>
	/// Updates the game streaming notifications option.
	/// </summary>
	/// <param name="gameStreamingNotifications">If set to <c>true</c> streaming notifications show up in the game scene.</param>
	public static void UpdateGameStreamingNotifications (bool gameStreamingNotifications) {
		NotificationManager.gameStreamingNotifications = gameStreamingNotifications;
	}

	// Set whether to deliver errors as streaming notifications.
	/// <summary>
	/// Updates the errors streaming notifications options.
	/// </summary>
	/// <param name="errorsStreamingNotifications">If set to <c>true</c> errors are delivered as streaming notifications.</param>
	public static void UpdateErrorsStreamingNotifications (bool errorsStreamingNotifications) {
		NotificationManager.errorsStreamingNotifications = errorsStreamingNotifications;
	}

}
