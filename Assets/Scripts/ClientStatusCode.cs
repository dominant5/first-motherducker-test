﻿/// <summary>
/// Client status codes. Used to keep track of the the client's status.
/// </summary>
public enum ClientStatusCode {
	CREATED,
	LOBBY,
	WAITING,
	GAME_STARTED
}
