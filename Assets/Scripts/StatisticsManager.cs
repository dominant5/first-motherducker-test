﻿using UnityEngine;
using UnityEngine.UI;
using SocketIO;
using System.Collections;

/// <summary>
/// Singleton that manages statistics.
/// </summary>
public class StatisticsManager : MonoBehaviour {

	/// <summary>
	/// The username text field of the statistics panel.
	/// </summary>
	public Text usernameText;

	/// <summary>
	/// The highscore text.
	/// </summary>
	public Text highscoreText;

	/// <summary>
	/// The wins text.
	/// </summary>
	public Text winsText;

	/// <summary>
	/// The losses text.
	/// </summary>
	public Text lossesText;

	/// <summary>
	/// The most ducklings text.
	/// </summary>
	public Text mostDucklingsText;

	/// <summary>
	/// The total ducklings text.
	/// </summary>
	public Text totalDucklingsText;

	/// <summary>
	/// The total ducklings detached text.
	/// </summary>
	public Text totalDucklingsDetachedText;

	/// <summary>
	/// The total ducklings stolen text.
	/// </summary>
	public Text totalDucklingsStolenText;

	/// <summary>
	/// Updates the statistics shown in the statistics menu.
	/// </summary>
	/// <param name="evt">The event containing the response from the server with
	/// 	all the statistics in it.</param>
	public void UpdateStatistics (SocketIOEvent evt) {
		string username;
		Debug.Log (evt);
		if (evt.data.HasField ("username")) {
			username = evt.data.GetField ("username").str;
		} else {
			username = "unidentified user";
		}
		if (usernameText && evt.data.HasField ("username")) {
			usernameText.text = evt.data.GetField ("username").str;
		}
		if (highscoreText && evt.data.HasField ("highscore")) {
			highscoreText.text = evt.data.GetField ("highscore").ToString ();
		}
		if (winsText && evt.data.HasField ("wins")) {
			winsText.text = evt.data.GetField ("wins").ToString ();
		}
		if (lossesText && evt.data.HasField ("losses")) {
			lossesText.text = evt.data.GetField ("losses").ToString ();
		}
		if (mostDucklingsText && evt.data.HasField ("mostCollectedDucklings")) {
			mostDucklingsText.text = evt.data.GetField ("mostCollectedDucklings").ToString ();
		}
		if (totalDucklingsText && evt.data.HasField ("totalCollectedDucklings")) {
			totalDucklingsText.text = evt.data.GetField ("totalCollectedDucklings").ToString ();
		}
		if (totalDucklingsDetachedText && evt.data.HasField ("totalDetachedDucklings")) {
			totalDucklingsDetachedText.text = evt.data.GetField ("totalDetachedDucklings").ToString ();
		}
		if (totalDucklingsStolenText && evt.data.HasField ("totalStolenDucklings")) {
			totalDucklingsStolenText.text = evt.data.GetField ("totalStolenDucklings").ToString ();
		}

		NotificationManager.GetInstance ().ReceiveNotification ("Received updated statistics for " + username);
	}

	/// <summary>
	/// Requests a statistics update for the current user.
	/// </summary>
	/// <param name="anim">The animator to use to open the statistics panel.</param>
	public void RequestStatisticsUpdate (Animator anim) {
		if (SessionManager.GetInstance ().IsLoggedInFull ()) {
			gameObject.GetComponent<OpenCloseWindow> ().OpenPanel (anim);
			JSONObject data = new JSONObject (JSONObject.Type.OBJECT);
			data.AddField ("username", SessionManager.GetInstance ().GetUsername ());
			Debug.Log (data);
			SocketIOManager.GetInstance ().GetSocket ().Emit ("stats:get", data);
			NotificationManager.GetInstance ().ReceiveNotification ("Requesting statistics for " + SessionManager.GetInstance ().GetUsername ());
		} else {
			NotificationManager.GetInstance ().ReceiveNotification ("Failed to find username, make sure you're logged in and try again!");
		}
	}

	// Used for initialisation.
	void Awake () {
		SocketIOManager.GetInstance ().GetSocket ().On ("stats:deliver", UpdateStatistics);
	}

	// used when the object is destroyed.
	void OnDestroy () {
		if (! SocketIOManager.instanceDestroyed)
			SocketIOManager.GetInstance ().GetSocket ().Off ("stats:deliver", UpdateStatistics);
	}
}
