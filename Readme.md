Motherducker
===
The latest and greatest from Dominant 5, the gurus of glorious gaming.

- [Getting started](#markdown-header-getting-started)
- [Dependencies not installed via npm](#markdown-header-dependencies-not-installed-via-npm)
- [Dev dependencies](#markdown-header-dev-dependencies)
- [Installing on an Android device](#markdown-header-installing-on-an-android-device)
- [Unit Tests](#markdown-header-unit-tests)
- [How to Play](#markdown-header-how-to-play)

Getting started
---
1. Make sure you're connected to the internet.
2. Ensure the [dependencies not installed via npm](#markdown-header-dependencies-not-installed-via-npm) are installed.
3. Clone the repo.
4. `cd` to the Server directory.
5. `npm install` to install the npm dependencies.
6. Start the mongo daemon. On PC, run `net start MongoDB` from a command prompt running as administrator. On Mac, run `sudo mongod`.
7. From the Server directory, run `node app` to start the server.
8. Start the app (either from the Unity editor or on Android).
9. In Options (in game), enter the IP address of the computer running the server (the device must be on the same network as the server), and press connect.
10. Get all your friends to do the same.
11. Get duckin'

Dependencies not installed via npm
---
- [Node.JS](https://nodejs.org)
- [MongoDB](https://www.mongodb.org/downloads#production)
    + [Installing MongoDB as a service on PC](http://docs.mongodb.org/manual/tutorial/install-mongodb-on-windows/#configure-a-windows-service-for-mongodb)
- [Python 2.5.0 to 3.0.0](https://www.python.org/downloads/) (must be in your PATH)

Dev dependencies
---
- Unity
- Android SDK in your PATH (used for building to Android devices from Unity)
- Blender in your PATH (used for map compilation)

Installing on an Android device
---
If you want to install a pre-build binary:

1. Download the pre-built binary [here](https://bitbucket.org/dominant5/first-motherducker-test/downloads).
2. Put it on your Android device.
3. Install it from the device. You may need to disable security checks.

If you want to install a custom build from Unity:

1. Open the project in Unity.
2. Open File -> Build Settings.
3. Click on Platform -> Android.
4. Click Build.

Unit tests
---
Mocha is used for unit tests. The test files are found in the Server/test directory.

In order to run unit tests:

1. Install mocha.
2. `cd` to the Server directory.
3. `mocha` to run all unit tests.

If your computer is slow, you may see some tests fail, however all tests should
ideally succeed. If any tests fail something could be wrong with your setup but
if more than three tests fail, something is likely wrong with your setup.

How to play
---
- Touch the left or right side of the screen to move left or right (left and right arrow keys on PC)
- Tap the button on the bottom (if it appears) to fire that projectile (spacebar on PC)
- Swipe down on the screen to dive underwater (down arrow key on PC)
- Collect as many ducklings as you can, and use the powerups to your advantage!
- Team with the highest score wins the game
- Powerups:
    + Speed: Speed up your ducks
    + Bullet Time: Slow down everyone else
    + Steal: Steal or take other ducklings
    + Lobbed Bomb: Detach and blow away ducklings in proximity
    + Magnet: Attract any ducklings to you
    + Invert Others: Invert the controls of everyone else
- Watch out for the hazard tile in the middle!