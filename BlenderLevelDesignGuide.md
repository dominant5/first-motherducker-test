Design guide for maps
=====================

Contents
--------
[Creating a new map](#creating)

[Generating the mdmap (Motherducker map)](#generating)

[Required elements in every map](#requiredElements)

[Specifying object types](#types)

[Special types](#specialTypes)

[UV and materials](#UVmat)

[Adding a spawn point in Blender](#spawnPoint)

[Adding a spawn area in Blender](#spawnArea)

[Ignored types](#ignoredTypes)

<a name="creating">Creating a new map</a>
------------------
Put .blend files in **Server/server/maps**. When executing the command `npm run convertmaps`, a .mdmap file is created for each .blend file in that folder.

<a name="generating">Generating the mdmap (Motherducker map)</a>
---------------------------------------
1. `cd` to the Server directory.
2. `npm run convertmaps`
3. A .mdmap file should have been created in the **Server/server/maps** folder for each .blend file.

<a name="requiredElements">Required elements in every map</a>
----------------------------------
- Spawn points for motherducks
- Spawn areas for ducklings
- Spawn areas for powerups
- Bounding walls
- [Water](#specialTypes)

<a name="types">Specifying object types</a>
-----------------------
Simply name the object with the desired types, separated by period characters, eg. **spawnArea.ducklings'**, or **'hazard'**.

<a name="specialTypes">Special types</a>
--------------------------------
- **v** : designates the object as visible, and is sent to clients to render in the game:started event. If this type is not present, the object is not sent, and thus not visible in the client.
- **materialMATERIALNAME** : designates the object's material, to be interpreted by the client. Only parsed if the object is visible as well. See [UV and materials](#UVmat)
- **water** : designates the object (should be a Cube) as a body of water. In the client, this object will be rendered as water (doesn't cast shadows).
- **ghost** : designates the object as not interacting in collisions (even if the object also has water type). This can be used for objects that are purely for visual purpose, eg an irregularly shaped body of water that is functionally composed of several water boxes, but should render as one.
- **hazard** : In the client, this object will be rendered with a red lava material.
- **hexagons** : In the client, this object will be rendered with a transparent hexagon grid visible on both sides of all faces.
- **proxy.TYPE** : designates the object (should be an Empty) as a proxy for a model (to be instantiated in the client). It is not added to the simulation world. Its position and rotation only are captured in the map.
- **spawn.TYPE** : designates the object (should be an Empty) as a spawn point for entities of **TYPE**. See [Adding a spawn point in Blender](#spawnPoint)
- **spawnArea.TYPE** : designates the object (must be a Plane) as a spawn area for entities of **TYPE**. See [Adding a spawn area in Blender](#spawnArea)

<a name="UVmat">UV and materials</a>
-------------------------------
1. UV unwrap the object in Blender.
2. Add the **v** type to the object.
3. Add the **material** type to the object, with the name of the material immediately after, eg. materialdirt, materialwater.

<a name="spawnPoint">Adding a spawn point in Blender</a>
-------------------------------
1. In Blender, create a **Cone Empty** (this represents the orientation the best).
2. Position and rotate it as desired. Scale of the empty doesn't matter.
3. Name it **spawn.TYPE**, where **TYPE** is the type of entity that should spawn there. Eg. **spawn.motherducks**, **spawn.specialPowerups**, etc.
4. There can be any number of these (**spawn.motherducks.001**, etc is fine).

<a name="spawnArea">Adding a spawn area in Blender</a>
------------------------------
1. In Blender, create a **Plane**.
2. Position, scale and rotate it as desired. The entities will spawn randomly anywhere in the plane.
3. Name it **spawnArea.TYPE**, where **TYPE** is the type of entity that should spawn in that area. Eg. **spawnArea.ducklings**, **spawnArea.powerups**, etc.
4. There can be any number of these (**spawnArea.ducklings.001**, etc is fine).

<a name="ignoredTypes">Ignored types</a>
-------------
The following types are ignored by the output script (process.py):

Meshes:

- Plane
- Cube
- Circle
- Sphere
- Icosphere
- Cylinder
- Cone
- Torus
- Grid
- Suzanne

Curves:

- BezierCurve
- BezierCircle
- NurbsCurve
- NurbsCircle
- NurbsPath

Surfaces:

- SurfCurve
- SurfCircle
- SurfPatch
- Surface
- SurfSphere
- SurfTorus

Other:

- MBall
- Text
- Armature
- Lattice
- Empty
- Any three digits, such as those automatically appended by Blender for duplicate object names.

**EXAMPLE:** the only type exported for an object named **hazard.Cube.001** will be **hazard**, as the **Cube** and **001** are ignored.

Names that are **NOT** ignored (because we may want to use them) include:

Lamps:

- Point
- Sun
- Spot
- Hemi
- Area

Force field:

- Field

Other:

- Speaker
- Camera