'use strict';

var UserModel = require('./user.model');
var FriendRequestListModel = require('./friend-request-list.model');
var q = require('q');

function create (username, cb) {
  if (typeof username === 'undefined' || username === null) {
    return cb(new Error('username is required.'));
  }
  UserModel.findOne({ username: username }, function (err, user) {
    if (!user) return cb(new Error('user not found'), null);
    FriendRequestListModel.create({ user: user._id }, function (err, list) {
      if (cb) cb(err, list);
    });
  });
};

function destroy (username, cb) {
  if (typeof username === 'undefined' || username === null) {
    return cb(new Error('username is required.'));
  }
  UserModel.findOne({ username: username }, function (err, user) {
    if (!user) return cb(new Error('user not found'), null);
    FriendRequestListModel.remove({ user: user._id }, function (err, list) {
      if (cb) cb(err, list);
    });
  });
};

/**
 * Adds a friend request to the FriendRequestListModel collection. Updates the 'to'
 * array of the requester's document, and the 'from' array of the requestee's
 * document.
 *
 * @method addRequest
 * @param {fromId} String the requester's user id.
 * @param {toId} String the requestee's user id.
 * @return {Promise} resolves to true if the request was newly added, false if
 * the request existed already, rejects with an error otherwise.
 */
function addRequest (fromName, toName) {
  var dfd = q.defer();
  if (typeof fromName === 'undefined' || fromName === null) {
    dfd.reject(new Error('fromName is required.'));
    return dfd.promise;
  } else if (typeof toName === 'undefined' || toName === null) {
    dfd.reject(new Error('toName is required.'));
    return dfd.promise;
  }

  fromName = String(fromName), toName = String(toName);
  if (fromName === toName) {
    dfd.reject(new Error('fromName and toName must be different.'));
    return dfd.promise;
  }

  UserModel.findOne({ username: fromName }, function (err, from) {
    if (!from) {
      dfd.reject(new Error('from user not found.'));
      return dfd.promise;
    }
    UserModel.findOne({ username: toName }, function (err, to) {
      if (!to) {
        dfd.reject(new Error('to user not found.'));
        return dfd.promise;
      }
      addRequests(from._id, to._id);
    });
  });

  function addRequests (fromId, toId) {
    FriendRequestListModel.update({ user: fromId }, { $addToSet: { to: toId } }, { upsert: true }, function (err, res) {
      FriendRequestListModel.update({ user: toId }, { $addToSet: { from: fromId } }, {upsert: true }, function (err, res) {
        var added = false;
        if (res.nModified > 0) added = true;
        else if (res.upserted && res.upserted.length > 0) added = true;
        dfd.resolve(added);
      });
    });
  };

  return dfd.promise;
};

/**
 * Removes a friend request from the FriendRequestListModel collection. Updates the
 * 'to' array of the requester's document, and the 'from' array of the
 * requestee's document.
 *
 * @method removeRequest
 * @param {fromName} String the requester's username.
 * @param {toName} String the requestee's username.
 * @return {Promise} resolves to true if the request was removed, false if the
 * request did not exist, rejects with an error otherwise.
 */
function removeRequest (fromName, toName) {
  var dfd = q.defer();

  if (typeof fromName === 'undefined' || fromName === null) {
    dfd.reject(new Error('fromName is required.'));
    return dfd.promise;
  } else if (typeof toName === 'undefined' || toName === null) {
    dfd.reject(new Error('toName is required.'));
    return dfd.promise;
  }

  fromName = String(fromName), toName = String(toName);
  if (fromName === toName) {
    dfd.reject(new Error('fromName and toName must be different.'));
    return dfd.promise;
  }

  UserModel.findOne({ username: fromName }, function (err, from) {
    if (!from) {
      dfd.reject(new Error('from user not found.'));
      return dfd.promise;
    }
    UserModel.findOne({ username: toName }, function (err, to) {
      if (!to) {
        dfd.reject(new Error('to user not found.'));
        return dfd.promise;
      }
      removeRequests(from._id, to._id);
    });
  });

  function removeRequests (fromId, toId) {
    FriendRequestListModel.update({ user: fromId }, { $pull: { to: toId } }, function (err, res) {
      FriendRequestListModel.update({ user: toId }, { $pull: { from: fromId } }, function (err, res) {
        var removed = false;
        if (res.nModified > 0) removed = true;
        else if (res.upserted && res.upserted.length > 0) removed = true;
        dfd.resolve(removed);
      });
    });
  };

  return dfd.promise;
};

module.exports = {
  create: create,
  destroy: destroy,
  addRequest: addRequest,
  removeRequest: removeRequest
};