var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

var FriendListSchema = new Schema({
  user: { type: ObjectId, ref: 'User', required: true },
  friends: [{ type: ObjectId, ref: 'User', }]
});

/* Validation */

// Validate unique friends
FriendListSchema
.path('friends')
.validate(function (friends, respond) {
  var tempArray = [];
  for (var i = 0, len = friends.length; i < len; i++) {
    var friend = friends[i];
    if (tempArray.indexOf(friend) !== -1) return respond(false);
    tempArray.push(friend);
  }
  respond(true);
});

module.exports = FriendListModel = mongoose.model('FriendList', FriendListSchema);