'use strict';
var UserModel = require('./user.model');
var FriendListModel = require('./friend-list.model');
var q = require('q');
var updateOptions = {
  db: { safe: true }
};

/* Creates an empty friend list for the given user. */
function create (username, cb) {
  if (typeof username === 'undefined' || username === null) {
    return cb(new Error('username is required.'));
  }
  UserModel.findOne({ username: username }, function (err, user) {
    FriendListModel.create({ user: user._id }, function (err, list) {
      if (cb) cb(err, list);
    });
  });
};

function destroy (username, cb) {
  if (typeof username === 'undefined' || username === null) {
    return cb(new Error('username is required.'));
  }
  FriendListModel.remove({ username: username }, function (err, list) {
    if (cb) cb(err, list);
  });
};

function addFriend (username, friendName) {
  var dfd = q.defer();
  if (typeof username === 'undefined' || username === null) { dfd.reject(new Error('username is required.')); return dfd.promise; }
  else if (typeof friendName === 'undefined' || friendName === null) { dfd.reject(new Error('friendName is required.')); return dfd.promise; }
  username = String(username), friendName = String(friendName);
  if (username === friendName) { dfd.reject(new Error('username and friendName must be different.')); return dfd.promise; }
  else {
    UserModel.findOne({ username: username }, function (err, user) {
      if (!user) dfd.reject(new Error('The specified user does not exist.'));
      else UserModel.findOne({ username: friendName }, function (err, friend) {
        if (!friend) dfd.reject(new Error('The specified friend does not exist.'));
        else FriendListModel.update({ user: user._id }, { $addToSet: { friends: friend._id } }, function (err, res) {
          var added = false;
          if (res.nModified > 0) added = true;
          else if (res.upserted && res.upserted.length > 0) added = true;
          dfd.resolve(added);
        });
      });
    });
  }

  return dfd.promise;
};

function hasFriend (username, friendName) {
  var dfd = q.defer();
  
  getFriendList(username).then(function (list) {
    var found = false;
    for (var i = 0; i < list.length; i++) {
      var friend = list[i];
      if (friend.username !== friendName) continue;
      found = true;
      break;
    }
    dfd.resolve(found);
  }, function (err) {
    dfd.reject(err);
  });

  return dfd.promise;
};

/**
 * Returns the user's friend list. The list is populated with the friends' names.
 *
 * @method getFriendList
 * @param {String} username the user's username in the users collection.
 * @return {Array} a list of the user's friends
 */
function getFriendList (username) {
  var dfd = q.defer();
  if (typeof username === 'undefined' || username === null) {
    dfd.reject(new Error('username is required.'));
    return dfd.promise;
  }
  username = String(username);
  UserModel.findOne({ username: username }, function (err, user) {
    if (!user) {
      dfd.reject(new Error('The specified user does not exist.'));
      return dfd.promise;
    }
    FriendListModel.findOne({ user: user._id })
    .populate({ path: 'friends', select: 'username -_id' })
    .exec(function (err, list) {
      if (err) { dfd.reject(err); return dfd.promise; }
      if (list) { dfd.resolve(list.friends); return dfd.promise; }
      create(username, function (err, list) {
        dfd.resolve(list.friends);
      });
    });
  });

  return dfd.promise;
};

module.exports = {
  create: create,
  destroy: destroy,
  addFriend: addFriend,
  hasFriend: hasFriend,
  getFriendList: getFriendList
};