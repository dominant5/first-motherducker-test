var User = require('./user.model');
var updateOptions = {
  db: { safe: true }
}
var bcrypt = require('bcrypt');

// Creates a new user in the DB.
function create (user, cb) {
  User.create(user, function (err, user) {
    if (cb) cb(err, user);
  });
};

// Updates an existing user in the DB.
function update (name, update, cb) {
  User.update({ username: name }, update, updateOptions, function(err, user) {
    if (cb) cb(err, user);
  });
};

// Deletes a user from the DB.
function destroy (user) {
   User.remove(user, function(err, user) {
    if(err) { return handleError(user, err); }
    return true;
  });
};

function findByUsername (name, cb) {
  User.findOne({ username: name }, function(err, user) {
    if (cb) cb(err, user); 
  });
};

function comparePassword (candidatePassword, user, cb) {
  bcrypt.compare(candidatePassword, user.password, function(err, isMatch) {
      if (err) return cb(err);
      cb(null, isMatch);
  });
};

function handleError(user, err) {
  console.log('Error for user', user);
};

module.exports = {
  create: create,
  update: update,
  destroy: destroy,
  findByUsername: findByUsername,
  comparePassword: comparePassword
};
