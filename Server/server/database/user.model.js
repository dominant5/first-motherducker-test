var mongoose = require('mongoose'),
ValidationError = mongoose.Error.ValidationError,
ValidatorError = mongoose.Error.ValidatorError,
Schema = mongoose.Schema,
bcrypt = require('bcrypt'),
SALT_WORK_FACTOR = 10

/**
 * User schema for MongoDB database
*/
var UserSchema = new Schema({
  /* The user's username */
  username: { type: String, required: true },
  /* The hash of the user's password */
  password: { type: String, required: true },
  /* The user's highest score */
  highscore: { type: Number, default: 0 },
  /* The user's number of wins */
  wins: { type: Number, default: 0 },
  /* The user's number of losses */
  losses: { type: Number, default: 0 },
  /* The user's most ducklings collected */
  mostCollectedDucklings: { type: Number, default: 0},
  /* The user's total number of ducks collected */
  totalCollectedDucklings: { type: Number, default: 0 },
  /* The user's total number of detached ducklings */
  totalDetachedDucklings: { type: Number, default: 0 },
  /* The user's total number of stolen ducklings */
  totalStolenDucklings: { type: Number, default: 0 }
});

module.exports = UserModel = mongoose.model('User', UserSchema);

/**
 * Runs checks before saving to database
 * Ensures uniqueness of user
 * Hashes password using bcrypt to store in database
 */
UserSchema.pre('save', function (next) {
  var user = this;
  if (!user.isModified('username')) return next();

  var username = user.username;
  UserModel.find({ username: username }, function (err, users) {
    if (!users.length) {
      next();
    } else {
      var error = new ValidationError(this);
      error.errors.username = new ValidatorError({
        path: 'username',
        message: 'Username exists',
        type: 'exists',
        value: username
      });
      return next(error);
    }
  });
});

UserSchema.pre('save', function (next) {
  var user = this;
  if (!user.isModified('password')) return next();
  /* Generate a salt */
  bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
  if (err) return next(err);
    /* Hash the password along with our new salt */
    bcrypt.hash(user.password, salt, function(err, hash) {
      if (err) return next(err);

      /* Override the cleartext password with the hashed one */
      user.password = hash;
      next();

    });
  });
});

