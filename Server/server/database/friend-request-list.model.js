var mongoose = require('mongoose');
var ValidationError = mongoose.Error.ValidationError;
var ValidatorError = mongoose.Error.ValidatorError;
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

var FriendRequestListSchema = new Schema({
  /* The user's id. */
  user: { type: ObjectId, ref: 'User', required: true },

  /* The users this user has received friend requests from. */
  from: [{ type: ObjectId, ref: 'User' }],

  /* The users this user has sent friend requests to. */
  to: [{ type: ObjectId, ref: 'User' }]
});

module.exports = FriendRequestListModel = mongoose.model('FriendRequestList', FriendRequestListSchema);

FriendRequestListSchema.pre('save', function (next) {
  if (!this.isNew) return next();
  var user = this.user;
  FriendRequestListModel.find({ user: user }, function (err, lists) {
    if (!lists.length) {
      next();
    } else {
      var error = new ValidationError(this);
      error.errors.user = new ValidatorError({
        path: 'user',
        message: 'Friend request list for given user already exists',
        type: 'exists',
        value: user
      });
      return next(error);
    }
  });
});