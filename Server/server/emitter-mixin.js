/**
 * Mixes in synchronous emitter functionality. Not to be confused with socket.io
 * sockets.
 *
 * @class EmitterMixin
 */

module.exports = function (target) {
  emitterMixin.call(target);
};

/**
 * Registers a callback for evt.
 *
 * @method on
 * @param {String} evt the name of the event
 * @param {Function} cb the callback
 * @param {Object} context the context object for the callback function.
 */
function on (evt, cb, context) {
  this.handlers[evt] = this.handlers[evt] || [];
  this.handlers[evt].push({ cb: cb, context: context });
}

/**
 * Emits an event.
 *
 * @method emit
 * @param {String} evt the name of the event.
 */
function emit (evt) {
  var handlers = this.handlers[evt];
  var args = Array.prototype.slice.call(arguments);
  args.splice(0, 1);
  if (!handlers) return;
  for (var i=0, len = handlers.length; i<len; i++) {
    var handler = handlers[i];
    handler.cb.apply(handler.context, args);
  }
}

var emitterMixin = (function () {
  return function () {
    this.handlers = {};
    this.on = on;
    this.emit = emit;
    return this;
  };
})();