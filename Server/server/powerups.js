module.exports = exp = {};

var Cannon = require('cannon'),
EntityUpdateMixin = require('./entity-update-mixin'),
TypesMixin = require('./types-mixin'),
StatesMixin = require('./states-mixin'),
merge = require('deepmerge'),
Fireables = require('./fireables'),
Projectile = require('./projectile'),
MathUtils = require('./math-utils');
Motherduck = require('./motherduck');

exp.randomPowerup = function (id, options) {
  constructors = [Speed, Steal, LobbedBomb, BulletTime, Magnet, InvertOthers];
  var index = MathUtils.randomInt(0, constructors.length - 1);
  var powerup = constructors[index](id, options);
  return powerup;
};

/**
 * Each powerup has its own object in exp, the constructor for these powerups
 * merges their default options with the base powerup options, and adds
 * its unique types. The default options contain any variables used by the
 * powerup along with the function that should be called by powerup.update
 * once the powerup has been picked up.
 */
exp.Powerup = Powerup = function (id, options, type) {
  this.options = merge(Powerup.defaultOptions, options || {});

  if (this.options.createBody) {
    this.body = new Cannon.Body(this.options.cannonOptions);
    this.body.addShape(new Cannon.Sphere(this.options.cannonOptions.radius));
    this.body.entity = this;
  }

  if (this.options.collideCb && this.options.collideCbContext) {
    this.body.addEventListener('collide', this.options.collideCb.bind(this.options.collideCbContext));
  }
  
  EntityUpdateMixin(this);
  this.setUpdateFn(this.options.idleFn);

  this.id = id;
  TypesMixin(this);
  this.addType('powerup');
  if (type) {
    this.addType(type);
    this.powerupType = type;
  }
  this.activated = false;
  this.alive = true;
  StatesMixin(this);

  return this;
};

Powerup.defaultOptions = {
  cannonOptions: {
    mass: 0.5,
    radius: 0.5
  },
  createBody: true,
  activeFn: null,
  idleFn: function(dt) {},
  collideCb: null,
  collideCbContext: null
};

/**
 * Returns the type of this powerup that's not 'powerup', eg. 'speed'
 *
 * @method getType
 * @return {String} the type
 */
Powerup.prototype.getType = function() {
  return this.types[1];
};

/**
 * Activates this powerup; sets its update function to its active function,
 * sets its target to the parameter, and destroys its body.
 *
 * @method activate
 * @param {Object} target the target entity.
 */
Powerup.prototype.activate = function (target, game) {
  this.activated = true;
  this.updateFn = this.options.activeFn;
  this.target = target;
  var activatedPowerup = this.target.activePowerups[this.powerupType];
  if (activatedPowerup) {
    delete this.target.activePowerups[this.powerupType];
    activatedPowerup.destroy(game);
    game.entityStore.remove(activatedPowerup);
  }
  if (!this.hasType('fireablePowerup')) {
    this.target.activePowerups[this.powerupType] = this;
  }
  if (this.options.activateFn) this.options.activateFn.call(this, target, game);
  this.destroyBody();
};

Powerup.prototype.destroy = function(game) {
  if (this.options.destroyFn) this.options.destroyFn.call(this, game);
  this.destroyBody();
};

/**
 * Removes the powerup's body from the world, and deletes it.
 *
 * @method destroyBody
 */
Powerup.prototype.destroyBody = function () {
  if (!this.body) return;
  if (this.body.world) this.body.world.remove(this.body);
  delete this.body;
};

/**
 * A powerup which multiplies the speed of the motherduck by 'factor'.
 *
 * @class Speed
 * @constructor
 * @param {String} id The ID of this powerup.
 * @param {Object} passedOptions The options to be used for this powerup.
 */
exp.Speed = Speed = function (id, passedOptions) {
  var options = merge(Speed.defaultOptions, passedOptions || {});
  var ret = new Powerup(id, options, 'speed');
  return ret;
};

/**
 * The default speed powerup options.
 *
 * @property Speed.defaultOptions
 * @type {Object}
 */
Speed.defaultOptions = {
  factor: 1.5,
  lifetime: 3,
  activateFn: function (target, game) {
     if (!this.target.hasType('motherduck')) return;
     this.target.options.turnSpeed *= this.options.factor;
     
     game.emitFn(game.id, 'game:get-powerup', {
      motherduckName: this.target.name,
      id: this.id,
      lifetime: this.options.lifetime,
      powerupType: this.powerupType
     });
  },
  activeFn: function(dt, game) {
    this.options.lifetime -= dt;
    this.alive = this.options.lifetime > 0;
    
    this.target.body.velocity.x *= this.options.factor;
    this.target.body.velocity.z *= this.options.factor;
    
    game.emitFn(game.id, 'game:update-ptimer', {
      motherduckName: this.target.name,
      lifetime: this.options.lifetime,
      powerupType: this.powerupType
    });
  },
  destroyFn: function() {
    this.target.options.turnSpeed /= this.options.factor;
  }
};

/**
 * A powerup which gives the motherduck the ability to shoot a projectile
 * in the direction they are facing which adds the first duckling it collides
 * with to their trail.
 *
 * @class Steal
 * @constructor
 * @param {String} id The ID of this powerup.
 * @param {Object} passedOptions The options to be used for this powerup.
 */
exp.Steal = Steal = function (id, passedOptions) {
  var options = merge(Steal.defaultOptions, passedOptions || {});
  var ret = new Powerup(id, options, 'steal');
  ret.addType('fireablePowerup');
  return ret;
};

/**
 * The default steal powerup options.
 *
 * @property Steal.defaultOptions
 * @type {Object}
 */
Steal.defaultOptions = {
  activateFn: function(target, game) {
    var fireable = new Fireables.StealFireable(game.getNextEID());
    fireable.target = this.target;
    this.target.getFireable (fireable);
    this.alive = false;
    
    game.emitFn(game.id, 'game:get-fireable', {
      motherduckName: this.target.name,
      id: fireable.id,
      ammunition: fireable.ammunition,
      fireableType: fireable.fireableType
    });
  }
};

/**
 * A powerup which gives the motherduck the ability to throw bombs which
 * explode on impact, detaching ducklings from their trails within a radius
 * and blasting them away.
 *
 * @class LobbedBomb
 * @constructor
 * @param {String} id The ID of this powerup.
 * @param {Object} passedOptions The options to be used for this powerup.
 */
exp.LobbedBomb = LobbedBomb = function (id, passedOptions) {
  var options = merge(LobbedBomb.defaultOptions, passedOptions || {});
  var ret = new Powerup(id, options, 'lobbedBomb');
  ret.addType('fireablePowerup');
  return ret;
};

/**
 * The default lobbed bomb powerup options.
 *
 * @property LobbedBomb.defaultOptions
 * @type {Object}
 */
LobbedBomb.defaultOptions = {
  activateFn: function(target, game) {
    var fireable = new Fireables.LobbedBombFireable(game.getNextEID());
    fireable.target = this.target;
    this.target.getFireable (fireable);
    this.alive = false;
    
    game.emitFn(game.id, 'game:get-fireable', {
      motherduckName: this.target.name,
      id: fireable.id,
      ammunition: fireable.ammunition,
      fireableType: fireable.fireableType
    });
  }
};

/**
 * A powerup which 'slows time' for every motherduck besides the one who collects this powerup.
 *
 * @class BulletTime
 * @constructor
 * @param {String} id The ID of this powerup.
 * @param {Object} passedOptions The options to be used for this powerup.
 */
exp.BulletTime = BulletTime = function (id, passedOptions) {
  var options = merge(BulletTime.defaultOptions, passedOptions || {});
  var ret = new Powerup(id, options, 'bulletTime');
  return ret;
};

/**
 * The default bullet-time powerup options.
 *
 * @property BulletTime.defaultOptions
 * @type {Object}
 */
BulletTime.defaultOptions = {
  factor: 1/5,
  lifetime: 3,
  activateFn: function (target, game) {
    game.entityStore.forEach('motherduck', function (motherduck) {
      if (motherduck !== this.target) motherduck.options.turnSpeed *= this.options.factor;
      game.emitFn(game.id, 'game:get-powerup', {
        motherduckName: motherduck.name,
        id: this.id,
        lifetime: (motherduck === this.target)? 0 : this.options.lifetime,
        factor: this.options.factor,
        powerupType: this.powerupType
       });
    }.bind(this));
  },
  activeFn: function(dt, game) {
    this.options.lifetime -= dt;
    this.alive = this.options.lifetime > 0;
    
    game.entityStore.forEach('motherduck', function (motherduck) {
      if (motherduck !== this.target) motherduck.body.velocity.scale(this.options.factor, motherduck.body.velocity);
      game.emitFn(game.id, 'game:update-ptimer', {
        motherduckName: motherduck.name,
        lifetime: this.options.lifetime,
        factor: this.options.factor,
        powerupType: this.powerupType
      });
    }.bind(this));

    game.entityStore.forEach('duckling', function (duckling) {
      if (duckling.leader === this.target) return;
      duckling.body.velocity.scale(this.options.factor, duckling.body.velocity);
    }.bind(this));
  },
  destroyFn: function(game) {
    game.entityStore.forEach('motherduck', function (motherduck) {
      if (this.alive) {
        game.emitFn(game.id, 'game:update-ptimer', {
          motherduckName: motherduck.name,
          lifetime: 0,
          factor: this.options.factor,
          powerupType: this.powerupType
        });
      }
      if (motherduck === this.target) return;
      motherduck.options.turnSpeed /= this.options.factor;
    }.bind(this));
  }
};

/**
 * A powerup which causes helpless ducklings to be pulled towards the
 * motherduck if they are within a range.
 *
 * @class BulletTime
 * @constructor
 * @param {String} id The ID of this powerup.
 * @param {Object} passedOptions The options to be used for this powerup.
 */
exp.Magnet = Magnet = function (id, passedOptions) {
  var options = merge(Magnet.defaultOptions, passedOptions || {});
  var ret = new Powerup(id, options, 'magnet');
  return ret;
};

/**
 * The default Magnet powerup options.
 *
 * @property Motherduck.defaultOptions
 * @type {Object}
 */
Magnet.defaultOptions = {
  range: 10,
  strength: 10,
  lifetime: 3,
  activateFn: function (target, game) {
    game.emitFn(game.id, 'game:get-powerup', {
      motherduckName: this.target.name,
      id: this.id,
      lifetime: this.options.lifetime,
      powerupType: this.powerupType
     });
  },
  activeFn: function(dt, game) {
    this.options.lifetime -= dt;
    this.alive = this.options.lifetime > 0;
    
    var targetPos = this.target.body.position;
    game.entityStore.forEach('duckling', function (duckling) {
      if (!duckling.hasState('helpless')) return;
      var ducklingPos = duckling.body.position;
      if (targetPos.distanceTo(ducklingPos) > this.options.range) return;
      var attractDir = targetPos.vsub(ducklingPos);
      attractDir.normalize();
      attractDir = attractDir.scale(this.options.strength);
      duckling.body.applyForce(attractDir, ducklingPos);
    }.bind(this));
    
    game.emitFn(game.id, 'game:update-ptimer', {
      motherduckName: this.target.name,
      lifetime: this.options.lifetime,
      powerupType: this.powerupType
    });
  }
};

/**
 * A powerup which inverts the turning controls for every motherduck
 * besides the one who collects this powerup.
 *
 * @class BulletTime
 * @constructor
 * @param {String} id The ID of this powerup.
 * @param {Object} passedOptions The options to be used for this powerup.
 */
exp.Invert = Invert = function (id, passedOptions) {
  var options = merge(Invert.defaultOptions, passedOptions || {});
  var ret = new Powerup(id, options, 'invert');
  return ret;
};

/**
 * The default invert powerup options.
 *
 * @property Invert.defaultOptions
 * @type {Object}
 */
Invert.defaultOptions = {
  lifetime: 3,
  activateFn: function(target, game) {
    if (!this.target.hasType('motherduck')) return;
    game.emitFn(game.id, 'game:get-powerup', {
      motherduckName: this.target.name,
      id: this.id,
      lifetime: this.options.lifetime,
      powerupType: this.powerupType
     });
    this.target.options.turnSpeed *= -1;
  },
  activeFn: function (dt, game) {
    this.options.lifetime -= dt;
    this.alive = this.options.lifetime > 0;
    
    game.emitFn(game.id, 'game:update-ptimer', {
      motherduckName: this.target.name,
      lifetime: this.options.lifetime,
      powerupType: this.powerupType
    });
  },
  destroyFn: function () {
    this.target.options.turnSpeed *= -1;
  }
};

exp.InvertOthers = InvertOthers = function (id, passedOptions) {
  var options = merge(InvertOthers.defaultOptions, passedOptions || {});
  var ret = new Powerup(id, options, 'invertOthers');
  return ret;
};

InvertOthers.defaultOptions = {
  activateFn: function(target, game) {
    game.entityStore.forEach('motherduck', function (motherduck) {
      if (motherduck === target) return;
      var invert = new Invert(game.getNextEID());
      game.entityStore.push(invert);
      invert.activate(motherduck, game);
    });
    this.alive = false;
  }
};