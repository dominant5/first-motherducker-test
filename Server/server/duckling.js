module.exports = exp = Duckling;

var Cannon = require('cannon'),
EntityUpdateMixin = require('./entity-update-mixin'),
TypesMixin = require('./types-mixin'),
StatesMixin = require('./states-mixin'),
merge = require('deepmerge');

/**
 * Responsible for controlling a duckling, one instance for every duckling.
 *
 * @class Duckling
 * @constructor
 * @param {String} id The ID of this duckling.
 * @param {Object} passedOptions The options to be used for this duckling.
 */
function Duckling (id, passedOptions) {
  this.options = merge(Duckling.defaultOptions, passedOptions || {});

  this.body = new Cannon.Body(this.options.cannonOptions);
  this.body.addShape(new Cannon.Sphere(this.options.cannonOptions.radius));
  this.body.entity = this;

  if (this.options.collideCb && this.options.collideCbContext) {
    this.body.addEventListener('collide', this.options.collideCb.bind(this.options.collideCbContext));
  }

  EntityUpdateMixin(this);

  this.id = (typeof id !== 'undefined')? id : this.body.id;
  TypesMixin(this);
  this.addType('duckling');
  StatesMixin(this);
  this.addState('helpless');
  this.next = null;
  this.prev = null;
  this.leader = null;

  return this;
}

/**
 * The default duckling options.
 *
 * @property Duckling.defaultOptions
 * @type {Object}
 */
Duckling.defaultOptions = {
  cannonOptions: {
    mass: 0.4,
    radius: 0.4,
    linearDamping: 0,
    angularDamping: 0.9
  },
  speed: 10,
  collideCb: null,
  collideCbContext: null
};

/**
 * The default update function for a duckling.
 *
 * @method defaultUpdateFn
 */
Duckling.defaultUpdateFn = function () {
  if(!this.next) return;  // Only update the duckling if it is following something
  this.body.velocity = this.next.body.position.vsub(this.body.position);
  this.body.velocity = this.body.velocity.scale(this.options.speed);
  
  if (!this.defaultUpdateFnDummyVec) this.defaultUpdateFnDummyVec = new Cannon.Vec3();
  this.defaultUpdateFnDummyVec.x = this.body.velocity.x;
  this.defaultUpdateFnDummyVec.z = this.body.velocity.z;
  this.body.quaternion.setFromVectors(Cannon.Vec3.UNIT_Z, this.defaultUpdateFnDummyVec);
};