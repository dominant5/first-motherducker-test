module.exports = Game;

var q = require('q');
var _ = require('underscore');
var merge = require('deepmerge');
var Client = require('./client');
var Loop = require('./game-loop');
var MathUtils = require('./math-utils');
var TypesMixin = require('./types-mixin');
var StatesMixin = require('./states-mixin');
var AI = require('./ai-player');
var Motherduck = require('./motherduck');
var Duckling = require('./duckling');
var Maps = require('./maps-util/maps');
var Cannon = require('cannon');
var CollisionStore = require('./collision-store');
var EntityStore = require('./entity-store');
var CollisionGroups = require('./game-collision-groups');
var TeamColours = require('./team-colours');
var Powerups = require('./powerups');
var EmitterMixin = require('./emitter-mixin');

/**
 * Responsible for simulating a game, sending state to clients, and processing
 * game-related messages from clients.
 *
 * @class Game
 * @constructor
 * @param {Number} id this game's desired id.
 * @param {Function} emitFn the function for emitting events to all the clients.
 * @param {Object} options any overriden options for this game.
 * @param {Object} options.start the start conditions for this game.
 * @param {Number} [options.start.numClients = 2] the number of clients required
 * to start this game.
 * @param {Number} [options.motherduckSpeed = 10] motherducks' constant speed.
 * @param {Object} options.ducklings duckling related options.
 * @param {Number} [options.ducklings.spawnFreq = 10.0] how often ducklings are
 * spawned (seconds).
 * @param {Number} [options.ducklings.spawnFreqVar = 1.0] variance of spawnFreq
 * (seconds).
 * @param {Number} [options.emitFps = 5] how many times per sec updates are sent
 * to clients. Should be less than simFps.
 * @param {Number} [options.simFps = 40] how many physics simulation steps per
 * second.
 */
function Game (id, emitFn, options) {
  /**
   * This game's id. Eg. Game1234567890.
   *
   * @property id
   * @type {Number}
   */
  this.id = id;

  /**
   * A proxy function used to emit to all clients in the room associated with
   * this game.
   *
   * @property emitFn
   * @type {Function}
   */
  this.emitFn = emitFn;

  this.options = merge(Game.defaultOptions, options || {});

  /**
   * This game's status. Defined in game-status-codes.js.
   *
   * @property status
   * @type {Number}
   */
  this.status = Game.StatusCodes.WAITING_ON_CLIENTS;

  /**
   * The cannon.js world associated with this game.
   *
   * @property world
   * @type {Object}
   */
  this.world = null;

  /**
   * The specific map that this game is played in.
   *
   * @property map
   * @type {Object}
   */
  this.map = null;

  /**
   * An object keeping track of the cannon.js materials used.
   *
   * @property materials
   * @type {Object}
   */
  this.materials = {};

  /**
   * Seconds until the next duckling is spawned.
   *
   * @property nextDucklingTime
   * @type {Number}
   */
  this.nextDucklingTime = 0;
  
  /**
   * Seconds until the next powerup is spawned.
   *
   * @property nextPowerupTime
   * @type {Number}
   */
  this.nextPowerupTime = 0;
  
  /**
   * Seconds since the game started.
   *
   * @property gameTimer
   * @type {Number}
   */
  this.gameTimer = this.options.gameTimer;

  /**
   * Seconds since the last broadcast of game state to clients.
   *
   * @property timeSinceLastEmit
   * @type {Number}
   */
  this.timeSinceLastEmit = 0;

  /**
   * Seconds between broadcasting game state to clients.
   *
   * @property emitFreq
   * @type {Number}
   */
  this.emitFreq = 1 / this.options.emitFps;

  /**
   * A vector representing the forward direction. Used for orienting newly
   * created cannon.js objects.
   *
   * @property vecForward
   * @type {Cannon.Vec3}
   */
  this.vecForward = new Cannon.Vec3(0, 0, 1); // forwards is +z

  /**
   * The clients in the game, indexed by client name.
   *
   * @property clients
   * @type {Object}
   */
  this.clients = {};

  /**
   * The list of messages from clients accumulated since the last update step.
   *
   * @property messages
   * @type {Array}
   */
  this.messages = [];

  /**
   * The collisions detected in world.step
   *
   * @property collisionStore
   * @type {CollisionStore}
   */
  this.collisionStore = new CollisionStore();

  /**
   * The entities in the world.
   *
   * @property entityStore
   * @type {EntityStore}
   */
  this.entityStore = new EntityStore();

  /**
   * The next available unique entity id.
   *
   * @property NEXT_ENTITY_ID
   * @type {Number}
   */
  this.NEXT_ENTITY_ID = 0;
  
  /**
   * The list of team colours.
   *
   * @property teamColours
   * @type {Array}
   */
  this.teamColours = [];

  EmitterMixin(this);

  /**
   * A scratch object used to emit data to clients, so that we're not creating
   * new objects in the update loop.
   *
   * @property updateData
   * @type {Object}
   */
  this.updateData = {
    entities: [],
    gt: this.gameTimer,
    t: String(Date.now())
  };

  /**
   * The instance of Loop used to schedule this game's update function.
   *
   * @property loop
   * @type {Loop}
   */
  this.loop = new Loop(this.update.bind(this), this.options.simFps);
}

Game.EventCodes = {
  STARTED: 0,
  ENDED: 1
};

Game.StatusCodes = {
  WAITING_ON_CLIENTS: 0,
  WAITING_TO_START: 1,
  STARTED: 2,
  ENDED: 3
};

/**
 * The default options for a game.
 *
 * @property Game.defaultOptions
 * @type {Object}
 */
Game.defaultOptions = {
  gameMode: 'ffa',
  aiOptions: {

  },
  start: {
    numClients: 2,
    teamSize: 1,
    fillWithAI: true
  },
  randomMotherduckSpawnPoints: true,
  duckSpeed: 10,
  duckTurnSpeed: 3,
  ducklings: {
    spawnFreq: 10.0, // seconds
    spawnFreqVar: 1.0 // variance of spawnFreq in seconds
  },
  powerups: {
    spawnFreq: 5.0,
    spawnFreqVar: 2.0
  },
  mapName: 'defaultflat',
  emitFps: 5, // how many times per sec updates are sent to clients. Should be less than simFps.
  simFps: 40, // how many physics simulation steps per sec
  gameTimer: 120 // how long the game should last
};

/**
 * Calculates the raw spawn rate for the given number of motherducks in the game
 * given a few parameters. This function solves the equation y = ax + b for a
 * and b,  where y = raw spawn rate per 100 metres squared per minute, and x =
 * number of motherducks. It uses oneRate and eightRate as a pair of solutions
 * to solve the equation. It then uses a and b to calculate the final spawn
 * rate.
 *
 * @method calculateSpawnRate
 * @param {Number} oneRate the desired spawn rate per 100 metres squared per
 * minute for one motherduck in the game
 * @param {Number} eightRate the desired spawn rate per 100 metres squared per 
 * minute for eight motherducks in the game
 * @param {Number} numDucks the number of motherducks to calculate spawn rate
 * for
 * @param {Number} totalSpawnArea the total area in metres of the spawn areas
 * for the given type of spawnable object.
 * @return {Number} the raw spawn rate (in seconds)
 */
Game.calculateSpawnRate = function (oneRate, eightRate, numDucks, totalSpawnArea) {
  var b = (8*oneRate - eightRate) / 7;
  var a = oneRate - b;
  var spawnRatePer100MsqPerMin = a * numDucks + b;
  var spawnRatePerMin = spawnRatePer100MsqPerMin * totalSpawnArea / (100 * 100);
  var spawnRate = 60 / spawnRatePerMin;
  return spawnRate;
};

/**
 * Updates the game simulation at simFps, and emits state to clients at emitFps.
 * Only for internal use.
 *
 * @private
 * @method update
 * @param {Number} dt seconds since last update
 */
Game.prototype.update = function (dt) {
  // update gameTimer
  this.gameTimer -= dt;
  if (this.gameTimer <= 0) { this.onGameEnded(); return; }

  // process messages
  for (var messageIdx = 0, len = this.messages.length; messageIdx < len; messageIdx++) {
    var message = this.messages[messageIdx];
    var motherduck = this.entityStore.getByName(message.clientName);
    if (!motherduck) continue;

    if (message.type === 'turn') {
      motherduck.setTurnType(message.turnType);
    }
    
    else if (message.type === 'fire') {
      motherduck.useFireable(this);
    }
    
    else if (message.type === 'dive') {
      motherduck.dive(this);
    }
  }
  this.messages = [];

  // step the world
  this.world.step(dt, dt, 20);

  // handle motherduck collisions with helpless ducklings
  this.collisionStore.forEach('motherduck', 'duckling', function (collision) {
    if (!collision.duckling.hasState('helpless')) return;
    collision.motherduck.addDuckling(collision.duckling, this);
  }.bind(this));
  
  // handle motherduck collisions with hazards
  this.collisionStore.forEach('motherduck', 'hazard', function (collision) {
    var toRemove = collision.motherduck.prev;
    if (!toRemove) return;
    collision.motherduck.removeDuckling(toRemove, this);
    var dist = 10;
    var displaceVec = toRemove.body.quaternion.vmult(new Cannon.Vec3(0, dist, 0));
    toRemove.body.velocity = displaceVec;
  }.bind(this));
  
  // handle collisions with powerups
  this.collisionStore.forEach('motherduck', 'powerup', function (collision) {
    if (collision.powerup.activated || !collision.powerup) return;
    collision.powerup.activate(collision.motherduck, this);
    this.emitFn(this.id, 'game:remove-entity', { key: collision.powerup.id });
  }.bind(this));
  
  // handle projectile collisions with the map
  this.collisionStore.forEach('projectile', 'map', function (collision) {
    if (!collision.projectile) return;
    this.emitFn(this.id, 'game:explosion', {
      p: collision.projectile.body.position,
      q: collision.projectile.body.quaternion
    });
    collision.projectile.explode('map', this, collision.map);
    this.emitFn(this.id, 'game:remove-entity', { key: collision.projectile.id });
    this.entityStore.remove(collision.projectile);
  }.bind(this));
  
  // handle projectile collisions with non-helpless ducklings
  this.collisionStore.forEach('projectile', 'duckling', function (collision) {
    if (!collision.projectile) return;
    collision.projectile.explode('duckling', this, collision.duckling);
    this.emitFn(this.id, 'game:remove-entity', { key: collision.projectile.id });
    this.entityStore.remove(collision.projectile);
  }.bind(this));

  this.collisionStore.clear();

  // update buoyancy
  var g = -this.world.gravity.y;
  var wAABB = new Cannon.AABB();
  var eAABB = new Cannon.AABB();
  this.entityStore.forEach('water', function (water) {
    // get water aabb in world space
    wAABB.copy(water.body.aabb);
    wAABB.lowerBound.vadd(water.body.position, wAABB.lowerBound);
    wAABB.upperBound.vadd(water.body.position, wAABB.upperBound);
    var surface = wAABB.upperBound.y;

    this.entityStore.forEach(['motherduck', 'duckling', 'powerup'], function (entity) {
      if (!entity.body) return;
      // get entity aabb in world space
      eAABB.copy(entity.body.aabb);
      eAABB.lowerBound.vadd(entity.body.position, eAABB.lowerBound);
      eAABB.upperBound.vadd(entity.body.position, eAABB.upperBound);

      if (!wAABB.overlaps(eAABB)) return;

      var xLen = eAABB.upperBound.x - eAABB.lowerBound.x;
      var yLen = eAABB.upperBound.y - eAABB.lowerBound.y;
      var zLen = eAABB.upperBound.z - eAABB.lowerBound.z;

      var a = xLen * zLen;
      var h = wAABB.contains(eAABB)? yLen : surface - eAABB.lowerBound.y;

      // apply a large force if the entity is falling in, small if it's floating up
      var forceFactor = entity.body.velocity.y > -0.3 * entity.body.mass? 1.2 : 3.5;

      // apply the buoyancy force
      entity.body.force.y = forceFactor * g * h * a;
    }.bind(this));
  }.bind(this));

  // update motherducks
  this.entityStore.forEach('motherduck', function (motherduck) {
    motherduck.update(dt, this);
  }.bind(this));

  // update ducklings
  this.entityStore.forEach('duckling', function (duckling) {
    duckling.update(dt, this);
  }.bind(this));
  
  // update powerups
  this.entityStore.forEach('powerup', function (powerup) {
    if (!powerup) return;
    powerup.update(dt, this);
    if (!powerup.alive) {
      var activatedPowerup = powerup.target.activePowerups[powerup.powerupType];
      if (activatedPowerup) {
        delete powerup.target.activePowerups[powerup.powerupType];
      }
      powerup.destroy(this);
      this.entityStore.remove(powerup);
    }
  }.bind(this));
  
  // update projectiles
  this.entityStore.forEach('projectile', function (projectile) {
    projectile.update(dt, this);
  }.bind(this));

  // duckling spawn logic
  this.nextDucklingTime -= dt;
  if (this.nextDucklingTime <= 0) {
    var spawnInfo = this.spawnDuckling();
    this.emitSpawn(spawnInfo);
    this.nextDucklingTime = this.getNextSpawnTime(this.options.ducklings.spawnFreq,
      this.options.ducklings.spawnFreqVar);
  }
  
  // powerup spawn logic
  this.nextPowerupTime -= dt;
  if (this.nextPowerupTime <= 0) {
    var spawnInfo = this.spawnPowerup();
    this.emitSpawn(spawnInfo);
    this.nextPowerupTime = this.getNextSpawnTime(this.options.powerups.spawnFreq,
      this.options.powerups.spawnFreqVar);
  }
  
  // emit world state
  this.timeSinceLastEmit += dt;
  if (this.timeSinceLastEmit >= this.emitFreq) {
    this.emitUpdate();
    this.timeSinceLastEmit = 0;
  }
};

/**
 * Emit the final scores when game has ended. Stops loop.
 *
 * @private
 * @method onGameEnded
 */
Game.prototype.onGameEnded = function() {
  this.updateData = {
    statistics: [],
    winner: [],
    mvp: {}
  };
  
  this.updateData.statistics = this.calcTeam();
  var winData = this.calcWinner(this.updateData.statistics);
  this.updateData.winner = winData.winner;
  this.updateData.mvp = winData.mvp;
  
  this.emitFn(this.id, 'game:ended', this.updateData);  
  this.emit(Game.EventCodes.ENDED, this);
  this.loop.stop();
};

/**
 * Calculate the individual and team scores.
 *
 * @private
 * @method calcTeam
 * @return {Array} a list of statistics for each team
 */
Game.prototype.calcTeam = function() {
  var teamSize = this.options.start.teamSize;
  var numTeams = (this.options.start.numClients+this.AICount())/teamSize;
  
  var data = [];
  
  for (var team = 0; team < numTeams; team++) {
    var motherducks = {};
    this.entityStore.forEach('motherduck', function (motherduck) {
      if (motherduck.team !== team) return;
      var finalScore = this.calcFinalScore({
        ducklingCount: motherduck.ducklingCount,
        highestCount: motherduck.highestCount,
        stolenCount: motherduck.stolenCount,
        detachedCount: motherduck.detachedCount
      });
      motherduck.score = finalScore;
      var mdStats = {
        finalScore: finalScore,
        ducklingCount: motherduck.ducklingCount,
        highestCount: motherduck.highestCount,
        stolenCount: motherduck.stolenCount,
        detachedCount: motherduck.detachedCount
      };
      motherducks[motherduck.name] = mdStats;
    }.bind(this));
    data.push(motherducks);
  }
  
  for (var i = 0; i < data.length; i++) {
    var teamStats = {
      finalScore: 0,
      ducklingCount: 0,
      highestCount: 0,
      stolenCount: 0,
      detachedCount: 0
    };
    for (var key1 in data[i]) {
      for (var key2 in data[i][key1]) {
        teamStats[key2] += data[i][key1][key2];
      }
    }
    data[i][i] = teamStats;
  }
  return data;
};

/**
 * Calculate the final score of any given statistic.
 *
 * @private
 * @method calcFinalScore
 * @param {Object} stats the statistics object
 * @return {Number} the final calculated score
 */
Game.prototype.calcFinalScore = function(stats) {
  var weights = {
    ducklingCount: 100,
    highestCount: 20,
    stolenCount: 5,
    detachedCount: 10
  };
  var finalScore = 0;
  for (var key in stats) {
    finalScore += stats[key] * weights[key];
  }
  return finalScore;
};

/**
 * Calculate the winner or winners of the game.
 *
 * @private
 * @method calcWinner
 * @param data a list of statistics for each team
 * @return {Array} an object of winner/s and mvp/s of the game
 */
Game.prototype.calcWinner = function(data) {
  var highest = 0;
  var winData = {
    winner:  [],
    mvp: {}
  };
  
  for (var i = 0; i < data.length; i++) {
    if (data[i][i].finalScore === highest) {
      winData.winner.push(i);
    } else if (data[i][i].finalScore > highest) {
      highest = data[i][i].finalScore;
      winData.winner = [i];
    }
  }
  
  highest = 0;
  
  for (var j = 0; j < winData.winner.length; j++) {
    var winner = winData.winner[j];
    for (var key in data[winner]) {
      if (key == winner) continue;
      if (data[winner][key].finalScore === highest) {
        winData.mvp[key] = key;
      } else if (data[winner][key].finalScore > highest) {
        highest = data[winner][key].finalScore;
        winData.mvp = {};
        winData.mvp[key] = key;
      }
    }
  }

  return winData;
};

/**
 * Emits current world state to clients.
 *
 * @private
 * @method emitUpdate
 */
Game.prototype.emitUpdate = function() {
  this.updateData.entities.length = 0;
  this.entityStore.forEach({ not: ['map'] }, function (entity) {
    if (entity.hasType('powerup') && entity.activated) return;

    var data = [];

    // position
    entity.curPos = entity.curPos || new Cannon.Vec3();
    entity.curPos.x = Number(entity.body.position.x.toFixed(2));
    entity.curPos.y = Number(entity.body.position.y.toFixed(2));
    entity.curPos.z = Number(entity.body.position.z.toFixed(2));
    entity.lastPos = entity.lastPos || new Cannon.Vec3();
    var sendP = !entity.curPos.almostEquals(entity.lastPos, 10e-3);
    if (sendP) {
      data.push([-1,
        entity.curPos.x,
        entity.curPos.y,
        entity.curPos.z
      ]);
    }
    entity.lastPos.copy(entity.curPos);

    // quaternion
    var qx = Number(entity.body.quaternion.x.toFixed(2));
    var qy = Number(entity.body.quaternion.y.toFixed(2));
    var qz = Number(entity.body.quaternion.z.toFixed(2));
    var qw = Number(entity.body.quaternion.w.toFixed(2));
    entity.lastQ = entity.lastQ || { x: 0, y: 0, z: 0, w: 0 };
    if (qx !== entity.lastQ.x ||
        qy !== entity.lastQ.y ||
        qz !== entity.lastQ.z ||
        qw !== entity.lastQ.w) {
      if (Math.abs(qx) === 0 &&
          Math.abs(qy) === 0 &&
          Math.abs(qz) === 0 &&
          Math.abs(qw) === 1) {
        data.push([-2]);
      } else {
        data.push([-3, qx, qy, qz, qw]);
      }
    }
    entity.lastQ.x = qx;
    entity.lastQ.y = qy;
    entity.lastQ.z = qz;
    entity.lastQ.w = qw;

    if (data.length > 0) { // pos and/or quaternion data was added
      this.updateData.entities.push(entity.id);
      for (var i = 0; i < data.length; i++) {
        for (var j = 0; j < data[i].length; j++) {
          this.updateData.entities.push(data[i][j]);
        }
      }
    }
  }.bind(this));
  this.updateData.gt = Number(this.gameTimer.toFixed(0));
  this.updateData.t = String(Date.now());
  this.emitFn(this.id, 'game:update', this.updateData);
};

/**
 * Callback event for cannon.js collisions. Adds the collision data to the
 * collision store.
 *
 * @method onCollision
 * @param {Object} evt the cannon.js collision.
 */
Game.prototype.onCollision = function(evt) {
  this.collisionStore.push(evt.body.entity, evt.target.entity, evt.contact);
};

/**
 * Creates a duckling and adds it to the world.
 *
 * @method spawnDuckling
 * @return {Object} an object representing the spawn info. Used for emitting
 * spawn event to clients.
 */
Game.prototype.spawnDuckling = function() {
  var duckling = new Duckling(this.getNextEID(), {
    cannonOptions: {
      material: this.materials['duckling']
    },
    speed: this.options.duckSpeed,
    collideCb: this.onCollision,
    collideCbContext: this
  });
  
  duckling.body.position = this.getRandomSpawnPos(this.map, 'ducklings');
  /* the origin point of the entity is centred at its base. Therefore we move
  it up by half its height. */
  duckling.body.position.y += duckling.body.boundingRadius;
  this.world.addBody(duckling.body);
  this.entityStore.push(duckling);

  // return spawn info (for emit)
  var spawnInfo = {
    type: 'duckling',
    id: duckling.id,
    p: duckling.body.position,
    q: duckling.body.quaternion
  };
  
  return spawnInfo;
};

/**
 * Creates a random powerup and adds it to the world.
 *
 * @method spawnPowerup
 * @return {Object} an object representing the spawn info. Used for emitting
 * spawn event to clients.
 */
Game.prototype.spawnPowerup = function() {
  var powerup = Powerups.randomPowerup(this.getNextEID(), {
    cannonOptions: {
      material: this.materials['powerup']
    },
    collideCb: this.onCollision,
    collideCbContext: this
  });
  
  powerup.body.position = this.getRandomSpawnPos(this.map, 'powerups');
  /* the origin point of the entity is centred at its base. Therefore we move
  it up by half its height. */
  powerup.body.position.y += powerup.body.boundingRadius;
  this.world.add(powerup.body);
  this.entityStore.push(powerup);

  // return spawn info (for emit)
  var spawnInfo = {
    type: 'powerup',
    powerupType: powerup.powerupType,
    id: powerup.id,
    radius: powerup.body.boundingRadius,
    color: powerup.color,
    p: powerup.body.position
  };
  
  return spawnInfo;
};

/**
 * Returns a random spawn position on the map.
 *
 * @method getRandomSpawnPos
 * @param {Object} map the map object.
 * @param {String} type the type of object to spawn
 * @return {Vec3}
 *
 * @example
 *     getRandomSpawnPos(Maps.loadMap('defaultflat', true));
 */
Game.prototype.getRandomSpawnPos = function(map, type) {
  var spawnAreas = map.spawnAreas[type];
  var cumulative = 0;
  var random = Math.random();
  for (var i = 0; i < spawnAreas.length; i++) {
    var spawnArea = spawnAreas[i];
    cumulative += spawnArea.weight;
    if (random < cumulative) {
      var fac1 = MathUtils.randomFloat(0, 1);
      var fac2 = MathUtils.randomFloat(0, 1);
      var ret = spawnArea.corner.clone();
      ret = ret.vadd(spawnArea.vec1.scale(fac1));
      ret = ret.vadd(spawnArea.vec2.scale(fac2));
      return ret;
    }
  }
};

/**
 * Returns spawnFreq plus/minus at most spawnFreqVar.
 *
 * @method getNextSpawnTime
 * @param {Number} spawnFreq the mean spawn time.
 * @param {Number} spawnFreqVar how much the returned value can vary from
 * spawnFreq
 * @return {Number} The next spawn time.
 */
Game.prototype.getNextSpawnTime = function(spawnFreq, spawnFreqVar) {
  return spawnFreq + (-spawnFreqVar + Math.random() * spawnFreqVar * 2);
};

/**
 * Returns the next unique entity id for this game.
 *
 * @method getNextEID
 * @return {Number} the entity id.
 */
Game.prototype.getNextEID = function() {
  return this.NEXT_ENTITY_ID++;
};

/**
 * Returns true if this game is ready to start based on start conditions in
 * options.start.
 *
 * @method readyToStart
 * @return {Boolean} true if ready, false otherwise.
 */
Game.prototype.readyToStart = function() {
  var start = this.options.start;
  if (start.numClients === 0) return true;
  return Object.keys(this.clients).length === start.numClients;
};

/**
 * Alerts clients that the game is ready, and waits for a ready message from all
 * clients before resolving.
 *
 * @method getClientsReady
 * @return {Promise} resolves when all clients have sent their ready message, or
 * rejects if the game is not ready to start.
 *
 * @example
 *     game.getClientsReady().then(function () {
 *       game.start();
 *     });
 */
Game.prototype.getClientsReady = function() {
  var dfd = q.defer();

  if (!this.readyToStart()) dfd.reject();

  var readyClients = {};
  
  var numClients = Object.keys(this.clients).length;
  function testReady () {
    if (Object.keys(readyClients).length < numClients) return;
    console.log('GAME:        ', 'ALL CLIENTS READY');
    dfd.resolve();
  }

  for (var key in this.clients) {
    (function (client) {
      client.on('game:ready', function () {
        console.log('GAME:        ', 'CLIENT READY       ', client.name);
        readyClients[client.name] = true;
        client.off('game:ready');
        testReady();
      });
    })(this.clients[key]);
  }

  this.emitFn(this.id, 'game:ready', {});

  return dfd.promise;
};

/**
 * Starts the game running (ie. creates the world, notifies clients that the
 * game has started, schedules the update fn).
 *
 * @method start
 */
Game.prototype.start = function() {
  if (!this.readyToStart()) return false;
  this.status = Game.StatusCodes.STARTED;
  for (var key in this.clients) {
    (function (client) {
      client.on('game:turn-stop', function () {
        this.handleClientInput(client, {
          type: 'turn',
          turnType: Motherduck.TurnType.STOP
        });
      }.bind(this));

      client.on('game:turn-left', function () {
        this.handleClientInput(client, {
          type: 'turn',
          turnType: Motherduck.TurnType.LEFT
        });
      }.bind(this));

      client.on('game:turn-right', function () {
        this.handleClientInput(client, {
          type: 'turn',
          turnType: Motherduck.TurnType.RIGHT
        });
      }.bind(this));

      client.on('game:fire', function () {
        this.handleClientInput(client, { type: 'fire' });
      }.bind(this));

      client.on('game:dive', function () {
        this.handleClientInput(client, { type: 'dive' });
      }.bind(this));

      client.setStatus(Client.StatusCodes.GAME_STARTED);
    }.bind(this))(this.clients[key]);
  }
  this.map = Maps.loadMap(this.options.mapName, true);
  this.world = this.createWorld(this.map);
  
  this.gameTimer = this.options.gameTimer;
  
  var data = {
    mapName: this.options.mapName,
    mapObjects: {},
    mapProxies: this.map.proxies,
    motherducks: {},
    powerups: {},
    teamColours: this.teamColours,
    gt: this.gameTimer
  };

  for (var i = 0; i < this.map.entities.length; i++) {
    var e = this.map.entities[i];
    if (!e.hasType('v')) continue;
    var obj = {
      types: e.types,
      v: e.body.shapes[0].vertices,
      f: e.quads,
      p: e.body.position,
      q: e.body.quaternion,
    };
    if (e.uv) obj.uv = e.uv;
    if (e.material) obj.material = e.material;
    data.mapObjects[e.id] = obj;
  }

  this.entityStore.forEach('motherduck', function (motherduck) {
    data.motherducks[motherduck.id] = {
      type: 'motherduck',
      name: motherduck.name,
      team: motherduck.team,
      p: {
        x: motherduck.body.position.x,
        y: motherduck.body.position.y,
        z: motherduck.body.position.z
      },
      q: motherduck.body.quaternion
    };
  });
  this.emitFn(this.id, 'game:started', data);
  this.timeSinceLastEmit = 0;
  this.loop.start();
  return true;
};

/**
 * Stops the game loop, and destroys this game.
 *
 * @method destroy
 */
Game.prototype.destroy = function() {
  this.loop.stop();
  delete this.loop;
  delete this.emitFn;
};

/**
 * Creates a cannon.js world based on a map and this game's list of clients.
 * Only for internal use.
 *
 * @private
 * @method createWorld
 * @return {Object} The world object.
 */
Game.prototype.createWorld = function(map) {
  var world = new Cannon.World();
  world.solver.iterations = 20;
  world.gravity.set(0, -20, 0);

  // Materials
  world.defaultContactMaterial.friction = 0;
  world.defaultContactMaterial.restitution = 0;
  var mapMat = this.materials['map'] = new Cannon.Material('map');
  var ducklingMat = this.materials['duckling'] = new Cannon.Material('duckling');
  this.materials['powerup'] = new Cannon.Material('powerup');
  this.materials['projectile'] = new Cannon.Material('projectile');
  world.addContactMaterial(new Cannon.ContactMaterial(mapMat, ducklingMat, {
    restitution: 0.2
  }));

  for (var i = 0; i < map.entities.length; i++) {
    var entity = map.entities[i];

    entity.id = this.getNextEID();
    if (entity.hasType('ghost')) continue;
    if (entity.hasType('water')) entity.body.collisionFilterGroup = CollisionGroups.WATER;

    entity.body.material = this.materials['map'];
    world.add(entity.body);
    this.entityStore.push(entity);
  }

  this.initMotherducks(world, map);
  return world;
};

/**
 * Adds motherducks to the game based on start options.
 *
 * @method initMotherducks
 * @param {Object} world the Cannon world.
 * @param {Object} map the map object.
 */
Game.prototype.initMotherducks = function(world, map) {
  var motherduckMat = this.materials['entity'] = new Cannon.Material('entity');
  var options = {
    cannonOptions: {
      material: motherduckMat
    },
    speed: this.options.duckSpeed,
    turnSpeed: this.options.duckTurnSpeed,
    collideCb: this.onCollision,
    collideCbContext: this
  };

  // create a motherduck entity for each client
  if (this.options.start.numClients !== 0) {
    for (var key in this.clients) {
      var client = this.clients[key];
      var motherduck = new Motherduck(client.name, this.getNextEID(), options);
      world.addBody(motherduck.body);
      motherduck.addType('client');
      this.entityStore.push(motherduck);
    }
  }

  // create a motherduck entity for each AI
  var aiCount = this.AICount();
  var aiNames = _.sample(AI.names, aiCount);
  for (var i = 0; i < aiCount; i++) {
    var ai = new AI(aiNames[i], this.getNextEID(), options);
    world.addBody(ai.body);
    this.entityStore.push(ai);
  }

  var motherducks = this.entityStore.getForType('motherduck');
  this.setTeams(motherducks, this.options.start.teamSize);
  this.setStartPositions(motherducks, map.spawnPoints.motherducks,
                        this.options.randomMotherduckSpawnPoints);
};

/**
 * Returns the number of AI players required for this game based on the start
 * options.
 *
 * @method AICount
 * @return {Number} the number of AI players required.
 */
Game.prototype.AICount = function() {
  if (!this.options.start.fillWithAI) return 0;
  var aiCount = (this.options.start.teamSize * 2) - this.options.start.numClients;
  if (aiCount < 0) {
    return 0;
  } else {
    return aiCount;
  }
};

/**
 * For each motherduck in the motherducks param, sets motherduck.team to a team number.
 * Assumes that there are the correct number of motherducks for teamSize to
 * halve them. Only for internal use.
 *
 * @private
 * @method setTeams
 * @param {Array} motherducks a list of motherducks.
 * @param {Number} teamSize the team size.
 */
Game.prototype.setTeams = function(motherducks, teamSize) {
  var team = 0;
  var teamCount = 0;
  var numTeams = (motherducks.length)/teamSize;
  this.teamColours = TeamColours(numTeams);
  for (var i = 0; i < motherducks.length; i++) {
    var m = motherducks[i];
    m.team = team;
    teamCount++;
    if (teamCount >= teamSize) {
      teamCount = 0;
      team++;
    }
  }
};

/**
 * For each motherduck in the motherducks param, sets motherduck.position based
 * on the spawnPoints param. Assumes each motherduck has a cannon.js rigidbody
 * as its body property. Only for internal use.
 *
 * @private
 * @method setStartPositions
 * @param {Array} motherducks a list of motherducks.
 * @param {Array} spawnPoints a list of spawn positions.
 * @param {Boolean} random if true, the spawn position selected for each
 * motherduck is randomized.
 */
Game.prototype.setStartPositions = function(motherducks, spawnPoints, random) {
  var indices = _.range(spawnPoints.length);
  if (random) indices = _.shuffle(indices);

  for (var i = 0; i < motherducks.length; i++) {
    var m = motherducks[i];
    var idx = indices[i];

    var spawnPoint = spawnPoints[idx];

    m.body.position.copy(spawnPoint.position);
    /* the origin point of the entity is centred at its base. Therefore we move
    it up by half its height. */
    m.body.position.y += m.body.boundingRadius;

    // set rotation
    m.body.quaternion.copy(spawnPoint.quaternion);
  }
};

/**
 * Adds a client to the game, and sets the client's status to WAITING.
 *
 * @method addClient
 * @return {Promise} A promise that resolves to a Boolean (true if game is ready
 * to start).
 * @example
 *     game.addClient(client).then(function (readyToStart) {
 *       if (readyToStart) game.start();
 *     });
 */
Game.prototype.addClient = function(client) {
  var dfd = q.defer();

  this.clients[client.id()] = client;

  client.join(this.id, function () {
    client.gameId = this.id;
    client.setStatus(Client.StatusCodes.WAITING);
    dfd.resolve(this.readyToStart());
  }.bind(this));

  return dfd.promise;
};

/**
 * Removes a client from the game, and sets the client's status to LOBBY.
 *
 * @method removeClient
 * @return {Promise} A promise that resolves to the number of clients left in
 * the game after the client was removed.
 * @example
 *     game.removeClient(client).then(function (clientsRemaining) {
 *       if (clientsRemaining === 0) game.destroy();
 *     });
 */
Game.prototype.removeClient = function(client) {
  var dfd = q.defer();

  client.leave(this.id, function () {
    client.off('game:ready');
    client.off('game:turn-stop');
    client.off('game:turn-left');
    client.off('game:turn-right');
    client.off('game:fire');
    client.off('game:dive');
    delete this.clients[client.id()];
    client.setStatus(Client.StatusCodes.LOBBY, function () {
      dfd.resolve(Object.keys(this.clients).length);
    }.bind(this));
  }.bind(this));

  return dfd.promise;
};

/**
 * Stores a message from a connected client in an array for processing during
 * the update loop. Only for internal use.
 *
 * @private
 * @method handleClientInput
 * @param {Client} client the client that sent the message
 * @param {Object} message the message data.
 */
Game.prototype.handleClientInput = function(client, message) {
  message.clientName = client.name;
  console.log(message);
  this.messages.push(message);
};

/**
 * Returns an object with info about this game.
 *
 * @method getInfo
 * @return {Object} The info object containing gameId, gameStatus, clientNames,
 * and world.
 */
Game.prototype.getInfo = function() {
  return {
    gameId: this.id,
    gameStatus: this.status,
    clientNames: this.clientNames(),
    world: this.world
  };
};

/**
 * Returns a list of names of clients in this game.
 *
 * @method clientNames
 * @return {Array} the list of client names.
 */
Game.prototype.clientNames = function() {
  var ret = [];

  for (var key in this.clients) {
    var client = this.clients[key];
    ret.push(client.name);
  }

  return ret;
};

Game.prototype.emitSpawn = function (spawnInfo) {
  this.emitFn(this.id, 'game:spawn', spawnInfo);
};