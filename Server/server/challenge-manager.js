module.exports = ChallengeManager;

var Server = require('./server');
var User = require('./database/user');
var FriendList = require('./database/friend-list');
var ErrorHandler = require('./error-handler');

/**
 * Responsible for managing challenges between users.
 *
 * @class ChallengeManager
 * @constructor
 * @param {Server} server The server instance.
 * @param {SessionManager} sessionManager The sessionManager instance.
 */
function ChallengeManager (server, sessionManager) {
  server.on(Server.CONNECT, this.onClientConnect.bind(this));
  server.on(Server.DISCONNECT, this.onClientDisconnect.bind(this));
  this.sessionManager = sessionManager;
  this.challenges = {};
}

/**
 * Register challenge related callbacks when a client connects.
 *
 * @method onClientConnect
 * @param {Client} client The data for the connected client
 */
ChallengeManager.prototype.onClientConnect = function(client) {
  client.on('challenge:sent', function(data) {
    this.onChallengeSent(client, data);
  }.bind(this));

  client.on('challenge:accepted', function (data) {
    this.onChallengeAccepted(client, data);
  }.bind(this));
};

/**
 * Unregister challenge related callbacks when a client disconnects.
 *
 * @method onClientDisconnect
 * @param {Client} client The data for the disconnecting client
 */
ChallengeManager.prototype.onClientDisconnect = function(client) {
  this.removeAllChallenges(client);
};

/**
 * Stores a challenge request in the database and updates the sender and receiver.
 *
 * @method onChallengeSent
 * @param {Client} client The data for the client sending the challenge
 * @param {Object} data The data for the client receiving the challenge
 */
ChallengeManager.prototype.onChallengeSent = function(client, data) {
  if (!this.sessionManager.inSession(client.name)) return;
  User.findByUsername(client.name, function (err, from) {
    if (!from) return ErrorHandler.emit(client, 'challenge:sent', new Error('Something went wrong.'));
    User.findByUsername(data.to, function (err, to) {
      if (!to) return ErrorHandler.emit(client, 'challenge:sent', new Error('Other user doesn\'t exist'));
      var toClient = this.sessionManager.getSessionClientByName(to.username);
      if (!toClient) return ErrorHandler.emit(client, 'challenge:sent', new Error('Other user not online'));
      if (this.challengeExists(client, toClient)) return ErrorHandler.emit(client, 'challenge:sent', new Error('Challenge already sent to this user.'));
      this.addChallenge(client, toClient);
      toClient.emit('challenge:sent', { from: from.username });
      client.emit('challenge:sent-success', { to: data.to });
    }.bind(this));
  }.bind(this));
};

/**
 * Updates both clients and removes the challenge when a challenge has been accepted.
 *
 * @method onChallengeAccepted
 * @param {Client} client The data for the client accepting the challenge
 * @param {Object} data The data for the client who sent the challenge
 */
ChallengeManager.prototype.onChallengeAccepted = function(client, data) {
  if (!this.sessionManager.inSession(client.name)) return;
  User.findByUsername(client.name, function (err, to) {
    if (!to) return ErrorHandler.emit(client, 'challenge:accepted', new Error('Something went wrong.'));
    User.findByUsername(data.from, function (err, from) {
      if (!from) return ErrorHandler.emit(client, 'challenge:accepted', new Error('Other user doesn\'t exist'));
      var fromClient = this.sessionManager.getSessionClientByName(from.username);
      if (!fromClient) return ErrorHandler.emit(client, 'challenge:accepted', new Error('Other user not online'));
      if (!this.challengeExists(fromClient, client)) return ErrorHandler.emit(client, 'challenge:accepted', new Error('Challenge does not exist anymore.'));
      this.removeChallenge(fromClient, client);
      fromClient.emit('challenge:accepted', { to: to.username });
      client.emit('challenge:accepted-success', { from: from.username });
    }.bind(this));
  }.bind(this));
};

/**
 * Check whether a challenge exists between two users.
 *
 * @method challengeExists
 * @param {Client} client The data for the client who sent the challenge
 * @param {Object} data The data for the client the challenge was sent to
 * @return {Boolean} True if the challenge exists.
 */
ChallengeManager.prototype.challengeExists = function(from, to) {
  var list = this.challenges[from.id()];
  if (!list) return false;
  return list.indexOf(to.id()) !== -1;
};

/**
 * Remove a challenge from the database. Assumes the challenge does not already exist.
 *
 * @method addChallenge
 * @param {Client} from The user to who sent the challenge
 * @param {Client} to The user to who the challenge was sent to
 */
ChallengeManager.prototype.addChallenge = function(from, to) {
  var fromId = from.id();
  this.challenges[fromId] = this.challenges[fromId] || [];
  this.challenges[fromId].push(to.id());
};

/**
 * Remove a challenge from the database. Assumes the challenge exists.
 *
 * @method removeChallenge
 * @param {Client} from The user to who sent the challenge
 * @param {Client} to The user to who the challenge was sent to
 */
ChallengeManager.prototype.removeChallenge = function(from, to) {
  var list = this.challenges[from.id()];
  var idx = list.indexOf(to.id());
  list.splice(idx, 1);
};

/**
 * Remove all challenges send from a user.
 *
 * @method removeAllChallenges
 * @param {Client} from The user to remove the challenges from
 */
ChallengeManager.prototype.removeAllChallenges = function(from) {
  var list = this.challenges[from.id()];
  if (list) list.length = 0;
};