module.exports = function (target) {
  statesMixin.call(target);
};

/**
 * Accepts any number of strings as parameters. Returns true iff the object
 * has all of the states.
 *
 * @method hasAllStates
 * @return {Boolean} true iff the object has all of the states passed.
 */
function hasAllStates () {
  for (var i = 0; i < arguments.length; i++) {
    var state = arguments[i];
    if (!this.hasState(state)) return false;
  }
  return true;
}

/**
 * Accepts any number of strings as parameters. Returns true if the object
 * has any of the states.
 *
 * @method hasAnyStates
 * @return {Boolean} true if the object has any of the states passed.
 */
function hasAnyStates () {
  for (var i = 0; i < arguments.length; i++) {
    var state = arguments[i];
    if (this.hasState(state)) return true;
  }
  return false;
}

/**
 * Checks whether an object has a specific state.
 *
 * @method hasState
 * @param {String} state The state to check for.
 * @return {Boolean} true if the object has the state passed.
 */
function hasState (state) {
  return this.states.indexOf(state) !== -1;
}

/**
 * Adds a state to an object.
 *
 * @method addState
 * @param {String} state The state to add.
 */
function addState (state) {
  if (this.states.indexOf(state) !== -1) return; // already added
  this.states.push(state);
}

/**
 * Removes a state to an object.
 *
 * @method removeState
 * @param {String} state The state to remove.
 */
function removeState (state) {
  var idx = this.states.indexOf(state);
  if (idx !== -1) this.states.splice(idx, 1);
}

var statesMixin = (function () {
  return function () {
    this.states = [];
    this.hasAllStates = hasAllStates;
    this.hasAnyStates = hasAnyStates;
    this.hasState = hasState;
    this.addState = addState;
    this.removeState = removeState;
    return this;
  };
})();