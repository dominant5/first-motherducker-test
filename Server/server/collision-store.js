module.exports = CollisionStore;

/**
 * An object used to store collisions for later use.
 *
 * @class CollisionStore
 * @constructor
 */
function CollisionStore () {
  /**
   * A list of all collisions in order.
   *
   * @property collisions
   * @type {Array}
   */
  this.collisions = [];

  /**
   * A map from key(eTypeA, eTypeB) to collisions between entities of those
   * types.
   *
   * @property pairMap
   * @type {Object}
   */
  this.pairMap = {};
  
  /**
   * A map from key(eTypeA) to all collisions for that typ.
   *
   * @property singleMap
   * @type {Object}
   */
  this.singleMap = {};
}

/**
 * Returns the number of collisions in this store.
 *
 * @method count
 * @return {Number} the number of collisions.
 */
CollisionStore.prototype.count = function() {
  return this.collisions.length;
};

/**
 * Adds a collision to this store. Puts it in the general list, and also in a
 * map based on the entityA and entityB's type. Stores an object with the
 * following structure:
 *
 *     var collision = {
 *       entityA: entityA,
 *       entityB: entityB,
 *       contact: contact,
 *       
 *       // for convenient access in forEach
 *       entityA.eType: entityA
 *       entityB.eType: entityB
 *     }
 *
 * @method push
 * @param {Object} entityA the first entity in the collision
 * @param {Object} entityB the second entity in the collision
 * @param {Object} contact the cannon.js contact
 */
CollisionStore.prototype.push = function(entityA, entityB, contact) {
  var collision = {
    entityA: entityA,
    entityB: entityB,
    contact: contact
  };

  // for convenient access in forEach
  for (var i = 0; i < entityA.types.length; i++) {
    collision[entityA.types[i]] = entityA;
  }
  for (var i = 0; i < entityB.types.length; i++) {
    collision[entityB.types[i]] = entityB;
  }

  for (var i = 0; i < entityA.types.length; i++) {
    var aType = entityA.types[i];
    this.singleMap[aType] = this.singleMap[aType] || [];
    this.singleMap[aType].push(collision);
    for (var j = 0; j < entityB.types.length; j++) {
      var bType = entityB.types[j];
      var key = this.key(aType, bType);
      this.pairMap[key] = this.pairMap[key] || [];
      this.pairMap[key].push(collision);
    }
  }
  for (var j = 0; j < entityB.types.length; j++) {
    var bType = entityB.types[j];
    if (!entityA.hasType(bType)) {
      this.singleMap[bType] = this.singleMap[bType] || [];
      this.singleMap[bType].push(collision);
    }
  }
  this.collisions.push(collision);
};

/**
 * Returns the list of collisions for a particular pair of entity types. The
 * list is in order of insertion. If no list was found, returns an empty array.
 *
 * @method getForTypePair
 * @param {String} eTypeA the first entity type
 * @param {String} eTypeB the second entity type
 * @return {Array} the list
 */
CollisionStore.prototype.getForTypePair = function(eTypeA, eTypeB) {
  var ret = this.pairMap[this.key(eTypeA, eTypeB)];
  if (!ret) ret = [];
  return ret;
};

/**
 * Returns the list of collisions for a particular entity type. The
 * list is in order of insertion. If no list was found, returns an empty array.
 *
 * @method getForType
 * @param {String} eType the entity type
 * @return {Array} the list
 */
CollisionStore.prototype.getForType = function(eType) {
  var ret = this.singleMap[eType];
  if (!ret) ret = [];
  return ret;
};

/**
 * Iterates over a list of collisions between eTypeA and eTypeB, calling fn for
 * each one and passing the collision. If the first parameter is a function,
 * then forEach iterates over the list of all collisions (and ignores further
 * params).
 *
 * @method forEach
 * @param {String or Function} eTypeA the first entity type (or a function if
 * iterating over all collisions)
 * @param {String} eTypeB the second entity type
 * @param {Function} fn the callback function.
 * @example
 *     store.forEach('motherduck', 'duckling', function (collision) {
 *       // iterates over collisions between motherduck and duckling entities
 *       console.log(collision.motherduck.position, collision.duckling.position);
 *     });
 *
 *     store.forEach(function (collision) {
 *       // iterates over ALL collisions in the store
 *     });
 */
CollisionStore.prototype.forEach = function(eTypeA, eTypeB, fn) {
  var list, func;
  if (typeof eTypeA === 'function') {
    list = this.collisions;
    func = eTypeA;
  } else if (typeof eTypeB === 'function') {
    list = this.getForType(eTypeA);
    func = eTypeB;
  } else {
    list = this.getForTypePair(eTypeA, eTypeB);
    func = fn;
  }
  for (var i = 0, l = list.length; i < l; i++) {
    func(list[i]);
  }
};

/**
 * Clears the store of all collisions.
 *
 * @method clear
 */
CollisionStore.prototype.clear = function() {
  delete this.collisions;
  this.collisions = [];
  delete this.pairMap;
  this.pairMap = {};
  delete this.singleMap;
  this.singleMap = {};
};

/**
 * Returns a string for the collisions map representing eTypeA and eTypeB.
 * Concatenates eTypeA and eTypeB in alphabetical order.
 *
 * @method key
 * @param {String} eTypeA the first entity type
 * @param {String} eTypeB the second entity type
 * @return {String} the key
 */
CollisionStore.prototype.key = function(eTypeA, eTypeB) {
  return eTypeA < eTypeB? eTypeA + eTypeB : eTypeB + eTypeA;
};