module.exports = exp = TeamColours;

var Cannon = require('cannon'),
_ = require('underscore');

function TeamColours (numTeams) {
  return _.sample(TeamColours.colourList, numTeams);
}

TeamColours.colourList = [
  {
    teamColour: new Cannon.Vec3(231, 231, 231),
    textColour: new Cannon.Vec3(0, 0, 0),
    colourName: 'White'
  },
  {
    teamColour: new Cannon.Vec3(255, 69, 0),
    textColour: new Cannon.Vec3(255, 255, 255),
    colourName: 'Orange'
  },
  {
    teamColour: new Cannon.Vec3(21, 21, 21),
    textColour: new Cannon.Vec3(255, 255, 255),
    colourName: 'Black'
  },
  {
    teamColour: new Cannon.Vec3(43, 25, 25),
    textColour: new Cannon.Vec3(255, 255, 255),
    colourName: 'Brown'
  },
  {
    teamColour: new Cannon.Vec3(0, 44, 255),
    textColour: new Cannon.Vec3(255, 255, 255),
    colourName: 'Blue'
  },
  {
    teamColour: new Cannon.Vec3(178, 0, 0),
    textColour: new Cannon.Vec3(255, 255, 255),
    colourName: 'Red'
  },
  {
    teamColour: new Cannon.Vec3(0, 83, 7),
    textColour: new Cannon.Vec3(255, 255, 255),
    colourName: 'Green'
  },
  {
    teamColour: new Cannon.Vec3(191, 26, 218),
    textColour: new Cannon.Vec3(255, 255, 255),
    colourName: 'Pink'
  },
];