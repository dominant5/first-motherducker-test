module.exports = EntityStore;

/**
 * An object used to store game entities.
 *
 * @class EntityStore
 * @constructor
 */
function EntityStore () {
  /**
   * A list of all entities.
   *
   * @property entities
   * @type {Array}
   */
  this.entities = [];

  /**
   * A map from entity.type to a list of entities of that type.
   *
   * @property typeMap
   * @type {Object}
   */
  this.typeMap = {};

  /**
   * A map from entity.id to the entity associated with that id.
   *
   * @property idMap
   * @type {Object}
   */
  this.idMap = {};

  /**
   * A map from entity.name to the entity associated with that name.
   *
   * @property nameMap
   * @type {Object}
   */
  this.nameMap = {};

  /**
   * If true, this entity store will not mutate itself via the push or remove
   * functions, and will defer those operations until unlock() is called.
   *
   * @property locked
   * @type {Boolean}
   */
  this.locked = false;

  /**
   * A temp container for entities that should be added, but were added while
   * this store was locked.
   *
   * @property toPush
   * @type {Array}
   */
  this.toPush = [];

  /**
   * A temp container for entities that should be removed, but were removed
   * while this store was locked.
   *
   * @property toRemove
   * @type {Array}
   */
  this.toRemove = [];
}

/**
 * Returns the number of entities in this store.
 *
 * @method count
 * @return {Number} the number of entities.
 */
EntityStore.prototype.count = function() {
  return this.entities.length;
};

/**
 * Adds an entity to this store. Puts it in the general list, and also in a map
 * based on the entity's type(s). If entity.id is defined, the entity is also
 * stored in the typeMap (can be accessed with getById). If entity.name is
 * defined, the entity is also stored in the nameMap (can be accessed with
 * getByName).
 *
 * @method push
 * @param {Object} entity the entity
 * @example
 *     var motherduck = new Motherduck('Client123');
 *     console.log(motherduck.id); // -> 0 (or some Number)
 *     store.push(motherduck);
 *     store.getById(0); // returns motherduck
 *     store.getByName('Client123'); // returns motherduck
 */
EntityStore.prototype.push = function(entity) {
  if (this.locked) {
    this.toPush.push(entity);
    return;
  }

  for (var i = 0, len = entity.types.length; i < len; i++) {
    var type = entity.types[i];
    this.typeMap[type] = this.typeMap[type] || [];
    this.typeMap[type].push(entity);
  }
  if (typeof entity.id !== 'undefined') this.idMap[entity.id] = entity;
  if (typeof entity.name !== 'undefined') this.nameMap[entity.name] = entity;
  this.entities.push(entity);
};

/**
 * Removes entity from this store.
 *
 * @method delete
 * @param {Object} entity the entity to remove from the store.
 */
EntityStore.prototype.remove = function(entity) {
  if (this.locked) {
    this.toRemove.push(entity);
    return;
  }

  var eIdx = this.entities.indexOf(entity);
  if (eIdx !== -1) this.entities.splice(eIdx, 1);

  for (var i = 0, len = entity.types.length; i < len; i++) {
    var typeArr = this.typeMap[entity.types[i]];
    if (typeArr) {
      var tIdx = typeArr.indexOf(entity);
      if (tIdx !== -1) typeArr.splice(tIdx, 1);
    }
  }

  if (typeof entity.id !== 'undefined') delete this.idMap[entity.id];
  if (typeof entity.name !== 'undefined') delete this.nameMap[entity.name];
};

/**
 * Returns the entity whose id is the id param passed.
 *
 * @method getById
 * @param id the entity id.
 * @return {Object} the entity, or undefined if not found.
 * @example
 *     var motherduck = new Motherduck();
 *     console.log(motherduck.id); // -> 0 (or some Number)
 *     store.push(motherduck);
 *     store.getById(0); // returns motherduck
 */
EntityStore.prototype.getById = function(id) {
  return this.idMap[id];
};

/**
 * Returns the entity whose name is the name param passed.
 *
 * @method getById
 * @param {String} name the entity name.
 * @return {Object} the entity, or undefined if not found.
 * @example
 *     var motherduck = new Motherduck('Client123');
 *     store.push(motherduck);
 *     store.getByName('Client123'); // returns motherduck
 */
EntityStore.prototype.getByName = function(name) {
  return this.nameMap[name];
};

/**
 * Returns the list of entities for a particular entity type. The list is in
 * order of insertion. If no list was found, returns an empty array.
 *
 * @method getForType
 * @param {String} type the entity type
 * @return {Array} the list
 */
EntityStore.prototype.getForType = function(type) {
  return this.typeMap[type] || [];
};

function forEachSimple (list, fn, store) {
  store.lock();
  for (var i = 0, len = list.length; i < len; i++) {
    fn(list[i]);
  }
  store.unlock(true);
}

function forEachComplex (list, fn, options, store) {
  store.lock();
  var testAny = options.any && options.any.length > 0;
  var testAll = options.all && options.all.length > 0;
  var testNot = options.not && options.not.length > 0;
  for (var i = 0, len = list.length; i < len; i++) {
    var e = list[i];
    if (testAny && !e.hasAnyTypes.apply(e, options.any)) continue;
    if (testAll && !e.hasAllTypes.apply(e, options.all)) continue;
    if (testNot && e.hasAnyTypes.apply(e, options.not)) continue;
    fn(e);
  }
  store.unlock(true);
}

/**
 * Iterates over a list of entities. calling fn for each one and passing the
 * entity. Can be called in a variety of ways, see example code.
 *
 * @method forEach
 * @example
 *     store.forEach('motherduck', function (motherduck) {
 *       // iterates over motherduck entities
 *     });
 *
 *     store.forEach(['motherduck', 'duckling'], function (entity) {
 *       // iterates over entities with any of the types in the array
 *       // (in this case, motherducks and ducklings)
 *     });
 *
 *     store.forEach({ none: ['map'] }, function (entity) {
 *       // iterates over all entities except those with the type 'map'
 *     });
 *
 *     store.forEach({ any: ['map'], none: ['hazard'] }, function (entity) {
 *       // iterates over all map entities except for those that also have the
 *       // hazard type.
 *     });
 *
 *     store.forEach({ all: ['map', 'hazard'] }, function (entity) {
 *       // iterates over all entities that have 'map' and 'hazard' types
 *     });
 *
 *     store.forEach(function (entity) {
 *       // iterates over ALL entities in the store
 *     });
 */
EntityStore.prototype.forEach = function() {
  this.lock();

  var argLen = arguments.length;
  if (argLen === 0) return;

  var arg0 = arguments[0];
  var arg1 = arguments[1];

  if (typeof arg0 === 'function') {
    forEachSimple(this.entities, arg0, this);
  }

  else if (typeof arg0 === 'string' && typeof arg1 === 'function') {
    forEachSimple(this.getForType(arg0), arg1, this);
  }

  else if (arg0.constructor === Array) {
    forEachComplex(this.entities, arg1, { any: arg0 }, this);
  }

  else if (typeof arg0 === 'object') {
    forEachComplex(this.entities, arg1, arg0, this);
  }
};

/**
 * Locks the entity store, so that push/remove operations are deferred until
 * unlock() is called. Used by forEach.
 *
 * @private
 * @method lock
 */
EntityStore.prototype.lock = function() {
  this.locked = true;
};

/**
 * Unlocks the entity store, so that push/remove operations are allowed to
 * complete. If the apply param is true, any push/remove operations attempted
 * while the store was locked are applied. If false, those operations are 
 * discarded.
 *
 * @method unlock
 * @param {Boolean} apply whether to apply push/remove operations attempted
 * while the store was locked.
 */
EntityStore.prototype.unlock = function(apply) {
  this.locked = false;
  if (apply) {
    for (var i = 0, len = this.toPush.length; i < len; i++) {
      this.push(this.toPush[i]);
    }
    for (var i = 0, len = this.toRemove.length; i < len; i++) {
      this.remove(this.toRemove[i]);
    }
  }
  this.toPush = [];
  this.toRemove = [];
};

/**
 * Clears the store of all entities.
 *
 * @method clear
 */
EntityStore.prototype.clear = function() {
  delete this.entities;
  this.entities = [];
  delete this.typeMap;
  this.typeMap = {};
  delete this.idMap;
  this.idMap = {};
  delete this.nameMap;
  this.nameMap = {};
  this.locked = false;
  this.toPush = [];
  this.toRemove = [];
};
