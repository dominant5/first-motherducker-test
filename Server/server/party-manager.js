module.exports = PartyManager;

var User = require('./database/user');
var SessionManager = require('./session-manager');
var FriendList = require('./database/friend-list');
var ErrorHandler = require('./error-handler');
var EmitterMixin = require('./emitter-mixin');

function PartyManager (server, sessionManager) {
  this.server = server;
  sessionManager.on(SessionManager.EventCodes.STARTED, this.onSessionStarted, this);
  sessionManager.on(SessionManager.EventCodes.ENDED, this.onSessionEnded, this);
  this.sessionManager = sessionManager;
  this.parties = {};
  EmitterMixin(this);
}

PartyManager.EventCodes = {
  PARTY_CREATED: 0,
  PARTY_CLOSED: 1,
  MEMBER_JOINED: 2,
  MEMBER_LEFT: 3
};

PartyManager.StatusCodes = {
  NOT_IN_PARTY: 0,
  PARTY_LEADER: 1,
  PARTY_MEMBER: 2
};

/**
 * Registers party related callbacks on the client.
 *
 * @method onSessionStarted
 * @param {Client} client The client that just started a session.
 */
PartyManager.prototype.onSessionStarted = function(client) {
  client.on('party:invite', function (data) {
    this.sendInvite(client, data.name);
  }.bind(this));

  client.on('party:join', function (data) {
    this.joinParty(client, data.leader);
  }.bind(this));

  client.on('party:leave', function () {
    this.leaveParty(client, client.name);
  }.bind(this));

  client.on('party:kick', function (data) {
    this.kickFromParty(client, data.name);
  }.bind(this));
};

/**
 * Stop a client from listening for party related emits and removes them from any party they are in.
 *
 * @method onSessionEnded
 * @param {Client} client The client ending their session.
 * @param {String} sessionName The name of the client (I'm pretty sure, idk sue me).
 */
PartyManager.prototype.onSessionEnded = function(client, sessionName) {
  client.off('party:invite');
  client.off('party:join');
  client.off('party:leave');
  client.off('party:kick');
  this.leaveParty(client, sessionName);
};

/**
 * Send a party invite to a client.
 *
 * @method sendInvite
 * @param {Client} inviter The client sending the invite.
 * @param {String} friendName The name of the client receiving the invite.
 */
PartyManager.prototype.sendInvite = function(inviter, friendName) {
  if (!this.sessionManager.inSession(inviter.name)) return;
  if (inviter.name === friendName) return ErrorHandler.emit(inviter, 'party:invite', new Error('Can\'t invite yourself to your party'));
  if (this.getPartyIdByMemberId(inviter.id())){
    return ErrorHandler.emit(inviter, 'party:invite', new Error('Can only send invites if you are the party leader or not in a party'));
  }

  function success (hasFriend) {
    if (!hasFriend) return ErrorHandler.emit(inviter, 'party:invite', new Error('You are not friends with this user'));
    var invitee = this.sessionManager.getSessionClientByName(friendName);
    if (!invitee) return ErrorHandler.emit(inviter, 'party:invite', new Error('Other user not online'));
    var partyId = inviter.id();
    var requesteeId = invitee.id();
    var party = this.partyExists(partyId)? this.parties[partyId] : this.createParty(partyId, inviter.name);
    party.invites[requesteeId] = friendName;
    invitee.emit('party:invite', { leader: inviter.name });
    inviter.emit('party:invite-sent', { to: friendName });
    inviter.partyStatus = PartyManager.StatusCodes.PARTY_LEADER;
  }

  function error (err) {
    
  }

  FriendList.hasFriend(inviter.name, friendName)
  .then(success.bind(this), error.bind(this));
};

/**
 * Put a client into a party.
 *
 * @method joinParty
 * @param {Client} client The client joining the party.
 * @param {String} leaderName The name of the leader of the party.
 */
PartyManager.prototype.joinParty = function(client, leaderName) {
  if (!this.sessionManager.inSession(client.name)) return;
  var leader = this.sessionManager.getSessionClientByName(leaderName);
  if (!leader) return ErrorHandler.emit(client, 'party:join', new Error('Leader not online'));
  var partyId = leader.id();
  var clientId = client.id();
  if (!this.partyExists(partyId)) return ErrorHandler.emit(client, 'party:join', new Error('Party doesn\'t exist'));
  var party = this.parties[partyId];
  if (Object.keys(party.members).length + 1 >= 4) {
    return ErrorHandler.emit(client, 'party:join', new Error('The party is full (maximum 4)'));
  }
  if (clientId in party.members) {
    return ErrorHandler.emit(client, 'party:join', new Error('You are already in this party'));
  }
  if (!(clientId in party.invites)) {
    return ErrorHandler.emit(client, 'party:join', new Error('You haven\'t been invited to this party'));
  }

  if (this.partyExists(clientId) || this.getPartyIdByMemberId(clientId)) {
    this.leaveParty(client, client.name);
  }

  client.join(partyId, function () {
    client.partyStatus = PartyManager.StatusCodes.PARTY_MEMBER;
    party.members[clientId] = client.name;
    delete party.invites[clientId];
    this.server.to(partyId, 'party:joined', {
      leader: leaderName,
      name: client.name,
      members: this.getPartyMemberNames(partyId)
    });
    this.emit(PartyManager.EventCodes.MEMBER_JOINED, party, client);
  }.bind(this));
};

/**
 * Remove a client from a party.
 *
 * @method leaveParty
 * @param {Client} client The client leaving the party.
 * @param {String} name The name of the client leaving the party.
 */
PartyManager.prototype.leaveParty = function(client, name) {
  if (this.partyExists(client.id())) return this.closeParty(client.id()); // client is the leader
  var partyId = this.getPartyIdByMemberId(client.id());
  if (!partyId) return; // client is not in a party
  var party = this.parties[partyId];
  this.server.to(partyId, 'party:left', { leader: party.leaderName, name: name });
  client.leave(partyId, function () {
    client.partyStatus = PartyManager.StatusCodes.NOT_IN_PARTY;
    delete party.members[client.id()];
    if (Object.keys(party.members).length === 0) this.closeParty(partyId);
    this.emit(PartyManager.EventCodes.MEMBER_LEFT, party, client);
  }.bind(this));
};

/**
 * Kick a client from a party.
 *
 * @method kickFromParty
 * @param {Client} leader The leader of the party.
 * @param {String} name The name of the client being kicked from the party.
 */
PartyManager.prototype.kickFromParty = function(leader, name) {
  if (this.getPartyIdByMemberId(leader.id()) !== null) return ErrorHandler.emit(leader, 'party:kick', new Error('Only the party leader can kick members'));
  var partyId = leader.id();
  if (!this.partyExists(partyId)) return ErrorHandler.emit(leader, 'party:kick', new Error('You are not in a party'));
  var party = this.parties[partyId];
  var memberId = null;
  // Iterate over the party members, assign memberId if the kickee is in the party
  for (var id in party.members) {
    var memberName = party.members[id];
    if (memberName !== name) continue;
    memberId = id;
    break;
  }
  if (!memberId) return ErrorHandler.emit(leader, 'party:kick', new Error('The given user is not in your party'));
  var client = this.sessionManager.getSessionClientByName(name);
  if (!client) return ErrorHandler.emit(leader, 'party:kick', new Error('The given user is not online'));
  this.server.to(partyId, 'party:kicked', { leader: party.leaderName, name: name });
  client.leave(partyId, function () {
    client.partyStatus = PartyManager.StatusCodes.NOT_IN_PARTY;
    delete party.members[memberId];
    if (Object.keys(party.members).length === 0) this.closeParty(partyId);
  }.bind(this));
};

/**
 * Check if a party exists.
 *
 * @method partyExists
 * @param {String} partyId The id of the party to search for.
 * @return {Boolean} True if a party with id = partyId exists.
 */
PartyManager.prototype.partyExists = function(partyId) {
  return partyId in this.parties;
};

/**
 * Find the party that a client is in.
 *
 * @method getPartyIdByMemberId
 * @param {String} memberId We want to find a party that has a client with this id in it.
 * @return {String} The id of the party that has the client in it. Null if no party exists.
 */
PartyManager.prototype.getPartyIdByMemberId = function(memberId) {
  for (var partyId in this.parties) {
    var party = this.parties[partyId];
    if (memberId in party.members) return partyId;
  }
  return null;
};

/**
 * Get the list of clients in a specific party.
 *
 * @method getPartyMemberClients
 * @param {String} partyId The id of the party of which we want the clients.
 * @return {Array} The list of party members. Null if no party exists with that id.
 */
PartyManager.prototype.getPartyMemberClients = function(partyId) {
  if (!this.partyExists(partyId)) return null;
  var party = this.parties[partyId];
  var ret = [];
  
  var leader = this.sessionManager.getSessionClientByName(party.leaderName);
  if (leader) ret.push(leader);

  for (var key in party.members) {
    (function (name) {
      var client = this.sessionManager.getSessionClientByName(name);
      if (!client) return;
      ret.push(client);
    }.bind(this))(party.members[key]);
  }

  return ret;
};

/**
 * Get the list of names of clients in a specific party.
 *
 * @method getPartyMemberNames
 * @param {String} partyId The id of the party of which we want the clients.
 * @return {Array} The list of party member's names. Null if no party exists with that id.
 */
PartyManager.prototype.getPartyMemberNames = function(partyId) {
  if (!this.partyExists(partyId)) return null;
  var party = this.parties[partyId];
  var ret = [];
  ret.push(party.leaderName);
  for (var key in party.members) {
    ret.push(party.members[key]);
  }
  return ret;
};

/**
 * Create a new party. Assumes party does not exist already.
 *
 * @method createParty
 * @param {String} partyId The id of the new party.
 * @param {String} leaderName The name of the leader of the new party.
 * @return {Object} The newly created party.
 */
PartyManager.prototype.createParty = function(partyId, leaderName) {
  this.parties[partyId] = {
    members: {},
    invites: {},
    leaderName: leaderName
  };
  var leader = this.sessionManager.getSessionClientByName(leaderName);
  if (leader) {
    leader.join(partyId, function () {
      leader.partyStatus = PartyManager.StatusCodes.PARTY_MEMBER;
    }.bind(this));
  }

  this.emit(PartyManager.EventCodes.PARTY_CREATED, this.parties[partyId]);
  return this.parties[partyId];
};

/**
 * Deletes a party. Only deletes the party, doesn't send any events.
 *
 * @method deleteParty
 * @param {String} partyId The id of the party to delete.
 */
PartyManager.prototype.deleteParty = function(partyId) {
  delete this.parties[partyId];
};

/**
 * Closes a party, updates all the members and deletes the party.
 *
 * @method deleteParty
 * @param {String} partyId The id of the party to close.
 */
PartyManager.prototype.closeParty = function(partyId) {
  var party = this.parties[partyId];
  if (!party) return;
  this.server.to(partyId, 'party:closed', { leader: party.leaderName });
  var leader = this.sessionManager.getSessionClientByName(party.leaderName);
  if (leader) {
    leader.leave(partyId, function () {
      leader.partyStatus = PartyManager.StatusCodes.NOT_IN_PARTY;
    });
  }
  for (var key in party.members) {
    (function (name) {
      var client = this.sessionManager.getSessionClientByName(name);
      if (!client) return;
      client.leave(partyId, function () {
        client.partyStatus = PartyManager.StatusCodes.NOT_IN_PARTY;
      });
    }.bind(this))(party.members[key]);
  }
  this.emit(PartyManager.EventCodes.PARTY_CLOSED, party);
  this.deleteParty(partyId);
};