var exp = module.exports = {};

var Cannon = require('cannon'),
TypesMixin = require('./types-mixin'),
StatesMixin = require('./states-mixin'),
merge = require('deepmerge'),
Projectile = require('./projectile');

/**
 * A fireable is the object attached to a motherduck when they pick up
 * a powerup which gives them an activated ability. Each fireable has
 * its own constructor which merges the default options, sets variables
 * and functions from those options, and adds the types to the fireable.
 * The default options for each fireable define any variables used, as
 * well as the fireFn which will be called when the motherduck uses that
 * fireable's ability.
 */

/**
 * The generic fireable object.
 *
 * @class Fireable
 * @constructor
 * @param {String} id The ID of this fireable.
 * @param {Object} passedOptions The options to be used for this fireable.
 * @param {String} type The type to be attached to the fireable.
 */
exp.Fireable = Fireable = function (id, passedOptions, type) {
  this.options = passedOptions;

  TypesMixin(this);
  this.id = id;
  this.ammunition = this.options.ammunition;
  this.addType('fireable');
  if (type) {
    this.addType(type);
    this.fireableType = type;
  }
  this.alive = true;
  this.target = this.options.target;
  StatesMixin(this);

  return this;
};

/**
 * Returns the type of this fireable that's not 'fireable', eg. 'steal'
 *
 * @method getType
 * @return {String} the type
 */
Fireable.prototype.getType = function() {
  return this.types[1];
};

/**
 * The steal fireable object.
 *
 * @class StealFireable
 * @constructor
 * @param {String} id The ID of this fireable.
 * @param {Object} passedOptions The options to be used for this fireable.
 */
exp.StealFireable = StealFireable = function (id, passedOptions) {
  var options = merge(StealFireable.defaultOptions, passedOptions || {});
  var ret = new Fireable(id, options, 'steal');
  ret.ammunition = options.ammunition;
  ret.fireFn = options.fireFn;
  return ret;
};

/**
 * The default steal fireable options, the fire function is defined here.
 *
 * @property StealFireable.defaultOptions
 * @type {Object}
 */
StealFireable.defaultOptions = {
  ammunition: 3,
  /**
   * The function to be called when a player fires a steal projectile.
   *
   * @method fireFn
   * @param {Game} game The game instance
   */
  fireFn: function(game) {
    this.ammunition -= 1;
    
    var projectileOpts = {
      cannonOptions: { material: game.projectileMat },
      collideCb: game.onCollision,
      collideCbContext: game
    };
    var projectileID = game.getNextEID();
    var projectile = new Projectile.StealProjectile(projectileID, projectileOpts);
    var targetPos = this.target.body.position;
    var targetQuat = this.target.body.quaternion;
    
    projectile.options.cannonOptions = { material: game.projectileMat };
    projectile.options.collideCb = game.onCollision;
    projectile.options.collideCbContext = game;
    projectile.owner = this.target;
    
    // Set the projectile position to be the firing motherduck's position plus 1.5 units in
    // the direction the motherduck is facing
    projectilePos = targetPos.vadd(targetQuat.vmult(new Cannon.Vec3(0,0,1.5)));
    projectile.body.position.set(projectilePos.x, projectilePos.y, projectilePos.z);
    projectile.body.quaternion.set(targetQuat.x, targetQuat.y, targetQuat.z, targetQuat.w);
    game.world.addBody(projectile.body);
    game.entityStore.push(projectile);
    
    var spawnInfo = {
      type: 'projectile',
      projectileType: projectile.projectileType,
      id: projectileID,
      p: projectile.body.position,
      q: projectile.body.quaternion
    };
    game.emitSpawn(spawnInfo);
    
    game.emitFn(game.id, 'game:update-ammo', {
      motherduckName: this.target.name,
      ammunition: this.ammunition
    });
    
    this.alive = this.ammunition > 0;
  }
};

/**
 * The lobbed bomb fireable object.
 *
 * @class LobbedBombFireable
 * @constructor
 * @param {String} id The ID of this fireable.
 * @param {Object} passedOptions The options to be used for this fireable.
 */
exp.LobbedBombFireable = LobbedBombFireable = function (id, passedOptions) {
  var options = merge(LobbedBombFireable.defaultOptions, passedOptions || {});
  var ret = new Fireable(id, options, 'lobbedBomb');
  ret.ammunition = options.ammunition;
  ret.launchSpeed = options.launchSpeed;
  ret.fireFn = options.fireFn;
  return ret;
};

/**
 * The default lobbed bomb fireable options, the fire function is defined here.
 *
 * @property StealFireable.defaultOptions
 * @type {Object}
 */
LobbedBombFireable.defaultOptions = {
  ammunition: 3,
  launchSpeed: 17,
  /**
   * The function to be called when a player fires a lobbed bomb projectile.
   *
   * @method fireFn
   * @param {Game} game The game instance
   */
  fireFn: function(game) {
    this.ammunition -= 1;
    
    var projectileOpts = {
      cannonOptions: { material: game.projectileMat },
      collideCb: game.onCollision,
      collideCbContext: game
    };
    var projectileID = game.getNextEID();
    var projectile = new Projectile.LobbedBombProjectile(projectileID, projectileOpts);
    projectile.owner = this.target;

    var targetPos = this.target.body.position;
    var targetQuat = this.target.body.quaternion;
    var direction = targetQuat.vmult(Cannon.Vec3.UNIT_Z);
    direction.y = 0;
    direction.normalize(); // point it flat in the right direction (no elevation)
    direction.y = 0.6;
    direction.normalize(); // give it some elevation
    
    projectile.body.quaternion.setFromVectors(Cannon.Vec3.UNIT_Z, direction);
    direction.scale(this.launchSpeed, direction); // give it some pop
    
    // Set the projectile position to be the firing motherduck's position plus 1.5 units in
    // the direction the motherduck is facing and 0.5 units up
    projectilePos = targetPos.vadd(targetQuat.vmult(new Cannon.Vec3(0,0.5,1.5)));
    projectile.body.position.set(projectilePos.x, projectilePos.y, projectilePos.z);
    projectile.body.velocity.set(direction.x, direction.y, direction.z);
    
    game.world.addBody(projectile.body);
    game.entityStore.push(projectile);
    
    var spawnInfo = {
      type: 'projectile',
      projectileType: projectile.projectileType,
      id: projectileID,
      p: projectile.body.position,
      q: projectile.body.quaternion
    };
    game.emitSpawn(spawnInfo);
    
    game.emitFn(game.id, 'game:update-ammo', {
      motherduckName: this.target.name,
      ammunition: this.ammunition
    });
    
    this.alive = this.ammunition > 0;
  }
};