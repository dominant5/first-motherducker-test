module.exports = AccountManager;

var Server = require('./server');
var User = require('./database/user');
var FriendList = require('./database/friend-list');
var FriendRequestList = require('./database/friend-request-list');
var ErrorHandler = require('./error-handler');

/**
 * Responsible for managing account registration and friend requests.
 *
 * @class AccountManager
 * @constructor
 * @param {Server} server The server instance.
 * @param {SessionManager} sessionManager The sessionManager instance.
 */
function AccountManager (server, sessionManager) {
  server.on(Server.CONNECT, this.onClientConnect.bind(this));
  this.sessionManager = sessionManager;
}

/**
 * Makes a newly connected client start listening for signup and friend request emits.
 *
 * @method onClientConnect
 * @param {Client} client The data for the connected client
 */
AccountManager.prototype.onClientConnect = function(client) {
  client.on('acc:signup', function(data) {
    this.onClientSignup(client, data);
  }.bind(this));

  client.on('acc:friend-request', function (data) {
    this.onFriendRequest(client, data);
  }.bind(this));

  client.on('acc:friend-request-accepted', function (data) {
    this.onFriendRequestAccepted(client, data);
  }.bind(this));
};

/**
 * Creates a new user entry in the database when a client signs up.
 *
 * @method onClientSignup
 * @param {Client} client The data for the client signing up
 * @param {Object} data The username and password the client is signing up with
 */
AccountManager.prototype.onClientSignup = function(client, data) {
  var newUser = {
    username: data.username,
    password: data.password
  };

  User.create(newUser, function (err, user) {
    if (err) return ErrorHandler.emit(client, 'acc:signup', err);
    FriendList.create(user.username, function (err, friendList) {
      if (err) return ErrorHandler.emit(client, 'acc:signup', err);
      this.sessionManager.onClientLogin(client, newUser, function () {
        client.emit('acc:signup-success', { username: user.username });
      });
    }.bind(this));
  }.bind(this));
};

/**
 * Stores a friend request in the database and updates the sender and receiver.
 *
 * @method onFriendRequest
 * @param {Client} client The data for the client sending the friend request
 * @param {Object} data The data for the client receiving the friend request
 */
AccountManager.prototype.onFriendRequest = function(client, data) {
  if (!this.sessionManager.inSession(client.name)) return;
  var fromName = client.name;
  var toName = data.name;
  FriendRequestList.addRequest(fromName, toName).then(function (added) {
    if (!added) {
      return ErrorHandler.emit(client, 'acc:friend-request', new Error('Request already sent.'));
    } else {
      var toClient = this.sessionManager.getSessionClientByName(toName);
      console.log(client, data, added, toClient);
      toClient.emit('acc:friend-request', { from: fromName });
      client.emit('acc:friend-request-sent', { to: toName });
    }
  }.bind(this), function (err) {
    return ErrorHandler.emit(client, 'acc:friend-request', err);
  }.bind(this));
};

/**
 * Updates both clients and removes the request when a friend request has been accepted.
 *
 * @method onFriendRequestAccepted
 * @param {Client} client The data for the client accepting the friend request
 * @param {Object} data The data for the client who sent the request
 */
AccountManager.prototype.onFriendRequestAccepted = function(client, data) {
  var fromName = data.from;
  var toName = client.name;
  FriendRequestList.removeRequest(fromName, toName).then(function (removed) {
    FriendList.addFriend(fromName, toName).then(function (added) {
      FriendList.addFriend(toName, fromName).then(function (added) {
        var fromClient = this.sessionManager.getSessionClientByName(fromName);
        fromClient.emit('acc:friend-request-accepted', { to: toName });
      }.bind(this));
    }.bind(this));
  }.bind(this));
};