module.exports = SessionManager;

var Server = require('./server');
var Client = require('./client');
var User = require('./database/user');
var ErrorHandler = require('./error-handler');
var EmitterMixin = require('./emitter-mixin');

/**
 * Responsible for managing each client's session with the server.
 *
 * @class SessionManager
 * @constructor
 * @param {Server} Server The server instance.
 */
function SessionManager (server) {
  /**
   * The db _ids associated with each logged-in user.
   *
   * @property sessions
   * @type {Object}
   */
  this.sessions = {};
  this.loggedIn = {};

  server.on(Server.CONNECT, this.onClientConnect.bind(this));
  server.on(Server.DISCONNECT, this.onClientDisconnect.bind(this));

  EmitterMixin(this);
}

SessionManager.EventCodes = {
  STARTED: 0,
  ENDED: 1
};

/**
 * Register session callbacks on the client when they connect.
 *
 * @method onClientConnect
 * @param {Object} client The client connecting
 */
SessionManager.prototype.onClientConnect = function(client) {
  client.on('session:login', function(data) {
    this.onClientLogin(client, data);
  }.bind(this));

  client.on('session:play-as-guest', function () {
    this.onClientPlayAsGuest(client);
  }.bind(this));

  client.on('session:logout', function() {
    this.onClientLogout(client);
  }.bind(this));
};

/**
 * Delete the client's session when they disconnect.
 *
 * @method onClientDisconnect
 * @param {Object} client The client disconnecting
 */
SessionManager.prototype.onClientDisconnect = function(client) {
  this.removeSession(client);
};

/**
 * Get a client's details from their name if they have a session.
 *
 * @method getSessionClientByName
 * @param {String} name The name of the client whose session we are searching for.
 * @return {Object} The client's details. Null if no session exists.
 */
SessionManager.prototype.getSessionClientByName = function(name) {
  for (var key in this.sessions) {
    var client = this.sessions[key];
    if (client.name === name) return client;
  }
  return null;
};

/**
 * Get a client's details from their name if they are logged in.
 *
 * @method getLoggedInClientByName
 * @param {String} name The name of the client whose details we are searching for.
 * @return {Object} The client's details. Null if no client is logged in with that name.
 */
SessionManager.prototype.getLoggedInClientByName = function(name) {
  for (var key in this.loggedIn) {
    var client = this.sessions[key];
    if (client.name === name) return client;
  }
  return null;
};

/**
 * Checks if there is a session for this client name.
 *
 * @method inSession
 * @param {String} name The name of the client to search for.
 * @return {Boolean} True if there is a session for this client.
 */
SessionManager.prototype.inSession = function(name) {
  return this.getSessionClientByName(name) !== null;
};

/**
 * Adds a new session for a client, only adds the client to loggedIn if asGuest is false.
 *
 * @method addSession
 * @param {Object} client The details of the new client.
 * @param {Boolean} asGuest True if the client is starting a session as a guest.
 * @param {String} sessionName The name to store the session as.
 */
SessionManager.prototype.addSession = function(client, asGuest, sessionName) {
  this.sessions[client.id()] = client;
  client.setName(sessionName);
  client.setStatus(Client.StatusCodes.LOBBY);
  var evt = asGuest? 'session:play-as-guest-success' : 'session:login-success';
  if (!asGuest) {
    this.loggedIn[client.id()] = client;
  }
  client.emit(evt, { name: sessionName });
  this.emit(SessionManager.EventCodes.STARTED, client, sessionName);
};

/**
 * Deletes a client's session and loggedIn records, sets the client to guest.
 *
 * @method removeSession
 * @param {Object} client The details of the client ending the session.
 */
SessionManager.prototype.removeSession = function(client) {
  var sessionName = client.name;
  delete this.sessions[client.id()];
  delete this.loggedIn[client.id()];
  client.setStatus(Client.StatusCodes.CREATED);
  client.setName(client.guestName);
  client.emit('session:logout-success', {});
  this.emit(SessionManager.EventCodes.ENDED, client, sessionName);
};

/**
 * Handles client login, emits errors if login fails.
 *
 * @method onClientLogin
 * @param {Object} client The details of the client logging in.
 * @param {Object} data The username and password the client is logging in with.
 * @param {Function} cb A callback that can be sent with the login data, usually an emit.
 */
SessionManager.prototype.onClientLogin = function(client, data, cb) {
  if (this.inSession(data.username)) return ErrorHandler.emit(client, 'session:login', new Error('User already logged in'));
  User.findByUsername(data.username, function(err, user) {
    if (!user) return ErrorHandler.emit(client, 'session:login', new Error('User doesn\'t exist'));
    User.comparePassword(data.password, user, function(err, isMatch) {
      if (err) return ErrorHandler.emit(client, 'session:login', new Error('Error using bcrypt'));
      if (!isMatch) return ErrorHandler.emit(client, 'session:login', new Error('Incorrect password'));
      this.addSession(client, false, user.username);
      if (cb) cb();
    }.bind(this));
  }.bind(this));
};

/**
 * Creates a new guest session.
 *
 * @method onClientPlayAsGuest
 * @param {Object} client The details of the client starting the session.
 */
SessionManager.prototype.onClientPlayAsGuest = function(client) {
  this.addSession(client, true, client.guestName);
};

/**
 * Removes a client's session when they log out.
 *
 * @method onClientLogout
 * @param {Object} client The details of the client logging out.
 */
SessionManager.prototype.onClientLogout = function(client) {
  this.removeSession(client);
};
