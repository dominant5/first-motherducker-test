var exp = module.exports = {};

var Cannon = require('cannon'),
CollisionGroups = require('./game-collision-groups'),
EntityUpdateMixin = require('./entity-update-mixin'),
TypesMixin = require('./types-mixin'),
StatesMixin = require('./states-mixin'),
merge = require('deepmerge');

/**
 * Each projectile has its own object in exp, the constructor for these projectiles
 * merges their default options with the base projectile options, and adds
 * its unique types. The default options contain any variables used by the
 * projectile along with the updateFn, which defines the movement of the
 * projectile, and the explode function, which defines how the projectile should behave
 * when it collides with something in the world.
 */

exp.Projectile = Projectile = function (id, options, type) {
  this.options = merge(Projectile.defaultOptions, options || {});

  this.body = new Cannon.Body(this.options.cannonOptions);
  this.body.addShape(new Cannon.Sphere(this.options.cannonOptions.radius));
  this.body.entity = this;

  if (this.options.collideCb && this.options.collideCbContext) {
    this.body.addEventListener('collide', this.options.collideCb.bind(this.options.collideCbContext));
  }

  EntityUpdateMixin(this);
  this.setUpdateFn(this.options.updateFn);

  this.id = (typeof id !== 'undefined')? id : this.body.id;
  TypesMixin(this);
  this.addType('projectile');
  if (type) {
    this.addType(type);
    this.projectileType = type;
  }
  StatesMixin(this);

  return this;
};

/**
 * Returns the type of this projectile that's not 'projectile', eg. 'steal'
 *
 * @method getType
 * @return {String} the type
 */
Projectile.prototype.getType = function() {
  return this.types[1];
};

/**
 * Removes the projectile's body from the world, and deletes it.
 *
 * @method destroyBody
 */
Projectile.prototype.destroyBody = function () {
  if (!this.body) return;
  if (this.body.world) this.body.world.remove(this.body);
  delete this.body;
};

/**
 * Executes the projectiles explode function for a specific collision.
 *
 * @method explode
 * @param {Object} type The type of the object the projectile collided with.
 * @param {Object} game The game object.
 * @param {Object} object The object that the projectile has collided with.
 */
Projectile.prototype.explode = function (type, game, object) {
  if (!this.options.explodeFns) {
    this.options.explodeBase();
    return;
  }
  var Fn = this.options.explodeFns[type]? this.options.explodeFns[type] : this.options.explodeFns['base'];
  Fn.call(this, game, object);
};

/**
 * The default projectile options.
 *
 * @property Projectile.defaultOptions
 * @type {Object}
 */
Projectile.defaultOptions = {
  cannonOptions: {
    mass: 0.1,
    radius: 0.25,
  },
  collideCb: null,
  collideCbContext: null,
  explodeBase: function () {
    this.destroyBody();
  }
};

/**
 * A projectile which puts any duckling that it collides with to the end of
 * the trail of the motherduck that fired the projectile.
 *
 * @class StealProjectile
 * @constructor
 * @param {String} id The ID of this projectile.
 * @param {Object} passedOptions The options to be used for this projectile.
 */
exp.StealProjectile = StealProjectile = function (id, passedOptions) {
  var options = merge(StealProjectile.defaultOptions, passedOptions || {});
  var ret = new Projectile(id, options, 'steal');
  return ret;
};

/**
 * The default steal projectile options.
 *
 * @property StealProjectile.defaultOptions
 * @type {Object}
 */
StealProjectile.defaultOptions = {
  speed: 20,
  updateFn: function () {
    var newVel = this.body.quaternion.vmult(Cannon.Vec3.UNIT_Z).scale(this.options.speed);
    this.body.velocity.set(newVel.x, newVel.y, newVel.z);
  },
  explodeFns: {
    duckling: function (game, duckling) {
      if (!this.body) return;
      if (!duckling.hasState('helpless')) {
        duckling.leader.removeDuckling(duckling);
        if (duckling.leader.team !== this.owner.team) {
          this.owner.stolenCount++;
        }
      }
      this.owner.addDuckling(duckling, game);
      this.destroyBody();
    },
    base: function () {
      this.destroyBody();
    }
  }
};

/**
 * A projectile that explodes on contact with anything, detaching ducklings
 * from any trail they are following and scattering them away from the contact point.
 *
 * @class LobbedBombProjectile
 * @constructor
 * @param {String} id The ID of this projectile.
 * @param {Object} passedOptions The options to be used for this projectile.
 */
exp.LobbedBombProjectile = LobbedBombProjectile = function (id, passedOptions) {
  var options = merge(LobbedBombProjectile.defaultOptions, passedOptions || {});
  var ret = new Projectile(id, options, 'lobbedBomb');
  ret.body.collisionFilterMask = CollisionGroups.DEFAULT | CollisionGroups.WATER;
  return ret;
};

/**
 * The default lobbed bomb projectile options.
 *
 * @property StealProjectile.defaultOptions
 * @type {Object}
 */
LobbedBombProjectile.defaultOptions = {
  explodeSpeed: 50,
  explodeRadius: 5,
  explodeFns: {
    base: function (game) {
      game.entityStore.forEach(['motherduck', 'duckling', 'powerup'], function (entity) {
        if (!this.body || !entity.body) return;
        if (this.body.position.distanceTo(entity.body.position) > this.options.explodeRadius) return;
        if (entity.hasType('duckling')) {
          if (!entity.hasState('helpless')) {
            entity.leader.removeDuckling(entity, game);
            if (entity.leader.team !== this.owner.team) {
              this.owner.detachedCount++;
            }
          }
        }
        var explodeDir = entity.body.position.vsub(this.body.position);
        var speedRatio = 1 / explodeDir.length() * this.options.explodeSpeed;
        explodeDir.normalize();
        var newVel = explodeDir.scale(speedRatio);
        entity.body.velocity.set(newVel.x, newVel.y, newVel.z);
      }.bind(this));
      this.destroyBody();
    }
  }
};