module.exports = exp = {};

/**
 * Name utils
 *
 * @class Name utils
 */

/**
 * Generates a (most likely) unique name.
 *
 * @method generateName
 * @param {String} [prefix the prefix of the generated name
 * @param {Number} [numDigits=10] The number of random digits (0-9) to append to
 * prefix
 * @return {String} the generated name
 * @example
 *     NameUtils.generateName('Game', 10);
 */
exp.generateName = function (prefix, numDigits) {
  var ret = prefix || 'Name';
  var n = numDigits || 10;
  for (var i = 0; i < n; i++) {
    ret += Math.floor(Math.random() * 10);
  }
  return ret;
};