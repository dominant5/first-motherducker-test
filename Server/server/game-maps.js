module.exports = exp = {};

/*
map.size.y is the height of the map. The map is positioned so that the floor
is at y = 0.

Consider the entity's origin point to be centred in x and z, and at the base of
the entity in y. Therefore, to set the position of an entity to be resting on
the floor of the map, set y to 0.
*/

exp.default = {
  size: {
    x: 40,
    y: 20,
    z: 40
  },
  spawnPoints: {
    motherducks: [
      // corners
      { p: { x: 19, y: 0, z: -19 }, q: { x: 0, y: 1, z: 0, w: Math.PI * -0.25 } },
      { p: { x: -19, y: 0, z: -19 }, q: { x: 0, y: 1, z: 0, w: Math.PI * 0.25 } },
      { p: { x: -19, y: 0, z: 19 }, q: { x: 0, y: 1, z: 0, w: -Math.PI * 1.25 } },
      { p: { x: 19, y: 0, z: 19 }, q: { x: 0, y: 1, z: 0, w: Math.PI * 1.25 } },

      // mid points
      { p: { x: 0, y: 0, z: -19 }, q: { x: 0, y: 1, z: 0, w: 0 } },
      { p: { x: 0, y: 0, z: 19 }, q: { x: 0, y: 1, z: 0, w: Math.PI } },
      { p: { x: -19, y: 0, z: 0 }, q: { x: 0, y: 1, z: 0, w: Math.PI * 0.5 } },
      { p: { x: 19, y: 0, z: 0 }, q: { x: 0, y: 1, z: 0, w: Math.PI * -0.5 } }
    ]
  }
};