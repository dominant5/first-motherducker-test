module.exports = Server;

var Client = require('./client');
var q = require('q');
var merge = require('deepmerge');
var NameUtils = require('./name-utils');
var EmitterMixin = require('./emitter-mixin');

/**
 * A HTTP and socket.io server.
 *
 * @class Server
 * @constructor
 * @param {Number} port the port to listen on.
 * @param {Object} app the app for handling HTTP requests.
 * @param {Object} options any overriden options. 
 */
function Server (port, app, options) {
  var listener = app || function (req, res) { res.send('Empty app'); };

  EmitterMixin(this);

  /**
   * The HTTP server
   *
   * @property httpServer
   * @type {Object}
   */
  this.httpServer = require('http').createServer(listener).listen(port);

  /**
   * The socket.io server
   *
   * @property io
   * @type {Object}
   */
  this.io = require('socket.io').listen(this.httpServer);
  
  this.options = merge(Server.defaultOptions, options || {});

  /**
   * The list of all connected Clients, indexed by client name.
   *
   * @property clients
   * @type {Object}
   */
  this.clients = {};
  
  /**
   * The list of all ping update callbacks, indexed by client name.
   *
   * @property pingUpdateCbs
   * @type {Object}
   */
  this.pingUpdateCbs = {};

  this.io.on('connect', this.createClient.bind(this));

  if (!this.options.silent) console.log('server started, listening on port ' + port);
}

Server.CONNECT = 0;
Server.DISCONNECT = 1;

Server.defaultOptions = {
  /**
   * If true, does not print console messages.
   *
   * @property options.silent
   * @type {Boolean}
   */
  silent: false,

  /**
   * Milliseconds between querying a particular client for a ping update.
   *
   * @property pingUpdateInterval
   * @type {Number}
   */
  pingUpdateInterval: 2000
};

/**
 * Creates a Client. Stores it for later deletion (eg. upon disconnect).
 * Notifies connectCallbacks.
 *
 * @method createClient
 * @param {Object} socket the socket.io socket
 */
Server.prototype.createClient = function (socket) {
  var client = new Client(socket);
  this.clients[client.id()] = client;
  
  client.on('ping', function () {
    this.sendPong(client.id());
  }.bind(this));
  
  client.on('ping-update', function (data) {
    client.ping = data.ping;
    this.pingUpdateCbs[client.id()](data.ping);
  }.bind(this));

  client.on('disconnect', function () {
    this.deleteClient(client.id());
  }.bind(this));

  this.emit(Server.CONNECT, client);
  
  this.schedulePingUpdateRequest(client.id(), 0);
};

/**
 * Deletes a Client. Notifies disconnectCallbacks.
 *
 * @method deleteClient
 * @param {String} clientName the Client's name.
 */
Server.prototype.deleteClient = function(clientId) {
  var client = this.clients[clientId];
  this.emit(Server.DISCONNECT, client);
  delete this.clients[clientId];
};

/**
 * Disconnects all clients; clears the clients, connectCallbacks, and
 * disconnectCallbacks lists.
 *
 * @method clear
 */
Server.prototype.clear = function () {
  for (var key in this.clients) {
    var client = this.clients[key];
    client.socket.disconnect();
    delete this.clients[key];
  }
  this.clients = {};
  this.handlers = {};
};

/**
 * Clears the server and additionally closes the HTTP server.
 *
 * @method stop
 */
Server.prototype.stop = function () {
  this.clear();
  this.httpServer.close();
};

/**
 * A wrapper around the socket.io to function. Emits an event to all clients
 * in the room.
 *
 * @method to
 * @param {String} room the name of the room to emit an event in
 * @param {String} event the name of the event we're sending
 * @param {Object} data the data to send with the event
 * @example
 *     server.to('Game1234567890', 'game:update', {
 *       // some game data
 *     });
 */
Server.prototype.to = function(room, event, data) {
  this.io.to(room).emit(event, data);
};

/**
 * Sends a ping update request to client.
 *
 * @method pingUpdateRequest
 * @param {String} clientName the client's name
 * @return {Promise} a promise that resolves with the clients new ping
 */
Server.prototype.pingUpdateRequest = function(cliendId) {
  var dfd = q.defer();
  
  var client = this.clients[cliendId];
  if (client) {
    client.emit('ping-update-request', {});
    this.pingUpdateCbs[cliendId] = function (ping) {
      dfd.resolve(ping);
    };
  } else {
    dfd.reject();
  }
  
  return dfd.promise;
};

/**
 * Schedule next ping update request.
 *
 * @method pingUpdateRequest
 * @param {String} clientName the client's name
 * @param {String} ping the client's ping
 */
Server.prototype.schedulePingUpdateRequest = function(clientId, ping) {
  this.pingUpdateRequest(clientId).then(function (ping) {
    setTimeout(function () {
      this.schedulePingUpdateRequest(clientId, ping);
    }.bind(this), this.options.pingUpdateInterval);
  }.bind(this));
};

/**
 * Send 'pong' to client.
 *
 * @method sendPong
 * @param {String} clientName the client's name
 */
Server.prototype.sendPong = function(clientId) {
  var client = this.clients[clientId];
  if (client) client.emit('pong', {});
};