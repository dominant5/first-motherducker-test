module.exports = function (target) {
  typesMixin.call(target);
};

/**
 * Accepts any number of strings as parameters. Returns true iff the object
 * has all of the types.
 *
 * @method hasAllTypes
 * @return {Boolean} true iff the object has all of the types passed.
 */
function hasAllTypes () {
  for (var i = 0; i < arguments.length; i++) {
    var type = arguments[i];
    if (!this.hasType(type)) return false;
  }
  return true;
}

/**
 * Accepts any number of strings as parameters. Returns true if the object
 * has any of the types.
 *
 * @method hasAnyTypes
 * @return {Boolean} true if the object has any of the types passed.
 */
function hasAnyTypes () {
  for (var i = 0; i < arguments.length; i++) {
    var type = arguments[i];
    if (this.hasType(type)) return true;
  }
  return false;
}

/**
 * Checks whether an object has a specific type.
 *
 * @method hasType
 * @param type The type to check for.
 * @return {Boolean} true if the object has the type passed.
 */
function hasType (type) {
  return this.types.indexOf(type) !== -1;
}

/**
 * Adds a type to an object.
 *
 * @method addType
 * @param type The type to add.
 */
function addType (type) {
  if (this.types.indexOf(type) !== -1) return; // already added
  this.types.push(type);
}

/**
 * Removes a type to an object.
 *
 * @method removeType
 * @param type The type to remove.
 */
function removeType (type) {
  var idx = this.types.indexOf(type);
  if (idx !== -1) this.types.splice(idx, 1);
}

var typesMixin = (function () {
  return function () {
    this.types = [];
    this.hasAllTypes = hasAllTypes;
    this.hasAnyTypes = hasAnyTypes;
    this.hasType = hasType;
    this.addType = addType;
    this.removeType = removeType;
    return this;
  };
})();