module.exports = exp = {};

/**
 * Math utils
 *
 * @class Math utils
 */

/**
 * Returns a random float between min (inclusive) and max (exclusive)
 *
 * @method randomFloat
 * @param {Number} min the min number
 * @param {Number} max the max number
 * @return {Number} the random float
 */
exp.randomFloat = function (min, max) {
  return Math.random() * (max - min) + min;
};

/**
 * Returns a random int between min (inclusive) and max (inclusive)
 *
 * @method randomInt
 * @param {Number} min the min number
 * @param {Number} max the max number
 * @return {Number} the random int
 */
exp.randomInt = function (min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};