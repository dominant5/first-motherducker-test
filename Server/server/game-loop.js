module.exports = Loop;

/**
 * Creates a gameloop function. Inspired by
 * https://github.com/timetocode/node-game-loop/blob/master/gameLoop.js
 *
 * @class Loop
 * @constructor
 * @param {Function} update the update function: (dt) -> void. dt is seconds.
 * @param {Number} fps the desired frames per second.
 */
function Loop (update, fps) {
  this.fps = fps;
  this.tickLenMs = 1000 / this.fps;
  this.prevTick = null; // set by start function
  this.actualTicks = 0;
  this.update = update;
  this.alive = false;
}

/**
 * The loop.
 *
 * @method run
 */
Loop.prototype.run = function () {
  if (!this.alive) return;

  var now = Date.now();
  this.actualTicks++;
  if (this.prevTick + this.tickLenMs <= now) {
    var dt = now - this.prevTick;
    this.prevTick = now;
    this.update(dt / 1000);
    this.actualTicks = 0;
  }

  if (Date.now() - this.prevTick < this.tickLenMs - 16) {
    setTimeout(this.run.bind(this));
  } else {
    setImmediate(this.run.bind(this));
  }
};

/**
 * Starts the loop running.
 *
 * @method start
 */
Loop.prototype.start = function () {
  this.alive = true;
  this.prevTick = Date.now();
  this.run();
};

/**
 * Stops the loop running.
 *
 * @method stop
 */
Loop.prototype.stop = function () {
  this.alive = false;
};