module.exports = exp = Motherduck;

var Cannon = require('cannon'),
Duckling = require('./duckling'),
EntityUpdateMixin = require('./entity-update-mixin'),
TypesMixin = require('./types-mixin'),
StatesMixin = require('./states-mixin'),
merge = require('deepmerge');

/**
 * Responsible for controlling a motherduck, one instance for every motherduck.
 *
 * @class Duckling
 * @constructor
 * @param {String} name The name of this motherduck.
 * @param {String} id The ID of this motherduck.
 * @param {Object} passedOptions The options to be used for this motherduck.
 */
function Motherduck (name, id, passedOptions) {
  this.options = merge(Motherduck.defaultOptions, passedOptions || {});

  this.body = new Cannon.Body(this.options.cannonOptions);
  this.body.addShape(new Cannon.Sphere(this.options.cannonOptions.radius));
  this.body.entity = this;

  if (this.options.collideCb && this.options.collideCbContext) {
    this.body.addEventListener('collide', this.options.collideCb.bind(this.options.collideCbContext));
  }

  EntityUpdateMixin(this);
  this.setUpdateFn(this.options.updateFn || Motherduck.defaultUpdateFn);

  this.id = (typeof id !== 'undefined')? id : this.body.id;
  this.name = (typeof name !== 'undefined')? name : this.id;
  TypesMixin(this);
  this.addType('motherduck');
  StatesMixin(this);
  this.turnType = Motherduck.TurnType.STOP;
  this.ducklingCount = 0;
  this.highestCount = 0;
  this.stolenCount = 0;
  this.detachedCount = 0;
  this.score = 0;
  this.team = -1;
  this.next = null;
  this.prev = null;
  this.end = this; // itself to start with
  this.activePowerups = {};
  this.fireable = null;

  return this;
}

/**
 * This defines the possible motherduck turn states.
 *
 * @property Motherduck.TurnType
 * @type {Object}
 */
Motherduck.TurnType = {
  LEFT: -1,
  STOP: 0,
  RIGHT: 1
};

/**
 * The default motherduck options.
 *
 * @property Motherduck.defaultOptions
 * @type {Object}
 */
Motherduck.defaultOptions = {
  cannonOptions: {
    mass: 0.6,
    radius: 0.5,
    linearDamping: 0,
    angularDamping: 0.9
  },
  speed: 10,
  diveForce: -1500,
  turnSpeed: 3,
  collideCb: null,
  collideCbContext: null,
  updateFn: null
};

/**
 * The default update function of a motherduck.
 *
 * @method defaultUpdateFn
 */
Motherduck.defaultUpdateFn = function () {
  var newVel = this.body.quaternion.vmult(Cannon.Vec3.UNIT_Z);
  this.body.velocity.x = newVel.x * this.options.speed;
  this.body.velocity.z = newVel.z * this.options.speed;
  this.body.angularVelocity.y = this.turnType * this.options.turnSpeed;
};

/**
 * Iterates through the motherduck's trail (including the motherduck).
 *
 * @method iterate
 * @param {Function} fn the visitor function to be called on each entity in
 * the trail.
 * @param {Boolean} backwards if true, iterates from the end to the motherduck.
 * if false, iterates from the motherduck to the end.
 */
Motherduck.prototype.iterate = function (fn, backwards) {
  var current = backwards? this : this.end;
  var nextEntity = backwards? 'prev' : 'next';
  fn(this, current);
  while (current[nextEntity]) {
    current = current[nextEntity];
    fn(this, current);
  }
};

/**
 * Sets this motherduck's turn type.
 *
 * @method setTurnType
 * @param {Motherduck.TurnType} turnType LEFT, STOP, or RIGHT
 */
Motherduck.prototype.setTurnType = function (turnType) {
  this.turnType = turnType;
};

/**
 * Dives the motherduck below the water.
 *
 * @method dive
 * @param {Game} game The game instance
 */
Motherduck.prototype.dive = function (game) {
  var wAABB = new Cannon.AABB();
  var eAABB = new Cannon.AABB();
  
  eAABB.copy(this.body.aabb);
  eAABB.lowerBound.vadd(this.body.position, eAABB.lowerBound);
  eAABB.upperBound.vadd(this.body.position, eAABB.upperBound);
  
  game.entityStore.forEach('water', function (water) {
    // get water aabb in world space
    wAABB.copy(water.body.aabb);
    wAABB.lowerBound.vadd(water.body.position, wAABB.lowerBound);
    wAABB.upperBound.vadd(water.body.position, wAABB.upperBound);
    var surface = wAABB.upperBound.y;
    
    if (!wAABB.overlaps(eAABB) || surface > eAABB.upperBound.y) return;
    
    this.body.force.y = this.options.diveForce;
  }.bind(this));
};

/**
 * Adds a duckling to the end of this motherduck's trail. Assumes the duckling
 * was helpless.
 *
 * @method addDuckling
 * @param {Object} d the duckling to add.
 * @param {Object} game The instance of the game object.
 * @param {Function} updateFn an optional update function for the duckling. If
 * not passed, Duckling.defaultUpdateFn is used.
 */
Motherduck.prototype.addDuckling = function (d, game, updateFn) {
  d.removeState('helpless');
  var end = this.end;

  // link them up
  end.prev = d;
  d.next = end;
  d.leader = this;
  
  // emit the duckling's team
  if (game) {
    game.emitFn(game.id, 'game:assign-team', {
      id: d.id,
      team: this.team
    });
  }

  // set the move fn
  d.setUpdateFn(updateFn || Duckling.defaultUpdateFn);

  // position the duckling behind the previous end duckling
  var dist = (d.body.boundingRadius + end.body.boundingRadius) * 1.3;
  var displaceVec = end.body.quaternion.vmult(new Cannon.Vec3(0, 0, -dist));
  end.body.position.vadd(displaceVec, d.body.position);

  // point the duckling towards the previous end duckling
  displaceVec.scale(-1, displaceVec); // flip it
  d.body.quaternion.setFromVectors(Cannon.Vec3.UNIT_Z, displaceVec);

  // clear any velocity
  d.body.velocity.set(0, 0, 0);
  d.body.angularVelocity.set(0, 0, 0);

  // update the motherduck's end duckling ref
  this.end = d;

  // increment count
  this.ducklingCount++;
  
  // update highest ducklings collected
  if (this.ducklingCount > this.highestCount) {
    this.highestCount = this.ducklingCount;
  }
};

/**
 * Removes a duckling in the trail from the trail, fixing prev and next
 * pointers (as if in a linked list). Assumes the duckling is in the trail.
 *
 * @method removeDuckling
 * @param {Object} d the duckling to remove.
 * @param {Object} game The instance of the game object.
 */
Motherduck.prototype.removeDuckling = function (d, game) {
  if(d === this.end) this.end = d.next;
  d.next.prev = d.prev;
  if(d.prev) d.prev.next = d.next;
  d.next = null;
  d.prev = null;
  d.addState('helpless');
  d.setUpdateFn(null);
  if (game) {
    game.emitFn(game.id, 'game:assign-team', {
      id: d.id,
      team: -1
    });
  }
  this.ducklingCount--;
};

/**
 * Assign the motherducks fireable ability.
 *
 * @method getFireable
 * @param {Object} fireable The fireable aquired by the motherduck.
 */
Motherduck.prototype.getFireable = function (fireable) {
  if (this.fireable) {
    delete this.fireable;
  }
  this.fireable = fireable;
};

/**
 * Uses the motherducks fireable ability.
 *
 * @method useFireable
 * @param {Object} game The instance of the game object.
 */
Motherduck.prototype.useFireable = function (game) {
  if (this.fireable) {
    this.fireable.fireFn(game);
    if (!this.fireable.alive) {
      delete this.fireable;
      this.fireable = null;
    }
  }
};