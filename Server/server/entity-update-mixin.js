module.exports = function (target) {
  entityUpdateMixin.call(target);
};

/**
 * Sets this entity's update function, binding it to this.
 *
 * @method setUpdateFn
 * @param {Function} fn the function (or null to effectively disable update)
 */
function setUpdateFn (fn) {
  if (fn) this.updateFn = fn.bind(this);
  else this.updateFn = null;
}

/**
 * Calls this entity's update function if defined.
 *
 * @method update
 * @param {Number} dt seconds since last update
 */
function update (dt, game) {
  if (this.updateFn) this.updateFn(dt, game);
}

var entityUpdateMixin = (function () {
  return function () {
    this.updateFn = null;
    this.setUpdateFn = setUpdateFn;
    this.update = update;
    return this;
  };
})();