module.exports = StatsManager;

var User = require('./database/user');
var ErrorHandler = require('./error-handler');
var SessionManager = require('./session-manager');
var GameManager = require('./game-manager');
var q = require('q');

function StatsManager (server, sessionManager, gameManager) {
  sessionManager.on(SessionManager.EventCodes.STARTED, this.onSessionStarted, this);
  sessionManager.on(SessionManager.EventCodes.ENDED, this.onSessionEnded, this);
  this.sessionManager = sessionManager;
  gameManager.on(GameManager.EventCodes.GAME_ENDED, this.onGameEnded, this);
}

/**
 * Registers stat related callbacks on a client when they start a session.
 *
 * @method onSessionStarted
 * @param {Object} client The client starting the session.
 */
StatsManager.prototype.onSessionStarted = function (client) {
  client.on('stats:get', function (data) {
    this.sendStatsToClient(client, data.username);
  }.bind(this));
};

/**
 * Unegisters stat related callbacks on a client when they end a session.
 *
 * @method onSessionEnded
 * @param {Object} client The client ending the session.
 */
StatsManager.prototype.onSessionEnded = function (client) {
  client.off('stats:get');
};

/**
 * Calls updateStats on all the motherducks in a game when the game finishes.
 *
 * @method onGameEnded
 * @param {Object} game The game that has just finished.
 */
StatsManager.prototype.onGameEnded = function (game) {
    game.entityStore.forEach('motherduck', function (motherduck) {
  		var client = this.sessionManager.getLoggedInClientByName(motherduck.name);
  		if (client) this.updateStats(motherduck, game.updateData);
  	}.bind(this));
};

/**
 * Sends stats about the given user to the given client.
 *
 * @method sendStatsToClient
 * @param {Client} client the client to send the stats to
 * @param {String} username the name of the user whose stats are to be sent
 */
StatsManager.prototype.sendStatsToClient = function (client, username) {
  this.getStats(username).then(function (stats) {
    stats.username = username;
    client.emit('stats:deliver', stats);
  }, function (err) {
    ErrorHandler.emit(client, 'stats:get', err);
  });
};

/**
 * Updates the statistics of a motherduck with the data from a recently finished game.
 *
 * @method updateStats
 * @param {Object} motherduck The motherduck to update the statistics for.
 * @param {Object} data The statistics related data from the game that just finished.
 */
StatsManager.prototype.updateStats = function (motherduck, data) {
  var dfd = q.defer();
  this.getStats(motherduck.name).then(function (stats) {
    var updateDetails = {
      highscore: (Math.max(stats.highscore, motherduck.score)),
      wins: stats.wins,
      losses: stats.losses,
      mostCollectedDucklings: Math.max(stats.mostCollectedDucklings, motherduck.highestCount),
      totalCollectedDucklings: stats.totalCollectedDucklings + motherduck.ducklingCount,
      totalDetachedDucklings: stats.totalDetachedDucklings + motherduck.detachedCount,
      totalStolenDucklings: stats.totalStolenDucklings + motherduck.stolenCount
    };
    var winner = false;
    // Check if this motherduck was on the winning team
    for (var i = 0; i < data.winner.length; i++) {
      if (data.statistics[data.winner[i]][motherduck.name]) {
        updateDetails.wins ++; // Increment win stat if winner
        winner = true;
        break;
      }
    }
    if (!winner) updateDetails.losses++; // Increment loss stat if loser
    User.update(motherduck.name, updateDetails, function (err, user) {
      if (!user) {
        dfd.reject(new Error('Error updating stats'));
        return dfd.promise;
      }
      dfd.resolve(user);
    });
  }, function (err) {
    dfd.reject(new Error('Error retrieving stats'));
  });
  return dfd.promise;
};

/**
 * Retrieve a user's statistics.
 *
 * @method getStats
 * @param {String} username The username of the user to find statistics for.
 * @return {Promise} A promise that resolves to an object containing the user's statistics.
 */
StatsManager.prototype.getStats = function (username) {
  var dfd = q.defer();
  User.findByUsername(username, function (err, user) {
      if (!user) {
        dfd.reject(new Error('The specified user does not exist.'));
        return dfd.promise;
      }
      var stats = {
        highscore: user.highscore,
        wins: user.wins,
        losses: user.losses,
        mostCollectedDucklings: user.mostCollectedDucklings,
        totalCollectedDucklings: user.totalCollectedDucklings,
        totalDetachedDucklings: user.totalDetachedDucklings,
        totalStolenDucklings: user.totalStolenDucklings
      };
      dfd.resolve(stats);
  });
  return dfd.promise;
};