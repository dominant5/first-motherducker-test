module.exports = AI;

var Cannon = require('cannon');
var Motherduck = require('./motherduck');
var merge = require('deepmerge');

/**
 * Responsible for controlling an AI motherduck, one instance for every AI.
 *
 * @class AI
 * @constructor
 * @param {String} name The name of this AI.
 * @param {String} id The ID of this AI.
 * @param {Object} passedOptions The options to be used for this AI.
 * @param {Motherduck} ret The new motherduck instance to be returned, has type 'AI'.
 */
function AI (name, id, passedOptions) {
  var options = merge(AI.defaultOptions, passedOptions || {});
  var ret = new Motherduck(name, id, options);
  ret.addType('AI');
  ret.fireableCooldown = 0;
  return ret;
}

/**
 * The default weights for objectives in the game world used by the AI.
 *
 * @property AI.defaultTargetValues
 * @type {Object}
 */
AI.defaultTargetValues = {
  duckling: 1,
  powerup: {
    speed: 0.5,
    steal: 1.5,
    lobbedBomb: 1.5,
    bulletTime: 1,
    magnet: 1.5,
    invertOthers: 1.5
  },
  motherduck: 0.25
};

/**
 * The default functions used to calculate the desirablility of different objectives in the world.
 *
 * @property AI.defaultValueFns
 * @type {Object}
 */
AI.defaultValueFns = {
  duckling: function (duckling) {
    if (!duckling.hasState('helpless')) return 0;

    var dY = duckling.body.position.y - this.body.position.y;
    if (Math.abs(dY) > this.body.boundingRadius * 3) return 0; // too low/high

    var dist = this.body.position.distanceTo(duckling.body.position);
    return (1 / dist) * this.options.targetValues.duckling;
  },
  powerup: function (powerup) {
    if (powerup.activated || !powerup.body) return 0;
    
    var dY = powerup.body.position.y - this.body.position.y;
    if (Math.abs(dY) > this.body.boundingRadius * 3) return 0; // too low/high

    var dist = this.body.position.distanceTo(powerup.body.position);
    return (1 / dist) * this.options.targetValues.powerup[powerup.getType()];
  },
  motherduck: function (motherduck) {
    if (!this.fireable) return 0;
    if (motherduck === this) return 0;
    if (motherduck.team === this.team) return 0;

    var dY = motherduck.body.position.y - this.body.position.y;
    if (Math.abs(dY) > this.body.boundingRadius * 3) return 0; // too low/high
    
    var value = motherduck.ducklingCount * this.options.targetValues.motherduck;
    var type = this.fireable.getType();
    if (type === 'steal') {
      value *= 1 / this.body.position.distanceTo(motherduck.body.position);
    } else if (type === 'lobbedBomb') {
      var idealDist = this.options.fireParams.lobbedBomb.idealDist;
      var displaceVec = this.body.quaternion.vmult(new Cannon.Vec3(0, 0, idealDist));
      var idealPos = this.body.position.vadd(displaceVec);
      var distDelta = motherduck.body.position.distanceTo(idealPos);
      value *= 1 / distDelta;
    }
    return value;
  }
};

/**
 * The default weights for obstacles in the game world used by the AI.
 *
 * @property AI.defaultDangerValues
 * @type {Object}
 */
AI.defaultDangerValues = {
  hazard: 1,
  projectile: {
    steal: 1,
    lobbedBomb: 1
  }
};

/**
 * The default functions used to calculate how undesirable different obstacles in the world are.
 *
 * @property AI.defaultDangerFns
 * @type {Object}
 */
AI.defaultDangerFns = {
  hazard: function (hazard) {
    var dY = hazard.body.position.y - this.body.position.y;
    if (Math.abs(dY) > this.body.boundingRadius) return 0; // too low/high

    var dist = this.body.position.distanceTo(hazard.body.position);
    return (1 / dist) * this.options.dangerValues.hazard;
  },
  projectile: function (projectile) {
    if (projectile.owner === this) return;
    var dist = this.body.position.distanceTo(projectile.body.position);
    return (1 / dist) * this.options.dangerValues.projectile[projectile.getType()];
  }
};

/**
 * Parameters used by the AI when deciding whether to fire a powerup.
 *
 * @property AI.defaultFireParams
 * @type {Object}
 */
AI.defaultFireParams = {
  steal: {
    maxDist: 10,
    cooldown: 0.4
  },
  lobbedBomb: {
    idealDist: 10,
    maxDistDelta: 5,
    cooldown: 0.6
  }
};

/**
 * The default functions used to calculate whether the AI should fire a powerup.
 *
 * @property AI.defaultShouldFireFns
 * @type {Object}
 */
AI.defaultShouldFireFns = {
  steal: function (dt, game, target) {
    if (!target.hasType('motherduck')) return false;
    if (target.team === this.team) return false;
    if (target.ducklingCount === 0) return false;

    var dPos = this.body.pointToLocalFrame(target.body.position);
    if (dPos.z < 0) return false; // it's behind
    if (Math.abs(dPos.y) > this.body.boundingRadius) return false; // it's too high/low
    var dist = dPos.length();
    if (dist > this.options.fireParams.steal.maxDist) return false;

    var localVel = this.body.vectorToLocalFrame(target.body.velocity);
    if (dPos.x < 0 && localVel.x < 0) return false;
    if (dPos.x > 0 && localVel.x > 0) return false;

    return true;
  },
  lobbedBomb: function (dt, game, target) {
    if (!target.hasType('motherduck')) return false;
    if (target.team === this.team) return false;
    if (target.ducklingCount === 0) return false;

    var dPos = this.body.pointToLocalFrame(target.body.position);
    if (dPos.z < 0) return false; // it's behind
    if (Math.abs(dPos.y) > this.body.boundingRadius) return false; // it's too high/low

    var idealDist = this.options.fireParams.lobbedBomb.idealDist;
    var displaceVec = this.body.quaternion.vmult(new Cannon.Vec3(0, 0, idealDist));
    var idealPos = this.body.position.vadd(displaceVec);
    var distDelta = target.body.position.distanceTo(idealPos);
    if (distDelta > this.options.fireParams.lobbedBomb.maxDistDelta) return false;

    return true;
  }
};

/**
 * The default update function of an AI.
 *
 * @method defaultUpdateFn
 * @param {float} dt The time elapsed since the last update, in seconds.
 * @param {Game} game The instance of the game.
 */
AI.defaultUpdateFn = function (dt, game) {
  var targetValue = 0;
  var target;
  var dangerValue = 0;
  var danger;

  // calculate target (duckling, powerup, motherduck with ducklings)
  game.entityStore.forEach('duckling', function (duckling) {
    var value = this.options.valueFns.duckling.call(this, duckling);
    if (value > targetValue) {
      targetValue = value;
      target = duckling;
    }
  }.bind(this));

  game.entityStore.forEach('powerup', function (powerup) {
    var value = this.options.valueFns.powerup.call(this, powerup);
    if (value > targetValue) {
      targetValue = value;
      target = powerup;
    }
  }.bind(this));

  if (this.fireable) {
    game.entityStore.forEach('motherduck', function (motherduck) {
      var value = this.options.valueFns.motherduck.call(this, motherduck);
      if (value > targetValue) {
        targetValue = value;
        target = motherduck;
      }
    }.bind(this));
  }

  // calculate danger
  game.entityStore.forEach('hazard', function (hazard) {
    var value = this.options.dangerFns.hazard.call(this, hazard);
    if (value > dangerValue) {
      dangerValue = value;
      danger = hazard;
    }
  }.bind(this));

  game.entityStore.forEach('projectile', function (projectile) {
    var value = this.options.dangerFns.projectile.call(this, projectile);
    if (value > dangerValue) {
      dangerValue = value;
      danger = projectile;
    }
  }.bind(this));

  if (targetValue > dangerValue) {
    if (target && target.body) {
      // figure out if need to turn left or right
      var localDelta = this.body.pointToLocalFrame(target.body.position);
      this.setTurnType(localDelta.x < 0? Motherduck.TurnType.LEFT :
        Motherduck.TurnType.RIGHT);

      // calculate fire
      this.fireableCooldown -= dt;
      if (this.fireableCooldown <= 0 && this.fireable) {
        var type = this.fireable.getType();
        if (this.options.shouldFireFns[type].call(this, dt, game, target)) {
          this.useFireable(game);
          this.fireableCooldown = this.options.fireParams[type].cooldown;
        }
      }
    }
  } else {
    if (danger && danger.body) {
      // figure out if need to turn left or right
      var localDelta = this.body.pointToLocalFrame(danger.body.position);
      this.setTurnType(localDelta.x > 0? Motherduck.TurnType.LEFT :
        Motherduck.TurnType.RIGHT);
    }
  }

  // move forwards
  Motherduck.defaultUpdateFn.call(this, dt, game);
};

/**
 * The default AI options.
 *
 * @property AI.defaultOptions
 * @type {Object}
 */
AI.defaultOptions = {
  updateFn: AI.defaultUpdateFn,
  valueFns: AI.defaultValueFns,
  targetValues: AI.defaultTargetValues,
  dangerFns: AI.defaultDangerFns,
  dangerValues: AI.defaultDangerValues,
  fireParams: AI.defaultFireParams,
  shouldFireFns: AI.defaultShouldFireFns,
};

/**
 * The different possible AI names, Rocket League for LYFE.
 */
AI.names = ['Armstrong','Bandit','Beast','Boomer','Buzz','Casper','Caveman',
'C-Block','Centice','Chipper','Cougar','Dude','Foamer','Fury','Gerwin','Goose',
'Heater','Hollywood','Hound','Iceman','Imp','Jester','JM','Junker','Khan',
'Maverick','Middy','Merlin','Mountain','Myrtle','Outlaw','Poncho','Rainmaker',
'Raja','Rex','Roundhouse','Sabretooth','Saltie','Samara','Scout','Shepard',
'Slider','Squall','Sticks','Stinger','Storm','Sundown','Sultan','Swabbie',
'Tusk','Tex','Viper','Wolfman','Yuri'];