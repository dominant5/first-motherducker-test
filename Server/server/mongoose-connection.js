var mongoose = require ('mongoose');
var options = {
  db: {
    safe: true
  }
};

function connect (url, cb) {
  mongoose.connect(url, options, cb);
}

function disconnect (cb) {
  mongoose.connection.close(cb);
}

function gracefulExit () {
  disconnect(function () {
    process.exit(0);
  });
}
process.on('SIGINT', gracefulExit);
process.on('SIGTERM', gracefulExit);

module.exports = {
  connect: connect,
  disconnect: disconnect,
  localDevUrl: 'mongodb://localhost/motherducker-dev',
  localTestUrl: 'mongodb://localhost/motherducker-test'
};