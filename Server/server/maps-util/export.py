# Exports a blender file to a collada file.

import sys
import bpy

def main():
	args = getArgs(sys.argv)
	output = args[0]
	triangulate = True
	if len(args) > 1 and args[1] == 'False':
		triangulate = False
	export(output, triangulate)

def getArgs(argv):
	usage = 'usage: blender <filename>.blend -b --python export.py -- <output path> <triangulate>'
	if not '--' in argv:
		print(usage)
		sys.exit()
	args = argv[argv.index('--') + 1:]
	if len(args) == 0:
		print(usage)
		sys.exit()
	return args

def export(output, triangulate):
	bpy.ops.wm.collada_export(filepath=output,
		check_existing=False,
		filter_blender=False,
		filter_image=False,
		filter_movie=False,
		filter_python=False,
		filter_font=False,
		filter_sound=False,
		filter_text=False,
		filter_btx=False,
		filter_collada=True,
		filter_folder=True,
		filemode=8,
		active_uv_only=True,
		triangulate=triangulate,
		apply_modifiers=True,
		export_transformation_type_selection='transrotloc')

if __name__ == '__main__':
	main()