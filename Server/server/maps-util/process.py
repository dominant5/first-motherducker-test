# Processes a COLLADA file into a format accessible by Motherducker's map
# importer module. Expects all faces to be triangulated, and transform type to
# be TransRotLoc. Handles arbitrarily nested trees of objects.
#
# ### Structure of exported object ###
# {
#   objects: [],
#
#   spawnAreas: {
#     ducklings: [],
#     powerups: [],
#     etc
#   },
#
#   spawnPoints: {
#     motherducks: [],
#     specialPowerups: [],
#     any other types, etc
#   },
#
#   proxies: {
#     tree: [],
#     etc
#   }
# }
#
# ### Regular objects ###
# Anything with a mesh will be added to the exported object in the objects
# array.
#
# ### Spawn points ###
# Any object with no mesh (eg. empty) that has a name with the format
# 'spawn.TYPE' will be added to the exported object in the spawnPoints.TYPE
# array.

import sys
import os
import argparse
import json
import re
import xml.etree.ElementTree as ET
import math

def main():
  args = parseArgs()
  triFile = parseInputFile(args.tri)
  noTriFile = parseInputFile(args.notri)
  converted = process(triFile, noTriFile)
  writeOutputFile(args.output, converted, args.pretty)

def parseArgs():
  parser = argparse.ArgumentParser()
  parser.add_argument('-tri', help = 'Triangulated input file name', required = True)
  parser.add_argument('-notri', help = 'Non triangulated input file name', required = True)
  parser.add_argument('-o', '--output', help = 'Output file name', required = False)
  parser.add_argument('-p', '--pretty', help = 'Pretty print', default = False, action = 'store_true', required = False)
  args = parser.parse_args()
  setName(args)
  if not args.output:
    setDefaultOutput(args)
  return args

def setName(args):
  basename = os.path.basename(os.path.normpath(args.tri))
  args.name, ext = os.path.splitext(basename)
  return args

def setDefaultOutput(args):
  root, ext = os.path.splitext(args.tri)
  args.output = root + '.js'
  return args

def parseInputFile(input):
  if not os.path.isfile(input):
    print ('Input file ' + input + ' doesn\'t exist!')
    sys.exit()
  return open(input, 'r').read()

def writeOutputFile(output, data, pretty):
  indent = None
  separators = (',', ':')
  if pretty:
    indent = 2
    separators = (',', ': ')

  out = open(output, 'w')
  out.write('var map = ')
  out.write(json.dumps(data, indent = indent, separators = separators))
  out.write(';\n')
  out.write('module.exports = map;')

def process(xmlstringTri, xmlstringNoTri):
  ret = {
    'objects': [],
    'spawnPoints': {},
    'spawnAreas': {},
    'proxies': {}
  }

  # remove the xml namespace
  xmlstringTri = re.sub(' xmlns="[^"]+"', '', xmlstringTri, count = 1)
  xmlstringNoTri = re.sub(' xmlns="[^"]+"', '', xmlstringNoTri, count = 1)

  triRoot = ET.fromstring(xmlstringTri)
  noTriRoot = ET.fromstring(xmlstringNoTri)

  # process objects
  nodes = triRoot.find('.//visual_scene')
  triGeometries = triRoot.find('.//library_geometries')
  noTriGeometries = noTriRoot.find('.//library_geometries')
  for node in nodes.iter('node'):
    processOne(node, triGeometries, noTriGeometries, ret)

  return ret

def processOne(node, triGeometries, noTriGeometries, out):
  obj = {}

  addTypes(node, obj)
  addTransform(node, obj)

  if 'spawn' in obj['types']:
    obj['types'].remove('spawn')
    if len(obj['types']) > 0:
      spawnType = obj['types'][0]
      if not spawnType in out['spawnPoints']:
        out['spawnPoints'][spawnType] = []
      obj.pop('types', None) # delete the types property, don't need it
      out['spawnPoints'][spawnType].append(obj)
    return

  elif 'spawnArea' in obj['types']:
    obj['types'].remove('spawnArea')
    if len(obj['types']) > 0:
      spawnAreaType = obj['types'][0]
      if not spawnAreaType in out['spawnAreas']:
        out['spawnAreas'][spawnAreaType] = []
      obj.pop('types', None) # delete the types property, don't need it
      addVerts(node, triGeometries, obj)
      obj.pop('faces', None) # delete the faces property, don't need it
      bakeSpawnAreaVertTransform(obj)
      obj.pop('position', None) # delete the position property, don't need it
      obj.pop('rotation', None) # delete the rotation property, don't need it
      out['spawnAreas'][spawnAreaType].append(obj)
    return

  elif 'proxy' in obj['types']:
    obj['types'].remove('proxy')
    if len(obj['types']) > 0:
      proxyType = obj['types'][0]
      if not proxyType in out['proxies']:
        out['proxies'][proxyType] = []
      obj.pop('types', None) # delete the types property, don't need it
      out['proxies'][proxyType].append(obj)
    return

  geoRef = node.find('instance_geometry')
  if geoRef == None: # it's an Empty or an object with no mesh
    return

  # handle regular object
  else:
    addVerts(node, triGeometries, obj)
    addTriFaces(node, triGeometries, obj)
    if 'v' in obj['types']:
      addNoTriFaces(node, noTriGeometries, obj)
      addUVs(node, noTriGeometries, obj)
      addMaterial(node, obj)
    out['objects'].append(obj)

ignoreTypes = ['Plane', 'Cube', 'Circle', 'Sphere', 'Icosphere', 'Cylinder',
'Cone', 'Torus', 'Grid', 'Suzanne', 'BezierCurve', 'BezierCircle', 'NurbsCurve',
'NurbsCircle', 'NurbsPath', 'SurfCurve', 'SurfCircle', 'SurfPatch', 'Surface',
'SurfSphere', 'SurfTorus', 'MBall', 'Text', 'Armature', 'Lattice', 'Empty'];

# Populates out['types'] with the node's types. These are extracted from the
# node's name, separated by a '_' character. Note: Blender converts '.' to '_'
# when exporting to COLLADA, so in Blender, object types should be separated by
# a '.' character: type1.type2.type3, etc. This also ignores the '001', '002',
# etc appended to objects with duplicate names. Ignores default Blender object
# names in Mesh, Curve, Surface, Metaball, Armature and Empty menus, as well as
# Text and Lattice. This also ignores any type starting with 'material'
def addTypes(node, obj):
  obj['types'] = []

  types = node.attrib['name'].split('_')

  for x in range(0, len(types)):
    type = types[x]

    endDigits = re.match('^\d{3}$', types[x]) # matches exactly 3 digits
    if endDigits != None:
      continue

    material = re.match('^material', types[x]) # matches if word starts with 'material'
    if material != None:
      continue

    if (type in ignoreTypes):
      continue

    obj['types'].append(type)

# Sets out['material'] to the node's material (if present). This material is
# extracted from the node's name; it matches the first element starting with
# material, removes 'material' from the start, sets the property, and exits.
def addMaterial(node, obj):
  types = node.attrib['name'].split('_')

  for x in range(0, len(types)):
    type = types[x]

    material = re.match('^material', types[x]) # matches if word starts with 'material'
    if material == None:
      continue

    obj['material'] = type[8:] # strip 'material' from the start
    break

# Follows the COLLADA schema for the indices. Blender collada exporter exports
# Z-UP, we need Y-UP; see the comments for which axes were swapped.
def addTransform(node, obj):
  obj['position'] = {}
  obj['rotation'] = {}

  # position
  location = node[0].text.split(' ')
  obj['position']['x'] = float(location[0])
  obj['position']['y'] = float(location[2]) # y = z
  obj['position']['z'] = float(location[1]) # z = y

  # rotation
  obj['rotation']['z'] = float(node[2].text.split(' ')[3]) # z = y
  obj['rotation']['y'] = -float(node[1].text.split(' ')[3]) # y = -z
  obj['rotation']['x'] = float(node[3].text.split(' ')[3])

def bakeSpawnAreaVertTransform(obj):
  xR = obj['rotation']['x'] * math.pi / 180
  yR = obj['rotation']['y'] * math.pi / 180
  zR = obj['rotation']['z'] * math.pi / 180

  _00 = math.cos(yR) * math.cos(zR)
  _01 = math.cos(yR) * -math.sin(zR)
  _02 = math.sin(yR)
  _10 = math.sin(xR) * math.sin(yR) * math.cos(zR) + math.cos(xR) * math.sin(zR)
  _11 = math.cos(xR) * math.cos(zR) - math.sin(xR) * math.sin(yR) * math.sin(zR)
  _12 = math.cos(yR) * -math.sin(xR)
  _20 = math.sin(xR) * math.sin(zR) - math.cos(xR) * math.sin(yR) * math.cos(zR)
  _21 = math.cos(xR) * math.sin(yR) * math.sin(zR) + math.sin(xR) * math.cos(zR)
  _22 = math.cos(xR) * math.cos(yR)

  xP = obj['position']['x']
  yP = obj['position']['y']
  zP = obj['position']['z']

  for i in range(0, len(obj['verts']), 3):
    x = obj['verts'][i]
    y = obj['verts'][i+1]
    z = obj['verts'][i+2]

    obj['verts'][i] = _00 * x + _01 * y + _02 * z + xP
    obj['verts'][i+1] = _10 * x + _11 * y + _12 * z + yP
    obj['verts'][i+2] = _20 * x + _21 * y + _22 * z + zP

# Follows the COLLADA schema for the indices. Blender collada exporter exports
# Z-UP, we need Y-UP; see the comments for which axes were swapped.
def addVerts(node, geometries, obj):
  geoRef = node.find('instance_geometry')
  if geoRef == None:
    return
  id = geoRef.attrib['url']
  id = id[1:] # removes the '#' at the start of the id
  geo = geometries.find('.//geometry[@id=\'{}\']'.format(id))

  obj['verts'] = []

  # scale
  scale = node[4].text.split(' ')
  sX = float(scale[0])
  sY = float(scale[1])
  sZ = float(scale[2])

  # verts
  vertsEl = geo.find('.//float_array[@id=\'{}\']'.format(id + '-positions-array'))
  verts = vertsEl.text.split(' ')
  for x in range(0, len(verts), 3):
    obj['verts'].append(round(float(verts[x]) * sX, 4))
    obj['verts'].append(round(float(verts[x+2]) * sZ, 4)) # y = z
    obj['verts'].append(round(float(verts[x+1]) * sY, 4)) # z = y

def addTriFaces(node, triGeometries, obj):
  geoRef = node.find('instance_geometry')
  if geoRef == None:
    return
  id = geoRef.attrib['url']
  id = id[1:] # removes the '#' at the start of the id
  geo = triGeometries.find('.//geometry[@id=\'{}\']'.format(id))

  polyListEl = geo.find('.//polylist')
  list = polyListEl.find('p').text.split(' ')
  vectors = []
  attributes = polyListEl.findall('input[@semantic]')

  # parse vectors
  for x in range(0, len(list), len(attributes)):
    vector = []
    for y in range(0, len(attributes)):
      vector.append(list[x+y])
    vectors.append(vector)

  for x in range(0, len(attributes)):
    attr = attributes[x].attrib['semantic']
    offset = int(attributes[x].attrib['offset'])

    if attr == 'VERTEX':
      if not 'tris' in obj:
        obj['tris'] = []

      for y in range(0, len(vectors), 3):
        obj['tris'].append(int(vectors[y][offset]))
        obj['tris'].append(int(vectors[y+2][offset]))
        obj['tris'].append(int(vectors[y+1][offset]))

def addNoTriFaces(node, noTriGeometries, obj):
  geoRef = node.find('instance_geometry')
  if geoRef == None:
    return
  id = geoRef.attrib['url']
  id = id[1:] # removes the '#' at the start of the id
  geo = noTriGeometries.find('.//geometry[@id=\'{}\']'.format(id))

  polyListEl = geo.find('.//polylist')
  list = polyListEl.find('p').text.split(' ')
  vectors = []
  attributes = polyListEl.findall('input[@semantic]')

  # parse vectors
  for x in range(0, len(list), len(attributes)):
    vector = []
    for y in range(0, len(attributes)):
      vector.append(list[x+y])
    vectors.append(vector)

  for x in range(0, len(attributes)):
    attr = attributes[x].attrib['semantic']
    offset = int(attributes[x].attrib['offset'])

    if attr == 'VERTEX':
      if not 'quads' in obj:
        obj['quads'] = []

      for y in range(0, len(vectors)):
        obj['quads'].append(int(vectors[y][offset]))

def addUVs(node, noTriGeometries, obj):
  geoRef = node.find('instance_geometry')
  if geoRef == None:
    return
  id = geoRef.attrib['url']
  id = id[1:] # removes the '#' at the start of the id
  geo = noTriGeometries.find('.//geometry[@id=\'{}\']'.format(id))

  uvEl = geo.find('.//float_array[@id=\'{}\']'.format(id + '-map-array'))
  if uvEl == None:
    return

  obj['uv'] = []
  uv = uvEl.text.split(' ')
  for x in range(0, len(uv)):
    obj['uv'].append(float(uv[x]));

if __name__ == '__main__':
  main()