module.exports = exp = {};

var Cannon = require('cannon'),
StatesMixin = require('../states-mixin'),
TypesMixin = require('../types-mixin');

maps = {};

function loadMap (mapname, copy) {
  if (!maps[mapname]) {
    try {
      maps[mapname] = createMap(require('../maps/' + mapname + '.mdmap'))
    } catch (ex) {
      console.log(ex);
      return false;
    }
  }

  if (copy) return copyMap(maps[mapname]);

  return true;
};

function getMapSpawnAreaTotalAreas (mapname) {
  if (!maps[mapname]) {
    try {
      maps[mapname] = createMap(require('../maps/' + mapname + '.mdmap'))
    } catch (ex) {
      console.log(ex);
      return false;
    }
  }

  var ret = {};
  var totalAreas = maps[mapname].spawnAreaTotalAreas;
  for (var type in totalAreas) {
    ret[type] = totalAreas[type];
  }
  return ret;
};

function copyMap (map) {
  var ret = {
    entities: [],
    spawnPoints: {},
    spawnAreas: {},
    spawnAreaTotalAreas: {},
    proxies: {}
  };

  // create entities
  for (var i = 0; i < map.objects.length; i++) {
    var objSpec = map.objects[i];
    var entity = {};

    // quads
    if (objSpec.quads) entity.quads = objSpec.quads;

    // uv
    if (objSpec.uv) entity.uv = objSpec.uv;

    var body = new Cannon.Body({ mass: 0 });

    // apply spec
    body.addShape(objSpec.shape);
    body.position.copy(objSpec.position);
    body.quaternion.copy(objSpec.quaternion);

    entity.body = body;
    body.entity = entity;

    TypesMixin(entity);
    entity.addType('map');
    var types = objSpec.types;
    for (var j = 0; j < types.length; j++) {
      entity.addType(types[j]);
    }

    if (objSpec.material) entity.material = objSpec.material;

    StatesMixin(entity);
    ret.entities.push(entity);
  }

  // copy spawnPoints
  for (var type in map.spawnPoints) {
    ret.spawnPoints[type] = [];
    for (var i = 0; i < map.spawnPoints[type].length; i++) {
      var spawnPointSpec = map.spawnPoints[type][i];
      ret.spawnPoints[type].push({
        position: spawnPointSpec.position.clone(),
        quaternion: spawnPointSpec.quaternion.clone()
      });
    }
  }

  // copy spawnAreas
  for (var type in map.spawnAreas) {
    ret.spawnAreas[type] = [];
    for (var i = 0; i < map.spawnAreas[type].length; i++) {
      var spawnAreaSpec = map.spawnAreas[type][i];
      ret.spawnAreas[type].push({
        vec1: spawnAreaSpec.vec1.clone(),
        vec2: spawnAreaSpec.vec2.clone(),
        corner: spawnAreaSpec.corner.clone(),
        area: spawnAreaSpec.area,
        weight: spawnAreaSpec.weight
      });
    }
  }

  // copy spawnArea total areas
  for (var type in map.spawnAreaTotalAreas) {
    ret.spawnAreaTotalAreas[type] = map.spawnAreaTotalAreas[type];
  }

  // copy proxies
  for (var type in map.proxies) {
    ret.proxies[type] = [];
    for (var i = 0; i < map.proxies[type].length; i++) {
      var proxySpec = map.proxies[type][i];
      ret.proxies[type].push({
        p: proxySpec.position.clone(),
        q: proxySpec.quaternion.clone()
      });
    }
  }

  return ret;
};

function createMap (mapModule) {
  var map = {
    objects: [],
    spawnPoints: {},
    spawnAreas: {},
    spawnAreaTotalAreas: {},
    proxies: {}
  };
  for (var i = 0; i < mapModule.objects.length; i++) {
    var spec = mapModule.objects[i];
    var objSpec = {
      shape: null,
      position: null,
      quaternion: null,
      types: []
    };

    var rawVerts = spec.verts;
    var verts = [];
    for (var j = 0; j < rawVerts.length; j+=3) {
      verts.push(new Cannon.Vec3(rawVerts[j], rawVerts[j+1], rawVerts[j+2]));
    }

    var rawTris = spec.tris;
    var tris = [];
    for (var j = 0; j < rawTris.length; j+=3) {
      tris.push([rawTris[j], rawTris[j+1], rawTris[j+2]]);
    }

    if (spec.quads) {
      var rawQuads = spec.quads;
      var quads = [];
      for (var j = 0; j < rawQuads.length; j+=4) {
        quads.push([rawQuads[j], rawQuads[j+1], rawQuads[j+2], rawQuads[j+3]]);
      }
      objSpec.quads = quads;
    }

    if (spec.uv) {
      var rawUV = spec.uv;
      var uv = [];
      for (var j = 0; j < rawUV.length; j+=2) {
        uv.push([rawUV[j], rawUV[j+1]]);
      }
      objSpec.uv = uv;
    }

    objSpec.shape = new Cannon.ConvexPolyhedron(verts, tris);

    var pos = spec.position;
    objSpec.position = new Cannon.Vec3(pos.x, pos.y, pos.z);

    var rot = spec.rotation;
    objSpec.quaternion = new Cannon.Quaternion();
    objSpec.quaternion.setFromEuler(rot.x * Math.PI/180, rot.y * Math.PI/180, rot.z * Math.PI/180);

    var types = spec.types;
    for (var j = 0; j < types.length; j++) {
      objSpec.types.push(types[j]);
    }

    if (spec.material) objSpec.material = spec.material;
    
    map.objects.push(objSpec);
  }

  for (var type in mapModule.spawnPoints) {
    map.spawnPoints[type] = [];
    for (var i = 0; i < mapModule.spawnPoints[type].length; i++) {
      var spec = mapModule.spawnPoints[type][i];
      var spawnPointSpec = {
        position: null,
        quaternion: null
      };
      var pos = spec.position;
      spawnPointSpec.position = new Cannon.Vec3(pos.x, pos.y, pos.z);

      var rot = spec.rotation;
      spawnPointSpec.quaternion = new Cannon.Quaternion();
      spawnPointSpec.quaternion.setFromEuler(rot.x * Math.PI/180, rot.y * Math.PI/180, rot.z * Math.PI/180);

      map.spawnPoints[type].push(spawnPointSpec);
    }
  }

  for (var type in mapModule.spawnAreas) {
    map.spawnAreas[type] = [];
    map.spawnAreaTotalAreas[type] = 0;
    for (var i = 0; i < mapModule.spawnAreas[type].length; i++) {
      var verts = mapModule.spawnAreas[type][i].verts;
      var c1 = new Cannon.Vec3(verts[0], verts[1], verts[2]);
      var c2 = new Cannon.Vec3(verts[3], verts[4], verts[5]);
      var c3 = new Cannon.Vec3(verts[6], verts[7], verts[8]);
      var vec1 = c2.vsub(c1);
      var vec2 = c3.vsub(c1);
      var area = vec1.length() * vec2.length();
      map.spawnAreaTotalAreas[type] += area;
      var spawnAreaSpec = {
        vec1: vec1,
        vec2: vec2,
        corner: c1,
        area: area
      };
      map.spawnAreas[type].push(spawnAreaSpec);
    }

    for (var i = 0; i < map.spawnAreas[type].length; i++) {
      var spawnAreaSpec = map.spawnAreas[type][i];
      spawnAreaSpec.weight = spawnAreaSpec.area / map.spawnAreaTotalAreas[type];
    }
  }

  for (var type in mapModule.proxies) {
    map.proxies[type] = [];
    for (var i = 0; i < mapModule.proxies[type].length; i++) {
      var spec = mapModule.proxies[type][i];
      var proxySpec = {
        position: null,
        quaternion: null
      };
      var pos = spec.position;
      proxySpec.position = new Cannon.Vec3(pos.x, pos.y, pos.z);

      var rot = spec.rotation;
      proxySpec.quaternion = new Cannon.Quaternion();
      proxySpec.quaternion.setFromEuler(rot.x * Math.PI/180, rot.y * Math.PI/180, rot.z * Math.PI/180);

      map.proxies[type].push(proxySpec);
    }
  }
  
  return map;
};

module.exports = {
  loadMap: loadMap,
  getMapSpawnAreaTotalAreas: getMapSpawnAreaTotalAreas
}