import sys
import os
import argparse
import subprocess
import glob

def main():
  args = parseArgs()
  convertAll(args.input, args.output, args.scripts, args.keepDae)

def parseArgs():
  parser = argparse.ArgumentParser()
  parser.add_argument('-i', '--input', help = 'Input directory', required = True)
  parser.add_argument('-o', '--output', help = 'Output directory', required = False)
  parser.add_argument('-s', '--scripts', help = 'Directory containing export.py and process.py', required = True)
  parser.add_argument('-k', '--keepDae', help = 'Keep temp dae file', default = False, action = 'store_true', required = False)
  args = parser.parse_args()

  if not args.output:
    args.output = args.input

  # add trailing slashes
  args.input = os.path.join(args.input, '')
  args.output = os.path.join(args.output, '')
  args.scripts = os.path.join(args.scripts, '')

  return args

def convertOne(inputDir, outputDir, scriptsDir, name, keepDae):
  inputPath = inputDir + name
  outputPath = outputDir + name

  # blend -> dae (triangulated faces)
  subprocess.call('blender {}.blend -b --python {}export.py -- {}.dae True'.format(inputPath, scriptsDir, inputPath), shell = True)
  # blend -> dae (non-triangulated faces)
  subprocess.call('blender {}.blend -b --python {}export.py -- {}-notri.dae False'.format(inputPath, scriptsDir, inputPath), shell = True)

  # dae -> mdmap
  subprocess.call('python {}process.py -tri {}.dae -notri {}-notri.dae -o {}.mdmap'.format(scriptsDir, inputPath, inputPath, outputPath), shell = True)

  # delete dae
  if keepDae == False:
    os.remove(inputPath + '.dae')
    os.remove(inputPath + '-notri.dae')

def convertAll(inputDir, outputDir, scriptsDir, keepDae):
  for filename in glob.glob(inputDir + '*.blend'):
    basename = os.path.basename(os.path.normpath(filename))
    name, ext = os.path.splitext(basename)
    convertOne(inputDir, outputDir, scriptsDir, name, keepDae)

if __name__ == '__main__':
  main()