module.exports = GameManager;

var q = require('q');
var SessionManager = require('./session-manager');
var PartyManager = require('./party-manager');
var merge = require('deepmerge');
var NameUtils = require('./name-utils');
var ErrorHandler = require('./error-handler');
var Game = require('./game');
var Maps = require('./maps-util/maps');
var EmitterMixin = require('./emitter-mixin');

/**
 * Responsible for maintaining a list of running games, tracking their lifetime,
 * and adding/removing clients into games.
 *
 * @class GameManager
 * @constructor
 * @param {Server} server the server instance.
 * @param {Object} options any overriden options for this gameManager.
 * @param {Object} options.gameOptions any overriden options for games created
 * by this gameManager. This should be the same format as the options param for
 * the Game constructor.
 * events (defined in game-manager-event-codes.js).
 */
function GameManager (server, sessionManager, partyManager, options) {
  /**
   * The Server instance.
   *
   * @property server
   * @type {Server}
   */
  this.server = server;

  /**
   * The list of games, indexed by gameName.
   *
   * @property games
   * @type {Object}
   */
  this.games = {};

  EmitterMixin(this);

  sessionManager.on(SessionManager.EventCodes.STARTED, this.onSessionStarted, this);
  sessionManager.on(SessionManager.EventCodes.ENDED, this.onSessionEnded, this);
  this.sessionManager = sessionManager;

  this.partyManager = partyManager;

  this.options = merge(GameManager.defaultOptions, options || {});
}

GameManager.EventCodes = {
  GAME_CREATED: 0,
  GAME_STARTED: 1,
  CLIENT_JOINED_GAME: 2,
  CLIENT_LEFT_GAME: 3,
  GAME_ENDED: 4,
  DELETED_EMPTY_GAME: 5
};

GameManager.defaultOptions = {
  /**
   * Each entry in spawn has the following properties:
   * oneRate: see Game.calculateSpawnRate
   * eightRate: see Game.calculateSpawnRate
   * variance: the spawn frequency variance as a fraction of the spawn rate
   */
  spawn: {
    ducklings: {
      oneRate: 75,
      eightRate: 100,
      variance: 0.05
    },
    powerups: {
      oneRate: 30,
      eightRate: 50,
      variance: 0.05
    }
  }
};

/**
 * Registers game related callbacks on the client.
 *
 * @method onSessionStarted
 * @param {Client} client the client that just started a session
 */
GameManager.prototype.onSessionStarted = function(client) {
  client.on('game:find', function (data) {
    this.place(client, data);
  }.bind(this));
  client.on('game:leave', function () {
    this.disconnect(client);
  }.bind(this));
};

/**
 * Removes the client from its game (if it's in one).
 *
 * @method onSessionEnded
 * @param {Client} client the client that just ended a session
 */
GameManager.prototype.onSessionEnded = function(client) {
  client.off('game:find');
  client.off('game:leave');
  this.disconnect(client);
};

/**
 * Removes the client from the game it's in, and destroys the game if necessary.
 *
 * @method disconnect
 * @param {Client} client the client to remove.
 */
GameManager.prototype.disconnect = function(client) {
  var gameId = client.gameId;
  var game = this.games[gameId];
  if (!game) return;
  game.removeClient(client).then(function (clientCount) {
    this.emit(GameManager.EventCodes.CLIENT_LEFT_GAME, client);
    if (clientCount === 0) {
      game.destroy();
      delete this.games[gameId];
      this.emit(GameManager.EventCodes.DELETED_EMPTY_GAME, game);
    }
  }.bind(this));
};

/**
 * Places the client in a suitable game, otherwise in a new game.
 *
 * @method place
 * @param {Client} client the client to place.
 * @param {Object} data the game options to find.
 */
GameManager.prototype.place = function(client, data) {
  if (client.partyStatus === PartyManager.StatusCodes.PARTY_MEMBER) {
    return ErrorHandler.emit(client, 'game:find', new Error('Only the party leader can search for games'));
  }

  var clients = [];

  if (client.partyStatus === PartyManager.StatusCodes.PARTY_LEADER) {
    clients = this.partyManager.getPartyMemberClients(client.id());
  } else { // not in a party
    clients.push(client);
  }

  //In case no data or partial data was sent, use default.
  data.gameMode = data.gameMode || 'ffa';
  data.numClients = Number(data.numClients) || 2;
  if (data.gameMode === 'ffa') {
    data.teamSize = 1;
  } else {
    data.teamSize = Number(data.teamSize) || 1;
  }
  data.gameTimer = Number(data.gameTimer) || 120;

  var suitableGame;
  for (var key in this.games) {
    var game = this.games[key];
    if (game.status !== Game.StatusCodes.WAITING_ON_CLIENTS) continue;
    if (game.options.gameMode !== data.gameMode) continue;
    if (game.options.start.numClients !== data.numClients) continue;
    if (game.options.start.teamSize !== data.teamSize) continue;
    if (game.options.gameTimer !== data.gameTimer) continue;
    suitableGame = game;
    break;
  }
  suitableGame = suitableGame || this.createGame(data);
  this.addClientsToGame(clients, suitableGame).then(function (game) {
    if (game.readyToStart()) {
      game.status = Game.StatusCodes.WAITING_TO_START;
      game.getClientsReady().then(function () {
        this.emit(GameManager.EventCodes.GAME_STARTED, game);
        game.start();
      }.bind(this));
    }
  }.bind(this));
};

GameManager.prototype.addClientsToGame = function(clients, game) {
  var dfd = q.defer();

  var addedCount = 0;
  for (var i = 0; i < clients.length; i++) {
    (function (client) {
      game.addClient(client).then(function (readyToStart) {
        this.emit(GameManager.EventCodes.CLIENT_JOINED_GAME, {
          game: game,
          client: client
        });
        addedCount++;
        if (addedCount === clients.length) dfd.resolve(game);
      }.bind(this));
    }.bind(this))(clients[i]);
  }

  return dfd.promise;
};

GameManager.prototype.onGameEnded = function(game) {
  this.emit(GameManager.EventCodes.GAME_ENDED, game);

  for (var key in game.clients) {
    (function (client) {
      this.disconnect(client);
    }.bind(this))((game.clients[key]));
  }
};

/**
 * Creates a new Game and adds it to the list of games.
 *
 * @method createGame
 * @param {Object} dataOptions the game options to use.
 * @return {Game} the newly created game.
 */
GameManager.prototype.createGame = function(dataOptions) {
  var id = NameUtils.generateName('Game');
  var emitFn = function (gameId, event, data) {
    this.server.to(gameId, event, data);
  }.bind(this);
  var options = this.createOptions(dataOptions);
  var game = new Game(id, emitFn, options);
  game.on(Game.EventCodes.ENDED, this.onGameEnded, this);

  this.games[id] = game;
  this.emit(GameManager.EventCodes.GAME_CREATED, game);
  return game;
};

/**
 * Creates a new set of options for the game.
 *
 * @method createOptions
 * @param {Object} data the game options to use.
 * @return {Game} the newly created game.
 */
GameManager.prototype.createOptions = function(data)  {
  var numPlayers = 1;
  if (data.gameMode === 'ffa') {
    numPlayers = data.numClients;
  } else if (data.gameMode === 'team') {
    numPlayers = data.teamSize * 2;
  }
  
  var mapName = 'defaultflat';
  var totalAreas = Maps.getMapSpawnAreaTotalAreas(mapName);
  var ducklingSpawnRate = Game.calculateSpawnRate(
    this.options.spawn.ducklings.oneRate,
    this.options.spawn.ducklings.eightRate,
    numPlayers,
    totalAreas.ducklings);
  var ducklingSpawnVar = ducklingSpawnRate * this.options.spawn.ducklings.variance;
  var powerupSpawnRate = Game.calculateSpawnRate(
    this.options.spawn.powerups.oneRate,
    this.options.spawn.powerups.eightRate,
    numPlayers,
    totalAreas.powerups);
  var powerupSpawnVar = ducklingSpawnRate * this.options.spawn.powerups.variance;
  
  var options = {
    gameMode: data.gameMode,
    aiOptions: {   
    },
    start: {
      numClients: data.numClients,
      teamSize: data.teamSize,
      fillWithAI: true
    },
    randomMotherduckSpawnPoints: true,
    duckSpeed: 10,
    duckTurnSpeed: 3,
    ducklings: {
      spawnFreq: ducklingSpawnRate,
      spawnFreqVar: ducklingSpawnVar
    },
    powerups: {
      spawnFreq: powerupSpawnRate,
      spawnFreqVar: powerupSpawnVar
    },
    mapName: mapName,
    emitFps: 30,
    simFps: 120,
    gameTimer: data.gameTimer
  };
  
  return options;
};