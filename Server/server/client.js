module.exports = Client;

var NameUtils = require('./name-utils');
var PartyManager = require('./party-manager');

/**
 * A wrapper around a socket.io socket.
 * Keeps track of extra state: name, status, and gameId.
 *
 * @class Client
 * @constructor
 * @param {Object} socket the socket.io socket.
 * @param {String} name the client's name.
 */
function Client (socket) {
  /**
   * This socket.io socket.
   *
   * @property socket
   * @type {Object}
   */
  this.socket = socket;

  /**
   * The default guest name for this client.
   *
   * @property guestName
   * @type {String}
   */
  this.guestName = NameUtils.generateName('Client');

  /**
   * This client's name.
   *
   * @property name
   * @type {String}
   */
  this.name = this.guestName;

  /**
   * This client's party status.
   *
   * @property partyStatus
   * @type {Number}
   */
  this.partyStatus = PartyManager.StatusCodes.NOT_IN_PARTY;

  /**
   * This client's status.
   *
   * @property status
   * @type {Number}
   */
  this.status = Client.StatusCodes.CREATED;

  /**
   * This client's gameId.
   *
   * @property gameId
   * @type {String}
   */
  this.gameId = null;
  
  /**
   * This client's current ping.
   *
   * @property ping
   * @type {Number}
   */
  this.ping = 0;
}

Client.StatusCodes = {
  CREATED: 0,
  LOBBY: 1,
  WAITING: 2,
  GAME_STARTED: 3
};

/**
 * A wrapper around the socket.io join function. Joins the client into the room
 * given by room.
 *
 * @method join
 * @param {String} room the name of the room.
 * @param {Function} cb the function to call when the join is done.
 * @example
 *     // in Game.js
 *     client.join(this.id, function () {
 *       client.setStatus(Client.StatusCodes.WAITING);
 *     });
 */
Client.prototype.join = function(room, cb) {
  this.socket.join(room, cb);
};

/**
 * A wrapper around the socket.io leave function.
 *
 * @method leave
 * @param {String} room the name of the room.
 * @param {Function} cb the function to call when the leave is done.
 * @example
 *     // in PartyManager
 *     client.leave(partyId, function () {
 *       // do things
 *     });
 */
Client.prototype.leave = function(room, cb) {
  this.socket.leave(room, cb);
};

/**
 * A wrapper around the socket.io on function. Registers a callback for events
 * coming from the client side of the socket.
 *
 * @method on
 * @param {String} event the name of the event we're listening for.
 * @param {Function} cb the callback to invoke when the event occurs.
 * @example
 *     // in Game.js
 *     client.on('message', function (message) {
 *       this.handleClientMessage(client, message);
 *     });
 */
Client.prototype.on = function (event, cb) {
  this.socket.on(event, cb);
};

/**
 * A wrapper around the socket.io off function. Deregisters a callback for
 * events coming from the client side of the socket.
 *
 * @method off
 * @param {String} event the name of the event we're listening for.
 * @param {Function} cb the callback to invoke when the event occurs.
 * @example
 *     // in Game.js
 *     client.on('message', function (message) {
 *       this.handleClientMessage(client, message);
 *     });
 */
Client.prototype.off = function(event, cb) {
  if (typeof cb === 'undefined') {
    this.socket.removeAllListeners(event);
  } else {
    this.socket.removeListener(event, cb);
  }
};

/**
 * A wrapper around the socket.io emit function. Emits an event to the client.
 *
 * @method emit
 * @param {String} event the name of the event we're sending.
 * @param {Object} data the data to send with the event.
 * @example
 *     client.emit('client:status-change', { status: WAITING });
 */
Client.prototype.emit = function(event, data) {
  this.socket.emit(event, data);
};

/**
 * Returns an object with info about this client.
 *
 * @method getInfo
 * @return {Object} The info object containing socketId, name, status, and
 * gameId.
 */
Client.prototype.getInfo = function () {
  return {
    socketId: this.socket.id,
    name: this.name,
    partyStatus: this.partyStatus,
    status: this.status,
    gameId: this.gameId
  };
};

/**
 * Returns this client's socket id.
 *
 * @method id
 * @return {String} the id
 */
Client.prototype.id = function() {
  return this.socket.id;
};

/**
 * Sets this client's name to name.
 *
 * @method setName
 * @param {String} name the new name.
 */
Client.prototype.setName = function(name) {
  this.name = name;
};

/**
 * Sets this client's status, and emits a status change event
 * ('client:status-change').
 *
 * @method setStatus
 * @param {Number} status the client's new status. These are defined in
 * client-status-codes.js.
 * @param {Function} cb the function to call when when the emit is done.
 */
Client.prototype.setStatus = function (status, cb) {
  if (status === this.status) return;
  var oldStatus = this.status;
  this.status = status;
  var info = this.getInfo();
  info.oldStatus = oldStatus;
  this.socket.emit('client:status-change', info);
  if (cb) cb(info);
};
