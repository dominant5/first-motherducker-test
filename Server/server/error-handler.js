function emit (client, type, err) {
  client.emit('err', format(type, err));
}

function format (type, err) {
  var ret = {
    type: type,
    message: err.message
  };
  if (err.errors) {
    var errors = [];
    Object.keys(err.errors).forEach(function (field) {
      errors.push(err.errors[field].message);
    });
    ret.errors = errors;
  }
  return ret;
}

module.exports = {
  emit: emit,
  format: format
};