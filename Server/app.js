
'use strict';

require('v8-profiler');

var path = require('path');
var express = require('express');
var app = express().use(express.static(path.join(__dirname, 'public')));

var Maps = require('./server/maps-util/maps');
['defaultflat'].forEach(function (mapName) {
  Maps.loadMap(mapName);
});

var Server = require('./server/server');
var server = new Server(9000, app);
server.on(Server.CONNECT, function (client) {
  console.log('SERVER:      ', 'CLIENT CONNECTED   ', client.id());
});
server.on(Server.DISCONNECT, function (client) {
  console.log('SERVER:      ', 'CLIENT DISCONNECTED', client.id());
});

var conn = require ('./server/mongoose-connection');
conn.connect(conn.localDevUrl, function (err) {
  if (err) throw err;
  console.log('database connected');
});

var SessionManager = require('./server/session-manager');
var sessionManager = new SessionManager(server);
sessionManager.on(SessionManager.EventCodes.STARTED, function (client, sessionName) {
  console.log('SESSION MANAGER:', 'SESSION STARTED ', sessionName);
});
sessionManager.on(SessionManager.EventCodes.ENDED, function (client, sessionName) {
  console.log('SESSION MANAGER:', 'SESSION ENDED ', sessionName);
});

var PartyManager = require('./server/party-manager');
var partyManager = new PartyManager(server, sessionManager);
for (var key in PartyManager.EventCodes) {
  if (key === 'toString') continue;
  (function (key) {
    partyManager.on(PartyManager.EventCodes[key], function (party, client) {
      var args = ['PARTY MANAGER:', key, 'LEADER:', party.leaderName];
      if (client) {
        args.push('MEMBER:');
        args.push(client.name);
      }
      console.log.apply(console, args);
    });
  })(key);
}

var GameManager = require('./server/game-manager');
var gameManagerOptions = {
  multipliers: {
    ducklings: {
      spawnFreq: 6.0,
      spawnFreqVar: 1.0/3.0
    },
    powerups: {
      spawnFreq: 10.0,
      spawnFreqVar: 1.0/3.0
    }
  }
};
var gameManager = new GameManager(server, sessionManager, partyManager, gameManagerOptions);
for (var key in GameManager.EventCodes) {
  if (key === 'toString') continue;
  (function(key) {
    gameManager.on(GameManager.EventCodes[key], function() {
      console.log('GAME MANAGER:', key);
    });
  })(key);
}

var AccountManager = require ('./server/account-manager');
var accountManager = new AccountManager(server, sessionManager);

var StatsManager = require ('./server/stats-manager');
var statsManager = new StatsManager(server, sessionManager, gameManager);

