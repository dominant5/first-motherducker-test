(function (ctx) {
  var socket = io(window.location.href);
  socket.on('connect', onConnect);
  socket.on('disconnect', onDisconnect);
  socket.on('client:status-change', onStatusChange);
  socket.on('ping-update-request', sendPing);
  socket.on('pong', sendPingUpdate);

  function getSocket () {
    return socket;
  };

  var pingUpdateCb = null;
  function setPingUpdateCb (cb) {
    pingUpdateCb = cb;
  };

  function onConnect () {
    console.log('connect');
  };

  function onDisconnect () {
    console.log('disconnect');
  };

  function onStatusChange (data) {
    console.log('client:status-change', info);
  };

  function sendPing () {
    pingSentTime = Date.now();
    socket.emit('ping');
  };

  function sendPingUpdate () {
    var ping = Date.now() - pingSentTime;
    if (pingUpdateCb) pingUpdateCb(ping);
    socket.emit('ping-update', {
      ping: ping
    });
  };

  ctx.Socket = {
    getSocket: getSocket,
    setPingUpdateCb: setPingUpdateCb
  };
})(this);