var statsFps, statsMs,
scene, camera, renderer,
obsMode,
updateCount,
info,
world,
pendingMessages,
lastServerMessage,
fps,
pingSentTime,
fireable;

var $signup;
var $login;
var $playasguest;
var $logout;
var $getstats;
var $addfriend;
var $findgame;
var $name;
var $ping;
var $invitefriendtoparty;
var $leaveparty;
var $partymembers, partyMemberNames, partyLeader;

var keyListener;
var leftPressed, rightPressed;
var turnType;
var TurnType = {
  LEFT: -1,
  STOP: 0,
  RIGHT: 1
};

function init () {
  // Socket.io setup
  Socket.setPingUpdateCb(pingUpdate);
  socket = Socket.getSocket();
  socket.on('disconnect', onDisconnect);
  socket.on('client:status-change', onStatusChange);
  socket.on('game:ready', onGameReady);
  socket.on('game:started', onGameStarted);
  socket.on('game:spawn', onSpawn);
  socket.on('game:update', onGameUpdate);
  socket.on('game:ended', onGameEnded);
  socket.on('game:remove-entity', onRemoveEntity);
  socket.on('game:get-fireable', onGetFireable);
  socket.on('game:update-ammo', onUpdateAmmo);
  socket.on('err', onErr);
  socket.on('acc:signup-success', onSignupSuccess);
  socket.on('acc:friend-request', onReceiveFriendRequest);
  socket.on('acc:friend-request-sent', onFriendRequestSent);
  socket.on('acc:friend-request-accepted', onFriendRequestAccepted);
  socket.on('session:login-success', onLoginSuccess);
  socket.on('session:play-as-guest-success', onPlayAsGuestSuccess);
  socket.on('session:logout-success', onLogoutSuccess);
  socket.on('party:invite', onPartyInvite);
  socket.on('party:invite-sent', onPartyInviteSent);
  socket.on('party:joined', onJoinedParty);
  socket.on('party:left', onLeftParty);
  socket.on('party:closed', onPartyClosed);
  socket.on('stats:deliver', onStatsDeliver);
  
  fireables = {};

  var h = window.innerHeight;

  // stats setup
  statsFps = new Stats();
  statsFps.setMode(0);
  statsFps.domElement.style.position = 'absolute';
  statsFps.domElement.style.left = '0px';
  statsFps.domElement.style.top = (h - 48) + 'px';
  document.body.appendChild(statsFps.domElement);

  statsMs = new Stats();
  statsMs.setMode(1);
  statsMs.domElement.style.position = 'absolute';
  statsMs.domElement.style.left = '80px';
  statsMs.domElement.style.top = (h - 48) + 'px';
  document.body.appendChild(statsMs.domElement);

  // Three js scene setup
  scene = new THREE.Scene();
  camera = new THREE.TargetCamera(85, window.innerWidth / window.innerHeight, 0.1, 1000);
  renderer = new THREE.WebGLRenderer();
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.setClearColor(0xdddddd, 1);
  $('#three-js-container').append(renderer.domElement);

  obsMode = true;

  updateCount = 0;
  info = {
    name: '',
    status: -1,
  };
  world = {
    entities: {},
    map: {}
  };
  teams = {
  };
  fps = 60;
  pendingMessages = [];
  lastServerMessage = null;

  // ui elements
  $signup = $('#signup');
  $signup.click(handleSignUp);

  $login = $('#login');
  $login.click(handleLogin);

  $playasguest = $('#playasguest');
  $playasguest.click(handlePlayAsGuest);

  $logout = $('#logout');
  $logout.click(handleLogout);
  $logout.hide();

  $getstats = $('#getstats');
  $getstats.click(handleGetStats);
  $getstats.hide();

  $addfriend = $('#addfriend');
  $addfriend.click(handleFriendRequest);
  $addfriend.hide();

  $findgame = $('#findgame');
  $findgame.click(handleFindGame);
  $findgame.hide();

  $name = $('#name');
  $name.hide();

  $ping = $('#ping');

  $partydiv = $('#party');
  $partydiv.hide();

  $invitefriendtoparty = $('#invitefriendtoparty');
  $invitefriendtoparty.click(handleInviteFriendToParty);

  $leaveparty = $('#leaveparty');
  $leaveparty.click(handleLeaveParty);
  $leaveparty.hide();

  $partymembers = $('#partymembers');
  partyMemberNames = [];
  partyLeader = false;

  // vex
  vex.defaultOptions.className = 'vex-theme-os';

  // keypress
  keyListener = new window.keypress.Listener();
  leftPressed = false;
  rightPressed = false;
  turnType = TurnType.STOP;
  keyListener.stop_listening(); // until the game starts
  keyListener.register_combo({
    keys: 'left',
    on_keydown: function () {
      leftPressed = true;
    },
    prevent_repeat: true,
    on_keyup: function () {
      leftPressed = false;
    }
  });

  keyListener.register_combo({
    keys: 'right',
    on_keydown: function () {
      rightPressed = true;
    },
    on_keyup: function () {
      rightPressed = false;
    }
  });

  keyListener.register_combo({
    keys: 'space',
    on_keydown: sendFire,
    prevent_repeat: true
  });
  
  keyListener.register_combo({
    keys: 'down',
    on_keydown: sendDive,
    prevent_repeat: true
  });
};

function onDisconnect () {
  onLogoutSuccess();
};

function onStatusChange (data) {
  info.name = data.name;
  info.status = data.status;
  $name.text('Logged in as: ' + data.name);
};

function onGameReady () {
  socket.emit('game:ready');
};

function onGameStarted (data) {
  console.log('game:started', data);

  // keypress
  keyListener.listen();

  // init world data
  world.entities = {};

  // clear the scene
  scene.children = [];

  // TODO: apply different materials based on map object type
  var mapMaterial = new THREE.MeshNormalMaterial({ side: THREE.BackSide, transparent: true, opacity: 0.8 });
  var hazardMaterial = new THREE.MeshBasicMaterial({ side: THREE.BackSide, color: 0xff0000 });
  for (var key in data.mapObjects) {
    var spec = data.mapObjects[key];
    var mapObj = new THREE.Object3D();
    var p = spec.p;
    mapObj.position.set(p.x, p.y, -p.z); // Unity uses left-handed co-ord system. We flip z axis to match it (Three uses right-handed system).
    var q = spec.q;
    mapObj.quaternion.set(q.x, q.y, q.z, q.w);
    mapObj.quaternion = mapObj.quaternion.conjugate();
    var mapGeo = createConvexPolyGeo(spec.v, spec.f);
    var isHazard = spec.types.indexOf('hazard') !== -1;
    var mapMesh = new THREE.Mesh(mapGeo, isHazard? hazardMaterial : mapMaterial);
    mapObj.add(mapMesh);
    scene.add(mapObj);
  }
  
    // position camera in observation mode
  camera.position.x = 40 * 0.7;
  camera.position.y = 20;
  camera.position.z = -40 * 0.7;
  camera.lookAt(new THREE.Vector3(0, 0, 0));

  // create powerups
  for (var id in data.powerups) {
    var spec = data.powerups[id];
    createPowerup(id, spec);
  }

  // create motherducks
  for (var key in data.motherducks) {
    world.entities[key] = e = {};
    var spec = data.motherducks[key];
    
    teams[spec.name] = spec.team;

    var obj = new THREE.Object3D();
    obj.position.x = spec.p.x;
    obj.position.y = spec.p.y;
    obj.position.z = -spec.p.z;
    obj.quaternion.set(spec.q.x, spec.q.y, spec.q.z, spec.q.w);
    obj.quaternion = obj.quaternion.conjugate();
    scene.add(obj);
    e.body = obj;

    var sphereGeometry = new THREE.SphereGeometry(0.5, 8, 8);
    var sphereMaterial = new THREE.MeshBasicMaterial({ color: 0xff0000 });
    var sphere = new THREE.Mesh(sphereGeometry, sphereMaterial);
    obj.add(sphere);

    var dirGeometry = new THREE.Geometry();
    dirGeometry.vertices.push(new THREE.Vector3(0, 0, 0));
    dirGeometry.vertices.push(new THREE.Vector3(0, 0, -5));
    var dirMaterial = new THREE.LineBasicMaterial({ color: 0xff0000 });
    var line = new THREE.Line(dirGeometry, dirMaterial);
    obj.add(line);

    if (spec.name === info.name) {
      obsMode = false;
      var targetName = 'motherduck';
      camera.addTarget({
        name: targetName,
        targetObject: obj,
        cameraPosition: new THREE.Vector3(0, 5, 10),
        cameraRotation: new THREE.Vector3(45, 0, 0),
        fixed: false,
        stiffness: 1,
        matchRotation: false
      });
      camera.setTarget(targetName);
    }
  }

  setInterval(update, 1000 / fps);
};

function onRemoveEntity (data) {
  if (!world.entities[data.key]) return;
  scene.remove(world.entities[data.key].body);
  delete world.entities[data.key];
};

function onGameEnded (data) {
  console.log(data);
  keyListener.stop_listening();
  var won = 'loser';
  for (var i = 0; i < data.winner.length; i++) {
    if (teams[info.name] === data.winner[i]) {
      var won = 'winner';
    }
  }
  var message = '<p>' + info.name + ' ' + won + '</p>';
  // Iterate through each team
  for (var team = 0; team < data.statistics.length; team++) {
    message += '<p>Team ' + team + '</p>';
    var teamStatistic = data.statistics[team];
    // Iterate through each player
    for (var key in teamStatistic) {
      var player = teamStatistic[key];
      // This set of statistics is the team's overall scores
      if (key == team) key = 'Team ' + team;
      // Iterate through each statistic
      for (var stat in player) {
        message += '<p>  ' + key + ' ' + stat + ': ' + player[stat] + '</p>';
      }
    }
  }
  vex.dialog.alert({ message: message });
};

function createConvexPolyGeo (verts, faces) {
  var geo = new THREE.Geometry();
  for (var i = 0; i < verts.length; i++) {
    var v = verts[i];
    geo.vertices.push(new THREE.Vector3(v.x, v.y, -v.z));
  }
  for (var i = 0; i < faces.length; i++) {
    var f = faces[i];
    geo.faces.push(new THREE.Face3(f[3], f[1], f[0]));
    geo.faces.push(new THREE.Face3(f[3], f[2], f[1]));
  }
  geo.computeBoundingBox();
  geo.computeBoundingSphere();
  geo.computeFaceNormals();
  return geo;
};

function createPowerup (id, spec) {
  world.entities[id] = e = {};

  var color = 0xffffff;
  var type = spec.powerupType;
  if (type === 'lobbedBomb') color = 0x00ff00;
  else if (type === 'steal') color = 0xff0000;
  else if (type === 'bulletTime') color = 0xff00ff;
  else if (type === 'speed') color = 0xffcc00;
  else if (type === 'magnet') color = 0x00ffff;
  else if (type === 'invertOthers') color = 0xffffff;
  
  var powerupMaterial = new THREE.MeshBasicMaterial({ color: color });

  var obj = new THREE.Object3D();
  obj.position.x = spec.p.x;
  obj.position.y = spec.p.y;
  obj.position.z = -spec.p.z;
  scene.add(obj);
  e.body = obj;
  
  var len = spec.radius * 1.5;
  var powerupGeometry = new THREE.BoxGeometry(len, len, len);
  var powerupMesh = new THREE.Mesh(powerupGeometry, powerupMaterial);
  obj.add(powerupMesh);
};

function createDucklingObj (p, q) {
  var obj = new THREE.Object3D();
  obj.position.x = p.x;
  obj.position.y = p.y;
  obj.position.z = -p.z;
  obj.quaternion.set(q.x, -q.y, q.z, q.w);

  var sphereGeometry = new THREE.SphereGeometry(0.4, 8, 8);
  var sphereMaterial = new THREE.MeshBasicMaterial({ color: 0xffff00 });
  var sphere = new THREE.Mesh(sphereGeometry, sphereMaterial);
  obj.add(sphere);

  var dirGeometry = new THREE.Geometry();
  dirGeometry.vertices.push(new THREE.Vector3(0, 0, 0));
  dirGeometry.vertices.push(new THREE.Vector3(0, 0, -5));
  var dirMaterial = new THREE.LineBasicMaterial({ color: 0xffff00 });
  var line = new THREE.Line(dirGeometry, dirMaterial);
  obj.add(line);

  var crossGeometry = new THREE.Geometry();
  crossGeometry.vertices.push(new THREE.Vector3(-2, 0, 0));
  crossGeometry.vertices.push(new THREE.Vector3(2, 0, 0));
  var crossLine = new THREE.Line(crossGeometry, dirMaterial);
  obj.add(crossLine);

  return obj;
};

function createProjectileObj (p, q) {
  var obj = new THREE.Object3D();
  obj.position.x = p.x; 
  obj.position.y = p.y;
  obj.position.z = -p.z; // Unity uses left-handed co-ord system. We flip z axis to match it (Three uses right-handed system).
  obj.quaternion.set(q.x, q.y, q.z, q.w);
  obj.quaternion = obj.quaternion.conjugate(); // Unity uses left-handed co-ord system. We flip z axis to match it (Three uses right-handed system).

  var sphereGeometry = new THREE.SphereGeometry(0.4, 8, 8);
  var sphereMaterial = new THREE.MeshBasicMaterial({ color: 0x0000ff });
  var sphere = new THREE.Mesh(sphereGeometry, sphereMaterial);
  obj.add(sphere);

  var dirGeometry = new THREE.Geometry();
  dirGeometry.vertices.push(new THREE.Vector3(0, 0, 0));
  dirGeometry.vertices.push(new THREE.Vector3(0, 0, -5));
  var dirMaterial = new THREE.LineBasicMaterial({ color: 0x0000ff });
  var line = new THREE.Line(dirGeometry, dirMaterial);
  obj.add(line);

  var crossGeometry = new THREE.Geometry();
  crossGeometry.vertices.push(new THREE.Vector3(-2, 0, 0));
  crossGeometry.vertices.push(new THREE.Vector3(2, 0, 0));
  var crossLine = new THREE.Line(crossGeometry, dirMaterial);
  obj.add(crossLine);

  return obj;
};

function onSpawn (spawnInfo) {
  if (spawnInfo.type === 'duckling') {
    world.entities[spawnInfo.id] = e = {};
    e.body = createDucklingObj(spawnInfo.p, spawnInfo.q);
    scene.add(e.body);
  } else if (spawnInfo.type === 'projectile') {
    world.entities[spawnInfo.id] = e = {};
    e.body = createProjectileObj(spawnInfo.p, spawnInfo.q);
    scene.add(e.body);
  } else if (spawnInfo.type === 'powerup') {
    world.entities[spawnInfo.id] = e = {};
    e.body = createPowerup(spawnInfo.id, spawnInfo);
    scene.add(e.body);
  }
};

function onGameUpdate (message) {
  lastServerMessage = message;
};

function onGetFireable (data) {
  fireable = {
    ammunition: data.ammunition,
    type: data.type
  };
};

function onSignupSuccess () {
  console.log('signed up!');
  $signup.hide();
};

function handleSignUp () {
  vex.dialog.open({
    message: 'Enter a username and password:',
    input: '<input name=\'username\' type=\'text\' placeholder=\'Username\' required />\n<input name=\'password\' type=\'password\' placeholder=\'Password\' required />',
    buttons: [
    $.extend({}, vex.dialog.buttons.YES, { text: 'Log in' }),
    $.extend({}, vex.dialog.buttons.NO, { text: 'Back' })
    ],
    callback: function (data) {
      if (data === false) return;
      socket.emit('acc:signup', {
        username: data.username,
        password: data.password
      });
    }
  });
};

function onErr (err) {
  vex.dialog.alert(err.message);
  console.log('err', err);
};

function onReceiveFriendRequest (data) {
  console.log(data);
  vex.dialog.open({
    message: 'Accept friend request from ' + data.from + '?',
    buttons: [
    $.extend({}, vex.dialog.buttons.YES, { text: 'Yes' }),
    $.extend({}, vex.dialog.buttons.NO, { text: 'Later' })
    ],
    callback: function (result) {
      if (result === false) return;
      socket.emit('acc:friend-request-accepted', {
        from: data.from
      });
    }
  });
};

function handleGetStats () {
  socket.emit('stats:get', { username: info.name });
};

function handleFriendRequest () {
  vex.dialog.open({
    message: 'Enter your friend\'s username:',
    input: '<input name=\'name\' type=\'text\' placeholder=\'Your friend\'s name\' required />',
    buttons: [
    $.extend({}, vex.dialog.buttons.YES, { text: 'Send friend request' }),
    $.extend({}, vex.dialog.buttons.NO, { text: 'Back' })
    ],
    callback: function (data) {
      if (data === false) return;
      socket.emit('acc:friend-request', {
        name: data.name
      });
    }
  });
};

function handleFindGame () {
  function openModeDialog (cb) {
    var input = '<input name=\'gameMode\' type=\'radio\' value=\'ffa\' checked=\'true\'/>Free for all\n';
    input += '<input name=\'gameMode\' type=\'radio\' value = \'team\'/>Teams';
    vex.dialog.open({
      message: 'Select game mode:',
      input: input,
      buttons: [
      $.extend({}, vex.dialog.buttons.YES, { text: 'Next' }),
      $.extend({}, vex.dialog.buttons.NO, { text: 'Cancel' })
      ],
      callback: cb
    });
  };

  function openSizeDialog (data, cb) {
    if (data === false) return;
    var type = data.gameMode === 'ffa'? 'numClients' : 'teamSize';
    var message = data.gameMode === 'ffa'? 'Number of players:' : 'Team size:';
    var size;
    if (partyLeader) {
      size = partyMemberNames.length;
    } else if (data.gameMode === 'ffa') {
      size = 2;
    } else if (data.gameMode === 'team') {
      size = 1;
    }
    var input = '<input name=\'' + type + '\' type=\'number\' value=\'' + size + '\'/>';
    vex.dialog.open({
      message: message,
      input: input,
      buttons: [
      $.extend({}, vex.dialog.buttons.YES, { text: 'Find game' }),
      $.extend({}, vex.dialog.buttons.NO, { text: 'Cancel' })
      ],
      callback: cb
    });
  };

  openModeDialog(function (data1) {
    if (data1 === false) return;
    var gameMode = data1.gameMode;
    openSizeDialog(data1, function (data2) {
      if (data2 === false) return;
      var emitData = { gameMode: gameMode };
      if (gameMode === 'ffa') {
        emitData.numClients = data2.numClients;
      } else if (gameMode === 'team') {
        emitData.teamSize = data2.teamSize;
        emitData.numClients = data2.teamSize * 2;
      }
      socket.emit('game:find', emitData);
    });
  });
};

function onLoginSuccess (data) {
  console.log('logged in!', data);
  $signup.hide();
  $login.hide();
  $playasguest.hide();
  $logout.show();
  $getstats.show();
  $addfriend.show();
  $findgame.show();
  $name.show();
  $partydiv.show();
  partyMemberNames = [data.name];
  updatePartyMemberNameDisplay();
};

function onPlayAsGuestSuccess (data) {
  console.log('logged in as guest!', data);
  $signup.hide();
  $login.hide();
  $playasguest.hide();
  $logout.show();
  $findgame.show();
  $name.show();
};

function onLogoutSuccess () {
  console.log('logged out!');
  $signup.show();
  $login.show();
  $playasguest.show();
  $logout.hide();
  $getstats.hide();
  $addfriend.hide();
  $findgame.hide();
  $name.hide();
  $partydiv.hide();
  $invitefriendtoparty.show();
  $partymembers.empty();
};

function onFriendRequestSent () {
  console.log('friend request sent!');
};

function onFriendRequestAccepted () {
  console.log('friend request accepted!');
};

function handleLogin() {
  vex.dialog.open({
    message: 'Enter your username and password:',
    input: '<input name=\'username\' type=\'text\' placeholder=\'Username\' required />\n<input name=\'password\' type=\'password\' placeholder=\'Password\' required />',
    buttons: [
    $.extend({}, vex.dialog.buttons.YES, { text: 'Log in' }),
    $.extend({}, vex.dialog.buttons.NO, { text: 'Back' })
    ],
    callback: function (data) {
      if (data === false) return;
      socket.emit('session:login', {
        username: data.username,
        password: data.password
      });
    }
  });
};

function handlePlayAsGuest () {
  socket.emit('session:play-as-guest');
};

function handleLogout() {
  console.log('Logout emitted');
  socket.emit('session:logout');
};

function getTurnInput () {
  if (leftPressed && rightPressed) return TurnType.STOP;
  if (leftPressed) return TurnType.LEFT;
  if (rightPressed) return TurnType.RIGHT;
  return TurnType.STOP;
};

function sendTurn (type) {
  if (type == TurnType.STOP) {
      socket.emit('game:turn-stop');
    } else if (type == TurnType.LEFT) {
      socket.emit('game:turn-left');
    } else {
      socket.emit('game:turn-right');
    }
};

function sendFire () {
  socket.emit('game:fire');
};

function sendDive () {
  socket.emit('game:dive');
};

function onPartyInvite (data) {
  console.log(data);
  vex.dialog.open({
    message: 'Join ' + data.leader + '\'s party?',
    buttons: [
    $.extend({}, vex.dialog.buttons.YES, { text: 'Yes' }),
    $.extend({}, vex.dialog.buttons.NO, { text: 'No' })
    ],
    callback: function (result) {
      if (result === false) return;
      socket.emit('party:join', {
        leader: data.leader
      });
    }
  });
};

function onPartyInviteSent (data) {
  console.log('invite sent to', data.to);
};

function onJoinedParty (data) {
  console.log(data);
  if (data.name === info.name) {
    console.log('You joined ' + data.leader + '\'s party!');
    partyLeader = false;
    $invitefriendtoparty.hide();
  } else if (data.leader === info.name) {
    console.log(data.name + ' joined your party!');
    partyLeader = true;
  } else {
    console.log(data.name + ' joined the party!');
    partyLeader = false;
  }
  $leaveparty.show();
  partyMemberNames = data.members;
  updatePartyMemberNameDisplay();
};

function onLeftParty (data) {
  if (data.name === info.name) {
    partyMemberNames = [info.name];
    updatePartyMemberNameDisplay();
    $invitefriendtoparty.show();
    $leaveparty.hide();
    partyLeader = false;
  } else {
    var idx = partyMemberNames.indexOf(data.name);
    partyMemberNames.splice(idx, 1);
    updatePartyMemberNameDisplay();
    if (partyMemberNames.length === 1) $leaveparty.hide();
  }
};

function onPartyClosed () {
  partyMemberNames = [info.name];
  updatePartyMemberNameDisplay();
  $invitefriendtoparty.show();
  $leaveparty.hide();
  partyLeader = false;
};

function onStatsDeliver (data) {
  console.log(data);
  var message = '<p>Stats for ' + data.username + '</p>';
  message += '<hr>';
  message += '<p>Highscore: ' + data.highscore + '</p>';
  message += '<p>Wins: ' + data.wins + '</p>';
  message += '<p>Losses: ' + data.losses + '</p>';
  message += '<p>Most ducklings collected in a game: ' + data.mostCollectedDucklings + '</p>';
  message += '<p>Total ducklings collected: ' + data.totalCollectedDucklings + '</p>';
  message += '<p>Total ducklings detached: ' + data.totalDetachedDucklings + '</p>';
  message += '<p>Total ducklings stolen: ' + data.totalStolenDucklings + '</p>';
  vex.dialog.alert({ message: message });
};

function updatePartyMemberNameDisplay () {
  $partymembers.empty();
  for (var i = 0; i < partyMemberNames.length; i++) {
    var name = partyMemberNames[i];
    var el = $('<span>');
    el.attr('id', name);
    if (name === info.name) el.css('color', 'blue');
    el.text(name);
    $partymembers.append(el);
  }
};

function handleInviteFriendToParty () {
  vex.dialog.open({
    message: 'Enter your friend\'s username:',
    input: '<input name=\'name\' type=\'text\' placeholder=\'Your friend\'s name\' required />',
    buttons: [
    $.extend({}, vex.dialog.buttons.YES, { text: 'Send invite' }),
    $.extend({}, vex.dialog.buttons.NO, { text: 'Back' })
    ],
    callback: function (data) {
      if (data === false) return;
      socket.emit('party:invite', { name: data.name });
    }
  });
};

function handleLeaveParty () {
  socket.emit('party:leave');
};

function onUpdateAmmo (data) {
  if (data.ammunition === 0) {
    fireable = null;
  }
};

function update () {
  statsFps.begin();
  statsMs.begin();

  // process server messages
  if (lastServerMessage) {
    var i = 0;
    var entities = lastServerMessage.entities;
    var len = entities.length;
    var e = null;
    while (i < len) {
      var token = entities[i];
      if (token >= 0) { // entity id
        e = world.entities[token];
        i++;
      } else if (token === -1) { // position
        if (e) {
          e.body.position.x = entities[i+1];
          e.body.position.y = entities[i+2];
          e.body.position.z = -entities[i+3];
        }
        i += 4;
      } else if (token === -2) { // identity quaternion
        if (e) {
          e.body.quaternion.set(0, 0, 0, 1);
        }
        i++;
      } else if (token === -3) { // quaternion
        if (e) {
          e.body.quaternion.set(entities[i+1], -entities[i+2], entities[i+3], entities[i+4]);
        }
        i += 5;
      } else { // shouldn't happen, but a catch-all just in case
        i = len;
      }
    }
    lastServerMessage = null;
  }

  var turnInput = getTurnInput();
  if (turnInput !== turnType) {
    turnType = turnInput;
    sendTurn(turnInput);
  }

  // var entity = world.entities[info.name];

  // // discard acknowledged messages
  // var proc_ts = entity.proc_ts;
  // for (var i = 0; i < pendingMessages.length; i++) {
  //   var message = pendingMessages[i];
  //   if (message.ts < proc_ts) continue;
  //   pendingMessages.splice(0, i + 1);
  //   break;
  // }

  // // client-side prediction
  // for (var j = 0; j < pendingMessages.length; j++) {
  //   var message = pendingMessages[j];
  //   if (message.input === 'left') entity.x--;
  //   else if (message.input === 'right') entity.x++;
  //   else if (message.input === 'up') entity.y++;
  //   else if (message.input === 'down') entity.y--;
  // }

  camera.update();
  renderer.render(scene, camera);

  statsMs.end();
  statsFps.end();
};

function pingUpdate (ping) {
  $ping.text('Ping: ' + ping + ' ms');
};
