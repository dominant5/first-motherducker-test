var expect = require('expect.js');
var q = require('q');
var conn = require('../server/mongoose-connection');
var UserModel = require('../server/database/user.model');
var FriendRequestListModel = require('../server/database/friend-request-list.model');
var FriendListModel = require('../server/database/friend-list.model');
var SessionManager = require('../server/session-manager');
var AccountManager = require('../server/account-manager');
var Server = require('../server/server');
var io = require('socket.io-client');
var port = 9000;
var socketURL = 'http://localhost:' + port;
var clientSideSocketOptions = {
  transports: ['websocket'],
  'force new connection': true
};

describe('AccountManager', function () {
  var server, sessionManager, accountManager, clients, clientSideSockets;

  var initClientSideSockets = function (numClients) {
    var dfd = q.defer();

    server.on(Server.CONNECT, function (client) {
      clients.push(client);
      if (clients.length === numClients) dfd.resolve();
    });

    for (var i = 0; i < numClients; i++) {
      clientSideSockets.push(io(socketURL, clientSideSocketOptions));
    }

    return dfd.promise;
  };

  beforeEach(function (done) {
    var app = function (req, res) {};
    var opts = { silent: true };
    server = new Server(port, app, opts);
    sessionManager = new SessionManager(server);
    accountManager = new AccountManager(server, sessionManager);
    clients = [];
    clientSideSockets = [];

    conn.connect(conn.localTestUrl, function () {
      done();
    });
  });

  afterEach(function (done) {
    server.stop();
    for (var i = 0; i < clientSideSockets.length; i++) {
      var s = clientSideSockets[i];
      s.disconnect();
    }
    UserModel.remove().exec().then(function () {
      FriendRequestListModel.remove().exec().then(function () {
        FriendListModel.remove().exec().then(function () {
          done();
        });
      });
    });
  });
  
  describe('onClientSignup', function () {
    it('should successfully sign the client up if username available', function (done) {
      initClientSideSockets(1).then(function () {
        clientSideSockets[0].on('acc:signup-success', function () {
          UserModel.find().exec().then(function (users) {
            expect(users.length).to.be(1);
            expect(users[0].username).to.be('test');
            done();
          });
        });

        clientSideSockets[0].emit('acc:signup', {
          username: 'test',
          password: 'test'
        });
      });
    });

    it('should fail to sign the client up if the username exists', function (done) {
      initClientSideSockets(2).then(function () {
        clientSideSockets[1].on('err', function (data) {
          expect(data.message).to.be('Validation failed');
          expect(data.errors.length).to.be(1);
          expect(data.errors[0]).to.be('Username exists');
          done();
        });

        clientSideSockets[0].on('acc:signup-success', function () {
          clientSideSockets[1].emit('acc:signup', {
            username: 'test',
            password: 'test'
          });          
        });

        clientSideSockets[0].emit('acc:signup', {
          username: 'test',
          password: 'test'
        });
      });
    });
  });

  describe('onFriendRequest', function () {
    it('should successfully add a friend request if from and to users exist', function (done) {
      initClientSideSockets(2).then(function () {
        clientSideSockets[1].on('acc:friend-request-sent', function () {
          UserModel.findOne({ username: 'a' }, function (err, a) {
            UserModel.findOne({ username: 'b' }, function (err, b) {
              FriendRequestListModel.findOne({ user: a._id }, function (err, listA) {
                expect(listA).not.to.be(null);
                expect(listA.to.length).to.be(0);
                expect(listA.from.length).to.be(1);
                expect(String(listA.from[0])).to.equal(String(b._id));

                FriendRequestListModel.findOne({ user: b._id }, function (err, listB) {
                  expect(listB).not.to.be(null);
                  expect(listB.to.length).to.be(1);
                  expect(listB.from.length).to.be(0);
                  expect(String(listB.to[0])).to.equal(String(a._id));
                  done();
                });
              });
            });
          });
        });

        clientSideSockets[1].on('acc:signup-success', function () {
          clientSideSockets[1].emit('acc:friend-request', {
            name: 'a'
          });
        });

        clientSideSockets[0].on('acc:signup-success', function () {
          clientSideSockets[1].emit('acc:signup', {
            username: 'b',
            password: 'b'
          });          
        });

        clientSideSockets[0].emit('acc:signup', {
          username: 'a',
          password: 'a'
        });
      });
    });
  });

  describe('onFriendRequestAccepted', function () {
    it('should successfully add the friends to each others\' friend list', function (done) {
      initClientSideSockets(2).then(function () {
        clientSideSockets[1].on('acc:friend-request-accepted', function (data) {
          function inspectFriendLists (aId, bId) {
            FriendListModel.findOne({ user: aId }, function (err, listA) {
              expect(listA).not.to.be(null);
              expect(listA.friends.length).to.be(1);
              expect(String(listA.friends[0])).to.equal(String(bId));

              FriendListModel.findOne({ user: bId }, function (err, listB) {
                expect(listB).not.to.be(null);
                expect(listB.friends.length).to.be(1);
                expect(String(listB.friends[0])).to.equal(String(aId));
                done();
              });
            });
          };

          function inspectFriendRequestLists (aId, bId) {
            FriendRequestListModel.findOne({ user: aId }, function (err, listA) {
              expect(listA).not.to.be(null);
              expect(listA.to.length).to.be(0);
              expect(listA.from.length).to.be(0);

              FriendRequestListModel.findOne({ user: bId }, function (err, listB) {
                expect(listB).not.to.be(null);
                expect(listB.to.length).to.be(0);
                expect(listB.from.length).to.be(0);
                inspectFriendLists(aId, bId);
              });
            });
          };

          UserModel.findOne({ username: 'a' }, function (err, a) {
            UserModel.findOne({ username: 'b' }, function (err, b) {
              inspectFriendRequestLists(a._id, b._id);
            });
          });
        });

        clientSideSockets[0].on('acc:friend-request', function () {
          clientSideSockets[0].emit('acc:friend-request-accepted', {
            from: 'b'
          });
        });

        clientSideSockets[1].on('acc:signup-success', function () {
          clientSideSockets[1].emit('acc:friend-request', {
            name: 'a'
          });
        });

        clientSideSockets[0].on('acc:signup-success', function () {
          clientSideSockets[1].emit('acc:signup', {
            username: 'b',
            password: 'b'
          });          
        });

        clientSideSockets[0].emit('acc:signup', {
          username: 'a',
          password: 'a'
        });
      });
    });
  });
});