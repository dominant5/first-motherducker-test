var expect = require('expect.js');
var Client = require('../server/client');
var Server = require('../server/server');
var PartyManager = require('../server/party-manager');
var io = require('socket.io-client');
var port = 9000;
var socketURL = 'http://127.0.0.1:' + port;
var options = {
  transports: ['websocket'],
  'force new connection': true
};

describe('Client', function () {
  var server, name, socket, clientSideSocket;

  beforeEach(function (done) {
    var app = function (req, res) {};
    var opts = {
      silent: true
    };
    server = new Server(port, app, opts);
    server.on(Server.CONNECT, function (client) {
      socket = client.socket;
      done();
    });
    clientSideSocket = io(socketURL, options);
    name = 'Test client';
  });

  afterEach(function (done) {
    clientSideSocket.disconnect();
    server.stop();
    done();
  });

  it('create', function (done) {
    var client = new Client(socket);
    expect(client.socket).to.equal(socket);
    expect(client.name).to.equal(client.guestName);
    expect(client.partyStatus).to.equal(PartyManager.StatusCodes.NOT_IN_PARTY);
    expect(client.status).to.equal(Client.StatusCodes.CREATED);
    expect(client.gameId).to.equal(null);
    done();
  });

  it('join', function (done) {
    var client = new Client(socket);
    client.join('room', function () {
      clientSideSocket.on('hello', function (data) {
        expect(data).to.equal('foo');
        done();
      });

      server.to('room', 'hello', 'foo');
    });
  });

  it('leave', function (done) {
    var client = new Client(socket);

    setTimeout(function () {
      done();
    }, 50);

    clientSideSocket.on('bar', function () {
      done(new Error('This should not happen'));
    });

    clientSideSocket.on('hello', function (data) {
      client.leave('room', function () {
        server.to('room', 'bar', 'foo');
      });
    });

    client.join('room', function () {
      server.to('room', 'hello', 'foo');
    });
  });

  it('on', function (done) {
    var client = new Client(socket);
    client.on('hello', function (data) {
      expect(data).to.equal('foo');
      done();
    });

    clientSideSocket.emit('hello', 'foo');
  });

  it('off', function (done) {
    var client = new Client(socket, name);
    var receivedCount = 0;
    client.on('hello', function (data) {
      client.off('hello');
      receivedCount++;
      clientSideSocket.emit('hello');
      setTimeout(function () {
        expect(receivedCount).to.be(1);
        done();
      }, 50);
    });

    clientSideSocket.emit('hello');
  });

  it('emit', function (done) {
    var client = new Client(socket);
    clientSideSocket.on('hello', function (data) {
      expect(data).to.equal('foo');
      done();
    });
    client.emit('hello', 'foo');
  });

  it('getInfo', function (done) {
    var client = new Client(socket);
    client.setName(name);
    var info = client.getInfo();
    expect(info.socketId).to.eql(socket.id);
    expect(info.name).to.eql(name);
    expect(info.partyStatus).to.eql(PartyManager.StatusCodes.NOT_IN_PARTY);
    expect(info.status).to.eql(Client.StatusCodes.CREATED);
    expect(info.gameId).to.eql(null);
    done();
  });

  it('setStatus', function (done) {
    var client = new Client(socket);
    client.setName(name);
    expect(client.status).to.eql(Client.StatusCodes.CREATED);
    clientSideSocket.on('client:status-change', function (info) {
      expect(info.socketId).to.eql(socket.id);
      expect(info.name).to.eql(name);
      expect(info.oldStatus).to.eql(Client.StatusCodes.CREATED);
      expect(info.status).to.eql(Client.StatusCodes.WAITING);
      clientSideSocket.disconnect();
      done();
    });
    client.setStatus(Client.StatusCodes.WAITING);
  });
});