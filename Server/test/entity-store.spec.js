var expect = require('expect.js');
var TypesMixin = require('../server/types-mixin');
var EntityStore = require('../server/entity-store');

function createDummyEntity (types, id) {
  var ret = {};
  TypesMixin(ret);
  for (var i = 0; i < types.length; i++) {
    var type = types[i];
    ret.addType(type);
  }
  if (typeof id !== 'undefined') ret.id = id;
  return ret;
};

describe('Entity Store', function () {
  var store;

  beforeEach(function (done) {
    store = new EntityStore();
    done();
  });

  describe('push', function () {
    it('should store an id ref for an entity whose id is a falsy value, except for undefined', function (done) {
      var count = 0, idCount = 0;
      [false, 0, '', null, NaN].forEach(function (id) {
        store.push(createDummyEntity(['motherduck'], id));
        expect(store.count()).to.be(++count);
        expect(Object.keys(store.idMap).length).to.be(++idCount);
      });
      
      store.push(createDummyEntity(['motherduck'], undefined));
      expect(store.count()).to.be(++count);
        expect(Object.keys(store.idMap).length).to.be(idCount);
      done();
    });

    it('should put entities in order into store.entities', function (done) {
      store.push(createDummyEntity(['motherduck'], 'bob'));
      store.push(createDummyEntity(['motherduck'], 'jane'));
      store.push(createDummyEntity(['motherduck'], 't'));
      store.push(createDummyEntity(['motherduck'], 'mart'));

      expect(store.entities.length).to.be(4);
      expect(store.entities[0].id).to.equal('bob');
      expect(store.entities[1].id).to.equal('jane');
      expect(store.entities[2].id).to.equal('t');
      expect(store.entities[3].id).to.equal('mart');

      // only one type of entity so far (motherduck)
      expect(Object.keys(store.typeMap).length).to.be(1);

      expect(Object.keys(store.idMap).length).to.be(4);

      done();
    });

    it('should put entities into the right buckets in typeMap', function (done) {
      store.push(createDummyEntity(['motherduck']));
      expect(store.getForType('motherduck').length).to.be(1);
      expect(store.getForType('duckling').length).to.be(0);

      // only one type of entity so far (motherduck)
      expect(Object.keys(store.typeMap).length).to.be(1);

      store.push(createDummyEntity(['motherduck']));
      expect(store.getForType('motherduck').length).to.be(2);
      expect(store.getForType('duckling').length).to.be(0);

      // only one type of entity so far (motherduck)
      expect(Object.keys(store.typeMap).length).to.be(1);

      store.push(createDummyEntity(['duckling']));
      expect(store.getForType('motherduck').length).to.be(2);
      expect(store.getForType('duckling').length).to.be(1);

      // now two types of entities (motherduck and duckling)
      expect(Object.keys(store.typeMap).length).to.be(2);

      // add an entity with multiple types
      store.push(createDummyEntity(['hazard', 'map']));
      expect(store.getForType('hazard').length).to.be(1);
      expect(store.getForType('map').length).to.be(1);

      expect(Object.keys(store.idMap).length).to.be(0);

      done();
    });

    it('setting entity.id should store the entity in idMap', function (done) {
      var motherduck = createDummyEntity(['motherduck'], 'Client123');
      store.push(motherduck);
      expect(store.entities.length).to.be(1);
      expect(Object.keys(store.typeMap).length).to.be(1);
      expect(Object.keys(store.idMap).length).to.be(1);
      expect(store.idMap['Client123']).to.be(motherduck);
      done();
    });

    it('setting entity.name should store the entity in nameMap', function (done) {
      var motherduck = createDummyEntity(['motherduck'], 0);
      motherduck.name = 'Client123';
      store.push(motherduck);
      expect(store.entities.length).to.be(1);
      expect(Object.keys(store.typeMap).length).to.be(1);
      expect(Object.keys(store.nameMap).length).to.be(1);
      expect(store.nameMap['Client123']).to.be(motherduck);
      done();
    });
  });

  describe('remove', function () {
    it('should remove an entity from the store if it was in the store', function (done) {
      var hazard = createDummyEntity(['hazard', 'map'], 0);
      hazard.name = 'Client123';
      store.push(hazard);
      expect(store.entities.length).to.be(1);
      expect(Object.keys(store.typeMap).length).to.be(2);
      expect(Object.keys(store.typeMap.hazard).length).to.be(1);
      expect(Object.keys(store.typeMap.map).length).to.be(1);
      expect(Object.keys(store.idMap).length).to.be(1);
      expect(Object.keys(store.nameMap).length).to.be(1);

      store.remove(hazard);
      expect(store.entities.length).to.be(0);
      expect(Object.keys(store.typeMap).length).to.be(2);
      expect(Object.keys(store.typeMap.hazard).length).to.be(0);
      expect(Object.keys(store.idMap).length).to.be(0);
      expect(Object.keys(store.nameMap).length).to.be(0);
      done();
    });
  });

  describe('getById', function () {
    it('should return the entity stored by the id', function (done) {
      var motherduck = createDummyEntity(['motherduck'], 'Client123');
      store.push(motherduck);
      expect(store.getById('Client123')).to.be(motherduck);
      done();
    });

    it('should override an existing reference', function (done) {
      var motherduck0 = createDummyEntity(['motherduck'], 'Client123');
      var motherduck1 = createDummyEntity(['motherduck'], 'Client123');
      store.push(motherduck0);
      expect(Object.keys(store.idMap).length).to.be(1);
      expect(store.getById('Client123')).to.be(motherduck0);
      store.push(motherduck1);
      expect(Object.keys(store.idMap).length).to.be(1);
      expect(store.getById('Client123')).to.be(motherduck1);
      done();
    });
  });

  describe('getByName', function () {
    it('should return the entity stored by the name', function (done) {
      var motherduck = createDummyEntity(['motherduck'], 0);
      motherduck.name = 'Client123';
      store.push(motherduck);
      expect(store.getByName('Client123')).to.be(motherduck);
      done();
    });

    it('should override an existing reference', function (done) {
      var motherduck0 = createDummyEntity(['motherduck'], 0);
      motherduck0.name = 'Client123';
      var motherduck1 = createDummyEntity(['motherduck'], 1);
      motherduck1.name = 'Client123';
      store.push(motherduck0);
      expect(Object.keys(store.nameMap).length).to.be(1);
      expect(store.getByName('Client123')).to.be(motherduck0);
      store.push(motherduck1);
      expect(Object.keys(store.nameMap).length).to.be(1);
      expect(store.getByName('Client123')).to.be(motherduck1);
      done();
    });
  });

  describe('getForType', function () {
    it('should return all entities for the given type, and nothing else', function (done) {
      store.push(createDummyEntity(['motherduck'], 'bob'));
      store.push(createDummyEntity(['motherduck'], 'jane'));
      store.push(createDummyEntity(['duckling'], 't'));
      store.push(createDummyEntity(['duckling'], 'mart'));

      var motherduckEntities = store.getForType('motherduck');
      expect(motherduckEntities).not.to.be(null);
      expect(motherduckEntities.length).to.be(2);
      expect(motherduckEntities[0].id).to.equal('bob');
      expect(motherduckEntities[1].id).to.equal('jane');

      var ducklingEntities = store.getForType('duckling');
      expect(ducklingEntities).not.to.be(null);
      expect(ducklingEntities.length).to.be(2);
      expect(ducklingEntities[0].id).to.equal('t');
      expect(ducklingEntities[1].id).to.equal('mart');
      done();
    });

    it('should return an empty array if no entities were found for the given type', function (done) {
      var entities = store.getForType('foo');
      expect(entities.length).to.be(0);
      done();
    });
  });

  describe('forEach', function () {
    beforeEach(function (done) {
      store.push(createDummyEntity(['motherduck'], 'bob'));
      store.push(createDummyEntity(['motherduck'], 'jane'));
      store.push(createDummyEntity(['duckling'], 't'));
      store.push(createDummyEntity(['duckling'], 'mart'));
      store.push(createDummyEntity(['powerup'], 'my'));
      store.push(createDummyEntity(['powerup'], 'man'));
      store.push(createDummyEntity(['map', 'hazard'], 'kenny'));
      store.push(createDummyEntity(['map', 'hazard'], 'g'));
      store.push(createDummyEntity(['map'], 'homie'));
      done();
    });

    describe('forEach(\'TYPE\', fn)', function () {
      it('should iterate over the correct entities in order', function (done) {
        var expectations = ['bob', 'jane', 't', 'mart'];
        var idx = 0;
        store.forEach('motherduck', function (motherduck) {
          expect(motherduck.id).to.equal(expectations[idx]);
          idx++;
        });
        store.forEach('duckling', function (duckling) {
          expect(duckling.id).to.equal(expectations[idx]);
          idx++;
        });
        expect(idx).to.be(expectations.length); // to make sure we got them all
        done();
      });
    });

    describe('forEach(fn)', function () {
      it('should iterate over all entities if only passed a callback function', function (done) {
        var expectations = ['bob', 'jane', 't', 'mart', 'my', 'man', 'kenny', 'g', 'homie'];
        var idx = 0;
        store.forEach(function (entity) {
          expect(entity.id).to.equal(expectations[idx]);
          idx++;
        });
        expect(idx).to.be(expectations.length); // to make sure we got them all
        done();
      });
    });

    describe('forEach([\'TYPE\'], fn)', function () {
      it('should only pass each entity once, even if the entity has multiple matched types', function (done) {
        var expectations = ['kenny', 'g', 'homie'];
        var idx = 0;
        var kennyCount = 0;
        var gCount = 0;
        store.forEach(['map', 'hazard'], function (entity) {
          called = true;
          expect(entity.id).to.equal(expectations[idx]);
          if (entity.id === 'kenny') kennyCount++;
          else if (entity.id === 'g') gCount++;
          idx++;
        });
        expect(idx).to.be(expectations.length);
        expect(kennyCount).to.be(1);
        expect(gCount).to.be(1);
        done();
      });
    });

    describe('forEach({ any: [\'TYPE\'] }, fn)', function () {
      it('should iterate over only the multiple types included', function (done) {
        var expectations = ['bob', 'jane', 'my', 'man'];
        var idx = 0;
        store.forEach({ any: ['motherduck', 'powerup'] }, function (entity) {
          called = true;
          expect(entity.id).to.equal(expectations[idx]);
          idx++;
        });
        expect(idx).to.be(expectations.length); // to make sure we got them all
        done();
      });

      it('should only pass each entity once, even if the entity has multiple matched types', function (done) {
        var expectations = ['kenny', 'g', 'homie'];
        var idx = 0;
        var kennyCount = 0;
        var gCount = 0;
        store.forEach({ any: ['map', 'hazard'] }, function (entity) {
          called = true;
          expect(entity.id).to.equal(expectations[idx]);
          if (entity.id === 'kenny') kennyCount++;
          else if (entity.id === 'g') gCount++;
          idx++;
        });
        expect(idx).to.be(expectations.length);
        expect(kennyCount).to.be(1);
        expect(gCount).to.be(1);
        done();
      });
    });

    describe('forEach({ all: [\'TYPE\'] }, fn)', function () {it('should iterate over only objects with all the types in all', function (done) {
        var expectations = ['kenny', 'g'];
        var idx = 0;
        store.forEach({ all: ['map', 'hazard'] }, function (entity) {
          called = true;
          expect(entity.id).to.equal(expectations[idx]);
          idx++;
        });
        expect(idx).to.be(expectations.length); // to make sure we got them all
        done();
      });
    });

    describe('forEach({ not: [\'TYPE\'] }, fn)', function () {
      it('should iterate over all entities except the single type excluded', function (done) {
        var expectations = ['bob', 'jane', 'my', 'man', 'kenny', 'g', 'homie'];
        var idx = 0;
        store.forEach({ not: ['duckling'] }, function (entity) {
          called = true;
          expect(entity.id).to.equal(expectations[idx]);
          idx++;
        });
        expect(idx).to.be(expectations.length); // to make sure we got them all
        done();
      });

      it('should iterate over all entities except the multiple types excluded', function (done) {
        var expectations = ['my', 'man', 'kenny', 'g', 'homie'];
        var idx = 0;
        store.forEach({ not: ['motherduck', 'duckling'] }, function (entity) {
          called = true;
          expect(entity.id).to.equal(expectations[idx]);
          idx++;
        });
        expect(idx).to.be(expectations.length); // to make sure we got them all
        done();
      });
    });

    describe('forEach({ any: [\'TYPE\'], not: [\'TYPE\'] }, fn)', function () {
      it('should iterate over only objects with types in any but not in not', function (done) {
        var expectations = ['homie'];
        var idx = 0;
        store.forEach({ any: ['map'], not: ['hazard'] }, function (entity) {
          called = true;
          expect(entity.id).to.equal(expectations[idx]);
          idx++;
        });
        expect(idx).to.be(expectations.length); // to make sure we got them all
        done();
      });
    });
  });

  describe('clear', function () {
    it('should clear the store', function (done) {
      var motherduck = createDummyEntity(['motherduck'], 0);
      motherduck.name = 'Client123';
      store.push(motherduck);
      expect(store.entities.length).to.be(1);
      expect(Object.keys(store.typeMap).length).to.be(1);
      expect(Object.keys(store.idMap).length).to.be(1);
      expect(Object.keys(store.nameMap).length).to.be(1);
      expect(store.count()).to.equal(1);
      store.clear();
      expect(store.entities.length).to.be(0);
      expect(Object.keys(store.typeMap).length).to.be(0);
      expect(Object.keys(store.idMap).length).to.be(0);
      expect(Object.keys(store.nameMap).length).to.be(0);
      expect(store.count()).to.equal(0);
      done();
    });
  });
});

