var expect = require('expect.js');
var TypesMixin = require('../server/types-mixin');

describe('TypesMixin', function () {
  it('should add a types property', function (done) {
    var obj = {};
    expect(obj.types).to.be(undefined);
    TypesMixin(obj);
    expect(obj.types).to.eql([]);
    done();
  });

  it('should add the helper functions', function (done) {
    var obj = {};
    expect(obj.hasAllTypes).to.be(undefined);
    expect(obj.hasAnyTypes).to.be(undefined);
    expect(obj.hasType).to.be(undefined);
    expect(obj.addType).to.be(undefined);
    expect(obj.removeType).to.be(undefined);
    TypesMixin(obj);
    expect(typeof obj.hasAllTypes).to.be('function');
    expect(typeof obj.hasAnyTypes).to.be('function');
    expect(typeof obj.hasType).to.be('function');
    expect(typeof obj.addType).to.be('function');
    expect(typeof obj.removeType).to.be('function');
    done();
  });

  describe('function behaviour', function () {
    var obj;

    beforeEach(function (done) {
      obj = {};
      TypesMixin(obj);
      done();
    });

    it('hasAllTypes should return true iff obj has all the types', function (done) {
      var type0 = 'foo';
      var type1 = 'bar';
      expect(obj.hasAllTypes(type0, type1)).to.be(false);
      obj.addType(type0);
      expect(obj.hasAllTypes(type0, type1)).to.be(false);
      obj.addType(type1);
      expect(obj.hasAllTypes(type0, type1)).to.be(true);
      done();
    });

    it('hasAnyTypes should return true if the obj has any of the types', function (done) {
      var type0 = 'foo';
      var type1 = 'bar';
      expect(obj.hasAnyTypes(type0, type1)).to.be(false);
      obj.addType(type0);
      expect(obj.hasAnyTypes(type0, type1)).to.be(true);
      obj.removeType(type0);
      expect(obj.hasAnyTypes(type0, type1)).to.be(false);
      obj.addType(type1);
      expect(obj.hasAnyTypes(type0, type1)).to.be(true);
      obj.addType(type0);
      expect(obj.hasAnyTypes(type0, type1)).to.be(true);
      done();
    });

    it('hasType should return true iff obj has the type', function (done) {
      var type = 'motherduck';
      expect(obj.hasType(type)).to.be(false);
      obj.addType(type);
      expect(obj.hasType(type)).to.be(true);
      obj.removeType(type);
      expect(obj.hasType(type)).to.be(false);
      obj.addType('motherduck-decoy');
      expect(obj.hasType(type)).to.be(false);
      done();
    });

    it('addType should only add a type if it\'s not already there', function (done) {
      var type = 'motherduck';
      expect(obj.types.length).to.be(0);
      obj.addType(type);
      expect(obj.types.length).to.be(1);
      obj.addType(type);
      expect(obj.types.length).to.be(1);
      obj.addType('motherduck-decoy');
      expect(obj.types.length).to.be(2);
      obj.addType('motherduck-decoy');
      expect(obj.types.length).to.be(2);
      done();
    });

    it('removeType should remove the type if present', function (done) {
      var type = 'motherduck';
      expect(obj.types.length).to.be(0);
      obj.addType(type);
      expect(obj.types.length).to.be(1);
      obj.removeType('motherduck-decoy');
      expect(obj.types.length).to.be(1);
      obj.removeType(type);
      expect(obj.types.length).to.be(0);
      done();
    });
  });
});