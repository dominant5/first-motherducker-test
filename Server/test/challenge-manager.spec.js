var expect = require('expect.js');
var q = require('q');
var conn = require('../server/mongoose-connection');
var UserModel = require('../server/database/user.model');
var SessionManager = require('../server/session-manager');
var AccountManager = require('../server/account-manager');
var ChallengeManager = require('../server/challenge-manager');
var Server = require('../server/server');
var io = require('socket.io-client');
var port = 9000;
var socketURL = 'http://localhost:' + port;
var clientSideSocketOptions = {
  transports: ['websocket'],
  'force new connection': true
};

describe('ChallengeManager', function () {
  var server, accountManager, sessionManager, challengeManager, clients, clientSideSockets;

  var initClientSideSockets = function (numClients) {
    var dfd = q.defer();

    server.on(Server.CONNECT, function (client) {
      clients.push(client);
      if (clients.length === numClients) dfd.resolve();
    });

    for (var i = 0; i < numClients; i++) {
      clientSideSockets.push(io(socketURL, clientSideSocketOptions));
    }

    return dfd.promise;
  };

  var initUsers = function (usernames) {
    var dfd = q.defer();

    var completedCount = 0;
    var count = usernames.length;
    var ret = {};
    initClientSideSockets(count).then(function () {
      for (var i = 0; i < count; i++) {
        (function (index) {
          var username = usernames[index];
          var clientSideSocket = clientSideSockets[index];
          var client = clients[index];

          clientSideSocket.on('acc:signup-success', function () {
            completedCount++;
            ret[username] = {
              clientSideSocket: clientSideSocket,
              client: client
            };
            if (completedCount === count) dfd.resolve(ret);
          });

          clientSideSocket.emit('acc:signup', {
            username: username,
            password: username
          });
        })(i);
      }
    });

    return dfd.promise;
  };

  beforeEach(function (done) {
    var app = function (req, res) {};
    var opts = { silent: true };
    server = new Server(port, app, opts);
    sessionManager = new SessionManager(server);
    accountManager = new AccountManager(server, sessionManager);
    challengeManager = new ChallengeManager(server, sessionManager);
    clients = [];
    clientSideSockets = [];

    conn.connect(conn.localTestUrl, function () {
      UserModel.remove().exec().then(function () {
        done();
      });
    });
  });

  afterEach(function (done) {
    server.stop();
    for (var i = 0; i < clientSideSockets.length; i++) {
      var s = clientSideSockets[i];
      s.disconnect();
    }
    UserModel.remove().exec().then(function () {
      done();
    });
  });

  describe('onClientDisconnect', function () {
    it('should remove all of a client\'s session challenges', function (done) {
      initUsers(['foo', 'bar', 'quux']).then(function (users) {
        users.foo.client.on('disconnect', function () {
          var fooId = users.foo.client.id();
          expect(challengeManager.challenges[fooId].length).to.be(0);
          done();
        });

        sentCount = 0;
        targetCount = 2;
        users.foo.clientSideSocket.on('challenge:sent-success', function () {
          sentCount++;
          if (sentCount !== targetCount) return;
          var fooId = users.foo.client.id();
          expect(challengeManager.challenges[fooId].length).to.be(2);
          users.foo.clientSideSocket.disconnect();
        });

        users.foo.clientSideSocket.emit('challenge:sent', { to: 'bar' });
        users.foo.clientSideSocket.emit('challenge:sent', { to: 'quux' });
      });
    });
  });
  
  describe('onChallengeSent', function () {
    it('should successfully send the challenge if both clients exist, are online, and the challenge hasn\'t already been sent', function (done) {
      initUsers(['foo', 'bar']).then(function (users) {
        var sentSuccess = false;
        var received = false;
        function finish () {
          if (!sentSuccess || !received) return;
          var fooId = users.foo.client.id();
          var barId = users.bar.client.id();
          expect(challengeManager.challenges[fooId].length).to.be(1);
          expect(challengeManager.challenges[fooId][0]).to.be(barId);
          done();
        };

        users.bar.clientSideSocket.on('challenge:sent', function (data) {
          expect(data.from).to.be('foo');
          received = true;
          finish();
        });

        users.foo.clientSideSocket.on('challenge:sent-success', function () {
          sentSuccess = true;
          finish();
        });

        users.foo.clientSideSocket.emit('challenge:sent', {
          to: 'bar'
        });
      });
    });

    it('should fail if the other user doesn\'t exist', function (done) {
      initUsers(['foo']).then(function (users) {
        users.foo.clientSideSocket.on('err', function (data) {
          expect(data.type).to.be('challenge:sent');
          expect(data.message).to.be('Other user doesn\'t exist');
          done();
        });

        users.foo.clientSideSocket.emit('challenge:sent', {
          to: 'bar'
        });
      });
    });

    it('should fail if the other client is not online', function (done) {
      initUsers(['foo', 'bar']).then(function (users) {
        users.foo.clientSideSocket.on('err', function (data) {
          expect(data.type).to.be('challenge:sent');
          expect(data.message).to.be('Other user not online');
          done();
        });

        users.bar.clientSideSocket.on('session:logout-success', function () {
          users.foo.clientSideSocket.emit('challenge:sent', {
            to: 'bar'
          });
        });

        users.bar.clientSideSocket.emit('session:logout');
      });
    });

    it('should fail if a challenge was already sent to the other client', function (done) {
      initUsers(['foo', 'bar']).then(function (users) {
        users.foo.clientSideSocket.on('err', function (data) {
          expect(data.type).to.be('challenge:sent');
          expect(data.message).to.be('Challenge already sent to this user.');
          done();
        });

        users.foo.clientSideSocket.on('challenge:sent-success', function () {
          users.foo.clientSideSocket.emit('challenge:sent', { to: 'bar' });
        });

        users.foo.clientSideSocket.emit('challenge:sent', { to: 'bar' });
      });
    });
  });

  describe('onChallengeAccepted', function () {
    it('should successfully accept the challenge if both clients exist, are online, and the challenge was previously sent', function (done) {
      initUsers(['foo', 'bar']).then(function (users) {
        var sentSuccess = false;
        var received = false;
        function finish () {
          if (!sentSuccess || !received) return;
          var fooId = users.foo.client.id();
          var barId = users.bar.client.id();
          expect(challengeManager.challenges[fooId].length).to.be(0);
          done();
        };

        users.foo.clientSideSocket.on('challenge:accepted', function (data) {
          received = true;
          finish();
        });

        users.bar.clientSideSocket.on('challenge:accepted-success', function () {
          sentSuccess = true;
          finish();
        });

        users.bar.clientSideSocket.on('challenge:sent', function (data) {
          users.bar.clientSideSocket.emit('challenge:accepted', { from: data.from });
        });

        users.foo.clientSideSocket.emit('challenge:sent', { to: 'bar' });
      });
    });

    it('should fail if the other user doesn\'t exist', function (done) {
      initUsers(['bar']).then(function (users) {
        users.bar.clientSideSocket.on('err', function (data) {
          expect(data.type).to.be('challenge:accepted');
          expect(data.message).to.be('Other user doesn\'t exist');
          done();
        });

        users.bar.clientSideSocket.emit('challenge:accepted', { from: 'foo' });
      });
    });

    it('should fail if the other client is not online', function (done) {
      initUsers(['foo', 'bar']).then(function (users) {
        users.bar.clientSideSocket.on('err', function (data) {
          expect(data.type).to.be('challenge:accepted');
          expect(data.message).to.be('Other user not online');
          done();
        });

        users.foo.clientSideSocket.on('session:logout-success', function () {
          users.bar.clientSideSocket.emit('challenge:accepted', { from: 'foo' });
        });

        users.foo.clientSideSocket.emit('session:logout');
      });
    });

    it('should fail if the challenge does not exist any more', function (done) {
      initUsers(['foo', 'bar']).then(function (users) {
        users.bar.clientSideSocket.on('err', function (data) {
          expect(data.type).to.be('challenge:accepted');
          expect(data.message).to.be('Challenge does not exist anymore.');
          done();
        });

        users.bar.clientSideSocket.on('challenge:accepted-success', function () {
          users.bar.clientSideSocket.emit('challenge:accepted', { from: 'foo' });
        });

        users.foo.clientSideSocket.on('challenge:sent-success', function () {
          users.bar.clientSideSocket.emit('challenge:accepted', { from: 'foo' });
        });

        users.foo.clientSideSocket.emit('challenge:sent', { to: 'foo' });
      });
    });
  });

  describe('challengeExists', function () {
    it('should return true if the given challenge exists', function (done) {
      initClientSideSockets(2).then(function () {
        challengeManager.addChallenge(clients[0], clients[1]);
        expect(challengeManager.challengeExists(clients[0], clients[1])).to.be(true);
        done();
      });
    });

    it('should return false if the given challenge does not exist', function (done) {
      initClientSideSockets(2).then(function () {
        expect(challengeManager.challengeExists(clients[0], clients[1])).to.be(false);
        challengeManager.addChallenge(clients[0], clients[1]);
        expect(challengeManager.challengeExists(clients[0], clients[1])).to.be(true);
        challengeManager.removeChallenge(clients[0], clients[1]);
        expect(challengeManager.challengeExists(clients[0], clients[1])).to.be(false);
        done();
      });
    });
  });

  describe('addChallenge', function () {
    it('should add a challenge', function (done) {
      initClientSideSockets(2).then(function () {
        expect(Object.keys(challengeManager.challenges).length).to.be(0);
        challengeManager.addChallenge(clients[0], clients[1]);
        expect(Object.keys(challengeManager.challenges).length).to.be(1);
        var fromId = clients[0].id();
        expect(challengeManager.challenges[fromId].length).to.be(1);
        var toId = clients[1].id();
        expect(challengeManager.challenges[fromId][0]).to.be(toId);
        done();
      });
    });
  });

  describe('removeChallenge', function () {
    it('should remove a challenge', function (done) {
      initClientSideSockets(2).then(function () {
        expect(Object.keys(challengeManager.challenges).length).to.be(0);
        challengeManager.addChallenge(clients[0], clients[1]);
        expect(Object.keys(challengeManager.challenges).length).to.be(1);
        var fromId = clients[0].id();
        expect(challengeManager.challenges[fromId].length).to.be(1);
        var toId = clients[1].id();
        expect(challengeManager.challenges[fromId][0]).to.be(toId);

        challengeManager.removeChallenge(clients[0], clients[1]);
        expect(Object.keys(challengeManager.challenges).length).to.be(1);
        expect(challengeManager.challenges[fromId].length).to.be(0);
        done();
      });
    });
  });

  describe('removeAllChallenges', function () {
    it('should remove all challenges for a given client', function (done) {
      initClientSideSockets(4).then(function () {
        challengeManager.addChallenge(clients[0], clients[1]);
        challengeManager.addChallenge(clients[0], clients[2]);
        challengeManager.addChallenge(clients[0], clients[3]);
        var fromId = clients[0].id();
        expect(challengeManager.challenges[fromId].length).to.be(3);
        challengeManager.removeAllChallenges(clients[0]);
        expect(challengeManager.challenges[fromId].length).to.be(0);
        done();
      });
    });
  });
});