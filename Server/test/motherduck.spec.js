var expect = require('expect.js');
var Motherduck = require('../server/motherduck');
var Duckling = require('../server/duckling');
var Cannon = require('cannon');

describe('Motherduck', function () {

  var name, id, defaultOptions;

  beforeEach(function (done) {
    name = 'Client123';
    id = 0;
    defaultOptions = {
      cannonOptions: {
        mass: 0.6,
        radius: 0.5,
        linearDamping: 0,
        angularDamping: 0.9
      },
      speed: 10,
      diveForce: -1500,
      turnSpeed: 3,
      collideCb: null,
      collideCbContext: null,
      updateFn: null
    };
    done();
  });

  describe('constructor', function () {
    it('should use default options if none provided', function (done) {
      var motherduck = new Motherduck();
      expect(motherduck.name).to.equal(motherduck.body.id); // uses body.id if no name passed
      expect(motherduck.id).to.equal(motherduck.body.id);
      expect(motherduck.options).to.eql(defaultOptions);
      done();
    });

    it('should use any overriden options', function (done) {
      var newName = 'BigMike';
      var newId = 420;
      var options = {
        cannonOptions: {
          radius: 1.0
        },
        updateFn: function () {
          return '123';
        }
      };
      var motherduck = new Motherduck(newName, newId, options);
      expect(motherduck.name).to.equal(newName);
      expect(motherduck.id).to.equal(newId);
      expect(motherduck.options.cannonOptions.radius).to.eql(options.cannonOptions.radius);
      expect(motherduck.body.boundingRadius).to.eql(options.cannonOptions.radius);
      expect(motherduck.updateFn()).to.equal('123');
      done();
    });

    it('should add properties not defined in options', function (done) {
      var motherduck = new Motherduck(name, id);
      expect(motherduck.hasType('motherduck')).to.be(true);
      expect(motherduck.states).to.eql([]);
      expect(motherduck.ducklingCount).to.equal(0);
      expect(motherduck.team).to.equal(-1);
      expect(motherduck.next).to.equal(null);
      expect(motherduck.prev).to.equal(null);
      expect(motherduck.end).to.equal(motherduck);
      done();
    });
  });

  describe('addDuckling', function () {
    it('should increment motherduck.ducklingCount', function (done) {
      var motherduck = new Motherduck(name, id);
      expect(motherduck.ducklingCount).to.equal(0);

      for (var i = 1; i <= 10; i++) {
        var d = new Duckling();
        motherduck.addDuckling(d);
        expect(motherduck.ducklingCount).to.equal(i);
      }

      done();
    });

    it('should link them up correctly', function (done) {
      var motherduck = new Motherduck(name, id);
      expect(motherduck.next).to.equal(null);
      expect(motherduck.prev).to.equal(null);
      expect(motherduck.end).to.equal(motherduck);

      var d1 = new Duckling();
      expect(d1.next).to.equal(null);
      expect(d1.prev).to.equal(null);
      motherduck.addDuckling(d1);
      expect(motherduck.next).to.equal(null);
      expect(motherduck.prev).to.equal(d1);
      expect(motherduck.end).to.equal(d1);
      expect(d1.next).to.equal(motherduck);
      expect(d1.prev).to.equal(null);

      var d2 = new Duckling();
      expect(d2.next).to.equal(null);
      expect(d2.prev).to.equal(null);
      motherduck.addDuckling(d2);
      expect(motherduck.next).to.equal(null);
      expect(motherduck.prev).to.equal(d1);
      expect(motherduck.end).to.equal(d2);
      expect(d1.prev).to.equal(d2);
      expect(d2.next).to.equal(d1);
      expect(d2.prev).to.equal(null);

      done();
    });

    it('should set the updateFn', function (done) {
      var motherduck = new Motherduck(name, id);

      var d = new Duckling();
      expect(d.updateFn).to.be(null);
      motherduck.addDuckling(d);
      expect(typeof d.updateFn).to.equal('function');

      done();
    });
  });
});