var expect = require('expect.js');
var TypesMixin = require('../server/types-mixin');
var CollisionStore = require('../server/collision-store');

function createDummyEntity (types) {
  var ret = {};
  TypesMixin(ret);
  for (var i = 0; i < types.length; i++) {
    var type = types[i];
    ret.addType(type);
  }
  return ret;
};

function createDummyContact (data) {
  return {
    data: data || 'foo'
  };
};

describe('Collision Store', function () {
  var store;

  beforeEach(function (done) {
    store = new CollisionStore();
    done();
  });

  describe('push', function () {
    it('should put collisions in order into store.collisions', function (done) {
      store.push(createDummyEntity(['motherduck']),
                createDummyEntity(['duckling']),
                createDummyContact('contact1'));

      store.push(createDummyEntity(['motherduck']),
                createDummyEntity(['motherduck']),
                createDummyContact('contact2'));

      store.push(createDummyEntity(['motherduck']),
                createDummyEntity(['map']),
                createDummyContact('contact3'));

      store.push(createDummyEntity(['motherduck']),
                createDummyEntity(['duckling']),
                createDummyContact('contact4'));

      expect(store.collisions.length).to.be(4);
      expect(store.collisions[0].contact.data).to.equal('contact1');
      expect(store.collisions[1].contact.data).to.equal('contact2');
      expect(store.collisions[2].contact.data).to.equal('contact3');
      expect(store.collisions[3].contact.data).to.equal('contact4');
      done();
    });

    it('should put collisions into the right buckets in store.map', function (done) {
      store.push(createDummyEntity(['motherduck']),
                createDummyEntity(['duckling']),
                createDummyContact());
      expect(store.getForTypePair('motherduck', 'duckling').length).to.be(1);
      expect(store.getForType('motherduck').length).to.be(1);
      expect(store.getForType('duckling').length).to.be(1);

      store.push(createDummyEntity(['motherduck']),
                createDummyEntity(['motherduck']),
                createDummyContact());
      expect(store.getForTypePair('motherduck', 'motherduck').length).to.be(1);
      expect(store.getForType('motherduck').length).to.be(2);

      store.push(createDummyEntity(['motherduck']),
                createDummyEntity(['map']),
                createDummyContact());
      expect(store.getForTypePair('motherduck', 'map').length).to.be(1);
      expect(store.getForType('motherduck').length).to.be(3);
      expect(store.getForType('map').length).to.be(1);

      store.push(createDummyEntity(['motherduck']),
                createDummyEntity(['duckling']),
                createDummyContact());
      expect(store.getForTypePair('motherduck', 'duckling').length).to.be(2);
      expect(store.getForType('motherduck').length).to.be(4);
      expect(store.getForType('duckling').length).to.be(2);

      store.push(createDummyEntity(['motherduck']),
                createDummyEntity(['hazard', 'map']),
                createDummyContact());
      expect(store.getForTypePair('motherduck', 'hazard').length).to.be(1);
      expect(store.getForTypePair('motherduck', 'map').length).to.be(2);
      expect(store.getForType('motherduck').length).to.be(5);
      expect(store.getForType('hazard').length).to.be(1);
      expect(store.getForType('map').length).to.be(2);

      done();
    });
  });

  describe('getForTypePair', function () {
    it('should return all collisions for the given types, and nothing else', function (done) {
      store.push(createDummyEntity(['motherduck']),
                createDummyEntity(['duckling']),
                createDummyContact('contact1'));

      // put in a collision where the entities are reversed
      store.push(createDummyEntity(['duckling']),
                createDummyEntity(['motherduck']),
                createDummyContact('contact2'));

      store.push(createDummyEntity(['motherduck']),
                createDummyEntity(['motherduck']),
                createDummyContact());

      store.push(createDummyEntity(['motherduck']),
                createDummyEntity(['map']),
                createDummyContact());

      var motherduckDucklingCollisions = store.getForTypePair('motherduck', 'duckling');
      expect(motherduckDucklingCollisions).not.to.be(null);
      expect(motherduckDucklingCollisions.length).to.be(2);
      expect(motherduckDucklingCollisions[0].contact.data).to.equal('contact1');
      expect(motherduckDucklingCollisions[1].contact.data).to.equal('contact2');
      done();
    });

    it('should return an empty array if no collisions were found for the given types', function (done) {
      var collisions = store.getForTypePair('foo', 'bar');
      expect(collisions.length).to.be(0);
      done();
    });
  });

  describe('getForType', function () {
    it('should return all collisions for the given type, and nothing else', function (done) {
      store.push(createDummyEntity(['motherduck']),
                createDummyEntity(['duckling']),
                createDummyContact('contact1'));

      // put in a collision where the entities are reversed
      store.push(createDummyEntity(['duckling']),
                createDummyEntity(['motherduck']),
                createDummyContact('contact2'));

      store.push(createDummyEntity(['motherduck']),
                createDummyEntity(['motherduck']),
                createDummyContact());

      store.push(createDummyEntity(['motherduck']),
                createDummyEntity(['map']),
                createDummyContact());

      var motherduckDucklingCollisions = store.getForType('motherduck');
      expect(motherduckDucklingCollisions).not.to.be(null);
      expect(motherduckDucklingCollisions.length).to.be(4);
      expect(motherduckDucklingCollisions[0].contact.data).to.equal('contact1');
      expect(motherduckDucklingCollisions[1].contact.data).to.equal('contact2');
      done();
    });

    it('should return an empty array if no collisions were found for the given type', function (done) {
      var collisions = store.getForType('foo');
      expect(collisions.length).to.be(0);
      done();
    });
  });

  describe('forEach', function () {
    beforeEach(function (done) {
      store.push(createDummyEntity(['motherduck']),
                createDummyEntity(['duckling']),
                createDummyContact('contact1'));

      // put in a collision where the entities are reversed
      store.push(createDummyEntity(['duckling']),
                createDummyEntity(['motherduck']),
                createDummyContact('contact2'));

      store.push(createDummyEntity(['motherduck']),
                createDummyEntity(['motherduck']),
                createDummyContact('contact3'));

      store.push(createDummyEntity(['motherduck']),
                createDummyEntity(['map']),
                createDummyContact('contact4'));

      done();
    });

    it('should iterate over the correct collisions in order', function (done) {
      var expectations = ['contact1', 'contact2'];
      var idx = 0;
      store.forEach('motherduck', 'duckling', function (collision) {
        expect(collision.contact.data).to.equal(expectations[idx]);
        idx++;
      });
      done();
    });

    it('should iterate over all collisions if only passed a callback function', function (done) {
      var expectations = ['contact1', 'contact2', 'contact3', 'contact4'];
      var idx = 0;
      store.forEach(function (collision) {
        expect(collision.contact.data).to.equal(expectations[idx]);
        idx++;
      });
      done();
    });
    
    it('should iterate over all collisions for one type if only passed one type and a function', function (done) {
      var expectations = ['contact1', 'contact2', 'contact3', 'contact4'];
      var idx = 0;
      store.forEach('motherduck', function (collision) {
        expect(collision.contact.data).to.equal(expectations[idx]);
        idx++;
      });
      done();
    });
  });

  describe('clear', function () {
    it('should clear the store', function (done) {
      store.push(createDummyEntity(['motherduck']),
                createDummyEntity(['duckling']),
                createDummyContact());
      expect(store.count()).to.equal(1);
      store.clear();
      expect(store.count()).to.equal(0);
      done();
    });
  });
});