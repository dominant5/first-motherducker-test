var expect = require('expect.js');
var q = require('q');
var conn = require('../server/mongoose-connection');
var UserModel = require('../server/database/user.model');
var SessionManager = require('../server/session-manager');
var Server = require('../server/server');
var Client = require('../server/client');
var io = require('socket.io-client');
var port = 9000;
var socketURL = 'http://localhost:' + port;
var clientSideSocketOptions = {
  transports: ['websocket'],
  'force new connection': true
};

describe('SessionManager', function () {
  var server, sessionManager, clients, clientSideSockets;

  var initClientSideSockets = function (numClients) {
    var dfd = q.defer();

    server.on(Server.CONNECT, function (client) {
      clients.push(client);
      if (clients.length === numClients) dfd.resolve();
    });

    for (var i = 0; i < numClients; i++) {
      clientSideSockets.push(io(socketURL, clientSideSocketOptions));
    }

    return dfd.promise;
  };

  beforeEach(function (done) {
    var app = function (req, res) {};
    var opts = { silent: true };
    server = new Server(port, app, opts);
    sessionManager = new SessionManager(server);
    clients = [];
    clientSideSockets = [];

    conn.connect(conn.localTestUrl, function () {
      UserModel.remove().exec().then(function () {
        done();
      });
    });
  });

  afterEach(function (done) {
    server.stop();
    for (var i = 0; i < clientSideSockets.length; i++) {
      var s = clientSideSockets[i];
      s.disconnect();
    }
    UserModel.remove().exec().then(function () {
      conn.disconnect(function () {
        done();
      });
    });
  });

  describe('inSession', function () {
    it('should return true if a client is logged in', function (done) {
      UserModel.create({ username: 'test', password: 'test' }, function () {
        initClientSideSockets(1).then(function () {
          expect(sessionManager.inSession(clients[0].name)).to.be(false);

          clientSideSockets[0].on('session:login-success', function (data) {
            expect(sessionManager.inSession(clients[0].name)).to.be(true);
            done();
          });

          clientSideSockets[0].emit('session:login', { username: 'test', password: 'test' });
        });
      });
    });
  });
  
  describe('onClientLogin', function () {
    it('should login from the client socket', function (done) {
      UserModel.create({ username: 'test', password: 'test' }, function () {
        initClientSideSockets(1).then(function () {
          expect(clients[0].status).to.be(Client.StatusCodes.CREATED);

          clientSideSockets[0].on('session:login-success', function (data) {
            expect(data.name).to.be('test');
            expect(Object.keys(sessionManager.sessions).length).to.be(1);
            expect(clients[0].status).to.be(Client.StatusCodes.LOBBY);
            done();
          });

          clientSideSockets[0].emit('session:login', { username: 'test', password: 'test' });
        });
      });
    });

    it('should create an error if user already logged in', function (done) {
      UserModel.create({ username: 'test', password: 'test' }, function () {
        initClientSideSockets(2).then(function () {
          expect(clients[0].status).to.be(Client.StatusCodes.CREATED);

          clientSideSockets[1].on('err', function (data) {
            expect(data.type).to.be('session:login');
            expect(data.message).to.be('User already logged in');
            done();
          });

          clientSideSockets[0].on('session:login-success', function (data) {
            expect(data.name).to.be('test');
            expect(Object.keys(sessionManager.sessions).length).to.be(1);
            expect(clients[0].status).to.be(Client.StatusCodes.LOBBY);
            clientSideSockets[1].emit('session:login', { username: 'test', password: 'test' });            
          });

          clientSideSockets[0].emit('session:login', { username: 'test', password: 'test' });
        });
      });
    });
  });

  describe('onClientPlayAsGuest', function () {
    it('should tell the client that they are playing as guest', function (done) {
      initClientSideSockets(1).then(function () {
        expect(clients[0].status).to.be(Client.StatusCodes.CREATED);

        clientSideSockets[0].on('session:play-as-guest-success', function (data) {
          expect(data.name).to.be(clients[0].name);
          expect(Object.keys(sessionManager.sessions).length).to.be(1);
          expect(clients[0].status).to.be(Client.StatusCodes.LOBBY);
          done();
        });

        clientSideSockets[0].emit('session:play-as-guest');
      });
    });
  });

  describe('onClientLogout', function () {
    it('should log the client out if a logged in client socket sends a logout message', function (done) {
      UserModel.create({ username: 'test', password: 'test' }, function () {
        initClientSideSockets(1).then(function () {
          expect(clients[0].status).to.be(Client.StatusCodes.CREATED);

          clientSideSockets[0].on('session:logout-success', function () {
            expect(Object.keys(sessionManager.sessions).length).to.be(0);
            expect(clients[0].status).to.be(Client.StatusCodes.CREATED);
            done();
          });

          clientSideSockets[0].on('session:login-success', function (data) {
            expect(Object.keys(sessionManager.sessions).length).to.be(1);
            expect(clients[0].status).to.be(Client.StatusCodes.LOBBY);
            clientSideSockets[0].emit('session:logout');
          });

          clientSideSockets[0].emit('session:login', { username: 'test', password: 'test' });
        });
      });
    });

    it('should log the client out if a guest client socket sends a logout message', function (done) {
      UserModel.create({ username: 'test', password: 'test' }, function () {
        initClientSideSockets(1).then(function () {
          expect(clients[0].status).to.be(Client.StatusCodes.CREATED);

          clientSideSockets[0].on('session:logout-success', function () {
            expect(Object.keys(sessionManager.sessions).length).to.be(0);
            expect(clients[0].status).to.be(Client.StatusCodes.CREATED);
            done();
          });

          clientSideSockets[0].on('session:play-as-guest-success', function (data) {
            expect(Object.keys(sessionManager.sessions).length).to.be(1);
            expect(clients[0].status).to.be(Client.StatusCodes.LOBBY);
            clientSideSockets[0].emit('session:logout');
          });

          clientSideSockets[0].emit('session:play-as-guest', { username: 'test' });
        });
      });
    });
  });
});