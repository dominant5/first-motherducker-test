'use strict';

var expect = require('expect.js');
var UserModel = require('../server/database/user.model');
var FriendRequestListModel = require('../server/database/friend-request-list.model');
var FriendRequestList = require('../server/database/friend-request-list');
var conn = require('../server/mongoose-connection');

describe('FriendRequestList', function () {
  var user;

  beforeEach(function (done) {
    user = { _id: '560e23ca9c05dc0aa0ef8ba5', username: 'test', password: 'test' };
    conn.connect(conn.localTestUrl, function () {
      done();
    });
  });

  afterEach(function (done) {
    UserModel.remove().exec().then(function () {
      FriendRequestListModel.remove().exec().then(function () {
        conn.disconnect(function () {
          done();
        });
      });
    });
  });

  describe('create', function () {
    beforeEach(function (done) {
      UserModel.create(user, function () {
        done();
      });
    });

    it('should create an error if no username', function (done) {
      FriendRequestList.create(null, function (err, list) {
        expect(err).not.to.be(null);
        expect(err.message).to.be('username is required.');
        done();
      });
    });

    it('should succeed if username provided', function (done) {
      FriendRequestList.create(user.username, function (err, list) {
        expect(err).to.be(null);
        FriendRequestListModel.find().exec().then(function (friendLists) {
          expect(friendLists.length).to.be(1);
          expect(friendLists[0].user.toString()).to.equal(user._id);
          done();
        });
      });
    });

    it('should create an error if a list for the given user id already exists', function (done) {
      FriendRequestList.create(user.username, function (err1, list1) {
        expect(err1).to.be(null);
        FriendRequestList.create(user.username, function (err2, list2) {
          expect(err2).not.to.be(null);
          expect(Object.keys(err2.errors).length).to.be(1);
          expect(err2.errors.user).not.to.be(null);
          expect(err2.errors.user.message).to.be('Friend request list for given user already exists');
          done();
        })
      });
    });
  });

  describe('destroy', function () {
    beforeEach(function (done) {
      UserModel.create(user, function () {
        done();
      });
    });

    it('should successfully remove a friendlist by user id', function (done) {
      function remove () {
        FriendRequestList.destroy(user.username, function (err, list) {
          FriendRequestListModel.count({}, function (err, count) {
            expect(count).to.be(0);
            done();
          });
        });
      };

      FriendRequestList.create(user.username, function (err, list) {
        FriendRequestListModel.count({}, function (err, count) {
          expect(count).to.be(1);
          remove();
        });
      });
    });
  });

  describe('addRequest', function () {
    var userA, userB;

    beforeEach(function (done) {
      userA = { _id: '560e23ca9c05dc0aa0ef8ba5', username: 'a', password: 'a' };
      userB = { _id: '560e23ca9c05dc0aa0ef8ba6', username: 'b', password: 'b' };
      UserModel.create(userA, function () {
        UserModel.create(userB, function () {
          done();
        });
      });
    });

    it('should fail if fromName is null', function (done) {
      FriendRequestList.addRequest(null, null).then(null, function (err) {
        expect(err).not.to.be(null);
        expect(err.message).to.be('fromName is required.');
        done();
      });
    });

    it('should fail if toName is null', function (done) {
      FriendRequestList.addRequest(userA.username, null).then(null, function (err) {
        expect(err).not.to.be(null);
        expect(err.message).to.be('toName is required.');
        done();
      });
    });

    it('should fail if fromName and toName are the same', function (done) {
      FriendRequestList.addRequest(userA.username, userA.username).then(null, function (err) {
        expect(err).not.to.be(null);
        expect(err.message).to.be('fromName and toName must be different.');
        done();
      });
    });

    it('should succeed if from and to are provided', function (done) {
      FriendRequestList.addRequest(userA.username, userB.username).then(function (added) {
        expect(added).to.be(true);
        FriendRequestListModel.findOne({ user: userA._id }).exec(function (err, fromList) {
          expect(err).to.be(null);
          expect(fromList.to.length).to.be(1);
          expect(fromList.to[0].toString()).to.equal(userB._id);

          FriendRequestListModel.findOne({ user: userB._id }).exec(function (err, toList) {
            expect(err).to.be(null);
            expect(toList.from.length).to.be(1);
            expect(toList.from[0].toString()).to.equal(userA._id);
            done();
          });
        });
      });
    });

    it('should make no change if the request is already added', function (done) {
      FriendRequestList.addRequest(userA.username, userB.username).then(function (added) {
        expect(added).to.be(true);
        FriendRequestList.addRequest(userA.username, userB.username).then(function (added) {
          expect(added).to.be(false);
          FriendRequestListModel.findOne({ user: userA._id }).exec(function (err, fromList) {
            expect(err).to.be(null);
            expect(fromList.to.length).to.be(1);
            expect(fromList.to[0].toString()).to.equal(userB._id);

            FriendRequestListModel.findOne({ user: userB._id }).exec(function (err, toList) {
              expect(err).to.be(null);
              expect(toList.from.length).to.be(1);
              expect(toList.from[0].toString()).to.equal(userA._id);
              done();
            });
          });
        });
      });
    });
  });
  
  describe('removeRequest', function () {
    var userA, userB;

    beforeEach(function (done) {
      userA = { _id: '560e23ca9c05dc0aa0ef8ba5', username: 'a', password: 'a' };
      userB = { _id: '560e23ca9c05dc0aa0ef8ba6', username: 'b', password: 'b' };
      UserModel.create(userA, function () {
        UserModel.create(userB, function () {
          done();
        });
      });
    });

    it('should fail if fromName is null', function (done) {
      FriendRequestList.removeRequest(null, null).then(null, function (err) {
        expect(err).not.to.be(null);
        expect(err.message).to.be('fromName is required.');
        done();
      });
    });

    it('should fail if toName is null', function (done) {
      FriendRequestList.removeRequest(userA.username, null).then(null, function (err) {
        expect(err).not.to.be(null);
        expect(err.message).to.be('toName is required.');
        done();
      });
    });

    it('should fail if from and to are the same', function (done) {
      FriendRequestList.removeRequest(userA.username, userA.username).then(null, function (err) {
        expect(err).not.to.be(null);
        expect(err.message).to.be('fromName and toName must be different.');
        done();
      });
    });

    it('should succeed if from and to are provided', function (done) {
      FriendRequestList.addRequest(userA.username, userB.username).then(function (added) {
        expect(added).to.be(true);
        FriendRequestListModel.findOne({ user: userA._id }).exec(function (err, fromList) {
          expect(err).to.be(null);
          expect(fromList.to.length).to.be(1);
          expect(fromList.to[0].toString()).to.equal(userB._id);

          FriendRequestListModel.findOne({ user: userB._id }).exec(function (err, toList) {
            expect(err).to.be(null);
            expect(toList.from.length).to.be(1);
            expect(toList.from[0].toString()).to.equal(userA._id);

            FriendRequestList.removeRequest(userA.username, userB.username).then(function (removed) {
              expect(removed).to.be(true);
              FriendRequestListModel.findOne({ user: userA._id }).exec(function (err, fromList) {
                expect(err).to.be(null);
                expect(fromList.to.length).to.be(0);
                FriendRequestListModel.findOne({ user: userB._id }).exec(function (err, toList) {
                  expect(err).to.be(null);
                  expect(toList.from.length).to.be(0);
                  done();
                });
              });
            });
          });
        });
      });
    });
  });
});