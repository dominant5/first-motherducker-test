var expect = require('expect.js');
var Powerups = require('../server/powerups');

describe('Powerups', function () {
  it('destroyBody should delete the body', function (done) {
    var powerup = new Powerups.Powerup();
    expect(powerup.body).not.to.be(undefined);
    powerup.destroyBody();
    expect(powerup.body).to.be(undefined);
    done();
  });
});