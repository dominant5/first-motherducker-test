var expect = require('expect.js');
var q = require('q');
var conn = require('../server/mongoose-connection');
var UserModel = require('../server/database/user.model');
var FriendListModel = require('../server/database/friend-list.model');
var FriendList = require('../server/database/friend-list');
var SessionManager = require('../server/session-manager');
var PartyManager = require('../server/party-manager');
var AccountManager = require('../server/account-manager');
var GameManager = require('../server/game-manager');
var Game = require('../server/game');
var Server = require('../server/server');
var Client = require('../server/client');
var io = require('socket.io-client');
var port = 9000;
var socketURL = 'http://localhost:' + port;
var clientSideSocketOptions = {
  transports: ['websocket'],
  'force new connection': true
};

describe('Game Manager', function () {
  var server, accountManager, sessionManager, clients, clientSideSockets, options;

  var initClientSideSockets = function (numClients) {
    var dfd = q.defer();

    server.on(Server.CONNECT, function (client) {
      clients.push(client);
      if (clients.length === numClients) dfd.resolve();
    });

    for (var i = 0; i < numClients; i++) {
      clientSideSockets.push(io(socketURL, clientSideSocketOptions));
    }

    return dfd.promise;
  };

  var initFriends = function (users) {
    var dfd = q.defer();
    if (users.length === 1) {
      dfd.resolve();
      return dfd.promise;
    }

    var usernames = [];
    for (var username in users) {
      usernames.push(username);
    }
    var remaining = usernames.length;

    for (var i = 0; i < usernames.length; i++) {
      (function (username) {
        var friendsRemaining = usernames.length - 1;
        for (var j = 0; j < usernames.length; j++) {
          if (j === i) continue;
          (function (friendName) {
            FriendList.addFriend(username, friendName).then(function () {
              friendsRemaining--;
              if (friendsRemaining > 0) return;
              remaining--;
              if (remaining === 0) dfd.resolve(users);
            });
          })(usernames[j]);
        }
      })(usernames[i]);
    }

    return dfd.promise;
  };

  var initUsers = function (usernames) {
    var dfd = q.defer();

    var completedCount = 0;
    var count = usernames.length;
    var ret = {};
    initClientSideSockets(count).then(function () {
      for (var i = 0; i < count; i++) {
        (function (index) {
          var username = usernames[index];
          var clientSideSocket = clientSideSockets[index];
          var client = clients[index];

          clientSideSocket.on('acc:signup-success', function () {
            completedCount++;
            ret[username] = {
              clientSideSocket: clientSideSocket,
              client: client
            };
            if (completedCount === count) dfd.resolve(ret);
          });

          clientSideSocket.emit('acc:signup', {
            username: username,
            password: username
          });
        })(i);
      }
    });

    return dfd.promise;
  };

  beforeEach(function (done) {
    var app = function (req, res) {};
    var opts = { silent: true };
    server = new Server(port, app, opts);
    sessionManager = new SessionManager(server);
    partyManager = new PartyManager(server, sessionManager);
    accountManager = new AccountManager(server, sessionManager);
    gameManager = new GameManager(server, sessionManager, partyManager, options);
    clients = [];
    clientSideSockets = [];

    options = {
      multipliers: {
        ducklings: {
          spawnFreq: 6.0,
          spawnFreqVar: 1.0/3.0
        },
        powerups: {
          spawnFreq: 10.0,
          spawnFreqVar: 1.0/3.0
        }
      }
    };
    
    conn.connect(conn.localTestUrl, function () {
      done();
    });
  });

  afterEach(function (done) {
    server.stop();
    for (var i = 0; i < clientSideSockets.length; i++) {
      var s = clientSideSockets[i];
      s.disconnect();
    }
    UserModel.remove().exec().then(function () {
      FriendListModel.remove().exec().then(function () {
        done();
      });
    });
  });

  describe('ctor', function () {
    it('should use default options if none provided', function (done) {
      var gameManager1 = new GameManager(server, sessionManager, partyManager);
      expect(gameManager1.options).to.eql(GameManager.defaultOptions);
      done();
    });

    it('create should accept custom options', function (done) {
      var newOptions = {
        multipliers: {
          ducklings: {
            spawnFreq: 5.0,
            spawnFreqVar: 1.0/3.0
          },
          powerups: {
            spawnFreq: 10.0,
            spawnFreqVar: 1.0/3.0
          }
        }
      };
      var gameManager1 = new GameManager(server, sessionManager, partyManager, newOptions);
      expect(gameManager1.options.multipliers.ducklings.spawnFreq).to.equal(5);
      done();
    });
  });

  describe('place', function () {
    it('client find game event should create a new game if no games found', function (done) {
      this.timeout(10000);
      initUsers(['foo']).then(function (users) {
        users.foo.clientSideSocket.on('client:status-change', function (info) {
          if (info.status !== Client.StatusCodes.WAITING) return;
          expect(Object.keys(gameManager.games).length).to.equal(1);
          done();
        });

        expect(Object.keys(gameManager.games).length).to.equal(0);
        users.foo.clientSideSocket.emit('game:find', {});
      });
    });

    it('client find game event should add the client to an existing game if found', function (done) {
      initUsers(['foo', 'bar']).then(function (users) {
        var data = {
          gameMode: 'team',
          numClients: 2,
          teamSize: 2
        };

        users.foo.clientSideSocket.on('client:status-change', function (info) {
          if (info.status !== Client.StatusCodes.WAITING) return;

          expect(Object.keys(gameManager.games).length).to.equal(1);
          // it should already have the first client in it
          var gameClients = gameManager.games[info.gameId].clients;
          expect(Object.keys(gameClients).length).to.equal(1);
          
          users.bar.clientSideSocket.on('client:status-change', function (info) {
            if (info.status !== Client.StatusCodes.WAITING) return;
            expect(Object.keys(gameManager.games).length).to.equal(1);
            var gameClients = gameManager.games[info.gameId].clients;
            expect(Object.keys(gameClients).length).to.equal(2);
            done();
          });

          users.bar.clientSideSocket.emit('game:find', data);
        });

        expect(Object.keys(gameManager.games).length).to.equal(0);
        users.foo.clientSideSocket.emit('game:find', data);
      });
    });

    it ('client find game event should not add clients with different game modes', function (done) {
      initUsers(['foo', 'bar']).then(function (users) {
        var data1 = {
          gameMode: 'ffa',
          numClients: 2,
          teamSize: 1
        };
        var data2 = {
          gameMode: 'team',
          numClients: 2,
          teamSize: 2
        };

        users.foo.clientSideSocket.on('client:status-change', function (info) {
          if (info.status !== Client.StatusCodes.WAITING) return;

          expect(Object.keys(gameManager.games).length).to.equal(1);
          // it should already have the first client in it
          var gameClients = gameManager.games[info.gameId].clients;
          expect(Object.keys(gameClients).length).to.equal(1);

          users.bar.clientSideSocket.on('client:status-change', function (info) {
            if (info.status !== Client.StatusCodes.WAITING) return;
            //two games should be created due to different game modes
            expect(Object.keys(gameManager.games).length).to.equal(2);
            var gameClients = gameManager.games[info.gameId].clients;
            expect(Object.keys(gameClients).length).to.equal(1);
            done();
          });

          users.bar.clientSideSocket.emit('game:find', data2);
        });

        expect(Object.keys(gameManager.games).length).to.equal(0);
        users.foo.clientSideSocket.emit('game:find', data1);
      });
    });

    it('client find game event should add the client to ffa game without teamSize', function (done) {
      initUsers(['foo', 'bar']).then(function (users) {
        var data = {
          gameMode: 'ffa',
          numClients: 2
        };

        users.foo.clientSideSocket.on('client:status-change', function (info) {
          if (info.status !== Client.StatusCodes.WAITING) return;

          expect(Object.keys(gameManager.games).length).to.equal(1);
          // it should already have the first client in it
          var gameClients = gameManager.games[info.gameId].clients;
          expect(Object.keys(gameClients).length).to.equal(1);

          users.bar.clientSideSocket.on('client:status-change', function (info) {
            if (info.status !== Client.StatusCodes.WAITING) return;
            expect(Object.keys(gameManager.games).length).to.equal(1);
            var gameClients = gameManager.games[info.gameId].clients;
            expect(Object.keys(gameClients).length).to.equal(2);
            expect(gameManager.games[info.gameId].options.start.teamSize).to.equal(1);
            done();
          });

          users.bar.clientSideSocket.emit('game:find', data);
        });

        expect(Object.keys(gameManager.games).length).to.equal(0);
        users.foo.clientSideSocket.emit('game:find', data);
      });
    });

    it('party leader find game event should add party members to the game', function (done) {
      initUsers(['foo', 'bar'])
      .then(initFriends)
      .then(function (users) {
        users.foo.clientSideSocket.on('game:started', function () {
          done();
        });

        users.foo.clientSideSocket.on('game:ready', function () {
          users.foo.clientSideSocket.emit('game:ready');
        });
        users.bar.clientSideSocket.on('game:ready', function () {
          users.bar.clientSideSocket.emit('game:ready');
        });

        users.foo.clientSideSocket.on('client:status-change', function (info) {
          if (info.status !== Client.StatusCodes.WAITING) return;
          var games = gameManager.games;
          var gameId = info.gameId;
          var game = games[gameId];
          expect(game.readyToStart()).to.be(true);
          expect(Object.keys(game.clients).length).to.be(2);
          expect(game.clients[users.foo.client.id()]).to.be(users.foo.client);
          expect(game.clients[users.bar.client.id()]).to.be(users.bar.client);
          expect(game.status).to.be(Game.StatusCodes.WAITING_TO_START);
        });

        users.foo.clientSideSocket.on('party:joined', function (data) {
          users.foo.clientSideSocket.emit('game:find', {
            gameMode: 'ffa',
            numClients: 2
          });
        });

        users.bar.clientSideSocket.on('party:invite', function () {
          users.bar.clientSideSocket.emit('party:join', { leader: 'foo' });
        });

        users.foo.clientSideSocket.emit('party:invite', { name: 'bar' });
      });
    });
  });

  describe('disconnect', function () {
    it('disconnect event from client waiting alone in a game should delete the game', function (done) {
      initUsers(['foo']).then(function (users) {
        gameManager.on(GameManager.EventCodes.DELETED_EMPTY_GAME, function () {
          expect(Object.keys(gameManager.games).length).to.equal(0);
          done();
        });

        users.foo.clientSideSocket.on('client:status-change', function (info) {
          if (info.status !== Client.StatusCodes.WAITING) return;
          expect(Object.keys(gameManager.games).length).to.equal(1);
          users.foo.clientSideSocket.disconnect();
        });

        users.foo.clientSideSocket.emit('game:find', {});
      });
    });

    it('disconnect event from client waiting with others in a game should not delete the game', function (done) {
      initUsers(['foo', 'bar']).then(function (users) {
        var isDone = false;
        gameManager.on(GameManager.EventCodes.CLIENT_LEFT_GAME, function () {
          if (isDone) return;
          expect(Object.keys(gameManager.games).length).to.equal(1);
          isDone = true;
          done();
        });
        var data = {
          gameMode: 'ffa',
          numClients: 3,
          teamSize: 1
        };

        users.foo.clientSideSocket.on('client:status-change', function (info) {
          if (info.status !== Client.StatusCodes.WAITING) return;

          users.bar.clientSideSocket.on('client:status-change', function (info) {
            if (info.status !== Client.StatusCodes.WAITING) return;
            users.bar.clientSideSocket.disconnect();
          });

          users.bar.clientSideSocket.emit('game:find', data);
        });

        users.foo.clientSideSocket.emit('game:find', data);
      });
    });
  });

  describe('createGame', function () {
    it('create game should have options set depending on data sent', function (done) {
      var data = {
        gameMode: 'ffa',
        numClients: 2,
        teamSize: 1
      }
      var game = gameManager.createGame(data);
      var gameOptions = gameManager.createOptions(data);
      expect(Object.keys(gameManager.games).length).to.equal(1);
      expect(game.options).to.eql(gameOptions);
      done();
    });
  });
});