var expect = require('expect.js');
var StatesMixin = require('../server/states-mixin');

describe('StatesMixin', function () {
  it('should add a states property', function (done) {
    var obj = {};
    expect(obj.states).to.be(undefined);
    StatesMixin(obj);
    expect(obj.states).to.eql([]);
    done();
  });

  it('should add the helper functions', function (done) {
    var obj = {};
    expect(obj.hasAllStates).to.be(undefined);
    expect(obj.hasAnyStates).to.be(undefined);
    expect(obj.hasState).to.be(undefined);
    expect(obj.addState).to.be(undefined);
    expect(obj.removeState).to.be(undefined);
    StatesMixin(obj);
    expect(typeof obj.hasAllStates).to.be('function');
    expect(typeof obj.hasAnyStates).to.be('function');
    expect(typeof obj.hasState).to.be('function');
    expect(typeof obj.addState).to.be('function');
    expect(typeof obj.removeState).to.be('function');
    done();
  });

  describe('function behaviour', function () {
    var obj;

    beforeEach(function (done) {
      obj = {};
      StatesMixin(obj);
      done();
    });

    it('hasAllStates should return true iff obj has all the states', function (done) {
      var state0 = 'foo';
      var state1 = 'bar';
      expect(obj.hasAllStates(state0, state1)).to.be(false);
      obj.addState(state0);
      expect(obj.hasAllStates(state0, state1)).to.be(false);
      obj.addState(state1);
      expect(obj.hasAllStates(state0, state1)).to.be(true);
      done();
    });

    it('hasAnyStates should return true if the obj has any of the states', function (done) {
      var state0 = 'foo';
      var state1 = 'bar';
      expect(obj.hasAnyStates(state0, state1)).to.be(false);
      obj.addState(state0);
      expect(obj.hasAnyStates(state0, state1)).to.be(true);
      obj.removeState(state0);
      expect(obj.hasAnyStates(state0, state1)).to.be(false);
      obj.addState(state1);
      expect(obj.hasAnyStates(state0, state1)).to.be(true);
      obj.addState(state0);
      expect(obj.hasAnyStates(state0, state1)).to.be(true);
      done();
    });

    it('hasState should return true iff obj has the state', function (done) {
      var state = 'helpless';
      expect(obj.hasState(state)).to.be(false);
      obj.addState(state);
      expect(obj.hasState(state)).to.be(true);
      obj.removeState(state);
      expect(obj.hasState(state)).to.be(false);
      obj.addState('helpless-decoy');
      expect(obj.hasState(state)).to.be(false);
      done();
    });

    it('addState should only add a state if it\'s not already there', function (done) {
      var state = 'helpless';
      expect(obj.states.length).to.be(0);
      obj.addState(state);
      expect(obj.states.length).to.be(1);
      obj.addState(state);
      expect(obj.states.length).to.be(1);
      obj.addState('helpless-decoy');
      expect(obj.states.length).to.be(2);
      obj.addState('helpless-decoy');
      expect(obj.states.length).to.be(2);
      done();
    });

    it('removeState should remove the state if present', function (done) {
      var state = 'helpless';
      expect(obj.states.length).to.be(0);
      obj.addState(state);
      expect(obj.states.length).to.be(1);
      obj.removeState('helpless-decoy');
      expect(obj.states.length).to.be(1);
      obj.removeState(state);
      expect(obj.states.length).to.be(0);
      done();
    });
  });
});