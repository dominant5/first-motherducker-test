var expect = require('expect.js');
var Maps = require('../server/maps-util/maps');
var Cannon = require('cannon');

describe('Maps', function () {
  describe('loadMap', function () {
    it('should return false if there is no map corresponding to mapname', function (done) {
      expect(Maps.loadMap('silly name')).to.be(false);
      done();
    });

    it('should return true if there is a map corresponding to mapname', function (done) {
      expect(Maps.loadMap('defaultflat')).to.be(true);
      done();
    });

    it('should return a copy of the map if passed true as second param', function (done) {
      var mapSpec = Maps.loadMap('defaultflat', true);
      expect(mapSpec.constructor).to.be(Object);

      var entities = mapSpec.entities;
      expect(entities.constructor).to.be(Array);
      if (entities.length > 0) {
        var sampleEntity = entities[0];
        expect(sampleEntity.constructor).to.be(Object);
        expect(sampleEntity.hasType('map')).to.be(true);
      }

      var spawnPoints = mapSpec.spawnPoints;
      expect(spawnPoints.constructor).to.be(Object);
      for (var type in spawnPoints) {
        for (var i = 0; i < spawnPoints[type].length; i++) {
          var spawnPoint = spawnPoints[type][i];
          expect(spawnPoint.constructor).to.be(Object);
          expect(spawnPoint.position.constructor).to.be(Cannon.Vec3);
          expect(spawnPoint.quaternion.constructor).to.be(Cannon.Quaternion);
        }
      }

      var spawnAreas = mapSpec.spawnAreas;
      expect(spawnAreas.constructor).to.be(Object);
      for (var type in spawnAreas) {
        for (var i = 0; i < spawnAreas[type].length; i++) {
          var spawnArea = spawnAreas[type][i];
          expect(spawnArea.constructor).to.be(Object);
          expect(spawnArea.vec1.constructor).to.be(Cannon.Vec3);
          expect(spawnArea.vec2.constructor).to.be(Cannon.Vec3);
          expect(spawnArea.corner.constructor).to.be(Cannon.Vec3);
          expect(spawnArea.area.constructor).to.be(Number);
          expect(spawnArea.weight.constructor).to.be(Number);
        }
      }

      var spawnAreaTotalAreas = mapSpec.spawnAreaTotalAreas;
      expect(spawnAreaTotalAreas.constructor).to.be(Object);
      for (var type in spawnAreaTotalAreas) {
        expect(spawnAreaTotalAreas[type].constructor).to.be(Number);
      }

      done();
    });
  });
});