var expect = require('expect.js');
var Server = require('../server/server');
var io = require('socket.io-client');
var port = 9000;
var socketURL = 'http://localhost:' + port;
var options = {
  transports: ['websocket'],
  'force new connection': true
};

describe('Server', function () {
  var server;

  beforeEach(function (done) {
    var app = function (req, res) {};
    var opts = {
      silent: true
    };
    server = new Server(port, app, opts);
    done();
  });

  afterEach(function (done) {
    server.stop();
    done();
  });

  it('connect event callback should get called on connection', function (done) {
    var socket;
    server.on(Server.CONNECT, function (client) {
      expect(client).not.to.equal(null);
      socket.disconnect();
      done();
    });
    socket = io(socketURL, options);
  });

  it('clear', function (done) {
    server.on(Server.CONNECT, function (client) {
      expect(Object.keys(server.clients).length).to.equal(1);
      expect(server.handlers[Server.CONNECT].length).to.equal(1);
      server.clear();
      expect(Object.keys(server.clients).length).to.equal(0);
      expect(Object.keys(server.handlers).length).to.equal(0);
      done();
    });
    var socket = io(socketURL, options);
    socket.disconnect();
  });

  it('to', function (done) {
    server.on(Server.CONNECT, function (client) {
      client.join('room');
      server.to('room', 'test', 'foo');
    });

    var socket = io(socketURL, options);

    socket.on('test', function (name) {
      expect(name).to.equal('foo');
      socket.disconnect();
      done();
    });
  });
});