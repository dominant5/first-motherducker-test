var expect = require('expect.js');
var Duckling = require('../server/duckling');
var Cannon = require('cannon');

describe('Duckling', function () {

  var id, defaultOptions;

  beforeEach(function (done) {
    defaultOptions = {
      cannonOptions: {
        mass: 0.4,
        radius: 0.4,
        linearDamping: 0,
        angularDamping: 0.9
      },
      speed: 10,
      collideCb: null,
      collideCbContext: null
    };
    done();
  });

  describe('constructor', function () {
    it('should use default options if none provided', function (done) {
      var duckling = new Duckling();
      expect(duckling.options).to.eql(defaultOptions);
      expect(duckling.id).to.equal(duckling.body.id);
      done();
    });

    it('should use any overridden options', function (done) {
      var newId = 420;
      var options = {
        cannonOptions: {
          radius: 1.0
        }
      };
      var duckling = new Duckling(newId, options);
      expect(duckling.id).to.equal(newId);
      expect(duckling.options.cannonOptions.radius).to.eql(options.cannonOptions.radius);
      expect(duckling.body.boundingRadius).to.eql(options.cannonOptions.radius);
      done();
    });

    it('should add properties not defined in options', function (done) {
      var duckling = new Duckling(id);
      expect(duckling.hasType('duckling')).to.be(true);
      expect(duckling.states).to.eql(['helpless']);
      expect(duckling.next).to.equal(null);
      expect(duckling.prev).to.equal(null);
      expect(duckling.leader).to.equal(null);
      done();
    });
  });
});