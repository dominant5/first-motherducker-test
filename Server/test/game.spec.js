var expect = require('expect.js');
var q = require('q');
var merge = require('deepmerge');
var Cannon = require('cannon');
var Game = require('../server/game');
var Client = require('../server/client');
var Server = require('../server/server');
var io = require('socket.io-client');
var port = 9000;
var socketURL = 'http://localhost:' + port;
clientSideSocketOptions = {
  transports: ['websocket'],
  'force new connection': true
};

describe('Game', function () {
  var initClientSideSockets = function (numClients) {
    var dfd = q.defer();

    server.on(Server.CONNECT, function (client) {
      clients.push(client);
      if (clients.length === numClients) dfd.resolve();
    });

    for (var i = 0; i < numClients; i++) {
      clientSideSockets.push(io(socketURL, clientSideSocketOptions));
    }

    return dfd.promise;
  };
  
  var initGame = function (gameOptions) {
    var dfd = q.defer();

    game = new Game(gameId, emitFn, gameOptions);

    for (var i = 0; i < clients.length; i++) {
      var client = clients[i];
      game.addClient(client).then(function (readyToStart) {
        if (readyToStart) dfd.resolve(game);
      });
    }

    return dfd.promise;
  };
  var game, gameId, server, clients, clientSideSockets, emitFn, defaultOptions;

  beforeEach(function (done) {
    gameId = 'Game 123';
    var app = function (req, res) {};
    var opts = {
      silent: true
    };
    server = new Server(port, app, opts);
    clients = [];
    clientSideSockets = [];

    emitFn = function (gameId, event, data) {
      server.to(gameId, event, data);
    };

    defaultOptions = {
      aiOptions: {

      },
      start: {
        numClients: 2,
        teamSize: 1,
        fillWithAI: true
      },
      randomMotherduckSpawnPoints: true,
      duckSpeed: 10,
      duckTurnSpeed: 3,
      ducklings: {
        spawnFreq: 10.0, // seconds
        spawnFreqVar: 1.0 // variance of spawnFreq in seconds
      },
      powerups: {
        spawnFreq: 5.0,
        spawnFreqVar: 2.0
      },
      mapName: 'defaultflat',
      emitFps: 5,
      gameMode: 'ffa',
      simFps: 40,
      gameTimer: 120
    };

    done();
  });

  afterEach(function (done) {
    server.stop();
    for (var i = 0; i < clientSideSockets.length; i++) {
      var s = clientSideSockets[i];
      s.disconnect();
    }
    done();
  });

  describe('constructor', function () {
    it('should use defaultOptions if none passed', function (done) {
      game = new Game(gameId, emitFn);
      expect(game.id).to.equal(gameId);
      expect(game.clients).to.eql([]);
      expect(game.emitFn).to.equal(emitFn);
      expect(game.options).to.eql(defaultOptions);
      expect(game.status).to.equal(Game.StatusCodes.WAITING_ON_CLIENTS);
      done();
    });

    it('should allow setting custom numClients', function (done) {
      defaultOptions.start.numClients = 1;
      game = new Game(gameId, emitFn, defaultOptions);
      expect(game.options).to.eql(defaultOptions);
      done();
    });
  });

  describe('getRandomSpawnPos', function () {
    it('should weight spawnArea selections based on their relative areas', function (done) {
      var spawnArea1 = {
        vec1: new Cannon.Vec3(30, 0, 0),
        vec2: new Cannon.Vec3(0, 0, 30),
        corner: new Cannon.Vec3(-30, 0, -30),
        area: 900,
        weight: 0.9
      };
      var spawnArea2 = {
        vec1: new Cannon.Vec3(10, 0, 0),
        vec2: new Cannon.Vec3(0, 0, 10),
        corner: new Cannon.Vec3(0, 0, 0),
        area: 100,
        weight: 0.1
      };
      var map = {
        spawnAreas: {
          ducklings: [spawnArea1, spawnArea2]
        },
        spawnAreaTotalAreas: {
          ducklings: 1000
        }
      };

      game = new Game(gameId, emitFn);
      var count = 1000000;
      var area1Count = 0;
      var area2Count = 0;
      for (var i = 0; i < count; i++) {
        var spawnPos = game.getRandomSpawnPos(map, 'ducklings');
        (spawnPos.x < 0)? area1Count++ : area2Count++;
      }
      var tolerance = 0.01;
      expect(area1Count / count).to.be.within(0.9 - tolerance, 0.9 + tolerance);
      expect(area2Count / count).to.be.within(0.1 - tolerance, 0.1 + tolerance);
      done();
    });
  });

  it('addClient', function (done) {
    initClientSideSockets(1).then(function () {
      game = new Game(gameId, emitFn, defaultOptions);
      expect(Object.keys(game.clients).length).to.equal(0);
      var client = clients[0];
      var clientSideSocket = clientSideSockets[0];
      clientSideSocket.on('client:status-change', function (info) {
        if (info.status !== Client.StatusCodes.WAITING) return;
        expect(Object.keys(game.clients).length).to.equal(1);
        expect(info.gameId).to.equal(gameId);
        done();
      });

      game.addClient(client);
    });
  });

  it('removeClient', function (done) {
    initClientSideSockets(1).then(function () {
      game = new Game(gameId, emitFn, defaultOptions);
      var client = clients[0];
      game.addClient(client).then(function () {
        expect(Object.keys(game.clients).length).to.equal(1);
        game.removeClient(client).then(function (clientCount) {
          expect(clientCount).to.equal(0);
          expect(Object.keys(game.clients).length).to.equal(0);
          done();
        });
      });
    });
  });

  describe('getClientsReady', function () {
    it('should only resolve when all clients have returned the ready event', function (done) {
      initClientSideSockets(defaultOptions.start.numClients).then(function () {
        for (var i = 0; i < clientSideSockets.length; i++) {
          (function (socket) {
            socket.on('game:ready', function () {
              socket.emit('game:ready');
            });
          })(clientSideSockets[i]);
        }

        initGame(defaultOptions).then(function (game) {
          game.getClientsReady().then(function () {
            done();
          });
        });
      });
    });
  });

  describe('start', function () {
    it('should start game with default options', function (done) {
      initClientSideSockets(defaultOptions.start.numClients).then(function () {
        initGame(defaultOptions).then(function (game) {
          expect(game.readyToStart()).to.be(true);
          done();
        });
      });
    });

    it('should start game with 1 client and 1 AI', function (done) {
      var newOptions = merge(defaultOptions, {}); // copy the options
      newOptions.start.numClients = 1;
      initClientSideSockets(newOptions.start.numClients).then(function () {
        initGame(newOptions).then(function (game) {
          game.start();
          expect(game.entityStore.getForType('motherduck').length).to.be(2);
          expect(game.entityStore.getForType('client').length).to.be(1);
          expect(game.entityStore.getForType('AI').length).to.be(1);
          done();
        });
      });
    });
  });

  it('destroy', function (done) {
    game = new Game(gameId, emitFn, defaultOptions);
    game.destroy();
    expect(game.loop).to.be(undefined);
    expect(game.emitFn).to.be(undefined);
    done();
  });

  describe('setTeams', function () {
    describe('should set the first half of the motherducks\' team to 0 and the second half to 1', function () {
      for (var i = 0; i < 10; i++) {
        (function (teamSize) {
          it('for teamSize = ' + teamSize, function (done) {
            var numClients = teamSize * 2;
            var motherducks = [];
            for (var j = 0; j < numClients; j++) {
              motherducks.push({ eType: 'motherduck' });
            }

            game = new Game(gameId, emitFn, defaultOptions);
            game.setTeams(motherducks, teamSize);

            for (var k = 0; k < teamSize; k++) {
              expect(motherducks[k].team).to.equal(0);
            }

            for (var l = teamSize; l < numClients; l++) {
              expect(motherducks[l].team).to.equal(1);
            }

            done();
          });
        })(i);
      }
    });
  });

  describe('setStartPositions', function () {
    var motherducks, spawnPositions, count = 4;

    beforeEach(function (done) {
      motherducks = [];
      for (var i = 0; i < count; i++) {
        motherducks.push({
          body: {
            position: new Cannon.Vec3(),
            quaternion: new Cannon.Quaternion(),
            boundingRadius: 0.5
          }
        });
      }

      spawnPositions = [];
      for (var i = 0; i < count; i++) {
        spawnPositions.push({
          position: new Cannon.Vec3(0, 0, i), quaternion: new Cannon.Quaternion()
        });
      }

      done();
    });

    it('should set start positions in order if not passed random', function (done) {
      game = new Game(gameId, emitFn, defaultOptions);
      game.setStartPositions(motherducks, spawnPositions);
      for (var i = 0; i < count; i++) {
        var motherduck = motherducks[i];
        var bRad = motherduck.body.boundingRadius;
        var p = spawnPositions[i].position;
        expect(motherduck.body.position).to.eql(new Cannon.Vec3(p.x, bRad, p.z));
      }
      done();
    });

    it('should set start positions in order if passed random = false', function (done) {
      game = new Game(gameId, emitFn, defaultOptions);
      game.setStartPositions(motherducks, spawnPositions, false);
      for (var i = 0; i < count; i++) {
        var motherduck = motherducks[i];
        var bRad = motherduck.body.boundingRadius;
        var p = spawnPositions[i].position;
        expect(motherduck.body.position).to.eql(new Cannon.Vec3(p.x, bRad, p.z));
      }
      done();
    });

    it('should set start positions out of order if passed random = true', function (done) {
      game = new Game(gameId, emitFn, defaultOptions);
      var numTests = 20;
      var randomizedRuns = 0;

      for (var i = 0; i < numTests; i++) {
        game.setStartPositions(motherducks, spawnPositions, true);
        var randomized = false;
        for (var j = 0; j < count; j++) {
          var motherduck = motherducks[j];
          var bRad = motherduck.body.boundingRadius;
          var p = spawnPositions[j].position;
          if (motherduck.body.position.z !== p.z) randomized = true;
        }
        if (randomized) randomizedRuns++;
      }

      expect(randomizedRuns).to.be.above(0);
      done();
    });
  });

  it('getInfo', function (done) {
    initClientSideSockets(1).then(function () {
      game = new Game(gameId, emitFn, defaultOptions);
      var client = clients[0];
      game.addClient(client).then(function () {
        var info = game.getInfo();
        expect(info.gameId).to.equal(gameId);
        expect(info.gameStatus).to.equal(Game.StatusCodes.WAITING_ON_CLIENTS);
        expect(info.clientNames.length).to.equal(1);
        expect(info.clientNames[0]).to.equal(client.name);
        expect(info.world).to.equal(null);
        done();
      });
    });
  });

  it('clientNames', function (done) {
    game = new Game(gameId, emitFn, defaultOptions);
    for (var i = 0; i < clients.length; i++) {
      var client = clients[i];
      game.addClient(client);
    }
    var names = [];
    for (var key in game.clients) {
      var client = game.clients[key];
      names.push(client.name);
    }
    expect(game.clientNames()).to.eql(names);
    done();
  });
});