var expect = require('expect.js');
var q = require('q');
var conn = require('../server/mongoose-connection');
var UserModel = require('../server/database/user.model');
var FriendListModel = require('../server/database/friend-list.model');
var FriendList = require('../server/database/friend-list');
var SessionManager = require('../server/session-manager');
var AccountManager = require('../server/account-manager');
var PartyManager = require('../server/party-manager');
var Server = require('../server/server');
var io = require('socket.io-client');
var port = 9000;
var socketURL = 'http://localhost:' + port;
var clientSideSocketOptions = {
  transports: ['websocket'],
  'force new connection': true
};

describe('PartyManager', function () {
  var server, accountManager, sessionManager, partyManager, clients, clientSideSockets;

  var initClientSideSockets = function (numClients) {
    var dfd = q.defer();

    server.on(Server.CONNECT, function (client) {
      clients.push(client);
      if (clients.length === numClients) dfd.resolve();
    });

    for (var i = 0; i < numClients; i++) {
      clientSideSockets.push(io(socketURL, clientSideSocketOptions));
    }

    return dfd.promise;
  };

  var initFriends = function (users) {
    var dfd = q.defer();
    if (users.length === 1) {
      dfd.resolve();
      return dfd.promise;
    }

    var usernames = [];
    for (var username in users) {
      usernames.push(username);
    }
    var remaining = usernames.length;

    for (var i = 0; i < usernames.length; i++) {
      (function (username) {
        var friendsRemaining = usernames.length - 1;
        for (var j = 0; j < usernames.length; j++) {
          if (j === i) continue;
          (function (friendName) {
            FriendList.addFriend(username, friendName).then(function () {
              friendsRemaining--;
              if (friendsRemaining > 0) return;
              remaining--;
              if (remaining === 0) dfd.resolve(users);
            });
          })(usernames[j]);
        }
      })(usernames[i]);
    }

    return dfd.promise;
  };

  var initUsers = function (usernames) {
    var dfd = q.defer();

    var completedCount = 0;
    var count = usernames.length;
    var ret = {};
    initClientSideSockets(count).then(function () {
      for (var i = 0; i < count; i++) {
        (function (index) {
          var username = usernames[index];
          var clientSideSocket = clientSideSockets[index];
          var client = clients[index];

          clientSideSocket.on('acc:signup-success', function () {
            completedCount++;
            ret[username] = {
              clientSideSocket: clientSideSocket,
              client: client
            };
            if (completedCount === count) dfd.resolve(ret);
          });

          clientSideSocket.emit('acc:signup', {
            username: username,
            password: username
          });
        })(i);
      }
    });

    return dfd.promise;
  };

  beforeEach(function (done) {
    var app = function (req, res) {};
    var opts = { silent: true };
    server = new Server(port, app, opts);
    sessionManager = new SessionManager(server);
    accountManager = new AccountManager(server, sessionManager);
    partyManager = new PartyManager(server, sessionManager);
    clients = [];
    clientSideSockets = [];

    conn.connect(conn.localTestUrl, function () {
      done();
    });
  });

  afterEach(function (done) {
    server.stop();
    for (var i = 0; i < clientSideSockets.length; i++) {
      var s = clientSideSockets[i];
      s.disconnect();
    }
    UserModel.remove().exec().then(function () {
      FriendListModel.remove().exec().then(function () {
        done();
      });
    });
  });

  describe('onSessionEnded', function () {
    it('should just remove the leaving user if not leader', function (done) {
      initUsers(['foo', 'bar', 'quux'])
      .then(initFriends)
      .then(function (users) {
        var leftMessageCount = 0;
        var targetLeftMessageCount = 2;
        var onLeftMessage = function (data) {
          leftMessageCount++;
          expect(data.leader).to.be('foo');
          expect(data.name).to.be('quux');
          if (leftMessageCount !== targetLeftMessageCount) return;
          var partyId = users.foo.client.id();
          var members = partyManager.parties[partyId].members;
          expect(Object.keys(members).length).to.be(1);
          expect(users.quux.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
          done();
        };
        users.foo.clientSideSocket.on('party:left', onLeftMessage);
        users.bar.clientSideSocket.on('party:left', onLeftMessage);

        var joinedCount = 0;
        var targetJoinedCount = 2;
        var onJoined = function () {
          joinedCount++;
          if (joinedCount !== targetJoinedCount) return;
          var partyId = users.foo.client.id();
          var members = partyManager.parties[partyId].members;
          expect(Object.keys(members).length).to.be(2);
          expect(users.quux.client.partyStatus).to.be(PartyManager.StatusCodes.PARTY_MEMBER);
          users.quux.clientSideSocket.disconnect();
        };
        users.bar.clientSideSocket.on('party:joined', onJoined);
        users.quux.clientSideSocket.on('party:joined', onJoined);

        users.bar.clientSideSocket.on('party:invite', function () {
          users.bar.clientSideSocket.emit('party:join', { leader: 'foo' });
        });

        users.quux.clientSideSocket.on('party:invite', function () {
          users.quux.clientSideSocket.emit('party:join', { leader: 'foo' });
        });

        users.foo.clientSideSocket.emit('party:invite', { name: 'bar' });
        users.foo.clientSideSocket.emit('party:invite', { name: 'quux' });
      });
    });
  });

  describe('sendInvite', function () {
    it('should fail if attempting to invite yourself', function (done) {
      initUsers(['foo']).then(function (users) {
        users.foo.clientSideSocket.on('err', function (data) {
          expect(data.type).to.be('party:invite');
          expect(data.message).to.be('Can\'t invite yourself to your party');
          done();
        });

        users.foo.clientSideSocket.emit('party:invite', { name: 'foo' });
      });
    });

    it('should fail if inviter is a member of a party', function (done) {
      initUsers(['foo', 'bar'])
      .then(initFriends)
      .then(function (users) {
        users.bar.clientSideSocket.on('err', function (data) {
          expect(data.type).to.be('party:invite');
          expect(data.message).to.be('Can only send invites if you are the party leader or not in a party');
          done();
        });

        users.bar.clientSideSocket.on('party:joined', function () {
          users.bar.clientSideSocket.emit('party:invite', { name: 'foo' });
        });

        users.bar.clientSideSocket.on('party:invite', function () {
          users.bar.clientSideSocket.emit('party:join', { leader: 'foo' });
        });

        users.foo.clientSideSocket.emit('party:invite', { name: 'bar' });
      });
    });

    it('should fail if the inviter and invitee are not friends', function (done) {
      initUsers(['foo', 'bar'])
      .then(function (users) {
        users.foo.clientSideSocket.on('err', function (data) {
          expect(data.type).to.be('party:invite');
          expect(data.message).to.be('You are not friends with this user');
          done();
        });

        users.bar.clientSideSocket.on('session:logout-success', function () {
          users.foo.clientSideSocket.emit('party:invite', { name: 'bar' });
        });

        users.bar.clientSideSocket.emit('session:logout');
      });      
    });

    it('should fail if the other client is not online', function (done) {
      initUsers(['foo', 'bar'])
      .then(initFriends)
      .then(function (users) {
        users.foo.clientSideSocket.on('err', function (data) {
          expect(data.type).to.be('party:invite');
          expect(data.message).to.be('Other user not online');
          done();
        });

        users.bar.clientSideSocket.on('session:logout-success', function () {
          users.foo.clientSideSocket.emit('party:invite', { name: 'bar' });
        });

        users.bar.clientSideSocket.emit('session:logout');
      });
    });

    it('should succeed otherwise', function (done) {
      initUsers(['foo', 'bar'])
      .then(initFriends)
      .then(function (users) {
        var sentSuccess = false;
        var received = false;
        function finish () {
          if (!sentSuccess || !received) return;
          var fooId = users.foo.client.id();
          var barId = users.bar.client.id();
          expect(Object.keys(partyManager.parties[fooId].invites).length).to.be(1);
          expect(barId in partyManager.parties[fooId].invites).to.be(true);
          done();
        };

        users.bar.clientSideSocket.on('party:invite', function (data) {
          expect(data.leader).to.be('foo');
          received = true;
          finish();
        });

        users.foo.clientSideSocket.on('party:invite-sent', function () {
          sentSuccess = true;
          finish();
        });

        users.foo.clientSideSocket.emit('party:invite', { name: 'bar' });
      });
    });
  });

  describe('joinParty', function () {
    it('should fail if the party doesn\'t exist', function (done) {
      initUsers(['foo', 'bar'])
      .then(initFriends)
      .then(function (users) {
        users.bar.clientSideSocket.on('err', function (data) {
          expect(data.type).to.be('party:join');
          expect(data.message).to.be('Party doesn\'t exist');
          done();
        });
        users.bar.clientSideSocket.emit('party:join', { leader: 'foo' });
      });
    });

    it('should fail if the user is already in the party', function (done) {
      initUsers(['foo', 'bar'])
      .then(initFriends)
      .then(function (users) {
        var party = partyManager.createParty(users.foo.client.id());
        party.members[users.bar.client.id()] = true;

        users.bar.clientSideSocket.on('err', function (data) {
          expect(data.type).to.be('party:join');
          expect(data.message).to.be('You are already in this party');
          done();
        });

        users.bar.clientSideSocket.emit('party:join', { leader: 'foo' });
      });
    });

    it('should fail if the user was not invited to the party', function (done) {
      initUsers(['foo', 'bar'])
      .then(initFriends)
      .then(function (users) {
        partyManager.createParty(users.foo.client.id());

        users.bar.clientSideSocket.on('err', function (data) {
          expect(data.type).to.be('party:join');
          expect(data.message).to.be('You haven\'t been invited to this party');
          done();
        });

        users.bar.clientSideSocket.emit('party:join', { leader: 'foo' });
      });
    });

    it('client should successfully join the party otherwise', function (done) {
      initUsers(['foo', 'bar'])
      .then(initFriends)
      .then(function (users) {
        var count = 0;
        var targetCount = 2;
        var onJoined = function (data) {
          count++;
          if (count !== targetCount) return;
          var leaderId = users.foo.client.id();
          var memberId = users.bar.client.id();
          var party = partyManager.parties[leaderId];
          expect(memberId in party.invites).to.be(false);
          expect(Object.keys(party.invites).length).to.be(0);
          expect(memberId in party.members).to.be(true);
          expect(Object.keys(party.members).length).to.be(1);
          expect(users.foo.client.partyStatus).to.be(PartyManager.StatusCodes.PARTY_LEADER);
          expect(users.bar.client.partyStatus).to.be(PartyManager.StatusCodes.PARTY_MEMBER);
          done();
        };
        users.foo.clientSideSocket.on('party:joined', onJoined);
        users.bar.clientSideSocket.on('party:joined', onJoined);

        users.bar.clientSideSocket.on('party:invite', function () {
          users.bar.clientSideSocket.emit('party:join', { leader: 'foo' });
        });

        expect(users.foo.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
        expect(users.bar.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
        users.foo.clientSideSocket.emit('party:invite', { name: 'bar' });
      });
    });

    it('client should leave their first party if they join a new party', function (done) {
      initUsers(['foo', 'bar', 'quux'])
      .then(initFriends)
      .then(function (users) {
        var count = 0;
        var targetCount = 2;
        var onJoined = function (data) {
          count++;
          if (count !== targetCount) return;

          expect(users.foo.client.partyStatus).to.be(PartyManager.StatusCodes.PARTY_LEADER);
          expect(users.bar.client.partyStatus).to.be(PartyManager.StatusCodes.PARTY_MEMBER);
          expect(users.quux.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);

          users.quux.clientSideSocket.on('party:joined', function () {
            expect(partyManager.partyExists(users.foo.client.id())).to.be(false);
            expect(partyManager.partyExists(users.quux.client.id())).to.be(true);

            expect(users.foo.client.partyStatus).to.be(PartyManager.StatusCodes.PARTY_MEMBER);
            expect(users.bar.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
            expect(users.quux.client.partyStatus).to.be(PartyManager.StatusCodes.PARTY_LEADER);
            done();
          });

          users.foo.clientSideSocket.on('party:invite', function () {
            users.foo.clientSideSocket.emit('party:join', { leader: 'quux' });
          });

          users.quux.clientSideSocket.emit('party:invite', { name: 'foo' });
        };
        users.foo.clientSideSocket.on('party:joined', onJoined);
        users.bar.clientSideSocket.on('party:joined', onJoined);

        users.bar.clientSideSocket.on('party:invite', function () {
          users.bar.clientSideSocket.emit('party:join', { leader: 'foo' });
        });

        expect(users.foo.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
        expect(users.bar.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
        expect(users.quux.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
        users.foo.clientSideSocket.emit('party:invite', { name: 'bar' });
      });
    });
  });

  describe('leaveParty', function () {
    it('should do nothing if the user was not in the party', function (done) {
      initUsers(['foo', 'bar', 'quux'])
      .then(initFriends)
      .then(function (users) {
        setTimeout(function () {
          expect(Object.keys(partyManager.parties).length).to.be(1);
          var party = partyManager.parties[users.foo.client.id()];
          expect(Object.keys(party.members).length).to.be(1);
          expect(party.members[users.bar.client.id()]).to.be('bar');
          done();
        }, 50);

        users.bar.clientSideSocket.on('party:joined', function () {
          users.quux.clientSideSocket.emit('party:leave', { leader: 'foo' });
        });

        users.bar.clientSideSocket.on('party:invite', function () {
          users.bar.clientSideSocket.emit('party:join', { leader: 'foo' });
        });

        users.foo.clientSideSocket.emit('party:invite', { name: 'bar' });
      });
    });

    it('should just remove the leaving user if not leader', function (done) {
      initUsers(['foo', 'bar', 'quux'])
      .then(initFriends)
      .then(function (users) {
        var leftMessageCount = 0;
        var targetLeftMessageCount = 3;
        var onLeftMessage = function (data) {
          leftMessageCount++;
          expect(data.leader).to.be('foo');
          expect(data.name).to.be('quux');
          if (leftMessageCount !== targetLeftMessageCount) return;
          var partyId = users.foo.client.id();
          var members = partyManager.parties[partyId].members;
          expect(Object.keys(members).length).to.be(1);
          expect(users.foo.client.partyStatus).to.be(PartyManager.StatusCodes.PARTY_LEADER);
          expect(users.bar.client.partyStatus).to.be(PartyManager.StatusCodes.PARTY_MEMBER);
          expect(users.quux.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
          done();
        };
        users.foo.clientSideSocket.on('party:left', onLeftMessage);
        users.bar.clientSideSocket.on('party:left', onLeftMessage);
        users.quux.clientSideSocket.on('party:left', onLeftMessage);

        var joinedCount = 0;
        var targetJoinedCount = 2;
        var onJoined = function () {
          joinedCount++;
          if (joinedCount !== targetJoinedCount) return;
          var partyId = users.foo.client.id();
          var members = partyManager.parties[partyId].members;
          expect(Object.keys(members).length).to.be(2);
          expect(users.foo.client.partyStatus).to.be(PartyManager.StatusCodes.PARTY_LEADER);
          expect(users.bar.client.partyStatus).to.be(PartyManager.StatusCodes.PARTY_MEMBER);
          expect(users.quux.client.partyStatus).to.be(PartyManager.StatusCodes.PARTY_MEMBER);
          users.quux.clientSideSocket.emit('party:leave', { leader: 'foo' });
        };
        users.bar.clientSideSocket.on('party:joined', onJoined);
        users.quux.clientSideSocket.on('party:joined', onJoined);

        users.bar.clientSideSocket.on('party:invite', function () {
          users.bar.clientSideSocket.emit('party:join', { leader: 'foo' });
        });

        users.quux.clientSideSocket.on('party:invite', function () {
          users.quux.clientSideSocket.emit('party:join', { leader: 'foo' });
        });

        expect(users.foo.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
        expect(users.bar.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
        expect(users.quux.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
        users.foo.clientSideSocket.emit('party:invite', { name: 'bar' });
        users.foo.clientSideSocket.emit('party:invite', { name: 'quux' });
      });
    });

    it('should close the party if the leaving user (not leader) is the last member', function (done) {
      initUsers(['foo', 'quux'])
      .then(initFriends)
      .then(function (users) {
        var closedMessageCount = 0;
        var targetClosedMessageCount = 1;
        var onClosedMessage = function (data) {
          closedMessageCount++;
          expect(data.leader).to.be('foo');
          if (closedMessageCount !== targetClosedMessageCount) return;
          expect(Object.keys(partyManager.parties).length).to.be(0);
          expect(users.foo.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
          expect(users.quux.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
          done();
        };
        users.foo.clientSideSocket.on('party:closed', onClosedMessage);
        users.quux.clientSideSocket.on('party:closed', onClosedMessage);

        var joinedCount = 0;
        var targetJoinedCount = 1;
        var onJoined = function () {
          joinedCount++;
          if (joinedCount !== targetJoinedCount) return;
          var partyId = users.foo.client.id();
          var members = partyManager.parties[partyId].members;
          expect(Object.keys(members).length).to.be(1);
          expect(users.foo.client.partyStatus).to.be(PartyManager.StatusCodes.PARTY_LEADER);
          expect(users.quux.client.partyStatus).to.be(PartyManager.StatusCodes.PARTY_MEMBER);
          users.quux.clientSideSocket.emit('party:leave', { leader: 'foo' });
        };
        users.quux.clientSideSocket.on('party:joined', onJoined);

        users.quux.clientSideSocket.on('party:invite', function () {
          users.quux.clientSideSocket.emit('party:join', { leader: 'foo' });
        });

        expect(users.foo.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
        expect(users.quux.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
        users.foo.clientSideSocket.emit('party:invite', { name: 'quux' });
      });
    });

    it('should close the party if the leaving user is the leader', function (done) {
      initUsers(['foo', 'bar', 'quux'])
      .then(initFriends)
      .then(function (users) {
        var closedMessageCount = 0;
        var targetClosedMessageCount = 3;
        var onClosedMessage = function (data) {
          closedMessageCount++;
          expect(data.leader).to.be('foo');
          if (closedMessageCount !== targetClosedMessageCount) return;
          expect(Object.keys(partyManager.parties).length).to.be(0);
          expect(users.foo.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
          expect(users.bar.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
          expect(users.quux.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
          done();
        };
        users.foo.clientSideSocket.on('party:closed', onClosedMessage);
        users.bar.clientSideSocket.on('party:closed', onClosedMessage);
        users.quux.clientSideSocket.on('party:closed', onClosedMessage);

        var joinedCount = 0;
        var targetJoinedCount = 2;
        var onJoined = function () {
          joinedCount++;
          if (joinedCount !== targetJoinedCount) return;
          var partyId = users.foo.client.id();
          var members = partyManager.parties[partyId].members;
          expect(Object.keys(members).length).to.be(2);
          expect(users.foo.client.partyStatus).to.be(PartyManager.StatusCodes.PARTY_LEADER);
          expect(users.bar.client.partyStatus).to.be(PartyManager.StatusCodes.PARTY_MEMBER);
          expect(users.quux.client.partyStatus).to.be(PartyManager.StatusCodes.PARTY_MEMBER);
          users.foo.clientSideSocket.emit('party:leave', { leader: 'foo' });
        };
        users.bar.clientSideSocket.on('party:joined', onJoined);
        users.quux.clientSideSocket.on('party:joined', onJoined);

        users.bar.clientSideSocket.on('party:invite', function () {
          users.bar.clientSideSocket.emit('party:join', { leader: 'foo' });
        });

        users.quux.clientSideSocket.on('party:invite', function () {
          users.quux.clientSideSocket.emit('party:join', { leader: 'foo' });
        });

        expect(users.foo.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
        expect(users.bar.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
        expect(users.quux.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
        users.foo.clientSideSocket.emit('party:invite', { name: 'bar' });
        users.foo.clientSideSocket.emit('party:invite', { name: 'quux' });
      });
    });
  });

  describe('kickFromParty', function () {
    it('should fail if the kicker is not the leader', function (done) {
      initUsers(['foo', 'bar', 'quux'])
      .then(initFriends)
      .then(function (users) {
        users.quux.clientSideSocket.on('err', function (data) {
          expect(data.type).to.be('party:kick');
          expect(data.message).to.be('Only the party leader can kick members');
          done();
        });

        var joinedCount = 0;
        var targetJoinedCount = 2;
        var onJoined = function () {
          joinedCount++;
          if (joinedCount !== targetJoinedCount) return;
          var partyId = users.foo.client.id();
          var members = partyManager.parties[partyId].members;
          expect(Object.keys(members).length).to.be(2);
          users.quux.clientSideSocket.emit('party:kick', { name: 'foo' });
        };
        users.bar.clientSideSocket.on('party:joined', onJoined);
        users.quux.clientSideSocket.on('party:joined', onJoined);

        users.bar.clientSideSocket.on('party:invite', function () {
          users.bar.clientSideSocket.emit('party:join', { leader: 'foo' });
        });

        users.quux.clientSideSocket.on('party:invite', function () {
          users.quux.clientSideSocket.emit('party:join', { leader: 'foo' });
        });

        users.foo.clientSideSocket.emit('party:invite', { name: 'bar' });
        users.foo.clientSideSocket.emit('party:invite', { name: 'quux' });
      });
    });

    it('should fail if the kicker is not in a party', function (done) {
      initUsers(['foo', 'bar', 'quux'])
      .then(initFriends)
      .then(function (users) {
        users.quux.clientSideSocket.on('err', function (data) {
          expect(data.type).to.be('party:kick');
          expect(data.message).to.be('You are not in a party');
          done();
        });

        var joinedCount = 0;
        var targetJoinedCount = 1;
        var onJoined = function () {
          joinedCount++;
          if (joinedCount !== targetJoinedCount) return;
          var partyId = users.foo.client.id();
          var members = partyManager.parties[partyId].members;
          expect(Object.keys(members).length).to.be(1);
          users.quux.clientSideSocket.emit('party:kick', { name: 'foo' });
        };
        users.bar.clientSideSocket.on('party:joined', onJoined);

        users.bar.clientSideSocket.on('party:invite', function () {
          users.bar.clientSideSocket.emit('party:join', { leader: 'foo' });
        });

        users.foo.clientSideSocket.emit('party:invite', { name: 'bar' });
      });
    });

    it('should fail if the kickee is not in the party', function (done) {
      initUsers(['foo', 'bar', 'quux'])
      .then(initFriends)
      .then(function (users) {
        users.foo.clientSideSocket.on('err', function (data) {
          expect(data.type).to.be('party:kick');
          expect(data.message).to.be('The given user is not in your party');
          done();
        });

        var joinedCount = 0;
        var targetJoinedCount = 1;
        var onJoined = function () {
          joinedCount++;
          if (joinedCount !== targetJoinedCount) return;
          var partyId = users.foo.client.id();
          var members = partyManager.parties[partyId].members;
          expect(Object.keys(members).length).to.be(1);
          users.foo.clientSideSocket.emit('party:kick', { name: 'quux' });
        };
        users.bar.clientSideSocket.on('party:joined', onJoined);

        users.bar.clientSideSocket.on('party:invite', function () {
          users.bar.clientSideSocket.emit('party:join', { leader: 'foo' });
        });

        users.foo.clientSideSocket.emit('party:invite', { name: 'bar' });
      });
    });

    it('should succeed otherwise', function (done) {
      initUsers(['foo', 'bar', 'quux'])
      .then(initFriends)
      .then(function (users) {
        var kickedMessageCount = 0;
        var targetKickedMessageCount = 3;
        var onKickedMessage = function (data) {
          kickedMessageCount++;
          expect(data.leader).to.be('foo');
          expect(data.name).to.be('quux');
          if (kickedMessageCount !== targetKickedMessageCount) return;
          var partyId = users.foo.client.id();
          var members = partyManager.parties[partyId].members;
          expect(Object.keys(members).length).to.be(1);
          expect(users.foo.client.partyStatus).to.be(PartyManager.StatusCodes.PARTY_LEADER);
          expect(users.bar.client.partyStatus).to.be(PartyManager.StatusCodes.PARTY_MEMBER);
          expect(users.quux.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
          done();
        };
        users.foo.clientSideSocket.on('party:kicked', onKickedMessage);
        users.bar.clientSideSocket.on('party:kicked', onKickedMessage);
        users.quux.clientSideSocket.on('party:kicked', onKickedMessage);

        var joinedCount = 0;
        var targetJoinedCount = 2;
        var onJoined = function () {
          joinedCount++;
          if (joinedCount !== targetJoinedCount) return;
          var partyId = users.foo.client.id();
          var members = partyManager.parties[partyId].members;
          expect(Object.keys(members).length).to.be(2);
          expect(users.foo.client.partyStatus).to.be(PartyManager.StatusCodes.PARTY_LEADER);
          expect(users.bar.client.partyStatus).to.be(PartyManager.StatusCodes.PARTY_MEMBER);
          expect(users.quux.client.partyStatus).to.be(PartyManager.StatusCodes.PARTY_MEMBER);
          users.foo.clientSideSocket.emit('party:kick', { name: 'quux' });
        };
        users.bar.clientSideSocket.on('party:joined', onJoined);
        users.quux.clientSideSocket.on('party:joined', onJoined);

        users.bar.clientSideSocket.on('party:invite', function () {
          users.bar.clientSideSocket.emit('party:join', { leader: 'foo' });
        });

        users.quux.clientSideSocket.on('party:invite', function () {
          users.quux.clientSideSocket.emit('party:join', { leader: 'foo' });
        });

        expect(users.foo.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
        expect(users.bar.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
        expect(users.quux.client.partyStatus).to.be(PartyManager.StatusCodes.NOT_IN_PARTY);
        users.foo.clientSideSocket.emit('party:invite', { name: 'bar' });
        users.foo.clientSideSocket.emit('party:invite', { name: 'quux' });
      });
    });
  });

  describe('partyExists', function () {
    it('should return true if the party exists', function (done) {
      initClientSideSockets(1).then(function () {
        var partyId = clients[0].id();
        expect(partyManager.partyExists(partyId)).to.be(false);
        partyManager.createParty(partyId);
        expect(partyManager.partyExists(partyId)).to.be(true);
        done();
      });
    });

    it('should return true if the party was deleted', function (done) {
      initClientSideSockets(1).then(function () {
        var partyId = clients[0].id();
        partyManager.createParty(partyId);
        expect(partyManager.partyExists(partyId)).to.be(true);
        partyManager.deleteParty(partyId);
        expect(partyManager.partyExists(partyId)).to.be(false);
        done();
      });
    });
  });

  describe('createParty', function () {
    it('should create a party if the party did not already exist', function (done) {
      initClientSideSockets(1).then(function () {
        var partyId = clients[0].id();
        expect(partyId in partyManager.parties).to.be(false);
        expect(Object.keys(partyManager.parties).length).to.be(0);
        partyManager.createParty(partyId);
        expect(partyId in partyManager.parties).to.be(true);
        expect(Object.keys(partyManager.parties).length).to.be(1);
        done();
      });
    });

    it('should do nothing if the party already existed', function (done) {
      initClientSideSockets(1).then(function () {
        var partyId = clients[0].id();
        partyManager.createParty(partyId);
        expect(partyId in partyManager.parties).to.be(true);
        expect(Object.keys(partyManager.parties).length).to.be(1);
        partyManager.createParty(partyId);
        expect(partyId in partyManager.parties).to.be(true);
        expect(Object.keys(partyManager.parties).length).to.be(1);
        done();
      });
    });
  });

  describe('deleteParty', function () {
    it('should delete a party if it existed', function (done) {
      initClientSideSockets(1).then(function () {
        var partyId = clients[0].id();
        partyManager.createParty(partyId);
        expect(partyId in partyManager.parties).to.be(true);
        expect(Object.keys(partyManager.parties).length).to.be(1);
        partyManager.deleteParty(partyId);
        expect(partyId in partyManager.parties).to.be(false);
        expect(Object.keys(partyManager.parties).length).to.be(0);
        done();
      });
    });

    it('should do nothing if the party does not exist', function (done) {
      initClientSideSockets(2).then(function () {
        var partyId = clients[0].id();
        var decoyId = clients[1].id();
        partyManager.createParty(partyId);
        expect(partyId in partyManager.parties).to.be(true);
        expect(Object.keys(partyManager.parties).length).to.be(1);
        partyManager.deleteParty(decoyId);
        expect(partyId in partyManager.parties).to.be(true);
        expect(Object.keys(partyManager.parties).length).to.be(1);
        done();
      });
    });
  });
});