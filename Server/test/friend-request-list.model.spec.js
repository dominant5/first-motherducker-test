'use strict';

var expect = require('expect.js');
var FriendRequestListModel = require('../server/database/friend-request-list.model');
var conn = require('../server/mongoose-connection');

describe('FriendRequestList Model', function () {
  var userId = '560e23ca9c05dc0aa0ef8ba5';

  beforeEach(function (done) {
    conn.connect(conn.localTestUrl, function () {
      FriendRequestListModel.remove().exec().then(function () {
        done();
      });
    });
  });

  afterEach(function (done) {
    FriendRequestListModel.remove().exec().then(function () {
      conn.disconnect(function () {
        done();
      });
    });
  });

  describe('create', function () {
    it('should create an error if no user id', function (done) {
      FriendRequestListModel.create({}, function (err, list) {
        expect(err).not.to.be(null);
        expect(Object.keys(err.errors).length).to.be(1);
        expect(err.errors.user).not.to.be(undefined);
        expect(err.errors.user.message).to.be('Path `user` is required.');
        done();
      });
    });

    it('should succeed if user id provided', function (done) {
      FriendRequestListModel.create({ user: userId }, function (err, list) {
        expect(err).to.be(null);
        FriendRequestListModel.find().exec().then(function (friendLists) {
          expect(friendLists.length).to.be(1);
          expect(friendLists[0].user.toString()).to.equal(userId);
          done();
        });
      });
    });

    it('should create an error if a list for the given user id already exists', function (done) {
      FriendRequestListModel.create({ user: userId }, function (err1, list1) {
        expect(err1).to.be(null);
        FriendRequestListModel.create({ user: userId }, function (err2, list2) {
          expect(err2).not.to.be(null);
          expect(Object.keys(err2.errors).length).to.be(1);
          expect(err2.errors.user).not.to.be(null);
          expect(err2.errors.user.message).to.be('Friend request list for given user already exists');
          done();
        })
      });
    });
  });

  describe('remove', function () {
    it('should successfully remove a friendlist by user id', function (done) {
      function remove () {
        FriendRequestListModel.remove({ user: userId }, function (err, list) {
          FriendRequestListModel.count({}, function (err, count) {
            expect(count).to.be(0);
            done();
          });
        });
      };

      FriendRequestListModel.create({ user: userId }, function (err, list) {
        FriendRequestListModel.count({}, function (err, count) {
          expect(count).to.be(1);
          remove();
        });
      });
    });
  });
});