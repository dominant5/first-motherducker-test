'use strict';

var expect = require('expect.js');
var UserModel = require('../server/database/user.model');
var FriendListModel = require('../server/database/friend-list.model');
var FriendList = require('../server/database/friend-list');
var conn = require('../server/mongoose-connection');

describe('FriendList', function () {
  var user;

  beforeEach(function (done) {
    user = { _id: '560e23ca9c05dc0aa0ef8ba5', username: 'test', password: 'test' };
    conn.connect(conn.localTestUrl, function () {
      UserModel.remove().exec().then(function () {
        FriendListModel.remove().exec().then(function () {
          done();
        });
      });
    });
  });

  afterEach(function (done) {
    UserModel.remove().exec().then(function () {
      FriendListModel.remove().exec().then(function () {
        done();
      });
    });
  });

  describe('addFriend', function () {
    var userA, userB;

    beforeEach(function (done) {
      userA = { _id: '560e23ca9c05dc0aa0ef8ba5', username: 'a', password: 'a' };
      userB = { _id: '560e23ca9c05dc0aa0ef8ba6', username: 'b', password: 'b' };

      UserModel.create(userA, function (err, a) {
        UserModel.create(userB, function (err, b) {
          FriendList.create(userA.username, function (err, listA) {
            FriendList.create(userB.username, function (err, listB) {
              done();
            });
          });
        });
      });
    });

    it('should fail if username is null', function (done) {
      FriendList.addFriend(null, null).then(null, function (err) {
        expect(err).not.to.be(null);
        expect(err.message).to.be('username is required.');
        done();
      });
    });

    it('should fail if friendName is null', function (done) {
      FriendList.addFriend(userA.username, null).then(null, function (err) {
        expect(err).not.to.be(null);
        expect(err.message).to.be('friendName is required.');
        done();
      });
    });

    it('should fail if username and friendName are the same', function (done) {
      FriendList.addFriend(userA.username, userA.username).then(null, function (err) {
        expect(err).not.to.be(null);
        expect(err.message).to.be('username and friendName must be different.');
        done();
      });
    });

    it('should succeed if user and friend are provided', function (done) {
      FriendList.addFriend(userA.username, userB.username).then(function (added) {
        expect(added).to.be(true);
        FriendListModel.findOne({ user: userA._id }, function (err, listA) {
          expect(err).to.be(null);
          expect(listA.friends.length).to.be(1);
          expect(String(listA.friends[0])).to.equal(userB._id);
          done();
        });
      });
    });
  });

  describe('hasFriend', function () {
    var userA, userB;

    beforeEach(function (done) {
      userA = { _id: '560e23ca9c05dc0aa0ef8ba5', username: 'a', password: 'a' };
      userB = { _id: '560e23ca9c05dc0aa0ef8ba6', username: 'b', password: 'b' };

      UserModel.create(userA, function (err, a) {
        UserModel.create(userB, function (err, b) {
          FriendList.create(userA.username, function (err, listA) {
            FriendList.create(userB.username, function (err, listB) {
              done();
            });
          });
        });
      });
    });

    it('should return false if user and friend are not friends', function (done) {
      FriendList.hasFriend(userA.username, userB.username).then(function (hasFriend) {
        expect(hasFriend).to.be(false);
        done();
      });
    });

    it('should return true if user and friend are friends', function (done) {
      FriendList.addFriend(userA.username, userB.username).then(function (added) {
        FriendList.hasFriend(userA.username, userB.username).then(function (hasFriend) {
          done();
        });
      });
    });
  });

  describe('getFriendList', function () {
    beforeEach(function (done) {
      UserModel.create(user, function () {
        FriendList.create(user.username, function () {
          done();
        });
      });
    });

    it('should fail if userId is null', function (done) {
      FriendList.getFriendList(null).then(null, function (err) {
        expect(err).not.to.be(null);
        expect(err.message).to.be('username is required.');
        done();
      });
    });

    it('should fail if the user does not exist', function (done) {
      FriendList.getFriendList('foo').then(null, function (err) {
        expect(err).not.to.be(null);
        expect(err.message).to.be('The specified user does not exist.');
        done();
      });
    });

    it('should create a new list if there is no list associated with the user', function (done) {
      function getList () {
        FriendList.getFriendList(user.username).then(function (list) {
          expect(list.length).to.be(0);
          FriendListModel.find().exec().then(function (lists) {
            expect(lists.length).to.be(1);
            expect(String(lists[0].user)).to.eql(user._id);
            done();
          });
        });
      };

      FriendListModel.remove().exec().then(function () {
        FriendListModel.find().exec().then(function (lists) {
          expect(lists.length).to.be(0);
          getList();
        });
      });
    });

    it('should not create a new list if there is a list associated with the user', function (done) {
      function getList () {
        FriendList.getFriendList(user.username).then(function (list) {
          expect(list.length).to.be(0);
          FriendListModel.find().exec().then(function (lists) {
            expect(lists.length).to.be(1);
            expect(String(lists[0].user)).to.eql(user._id);
            done();
          });
        });
      };

      FriendListModel.create(user, function () {
        FriendListModel.find().exec().then(function (lists) {
          expect(lists.length).to.be(1);
          expect(String(lists[0].user)).to.eql(user._id);
          getList();
        });
      });
    });

    it('should populate the friend list with usernames', function (done) {
      var friend = { _id: '560e23ca9c05dc0aa0ef8ba6', username: 'f', password: 'f' };

      function getList () {
        FriendList.getFriendList(user.username).then(function (list) {
          expect(list.length).to.be(1);
          expect(list[0].username).to.be(friend.username);
          done();
        });
      };

      FriendListModel.create(user, function () {
        UserModel.create(friend, function (err, b) {
          FriendList.create(friend.username, function (err, listB) {
            FriendList.addFriend(user.username, friend.username).then(function () {
              getList();
            });
          });
        });
      });
    });
  });
});