'use strict';

var expect = require('expect.js');
var FriendListModel = require('../server/database/friend-list.model');
var conn = require('../server/mongoose-connection');

describe('FriendList Model', function () {
  var userId = '560e23ca9c05dc0aa0ef8ba5';

  beforeEach(function (done) {
    conn.connect(conn.localTestUrl, function () {
      FriendListModel.remove().exec().then(function () {
        done();
      });
    });
  });

  afterEach(function (done) {
    FriendListModel.remove().exec().then(function () {
      conn.disconnect(function () {
        done();
      });
    });
  });

  describe('create', function () {
    it('should create an error if no user id', function (done) {
      FriendListModel.create({}, function (err, list) {
        expect(err).not.to.be(null);
        expect(Object.keys(err.errors).length).to.be(1);
        expect(err.errors.user).not.to.be(undefined);
        expect(err.errors.user.message).to.be('Path `user` is required.');
        done();
      });
    });

    it('should succeed if user id provided', function (done) {
      FriendListModel.create({ user: userId }, function (err, list) {
        expect(err).to.be(null);
        FriendListModel.find().exec().then(function (friendLists) {
          expect(friendLists.length).to.be(1);
          expect(friendLists[0].user.toString()).to.equal(userId);
          done();
        });
      });
    });
  });

  describe('remove', function () {
    it('should successfully remove a friendlist by user id', function (done) {
      function remove () {
        FriendListModel.remove({ user: userId }, function (err, list) {
          FriendListModel.count({}, function (err, count) {
            expect(count).to.be(0);
            done();
          });
        });
      };

      FriendListModel.create({ user: userId }, function (err, list) {
        FriendListModel.count({}, function (err, count) {
          expect(count).to.be(1);
          remove();
        });
      });
    });
  });
});