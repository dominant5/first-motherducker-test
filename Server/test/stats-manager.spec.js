expect = require('expect.js');
q = require('q');
conn = require('../server/mongoose-connection');
UserModel = require('../server/database/user.model');
SessionManager = require('../server/session-manager');
AccountManager = require('../server/account-manager');
PartyManager = require('../server/party-manager');
GameManager = require('../server/game-manager');
StatsManager = require('../server/stats-manager');
Server = require('../server/server');
io = require('socket.io-client');
port = 9000;
socketURL = 'http://localhost:' + port;
clientSideSocketOptions = {
  transports: ['websocket'],
  'force new connection': true
};

describe('StatsManager', function () {
  var server, accountManager, sessionManager, partyManager, gameManager, statsManager, clients, clientSideSockets;

  var initClientSideSockets = function (numClients) {
    var dfd = q.defer();

    server.on(Server.CONNECT, function (client) {
      clients.push(client);
      if (clients.length === numClients) dfd.resolve();
    });

    for (var i = 0; i < numClients; i++) {
      clientSideSockets.push(io(socketURL, clientSideSocketOptions));
    }

    return dfd.promise;
  };

  var initUsers = function (usernames) {
    var dfd = q.defer();

    var completedCount = 0;
    var count = usernames.length;
    var ret = {};
    initClientSideSockets(count).then(function () {
      for (var i = 0; i < count; i++) {
        (function (index) {
          var username = usernames[index];
          var clientSideSocket = clientSideSockets[index];
          var client = clients[index];

          clientSideSocket.on('acc:signup-success', function () {
            completedCount++;
            ret[username] = {
              clientSideSocket: clientSideSocket,
              client: client
            };
            if (completedCount === count) dfd.resolve(ret);
          });

          clientSideSocket.emit('acc:signup', {
            username: username,
            password: username
          });
        })(i);
      }
    });

    return dfd.promise;
  };

  beforeEach(function (done) {
    var app = function (req, res) {};
    var opts = { silent: true };
    server = new Server(port, app, opts);
    sessionManager = new SessionManager(server);
    accountManager = new AccountManager(server, sessionManager);
    partyManager = new PartyManager(server, sessionManager);
    gameManager = new GameManager(server, sessionManager, partyManager);
    statsManager = new StatsManager(server, sessionManager, gameManager);
    clients = [];
    clientSideSockets = [];

    conn.connect(conn.localTestUrl, function () {
      UserModel.remove().exec().then(function () {
        done();
      });
    });
  });

  afterEach(function (done) {
    server.stop();
    for (var i = 0; i < clientSideSockets.length; i++) {
      var s = clientSideSockets[i];
      s.disconnect();
    }
    UserModel.remove().exec().then(function () {
      done();
    });
  });

  describe('sendStatsToClient', function () {
    it('should fail if the user is not found', function (done) {
      initUsers(['foo']).then(function (users) {
        users.foo.clientSideSocket.on('err', function (err) {
          expect(err.type).to.be('stats:get');
          expect(err.message).to.be('The specified user does not exist.');
          done();
        });

        users.foo.clientSideSocket.emit('stats:get', { username: 'bar' });
      });
    });

    it('should return the stats associated with the given username', function (done) {
      initUsers(['foo']).then(function (users) {
        var expectedStats = {
          username: 'foo',
          highscore: 0,
          wins: 0,
          losses: 0,
          mostCollectedDucklings: 0,
          totalCollectedDucklings: 0,
          totalDetachedDucklings: 0,
          totalStolenDucklings: 0 
        };

        users.foo.clientSideSocket.on('stats:deliver', function (stats) {
          expect(stats).to.eql(expectedStats);
          done();
        });

        users.foo.clientSideSocket.emit('stats:get', { username: 'foo' });
      });
    });
  });

  describe('updateStats', function () {
    it ('should update the stats correctly', function (done) {
      initUsers(['foo']).then(function(users) {
        var motherDuck = {
          name: 'foo',
          score: 1,
          highestCount: 1,
          ducklingCount: 1,
          detachedCount: 1,
          stolenCount: 1
        };
        var data = {
          winner: [0],
          statistics: [0],
        };
        statsManager.updateStats(motherDuck, data).then(function() {
          statsManager.getStats('foo').then(function(stats) {
            expect(stats.highscore).to.be(1);
            expect(stats.wins).to.be(0);
            expect(stats.losses).to.be(1);
            expect(stats.mostCollectedDucklings).to.be(1);
            expect(stats.totalCollectedDucklings).to.be(1);
            expect(stats.totalDetachedDucklings).to.be(1);
            expect(stats.totalStolenDucklings).to.be(1);
            done();
          });
        });
      });
    });
  });

  describe('getStats', function () {
    it ('should return an object containing all user\'s stats', function (done) {
      initUsers(['bar']).then(function(users) {
        statsManager.getStats('bar').then(function(stats) {
          expect(stats.highscore).to.be(0);
          expect(stats.wins).to.be(0);
          expect(stats.losses).to.be(0);
          expect(stats.mostCollectedDucklings).to.be(0);
          expect(stats.totalCollectedDucklings).to.be(0);
          expect(stats.totalDetachedDucklings).to.be(0);
          expect(stats.totalStolenDucklings).to.be(0);
          done();
        });
      });
    });
  });

});