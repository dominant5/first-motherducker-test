var expect = require('expect.js');
var UserModel = require('../server/database/user.model');
var conn = require('../server/mongoose-connection');

describe('User model', function () {
  beforeEach(function (done) {
    conn.connect(conn.localTestUrl, function () {
      UserModel.remove().exec().then(function () {
        done();
      });
    });
  });

  afterEach(function (done) {
    UserModel.remove().exec().then(function () {
      conn.disconnect(function () {
        done();
      });
    });
  });

  describe('create', function () {
    it('should create an error if no username', function (done) {
      UserModel.create({ password: 'test' }, function (err, user) {
        expect(err).not.to.be(null);
        expect(Object.keys(err.errors).length).to.be(1);
        expect(err.errors.username).not.to.be(undefined);
        expect(err.errors.username.message).to.be('Path `username` is required.');
        done();
      });
    });

    it('should create an error if no password', function (done) {
      UserModel.create({ username: 'test' }, function (err, user) {
        expect(err).not.to.be(null);
        expect(Object.keys(err.errors).length).to.be(1);
        expect(err.errors.password).not.to.be(undefined);
        expect(err.errors.password.message).to.be('Path `password` is required.');
        done();
      });
    });

    it('should create an error if no username and no password', function (done) {
      UserModel.create({ }, function (err, user) {
        expect(err).not.to.be(null);
        expect(Object.keys(err.errors).length).to.be(2);
        expect(err.errors.password).not.to.be(undefined);
        expect(err.errors.password.message).to.be('Path `password` is required.');
        expect(err.errors.password).not.to.be(undefined);
        expect(err.errors.password.message).to.be('Path `password` is required.');
        done();
      });
    });

    it('should succeed if username and password provided', function (done) {
      UserModel.create({ username: 'test', password: 'test' }, function (err, user) {
        expect(err).to.be(null);
        UserModel.find().exec().then(function (users) {
          expect(users.length).to.be(1);
          expect(users[0].username).to.be('test');
          done();
        });
      });
    });

    it('should throw an error if the username already exists', function (done) {
      var user = { username: 'test', password: 'test' };
      UserModel.create(user, function (err1, user1) {
        expect(err1).to.be(null);
        UserModel.create(user, function (err2, user2) {
          expect(err2).not.to.be(null);
          done();
        });
      });
    });
  });
});