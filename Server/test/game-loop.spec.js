var expect = require('expect.js');
var Loop = require('../server/game-loop');

describe('Game loop', function () {
  var fps, loop;

  beforeEach(function (done) {
    fps = 60;
    done();
  });

  afterEach(function (done) {
    if (loop) {
      loop.stop();
      delete loop;
    }
    done();
  });

  it('constructor', function (done) {
    var update = function (dt) {};
    loop = new Loop(update, fps);
    expect(loop.fps).to.equal(fps);
    expect(loop.tickLenMs).to.equal(1000 / fps);
    expect(loop.prevTick).to.equal(null);
    expect(loop.actualTicks).to.equal(0);
    expect(loop.update).to.be(update);
    expect(loop.alive).to.be(false);
    done();
  });

  it('should run at the desired fps', function (done) {
    var desiredUpdates = fps, // run for one second
    actualUpdates = 0,
    startTime;
    var update = function (dt) {
      actualUpdates++;

      if (actualUpdates === desiredUpdates) {
        var now = Date.now();
        var expected = 1; // one second
        var duration = now - startTime;
        expect(duration / 1000).to.be.within(expected - 0.1, expected + 0.1);
        done();
      }
    };
    loop = new Loop(update, fps);
    startTime = Date.now();
    loop.start();
  });

  it('start', function (done) {
    var update = function (dt) {
      var expectedDt = 1 / fps;
      expect(dt).to.be.within(expectedDt - 0.003, expectedDt + 0.003);
      expect(loop.alive).to.be(true);
      expect(loop.prevTick).not.to.equal(null);
      done();
    };
    loop = new Loop(update, fps);
    loop.start();
  });

  it('stop', function (done) {
    var update = function (dt) {
      loop.stop();
      expect(loop.alive).to.be(false);
      done();
    };
    loop = new Loop(update, fps);
    loop.start();
    done();
  });
});